
$('#validate_form').validate({
      rules:{
        first_name: {
          required : true,
          minlength : 3,
          maxlength : 30, 
          no_space  : true, 
        },
        last_name: {
          required : true,
          minlength : 3,
          maxlength : 30, 
          no_space  : true,
        },
        father_name: {
          required : true,
          minlength : 3,
          maxlength : 30, 
          no_space  : true,  
        },
        mother_name: {
          required : true,
          minlength : 3,
          maxlength : 30, 
          no_space  : true,
        },
        birthdate: {
          required : true,
        },
        sex: {
          required : true,
        },
        present_address: {
          required : true,
        },
        permanent_address: {
          required : true,
        },
        phoneCode: {
          required : true,
          digits: true,
          minlength : 4,
          maxlength : 4,
        },
        phone: {
          required : true,
          digits: true,
          minlength : 7,
          maxlength : 7,
        },
        email: {
          required : true,
          email: true,
        },
        password: {
          required : true,
          minlength : 6,
        },
        password_confirm: {
          required : true,
          equalTo: "#password",
        },
        new: {
          required : true,
          minlength : 6,
        },
        new_confirm: {
          required : true,
          equalTo: "#password1",
        },
        blood: {
          required : true,
        },
        father_occupation: {
          required : true,
        },
        father_incom_range: {
          required : true,
        },
        indexNo: {
          required : true,
          digits : true,
        },
        facultiesSubject: {
          required : true,
        },
        workingHoure: {
          required : true,
        },
        studentId: {
          required : true,
        },
        guardianRelation: {
          required : true,
        },
        bookTitle: {
          required : true,
        },
        isbn_no: {
          digits : true,
        },
        bookAuthor: {
          required : true,
        },
        language: {
          required : true,
        },
        pages: {
          required : true,
          digits :true,
        },
        roomAmount: {
          required : true,
          digits :true,
          range : [0, 500]
        },
        gradeName: {
          required : true,
        },
        gradePoint: {
          required : true,
          digits :true,
          range : [0, 5],
        },
        numberFrom: {
          required : true,
          digits :true,
          range : [0, 100],
        },
         branch_name: {
          required : true,
        },
        nameTo: {
          required : true,
          digits :true,
          range : [0, 100],
        },
        examTitle: {
          required : true,
        },
        startDate: {
          required : true,
        },
        examMonth: {
          required : true,
        },
        class_id: {
          required : true,
        },
        totleTime: {
          required : true,
        },
        final: {
          required : true,
        },
        phone1: {
          required : true,
          digits: true,
          minlength : 11,
          maxlength : 11,
        },
        childCatgry: {
          required : true,
        },
        parent_category: {
          required : true,
        },
        cophone: {
          required : true,
          digits : true,
          minlength : 11,
          maxlength : 11,
        },
        coemail: {
          required : true,
          email: true,
        },
        cpphone: {
          required : true,
          digits : true,
          minlength : 11,
          maxlength : 11,
        },
        accountno: {
          digits : true,
          maxlength : 20,
        },
        date: {
          required : true,
        },
        endDate: {
          required : true,
        },
        reason: {
          required : true,
        }
      },
      messages:{
        first_name: {
          required : '*This field is required',
          minlength : '*First name should have 3 characters minimum',
          maxlength : '*First name should have 30 characters maximum',
          no_space :  '*First name should not have spaces',
        },
        last_name: {
          required : '*This field is required',
          minlength : '*last name should have 3 characters minimum',
          maxlength : '*last name should have 30 characters maximum',
          no_space :  '*last name should not have spaces',
        },
        father_name: {
          required : '*This field is required',
          minlength : '*father name should have 3 characters minimum',
          maxlength : '*father name should have 30 characters maximum',
          no_space :  '*father name should not have spaces',
        },
        mother_name: {
          required : '*This field is required',
          minlength : '*mother name should have 3 characters minimum',
          maxlength : '*mother name should have 30 characters maximum',
          no_space :  '*mother name should not have spaces', 
        },
        birthdate: {
          required : '*This field is required',
        },
        sex: {
          required : '*This field is required',
        },
        present_address: {
          required : '*This field is required',
        },
        permanent_address: {
          required : '*This field is required',
        },
        phoneCode: {
          required : '*This field is required',
          digits : '*please enter digits only',
          minlength : '*invalid code',
          maxlength : '*invalid code',
        },
        phone: {
          required : '*This field is required',
          digits : '*please enter digits only',
          minlength : '*invalid code',
          maxlength : '*invalid code',
        },
        email: {
          required : '*This field is required',
          email: '*Please enter a valid email',
        },
        blood: {
          required : '*This field is required',
        },
        father_occupation: {
          required : '*This field is required',
        },
        father_incom_range: {
          required : '*This field is required',
        },
        indexNo: {
          required : '*This field is required',
          digits : '*indexNo must contains digits only',
        },
        facultiesSubject: {
          required : '*This field is required',
        },
        workingHoure: {
          required : '*This field is required',
        },
        studentId: {
          required : '*This field is required',
        },
        guardianRelation: {
          required : '*This field is required',
        },
        bookTitle: {
          required : '*This field is required',
        },
        isbn_no: {
          digits : '*digits only',
        },
        bookAuthor: {
          required : '*This field is required',
        },
        language: {
          required : '*This field is required',
        },
        pages: {
          required : '*This field is required',
          digits : '*digits only',
        },
        roomAmount: {
          required : '*This field is required',
          digits : '*digits only',
          range : '*please enter room amount less then 500',
        },
        class_code: {
          required : '*This field is required',
        },
        gradeName: {
          required : '*This field is required',
        },
        gradePoint: {
          required : '*This field is required',
          digits : '*digits only',
          range : '*please enter point less or equalTo 5',
        },
        numberFrom: {
          required : '*This field is required',
          digits : '*digits only',
          range : '*please enter less then or equalTo 100',
        },
         branch_name: {
          required : '*This field is required',
        },
        nameTo: {
          required : '*This field is required',
          digits : '*digits only',
          range : '*please enter less then or equalTo 100',
        },
        examTitle: {
          required : '*This field is required',
        },
        startDate: {
          required : '*This field is required',
        },
        examMonth: {
          required : '*This field is required',
        },
        class_id: {
          required : '*This field is required',
        },
        totleTime: {
          required : '*This field is required',
        },
        final: {
          required : '*This field is required',
        },
        phone1: {
          required : '*This field is required',
          digits : '*please enter digits only',
          minlength : '*invalid number',
          maxlength : '*invalid number',
        },
        childCatgry: {
          required : '*This field is required',
        },
        parent_category: {
          required : '*This field is required',
        },
        cophone: {
          required : '*This field is required',
          digits : '*please enter digits only',
          minlength : '*invalid number',
          maxlength : '*invalid number',
        },
        coemail: {
          required : '*This field is required',
          email: '*Please enter a valid email',
        },
        cpphone: {
          required : '*This field is required',
          digits : '*please enter digits only',
          minlength : '*invalid number',
          maxlength : '*invalid number',
        },
        accountno: {
          digits : '*please enter digits only',
          maxlength : '*Account number should have 20 characters maximum',
        },
        date: {
          required : '*This field is required',
        },
        endDate: {
          required : '*This field is required',
        },
        reason: {
          required : '*This field is required',
        }
      }
});
