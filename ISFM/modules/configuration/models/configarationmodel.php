<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Configarationmodel extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->dbforge();
    }

    //This function return language name from database;
    public function language() {
        $data = array();
        $query = $this->db->query('SELECT language FROM configuration WHERE id=1');
        foreach ($query->result_array() as $row) {
            $data[] = $row['language'];
        }
        return $data;
    }

    public function search_student($str,$par) {
        $data = array();
        $this->db->WHERE('parient_id',$par);
        $this->db->like('id', $str, 'both');
        $this->db->or_like('year', $str, 'both');
        $this->db->or_like('student_id', $str, 'both');
        $this->db->or_like('roll_number', $str, 'both');
        $this->db->or_like('student_nam', $str, 'both');
        $this->db->or_like('farther_name', $str, 'both');
        $this->db->or_like('birth_date', $str, 'both');
        $this->db->or_like('sex', $str, 'both');
        $this->db->or_like('present_address', $str, 'both');
        $this->db->or_like('permanent_address', $str, 'both');
        $this->db->or_like('phone', $str, 'both');
        $this->db->or_like('father_occupation', $str, 'both');
        $this->db->or_like('father_incom_range', $str, 'both');
        $query = $this->db->get('student_info');
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        return $data;
    }

    public function search_student1($str,$par) {
        $data = array();
        $cls= "";
        $this->db->WHERE('parient_id',$par);
        $this->db->like('class_title', $str, 'both');
        $this->db->SELECT('class_id');
        $query = $this->db->get('class');
        foreach ($query->result_array() as $row) {
            $cls = $row['class_id'];
        }
        if(!empty($cls))
        {
            $this->db->WHERE('class_id',$cls);
            $query1 = $this->db->get('student_info');
            foreach ($query1->result_array() as $row1) {
                $data[] = $row1;
            }
        }
        return $data;
    }

    public function search_parent($str,$par) {
        $data = array();
        $this->db->WHERE('parient_id',$par);
        $this->db->like('id', $str, 'both');
        $this->db->or_like('student_id', $str, 'both');
        $this->db->or_like('parents_name', $str, 'both');
        $this->db->or_like('relation', $str, 'both');
        $this->db->or_like('phone', $str, 'both');
        $query = $this->db->get('parents_info');
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        return $data;
    }

    public function search_parent1($str,$par) {
        $data = array();
        $cls= "";
        $this->db->WHERE('parient_id',$par);
        $this->db->like('class_title', $str, 'both');
        $this->db->SELECT('class_id');
        $query = $this->db->get('class');
        foreach ($query->result_array() as $row) {
            $cls = $row['class_id'];
        }
        if(!empty($cls))
        {
            $this->db->WHERE('class_id',$cls);
            $query1 = $this->db->get('parents_info');
            foreach ($query1->result_array() as $row1) {
                $data[] = $row1;
            }
        }
        return $data;
    }

    public function search_staff($str,$par) {
        $data = array();
        $this->db->WHERE('parient_id',$par);
        $this->db->like('id', $str, 'both');
        $this->db->or_like('fullname', $str, 'both');
        $this->db->or_like('farther_name', $str, 'both');
        $this->db->or_like('birth_date', $str, 'both');
        $this->db->or_like('sex', $str, 'both');
        $this->db->or_like('present_address', $str, 'both');
        $this->db->or_like('permanent_address', $str, 'both');
        $this->db->or_like('phone', $str, 'both');
        $this->db->or_like('subject', $str, 'both');
        $this->db->or_like('position', $str, 'both');
        $this->db->or_like('working_hour', $str, 'both');
        $query = $this->db->get('teachers_info');
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        return $data;
    }

    public function search_staff1($str,$par) {
        $data = array();
        $this->db->WHERE('parient_id',$par);
        $this->db->where('group_id <>', '1');
        $this->db->like('id', $str, 'both');
        $this->db->or_like('full_name', $str, 'both');
        $this->db->or_like('farther_name', $str, 'both');
        $this->db->or_like('birth_date', $str, 'both');
        $this->db->or_like('sex', $str, 'both');
        $this->db->or_like('present_address', $str, 'both');
        $this->db->or_like('permanent_address', $str, 'both');
        $this->db->or_like('phone', $str, 'both');
        $this->db->or_like('working_hour', $str, 'both');
        $query = $this->db->get('userinfo');
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        return $data;
    }

    
    //This function will return specific employ salary list
    public function employee_salary1($id,$par) {
        $data = array();
        $this->db->WHERE('parient_id',$par);
        $this->db->WHERE('user_id',$id);
        $query = $this->db->get('salary');
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        return $data;
    }
    //This function will return class exam term
    public function checkClassFee($a) {
        $query = $this->db->query("SELECT id FROM set_fees WHERE Class_title = '$a'")->row();
        if (empty($query)) {
            $rowId = 'empty';
            return $rowId;
        } else {
            $rowId = $query->id;
            return $rowId;
        }
    }

    //This function will provide previous fees amount by class
    public function getClassFee($id) {
        $query = $this->db->query("SELECT * FROM set_fees WHERE id = $id");
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        return $data;
    }

    //this function will return all employe fromusers table
    public function employ($par="") {
        $data = array();
        $query = $this->db->query("SELECT id,username FROM users WHERE user_status='Employee' AND salary=0 AND parient_id = '$par'");
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        return $data;
    }

    //This function will cheack employe group
    public function employGroup($uId) {
        $data = array();
        $query = $this->db->query("SELECT group_id FROM users_groups WHERE user_id=$uId");
        foreach ($query->result_array() as $row) {
            $groupId = $row['group_id'];
            if ($groupId == 1 || $groupId == 4) {
                echo $groupId;
            } else {
                echo 'not';
            }
        }
    }

    //This function will reaturn set salary's informations.
    public function salaryInfo($par="") {
        $data = array();
        $query = $this->db->query("SELECT * FROM set_salary WHERE parient_id = '$par'");
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        return $data;
    }

    //This function will return all salery info 
    public function saleryConFigInfo($id) {
        $data = array();
        $query = $this->db->query("SELECT * FROM set_salary WHERE id= $id");
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        return $data;
    }

    //This function will show all user frope list with id
    public function allGroup() {
        $data = array();
        $query = $this->db->query("SELECT id,name FROM groups");
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        return $data;
    }

    //This function will check employee secority password is set? or not ?
    public function cheEmpPass() {
        $data = array();
        $query = $this->db->query("SELECT t_a_s_p FROM configuration");
        foreach ($query->result_array() as $row) {
            $data = $row['t_a_s_p'];
        }
        if (!empty($data)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //This function will return student item fee
    public function item_fee_info($id) {
        $data = array();
        $query = $this->db->query("SELECT * FROM fee_item WHERE id=$id");
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }return $data;
    }

}
