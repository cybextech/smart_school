<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->
<?php $user = $this->ion_auth->user()->row();
$userId = $user->id; ?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo lang('stu_clas_pageTitle'); ?> <small></small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo lang('home'); ?>
                        
                    </li>
                    <li>
                        <?php echo lang('header_setting'); ?>
                        
                    </li>
                    <li>
                        <?php echo lang('srch'); ?>
                        
                    </li>
                    <li id="result" class="pull-right topClock"></li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->        
        <!-- BEGIN PAGE CONTENT-->        
        <div class="row">
            <div class="col-md-12">
            <?php if(!empty($message)){?>
            <div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-6">
                        <div class="alert alert-info">
                        <h3><strong><?php echo $message; ?></strong></h3>
                        </div></div></div>
                
           <?php     } ?>
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo lang('stu_clas_pageTitle'); ?>
                        </div>
                        <div class="tools">
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>
                                <tr>
                                    <th>
                                        <?php echo lang('stu_clas_Student_ID'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('stu_clas_Roll_No'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('stu_clas_Photo'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('stu_clas_Student_Name'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('srch8'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('admi_FatherName'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('stu_clas_Phone_No'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('admi_DateOfBirth'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('srch5'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('admi_PresentAddress'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('admi_PermanentAddress'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('srch6'); ?>
                                    </th>
                                     <th>
                                        <?php echo lang('admi_FatherOccupation'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('srch7'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('stu_clas_Actions'); ?>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($studentInfo as $row) {
                                    //get student information from "user" table.
//                                    $class = $row['class_title'];
                                    $stuUserId = $row['user_id'];
                                    $query = $this->db->get_where('users', array('id' => $stuUserId));
                                    foreach ($query->result_array() as $row2) {
                                        $userdata = $row2;
                                    }
                                    $phoneNumber = $userdata['phone'];
                                    $email = $userdata['email'];

                                    //get student information from "student_info" table.
                                    $stdId = $row['student_id'];
                                    $photo = $row['student_photo'];
                                    $address = $row['present_address'];
                                    $clas_name = "";
                                    $clas_std_id = "";
                                    $this->db->where('student_id',$row['student_id']);
                                    $this->db->where('class_id',$row['class_id']);
                                    $this->db->select('class_title');
                                    $this->db->select('id');
                                    $clas_name = $this->db->get('class_students');
                                    foreach ($clas_name->result_array() as $rslt1) {
                                        $clas_name = $rslt1['class_title'];
                                        $clas_std_id = $rslt1['id'];
                                    }
                                    ?>

                                    <tr>
                                        <td>
                                            <?php echo stripslashes($row['student_id']); ?>
                                        </td>
                                        <td>
                                            <?php echo stripslashes($row['roll_number']); ?>
                                        </td>
                                        <td>
                                            <div class="tableImage">
                                                <img src="assets/uploads/<?php echo $photo; ?>" alt="">
                                            </div>
                                        </td>
                                        <td>
                                            <?php echo stripslashes($row['student_nam']); ?>
                                        </td>
                                        <td>
                                            <?php echo $clas_name; ?>
                                        </td>
                                        <td>
                                            <?php echo stripslashes($row['farther_name']); ?>
                                        </td>
                                        <td>
                                            <?php echo $phoneNumber; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['birth_date']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['sex']; ?>
                                        </td>
                                        <td>
                                            <?php echo stripslashes($address) ?>
                                        </td>
                                        <td>
                                            <?php echo $row['permanent_address']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['starting_year']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['father_occupation']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['father_incom_range']; ?>
                                        </td>
                                        <td>
                                            <a class="btn btn-xs green tableActionButtonMargin" href="index.php/students/students_details?id=<?php echo $clas_std_id; ?>&sid=<?php echo $row['student_id']; ?>&userId=<?php echo $stuUserId; ?>&class_id=<?php echo $row['class_id']; ?>" > <i class="fa fa-file-text-o"></i> <?php echo lang('header_academic'); ?> </a>
                                            <a class="btn btn-xs green tableActionButtonMargin" href="index.php/students/myAttendance?uisd=<?php echo $stuUserId; ?>" > <i class="fa fa-file-text-o"></i> <?php echo lang('header_attendance'); ?> </a>
                                            <a class="btn btn-xs green tableActionButtonMargin" href="index.php/account/due_pay?uisd=<?php echo $stuUserId; ?>" > <i class="fa fa-file-text-o"></i> <?php echo lang('header_account'); ?> </a>
                                            <a class="btn btn-xs green tableActionButtonMargin" href="index.php/sclass/ownClassRoutin?uisd=<?php echo $stuUserId; ?>" > <i class="fa fa-file-text-o"></i> <?php echo lang('header_routin'); ?> </a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->


<!--Begin Page Level Script-->
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script src="assets/admin/pages/scripts/table-advanced.js"></script>
<!--End Page Level Script-->
<script>
    jQuery(document).ready(function() {
        //here is auto reload after 1 second for time and date in the top
        jQuery(setInterval(function() {
            jQuery("#result").load("index.php/home/iceTime");
        }, 1000));
    });
</script>
