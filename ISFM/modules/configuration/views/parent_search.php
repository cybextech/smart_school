<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->
<?php $user = $this->ion_auth->user()->row();
$userId = $user->id; ?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo lang('header_parent_info'); ?> <small></small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo lang('home'); ?>
                        
                    </li>
                    <li>
                        <?php echo lang('header_setting'); ?>
                        
                    </li>
                    <li>
                        <?php echo lang('srch'); ?>
                        
                    </li>
                    <li id="result" class="pull-right topClock"></li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->        
        <!-- BEGIN PAGE CONTENT-->        
        <div class="row">
            <div class="col-md-12">
            <?php if(!empty($message)){?>
            <div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-6">
                        <div class="alert alert-info">
                        <h3><strong><?php echo $message; ?></strong></h3>
                        </div></div></div>
                
           <?php     } ?>
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo lang('header_parent_info'); ?>
                        </div>
                        <div class="tools">
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>
                                <tr>
                                    <th>
                                        <?php echo lang('stu_clas_Student_ID'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('stu_clas_Student_Name'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('srch8'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('par_gar_name'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('par_rela'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('par_email'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('par_pho_num'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('par_gur_pho'); ?>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($parentInfo as $row) {
                                    //get student information from "user" table.
//                                    $class = $row['class_title'];
                                    $stdId = $row['student_id'];
                                    $clas = $this->common->class_title1($row['class_id']);
                                    $sec = $this->common->section_title($row['section']);
                                    ?>

                                    <tr>
                                        <td>
                                            <?php echo $row['student_id']; ?>
                                        </td>
                                        <td>
                                            <?php echo $this->common->student_title($row['student_id']); ?>
                                        </td>
                                        <td>
                                            <?php echo $clas." ".$sec; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['parents_name']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['relation']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['email']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['phone']; ?>
                                        </td>
                                        <td>
                                            <div class="tableImage">
                                                <?php
                                                $puserid = $row['user_id'];
                                                $query = $this->db->get_where('users', array('id' => $puserid));
                                                foreach ($query->result_array() as $row1) { ?>
                                                    <img src="assets/uploads/<?php echo $row1['profile_image']; ?>" alt="">
                                                <?php } ?>
                                            </div>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->


<!--Begin Page Level Script-->
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script src="assets/admin/pages/scripts/table-advanced.js"></script>
<!--End Page Level Script-->
<script>
    jQuery(document).ready(function() {
        //here is auto reload after 1 second for time and date in the top
        jQuery(setInterval(function() {
            jQuery("#result").load("index.php/home/iceTime");
        }, 1000));
    });
</script>
