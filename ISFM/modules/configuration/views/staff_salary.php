<!--Start page level style-->
<link rel="stylesheet" type="text/css" href="assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!--Start page level style-->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo lang('acc_employsalary'); ?><small></small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo lang('home'); ?>
                    </li>
                    <li>
                        <?php echo lang('header_setting'); ?>
                    </li>
                    <li>
                        <?php echo lang('srch'); ?>
                    </li>
                    <li id="result" class="pull-right topClock"></li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
       
        <div class="row">
            <div class="col-md-12">
                <?php
                if (!empty($message)) {
                    echo '<br>' . $message;
                }
                ?>
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo "Employees Salary Details"; ?>
                        </div>
                        <div class="tools">
                            <a class="collapse" href="javascript:;">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>
                                <tr>
                                    <th>
                                        <?php echo lang('date'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('acc_paimont'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('acc_emp_title'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('acc_samount'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('acc_paymethod'); ?>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($salary_list as $row) { ?>
                                    <tr>
                                        <td>
                                            <?php echo date("d/m/Y", $row['date']) ?>
                                        </td>
                                        <td>
                                            <?php echo $row['month']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['employ_title']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['total_amount']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['method']; ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions fluid marginTopNone">
            <div class="col-md-offset-3 col-md-9">
                <button class="btn white col-sm-9 col-xs-12 routineGoBack" type="button" onclick="window.location.href = 'javascript:history.back()'"></button>
            </div>
        </div>
        <div class="form-actions fluid marginTopNone">
            <div class="col-md-offset-3 col-md-9">
                <button class="btn blue col-sm-9 col-xs-12 routineGoBack" type="button" onclick="window.location.href = 'javascript:history.back()'"><?php echo lang('back'); ?></button>
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->
<!-- BEGIN PAGE LEVEL PLUGINS -->