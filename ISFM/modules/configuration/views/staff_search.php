<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->
<?php $user = $this->ion_auth->user()->row();
$userId = $user->id; ?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo lang('srch3')." ".lang('clas_information'); ?> <small></small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo lang('home'); ?>
                        
                    </li>
                    <li>
                        <?php echo lang('header_setting'); ?>
                        
                    </li>
                    <li>
                        <?php echo lang('srch'); ?>
                        
                    </li>
                    <li id="result" class="pull-right topClock"></li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->        
        <!-- BEGIN PAGE CONTENT-->        
        <div class="row">
            <div class="col-md-12">
            <?php if(!empty($message)){?>
            <div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-6">
                        <div class="alert alert-info">
                        <h3><strong><?php echo $message; ?></strong></h3>
                        </div></div></div>
                
           <?php     } ?>
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo lang('srch3')." ".lang('clas_information'); ?>
                        </div>
                        <div class="tools">
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>
                                <tr>
                                    <th>
                                        <?php echo lang('srno'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('hrm_photos'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('hrm_tn'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('tea_pp'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('hrm_address'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('hrm_email'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('hrm_act'); ?>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1;
                                foreach ($staffInfo as $row) { 
                                    ?>
                                    <tr>
                                        <td>
                                            <?php echo $i; ?>
                                        </td>
                                        <td>
                                            <?php if($flagg == '1') { ?>
                                            <div class="tableImage">
                                                <img src="assets/uploads/<?php echo $row['users_photo']; ?>" alt="">
                                            </div>
                                            <?php } else { ?>
                                            <div class="tableImage">
                                                <img src="assets/uploads/<?php echo $row['teachers_photo']; ?>" alt="">
                                            </div>
                                            <?php } ?>
                                        </td>
                                        <td>
                                            <?php if($flagg == '1') { echo $row['full_name'];} else { echo $row['fullname'];} ?>
                                        </td>
                                        <td>
                                            <?php 
                                                if($flagg == '1') { 
                                                    $grup_id = $row['group_id'];
                                                    if ($grup_id == '6') {
                                                        echo 'Accountant';
                                                    } elseif ($grup_id == '7') {
                                                        echo 'Library Man';
                                                    } elseif ($grup_id == '8') {
                                                        echo 'Car Driver';
                                                    } elseif ($grup_id == '9') {
                                                        echo '4th Class Employee';
                                                    }
                                                } else { 
                                                    echo $row['position'];
                                                } 
                                            ?>
                                        </td>
                                        <td>
                                            <?php echo $row['present_address']; ?>
                                        </td>
                                        <td>
                                            <?php $uid = $row['user_id']; 
                                                $emal = "";
                                                $this->db->WHERE('id',$uid);
                                                $this->db->SELECT('email');
                                                $this->db->FROM('users');
                                                $q1 = $this->db->get();
                                                foreach ($q1->result_array() as $r1) {
                                                $emal = $r1['email'];
                                                }
                                                echo $emal;
                                            ?>
                                        </td>
                                        <td>
                                            <?php if($flagg == '1') { ?>
                                            <a class="btn btn-xs green" href="index.php/users/allUserInafoDetails?id=<?php echo $row['id']; ?>&uid=<?php echo $row['user_id']; ?>&photo=<?php echo $row['users_photo']; ?>"> <i class="fa fa-file-text-o"></i> Details </a>
                                            <?php } else { ?>
                                            <a class="btn btn-xs green" href="index.php/teachers/teacherDetails?id=<?php echo $row['id']; ?>&uid=<?php echo $row['user_id']; ?>&photo=<?php echo $row['teachers_photo']; ?>"> <i class="fa fa-file-text-o"></i> <?php echo lang('details'); ?> </a>
                                             <?php } ?>
                                             <a class="btn btn-xs green" href="index.php/dailyAttendance/employe_atten_view?id=<?php echo $row['user_id']; ?>"> <i class="fa fa-file-text-o"></i> <?php echo lang('att_attendance'); ?> </a>
                                             <a class="btn btn-xs green" href="index.php/configuration/staffSalary?id=<?php echo $row['user_id']; ?>"> <i class="fa fa-file-text-o"></i> <?php echo lang('header_account'); ?> </a>
                                        </td>
                                    </tr>
                                <?php 
                                 $i++;
                            }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->


<!--Begin Page Level Script-->
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script src="assets/admin/pages/scripts/table-advanced.js"></script>
<!--End Page Level Script-->
<script>
    jQuery(document).ready(function() {
        //here is auto reload after 1 second for time and date in the top
        jQuery(setInterval(function() {
            jQuery("#result").load("index.php/home/iceTime");
        }, 1000));
    });
</script>
