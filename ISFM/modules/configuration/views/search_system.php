<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo lang('srch1'); ?> <small></small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo lang('home'); ?>
                    </li>
                    <li>
                        <?php echo lang('header_setting'); ?>
                    </li>
                    <li>
                        <?php echo lang('srch'); ?>
                    </li>
                    <li id="result" class="pull-right topClock"></li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12 ">
                <div class="portlet box green ">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo lang('srch1'); ?>
                        </div>
                        <div class="tools">
                            <a href="" class="collapse">
                            </a>
                            <a href="" class="reload">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <?php
                        if (!empty($message)) {
                            echo $message;
                        } $form_attributs = array('class' => 'form-horizontal', 'role' => 'form');
                        echo form_open_multipart('configuration/search_system', $form_attributs);
                        ?>
                        <div class="form-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label"> <?php echo lang('lib_cate'); ?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                    <select class="form-control" name="category" data-validation="required" data-validation-error-msg="">
                                        <option value=""><?php echo lang('select'); ?> </option>
                                        <option value="student"> <?php echo lang('lib_stu'); ?>s </option>
                                        <option value="parent"> <?php echo lang('srch2'); ?> </option>
                                        <option value="staff"> <?php echo lang('srch3'); ?> </option>
                                        <!-- <option value="library"> <?php echo lang('header_library'); ?> </option>
                                        <option value="hostel"> <?php echo lang('srch4'); ?> </option> -->
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3"><span class="requiredStar"></span></label>
                                <div class="col-md-6">
                                    <input type="text" name="search" class="form-control" placeholder="Enter Name or any related thing for search" data-validation="alphanumeric" data-validation-allowing=" ">
                                </div>
                            </div>
                        </div>
                        <div class="form-actions fluid">
                            <div class="col-md-offset-3 col-md-9">
                                <button class="btn green" type="submit" name="submit" value="submit"><?php echo lang('srch'); ?></button>
                                <button class="btn red" type="reset"><?php echo lang('refresh'); ?></button>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<script>
    jQuery(document).ready(function () {
//here is auto reload after 1 second for time and date in the top
        jQuery(setInterval(function () {
            jQuery("#result").load("index.php/home/iceTime");
        }, 1000));
    });
</script>
<script src="assets/global/plugins/jquery.form-validator.min.js" type="text/javascript"></script>
<script> $.validate();</script>
<script type="text/javascript">

    function search_group(str) 
    {
        var xmlhttp;
        if (str.length === 0) {
            document.getElementById("group").innerHTML = "";
            return;
        }
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        }
        else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                document.getElementById("group").innerHTML = xmlhttp.responseText;
            }
        };
        xmlhttp.open("GET", "index.php/configuration/search_group?q=" + str, true);
        xmlhttp.send();
    }
</script>