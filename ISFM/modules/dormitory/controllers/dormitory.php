<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Dormitory extends MX_Controller {
    /**
     * This controller is using for managing full dormitory in this school
     *
     * Maps to the following URL
     * 		http://example.com/index.php/dormitory
     * 	- or -  
     * 		http://example.com/index.php/dormitory/<method_name>
     */
    function __construct() {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login');
        }
    }
    
    //This function is using to add new dormitory in this school
    public function addDormitory() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8))) {
            redirect('auth', 'refresh');
        }
        $parent_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parent_id = $this->session->userdata('parient_id');
        }
        if ($this->input->post('submit', TRUE)) {
            $dormitoriesName = $this->input->post('dormitoryName', TRUE);
            $dormitoriesRoom = $this->input->post('roomAmount', TRUE);
            $dormitoryInfo = array(
                'dormitory_name' => $this->db->escape_like_str($dormitoriesName),
                'parient_id' => $this->db->escape_like_str($parent_id),
                'dormitory_for' => $this->db->escape_like_str($this->input->post('dormitoryFor', TRUE)),
                'room_amount' => $this->db->escape_like_str($dormitoriesRoom),
            );
            if ($this->db->insert('dormitory', $dormitoryInfo)) {
                $nuid = $this->db->insert_id();
                $userid = $nuid - 1;
                for ($i = 1; $i <= $dormitoriesRoom; $i++) {
                    $dormitoryInfo_2 = array(
                        'dormitory_id' => $nuid,
                        'parient_id' => $this->db->escape_like_str($parent_id),
                        'dormitory_name' => $this->db->escape_like_str($dormitoriesName),
                        'room' => $this->db->escape_like_str('Room No: ' . $i)
                    );
                    $this->db->insert('dormitory_room', $dormitoryInfo_2);
                }
                redirect('dormitory/dormitoryReport', 'refresh');
            }
        } else {
            $this->load->view('temp/header');
            $this->load->view('addDormitory');
            $this->load->view('temp/footer');
        }
    }

    //This function is using to add new dormitory in this school
    public function addBed() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8))) {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        if ($this->input->post('submit', TRUE)) {
            $dormitoriesId = $this->input->post('dormitories', TRUE);
            $query = $this->common->getWhere('dormitory', 'id', $dormitoriesId);
            $room = $this->input->post('room', TRUE);
            foreach ($query as $row) {
                $dormitory_name = $row['dormitory_name'];
            }
            $check = $this->common->getWhere22('dormitory_room', 'dormitory_id', $dormitoriesId,'room',$room);
            if($check[0]['bed_amount'] > '0')
            {
                $data['message'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                                                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                                            <strong>Error!</strong>You already Added seats in this room
                                                                    </div>';
                $data['dormitory'] = $this->common->getAllData('dormitory',$parient_id);
                $this->load->view('temp/header');
                $this->load->view('addBed', $data);
                $this->load->view('temp/footer');
            }
            else
            {
                $bed_number = $this->input->post('Seat', TRUE);
                for ($i = 1; $i <= $bed_number; $i++) {
                    $tableData = array(
                        'dormitory_id' => $this->db->escape_like_str($dormitoriesId),
                        'parient_id' => $this->db->escape_like_str($parient_id),
                        'dormitory_name' => $this->db->escape_like_str($dormitory_name),
                        'room_number' => $this->db->escape_like_str($room),
                        'bed_number' => $this->db->escape_like_str('Seat No: ' . $i),
                    );
                    $this->db->insert('dormitory_bed', $tableData);
                }
                $tableData_2 = array(
                    'bed_amount' => $bed_number,
                    'free_seat' => $bed_number
                );
                if ($this->db->update('dormitory_room', $tableData_2, array('dormitory_id' => $dormitoriesId, 'room' => $room))) {
                    redirect('dormitory/dormitoryReport', 'refresh');
                }
            } 
        } else {
            $data['dormitory'] = $this->common->getAllData('dormitory',$parient_id);
            $this->load->view('temp/header');
            $this->load->view('addBed', $data);
            $this->load->view('temp/footer');
        }
    }

    //This function give the room amount in a dormitories and send the value by ajax
    public function ajaxDormitoryRoom() {
        $id = $this->input->get('q');
        $query = $this->common->getWhere('dormitory', 'id', $id);
        foreach ($query as $row) {
            $roomAmount = $row['room_amount'];
        }
        echo '<option value="">' . lang('dorc_1') . '</option>';
        for ($i = 1; $i <= $roomAmount; $i++) {
            echo '<option value="Room No: ' . $i . '">Room No: ' . $i . ' </option>';
        }
    }

    //This function give the full dormitory report
    public function dormitoryReport() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8))) {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $data['dormitory'] = $this->common->getAllData('dormitory',$parient_id);
        $data['dormitoryRoom'] = $this->common->getAllData('dormitory_room',$parient_id);
        $data['dormitory_bed'] = $this->common->getDorm($parient_id);
		$data['dormitory_visitors'] = $this->common->getAllData('dormitory_visitors',$parient_id);
        $this->load->view('temp/header');
        $this->load->view('dormitoryReport', $data);
        $this->load->view('temp/footer');
    }

    //This function will select men  for the seat
    public function selectMember() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8))) {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $data['dormitories'] = $this->common->getAllData('dormitory',$parient_id);
        $this->load->view('temp/header');
        $this->load->view('selectMember', $data);
        $this->load->view('temp/footer');
    }

    //This function return seat in a room
    public function ajaxSeatAmount() {
        $dormitories = $this->input->get('g');
        $dormitorie_id = $this->input->get('g_id');
        $room = $this->input->get('q');
        $query = $this->common->getWhere22('dormitory_bed', 'dormitory_name', $dormitories, 'room_number', $room);
        if (!empty($query)) {
            $i = 1;
            foreach ($query as $row) {
                if (!empty($row['student_id'])) {
                    echo '<div class="alert alert-success">
                                                <strong>' . $row['bed_number'] . '</strong>' . $row['student_name'] . '
                                                <a href="index.php/dormitory/removeSeatMember?id=' . $row['id'] . '" onClick="javascript:return dconfirm();" class="btn dorButton red"> ' . lang('dorc_2') . '</a>
                                        </div>';
                } else {
                    echo '<div class="alert alert-success">
                                                <strong>' . $row['bed_number'] . '</strong> &nbsp;&nbsp; Blank seat
                                                <a href="index.php/dormitory/seatBooking?id=' . $row['id'] . '&d_id='.$dormitorie_id.'&rm='.$room.'" class="btn dorButton green-meadow"> ' . lang('dorc_3') . ' </a>
                                        </div>';
                }
            }
        } else {
            echo '<div class="alert alert-danger">
                            <strong>' . lang('dorc_4') . ' </strong> ' . lang('dorc_5') . '
                    </div>';
        }
    }

    //This function can book a seat
    public function seatBooking() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8))) {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $id = $this->input->get('id');
        $d_id = $this->input->get('d_id');
        $rm = $this->input->get('rm');
        $free_seats = "";
        $data1 = "";
        $data3 = "";
        if ($this->input->post('submit', TRUE)) {
            $this->db->where('student_id', $this->input->post('studentId', TRUE));
            $this->db->where('parient_id', $parient_id);
            $query = $this->db->get('dormitory_bed');
            foreach ($query->result_array() as $row) {
                $data1 = $row['student_name'];
            }
            $this->db->where('student_id', $this->input->post('studentId', TRUE));
            $this->db->where('parient_id', $parient_id);
            $query3 = $this->db->get('student_info');
            foreach ($query3->result_array() as $row3) {
                $data3 = $row3['student_nam'];
            }
            if(!empty($data3))
            {
                if(empty($data1))
                {
                    $tdata = array(
                        'student_id' => $this->db->escape_like_str($this->input->post('studentId', TRUE)),
                        'student_name' => $this->db->escape_like_str($this->input->post('sudentName', TRUE)),
                        'class' => $this->db->escape_like_str($this->input->post('class_id', TRUE)),
                        'roll_number' => $this->db->escape_like_str($this->input->post('roll', TRUE))
                    );
                    $this->db->where('dormitory_id', $d_id);
                    $this->db->where('parient_id', $parient_id);
                    $this->db->where('room', $rm);
                    $query1 = $this->db->get('dormitory_room');
                    foreach ($query1->result_array() as $row1) {
                        $free_seats = $row1['free_seat'];
                    }
                    $free_seats = $free_seats - 1;
                    $seat_amount = array(
                        'free_seat' => $this->db->escape_like_str($free_seats),
                    );
                    $this->db->where('id', $id);
                    if ($this->db->update('dormitory_bed', $tdata)) 
                    {
                        $this->db->where('dormitory_id', $d_id);
                        $this->db->where('parient_id', $parient_id);
                        $this->db->where('room', $rm);
                        if($this->db->update('dormitory_room', $seat_amount))
                        {
                            redirect('dormitory/dormitoryReport', 'refresh');
                        }
                        
                    }
                }
                else
                {
                    $data['success'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                                                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                                            <strong>Error!</strong> For Student with this ID a bed has already been allocated
                                                                    </div>';
                    $data['hostel_id'] = $d_id ;
                    $data['rom_no'] = $rm ;
                    $this->load->view('temp/header');
                    $this->load->view('seatBook',$data);
                    $this->load->view('temp/footer');
                }
            }
            else
            {
                $data['success'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                                                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                                            <strong>Error!</strong> Sorry there is no student in our school with this ID kindly recheck the ID Thank you!
                                                                    </div>';
                    $data['hostel_id'] = $d_id ;
                    $data['rom_no'] = $rm ;
                    $this->load->view('temp/header');
                    $this->load->view('seatBook',$data);
                    $this->load->view('temp/footer');
            }
        } else {
            $data['hostel_id'] = $d_id ;
            $data['rom_no'] = $rm ;
            $this->load->view('temp/header');
            $this->load->view('seatBook',$data);
            $this->load->view('temp/footer');
        }
    }

    //This function give the student informations
    public function ajaxStudentInfo() {
        $studentId = $this->input->get('q');
        $query = $this->common->getWhere('class_students', 'student_id', $studentId);
        if (!empty($query)) {
            foreach ($query as $row) {
                echo '<div class="form-group">
                            <label class="control-label col-md-3">' . lang('dorc_6') . '</label>
                            <div class="col-md-8">
                                <input type="text" readonly="" placeholder="' . $row['student_title'] . '" class="form-control" name="sudentName" value="' . $row['student_title'] . '">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">' . lang('dorc_7') . '</label>
                            <div class="col-md-8">
                                <input type="text" readonly="" placeholder="" class="form-control" name="class" value="' . $this->common->class_title($row['section'])." ".$this->common->section_title($row['section']) . '">
                            </div>
                        </div>
                        <input type="hidden" name="class_id" value="' . $row['section'] . '">
                        <div class="form-group">
                            <label class="control-label col-md-3">' . lang('dorc_8') . '</label>
                            <div class="col-md-8">
                                <input type="text" readonly="" placeholder="' . $row['roll_number'] . '" class="form-control" name="roll" value="' . $row['roll_number'] . '">
                            </div>
                        </div>';
            }
        } else {
            echo '<div class="alert alert-danger">
                            <strong>' . lang('dorc_9') . '</strong> ' . lang('dorc_10') . ' 
                    </div>';
        }
    }

    //This function can remove any seat member's information
    public function removeSeatMember() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8))) {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $id = $this->input->get('id');
        $tdata = array(
            'student_id' => '0',
            'student_name' => '',
            'class' => '',
            'roll_number' => ''
        );
        $this->db->where('id', $id);
        if ($this->db->update('dormitory_bed', $tdata)) {
            redirect('dormitory/dormitoryReport', 'refresh');
        }
    }

    //This function can show tha full details about dormitories
    public function showDeatails() {
        $id = $this->input->get('id');
        $data['details'] = $this->common->getWhere('dormitory_bed', 'id', $id);
        $this->load->view('temp/header');
        $this->load->view('dormitoriesDetails', $data);
        $this->load->view('temp/footer');
    }
	
	//This function can show tha full details about dormitories
    public function dormitoryVisitorDetails() {
        $id = $this->input->get('id');
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        // var_dump($id );
        // die;
		$student_id = 0;
		$class_id = 0;
		//method to get visitor info
        $data['visitor_details'] = $this->common->getWhere('dormitory_visitors', 'id', $id);
		//getting student id from visitor info
		foreach($data['visitor_details'] as $row){
			$student_id = $row['student_id'];
		}
		//method to get student detail through student id
        $data['student_details'] = $this->common->studentDataById($student_id,$parient_id);
		//getting class id from student details
		foreach($data['student_details'] as $row2){
			$class_id = $row2['class_id'];
		}
		$data['class_title'] = $this->common->class_title1($class_id); 
        $this->load->view('temp/header');
        $this->load->view('dormitoryVisitorDetails', $data);
        $this->load->view('temp/footer');
    }
	
	//method to save the visitor of student 
	public function addDormitoryVisitor(){
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8))) {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
		if ($this->input->post('submit', TRUE)) {
            $tables = $this->config->item('tables', 'ion_auth');
            $student_id = $this->input->post('studentId');
            $visitor_name = $this->input->post('visitor_name');
            $visitor_cnic = $this->input->post('visitor_cnic');
            $visitor_pres_add = $this->input->post('visitor_present_address');
            $visitor_perm_add = $this->input->post('visitor_permanent_address');
            $visitor_relation = $this->input->post('relation');
            $visitor_gender = $this->input->post('gender');
            $email = strtolower($this->input->post('email', TRUE));
			$phone = $this->input->post('phoneCode', TRUE) . '' . $this->input->post('phone', TRUE);
			$cnic_copy = $this->input->post('cnic_copy');
			//$attached_docs_file_info = $this->input->post('submited_info');
            //Here is uploading the visitor's photo.
            $config['upload_path'] = './assets/uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = '10000';
            $config['max_width'] = '10240';
            $config['max_height'] = '7680';
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config);
            $this->upload->do_upload();
            $uploadFileInfo = $this->upload->data();
            $this->upload->display_errors('<p>', '</p>');
			
			$visitor_data = array(
				'parient_id'=> $parient_id, 
				'student_id'=> $student_id,
				'visitor_name'=> $visitor_name,
				'visitor_cnic'=> $visitor_cnic,
				'visitor_present_address'=> $visitor_pres_add,
				'visitor_permanent_address'=> $visitor_perm_add,
				'visitor_relation'=> $visitor_relation,
				'visitor_gender'=> $visitor_gender,
				'visitor_email'=> $email,
				'visitor_phone'=> $phone,
				'visitor_cnic_copy'=> $cnic_copy,
				'visitor_photo'=> $uploadFileInfo['file_name']
			);
			
			$dormitory_visitor_insertion = $this->db->insert('dormitory_visitors', $visitor_data);
			
			if($dormitory_visitor_insertion = true){
				$data['success'] = '<div class="alert alert-success">
										<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
										<strong>Success!</strong> Visitor added successfully!
									</div>';
									
				$this->load->view('temp/header');
				$this->load->view('dormitoryVisitor', $data);
				$this->load->view('temp/footer');
			}
			else{
				$data['success'] = '<div class="alert alert-danger">
										<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
										<strong>Failure!</strong> Something went wrong while adding visitor information!
									</div>';
				
				$this->load->view('temp/header');
				$this->load->view('dormitoryVisitor', $data);
				$this->load->view('temp/footer');
			}
		
        } else {
			$this->load->view('temp/header');
			$this->load->view('dormitoryVisitor');
			$this->load->view('temp/footer');
		}
	}

}
