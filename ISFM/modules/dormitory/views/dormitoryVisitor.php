<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo "Add Visitors"; ?> <small></small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo lang('home'); ?>
                    </li>
                    <li>
                        <?php echo lang('header_dormat'); ?>
                    </li>
                     <li>
                        <?php echo "Dormitory Visitor"; ?>
                    </li>
                    <li id="result" class="pull-right topClock"></li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12 ">
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box green ">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo lang('dor_gtndvi'); ?>
                        </div>
                        <div class="tools">
                            <a href="" class="collapse">
                            </a>
                            <a href="" class="reload">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <?php
                        $form_attributs = array('class' => 'form-horizontal', 'id' => 'validate_form','role' => 'form' );
                        echo form_open_multipart('dormitory/addDormitoryVisitor', $form_attributs);
                        ?>
                        <div class="form-body">
                            <?php
                            if (!empty($success)) {
                                echo $success;
                            }
                            ?>
							
							<div class="form-group">
                                <label class="col-md-3 control-label"> <?php echo lang('par_stu_id'); ?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" onkeyup="studentInfo(this.value)" placeholder="Student ID" name="studentId" data-validation="required" data-validation-error-msg="Please Enter Student ID">
                                </div>
                            </div>
                            <div id="ajaxResult">
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo "Visitor Name"; ?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                    <input data-validation="alphanumeric" data-validation-allowing=" " type="text" class="form-control" placeholder="Visitor Name" name="visitor_name" data-validation="required" data-validation-error-msg="Please Enter Visitor Name">
                                </div>
                            </div>
							<div class="form-group">
                                <label class="col-md-3 control-label"><?php echo "Visitor CNIC"; ?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                    <input type="text" data-validation="alphanumeric" data-validation-allowing=" " class="form-control" min="13" maxlength="13" placeholder="Visitor CNIC" name="visitor_cnic" data-validation="required" data-validation-error-msg="Please Enter Visitor CNIC">
                                </div>
                            </div>
							<div class="form-group">
                                <label class="col-md-3 control-label"><?php echo "Visitor's Present Address"; ?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                    <textarea type="text" class="form-control" placeholder="Present Address" name="visitor_present_address" data-validation="required" data-validation-error-msg="Please Enter Visitor's Address" ></textarea>
                                </div>
                            </div>
							<div class="form-group">
                                <label class="col-md-3 control-label"><?php echo "Visitor's Permanent Address"; ?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                    <textarea type="text" class="form-control" placeholder="Permanent Address" name="visitor_permanent_address" data-validation="required" data-validation-error-msg="Please Enter Visitor's Address" ></textarea>
                                </div>
                            </div>
							<div class="form-group">
                                <label class="col-md-3 control-label">Relation With Student <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                    <select class="form-control" name="relation" data-validation="required" data-validation-error-msg="You have to select anyone.">
                                        <option value=""><?php echo lang('select'); ?> </option>
                                        <option value="1"> Father </option>
                                        <option value="2"> Mother </option>
                                        <option value="3"> Brother </option>
                                        <option value="4"> Sister </option>
                                        <option value="5"> Spouse </option>
                                        <option value="6"> Friend </option>
                                        <option value="7"> Family Member </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Gender <span class="requiredStar"> * </span></label>
                                <div class="col-md-6 marginLeftSex">
                                    <div class="radio-list">
                                        <label class="radio-inline">
                                            <input type="radio" name="gender" id="optionsRadios4" value="Male" ><?php echo lang('tea_male'); ?></label>
                                        <label class="radio-inline">
                                            <input type="radio" name="gender" id="optionsRadios5" value="Female"> <?php echo lang('tea_female'); ?> </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="gender" id="optionsRadios6" value="Other"> <?php echo lang('tea_other'); ?> </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('tea_pn'); ?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-1">
                                    <input type="text"   min="0" maxlength="4"   class="form-control" name="phoneCode" placeholder="0300">
                                </div>
                                <div class="col-md-4">
                                    <input type="text" min="0" maxlength="7"  class="form-control" name="phone" placeholder="1234567">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('tea_email'); ?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                    <input type="email" data-validation="alphanumeric" data-validation-allowing="@ ."  class="form-control" onkeyup="checkEmail(this.value)" placeholder="demo@demo.com" name="email" placeholder="" >
                                    <div id="checkEmail" class="col-md-12"></div>
                                </div>
                            </div>
                            <div class="form-group last">
                                <label class="control-label col-md-3"><?php echo "Select Image"; ?> <span class="requiredStar"> </span></label>
                                <div class="col-md-9">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail">
                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail uploadImagePreview">
                                        </div>
                                        <div>
                                            <span class="btn default btn-file">
                                                <span class="fileinput-new"><?php echo lang('tea_sii'); ?> </span>
                                                <span class="fileinput-exists"><?php echo lang('tea_change'); ?> </span>
                                                <input type="file" name="userfile">
                                            </span>
                                            <a href="#" class="btn red fileinput-exists" data-dismiss="fileinput"><?php echo lang('tea_remove'); ?> </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="alert alert-success">
                                <strong><?php echo lang('tea_note'); ?> :</strong> <?php echo lang('tea_sadi'); ?>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"> </label>
                                <div class="col-md-9">
                                    <div class="checkbox-list">
                                        <label>
                                            <input name="cnic_copy" value="submited" type="checkbox"> CNIC Copy</label>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('tea_sfi'); ?> <span class="requiredStar">  </span></label>
                                <div class="col-md-6">
                                    <input type="text" data-validation="alphanumeric" data-validation-allowing=" " name="submited_info" class="form-control" placeholder="<?php echo lang('tea_fntaoi'); ?>" data-validation="required" data-validation-error-msg="">
                                </div>
                            </div> -->
                        </div>
                        <div class="form-actions fluid">
                            <div class="col-md-offset-3 col-md-6">
                                <button type="submit" class="btn green" name="submit" value="submit"><?php echo lang('tea_si'); ?></button>
                                <button type="reset" class="btn default"><?php echo lang('refresh'); ?></button>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->
<!-- BEGIN PAGE LEVEL script -->
<script type="text/javascript" src="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script type="text/javascript" src="assets/global/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
<script src="assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>
<script src="assets/global/plugins/typeahead/handlebars.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/typeahead/typeahead.bundle.min.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/components-form-tools.js"></script>
<script src="assets/global/plugins/jquery.form-validator.min.js" type="text/javascript"></script>
<script> $.validate();</script>
<script>
    jQuery(document).ready(function () {
            ComponentsFormTools.init();
        });
		function studentInfo(str) {
			var xmlhttp;
			if (str.length === 0) {
				document.getElementById("ajaxResult").innerHTML = "";
				return;
			}
			if (window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			} else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange = function () {
				if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
					document.getElementById("ajaxResult").innerHTML = xmlhttp.responseText;
				}
			};
			xmlhttp.open("GET", "index.php/users/studentInfoById?q=" + str, true);
			xmlhttp.send();
		}

    jQuery(document).ready(function () {
//here is auto reload after 1 second for time and date in the top
        jQuery(setInterval(function () {
            jQuery("#result").load("index.php/home/iceTime");
        }, 1000));
    });
</script>
<script type="text/javascript">
    var RecaptchaOptions = {
        theme: 'custom',
        custom_theme_widget: 'recaptcha_widget'
    };
</script>
<!-- END PAGE LEVEL script -->