<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="assets/global/plugins/select2/select2.css"/>
<link href="assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
<!-- BEGIN THEME STYLES -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo lang('dor_sdi'); ?> <small></small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo lang('home'); ?>
                    </li>
                    <li>
                        <?php echo lang('header_dormat'); ?>

                    </li>
                    <li>
                        <?php echo lang('header_add_dormito'); ?>
                    </li>
                    <li id="result" class="pull-right topClock"></li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXTRAS PORTLET-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            Dormitory Visitor Details
                        </div>
                    </div>
                    <div class="portlet-body">
                        <?php foreach ($visitor_details as $row) { ?>
                            <div class="alert alert-success">
                                <div class="col-md-6 profile-info">
									<div class="row">
										<div class="col-md-6">
											<img src="assets/uploads/<?php echo $row['visitor_photo']; ?>" style="height:150px;width:150px">
										</div>
									</div>
                                    <div class="row">
										<h4>Visitor Details</h4>
                                        <div class="col-sm-4 col-xs-6 detailsEvent">
                                            <span>: </span>
                                            Visitor Name
                                        </div>
                                        <div class="col-sm-8 col-xs-6 detailsEvent">
                                            <?php echo $row['visitor_name']; ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4 col-xs-6 detailsEvent">
                                            Student ID
                                            <span>: </span>
                                        </div>
                                        <div class="col-sm-8 col-xs-6 detailsEvent">
                                            <?php echo $row['student_id']; ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4 col-xs-6 detailsEvent">
                                            Visitor CNIC
                                            <span>: </span>
                                        </div>
                                        <div class="col-sm-8 col-xs-6 detailsEvent">
                                            <?php echo $row['visitor_cnic']; ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4 col-xs-6 detailsEvent">
                                            Present Address
                                            <span>: </span>
                                        </div>
                                        <div class="col-sm-8 col-xs-6 detailsEvent">
                                            <?php echo $row['visitor_present_address']; ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4 col-xs-6 detailsEvent">
                                            Permanent Address
                                            <span>: </span>
                                        </div>
                                        <div class="col-sm-8 col-xs-6 detailsEvent">
                                            <?php echo $row['visitor_permanent_address']; ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4 col-xs-6 detailsEvent">
                                            Relation
                                            <span>: </span>
                                        </div>
                                        <div class="col-sm-8 col-xs-6 detailsEvent">
                                            <?php relation_name_returner($row['visitor_relation']); ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4 col-xs-6 detailsEvent">
                                            Gender
                                            <span>: </span>
                                        </div>
                                        <div class="col-sm-8 col-xs-6 detailsEvent">
                                            <?php echo $row['visitor_gender']; ?>
                                        </div>
                                    </div>
									<div class="row">
                                        <div class="col-sm-4 col-xs-6 detailsEvent">
                                            E-mail
                                            <span>: </span>
                                        </div>
                                        <div class="col-sm-8 col-xs-6 detailsEvent">
                                            <?php echo $row['visitor_email']; ?>
                                        </div>
                                    </div>
									<div class="row">
                                        <div class="col-sm-4 col-xs-6 detailsEvent">
                                            Phone
                                            <span>: </span>
                                        </div>
                                        <div class="col-sm-8 col-xs-6 detailsEvent">
                                            <?php echo $row['visitor_phone']; ?>
                                        </div>
                                    </div>
									<div class="row">
                                        <div class="col-sm-4 col-xs-6 detailsEvent">
                                            File Info
                                            <span>: </span>
                                        </div>
                                        <div class="col-sm-8 col-xs-6 detailsEvent">
                                            <?php echo $row['visitor_documents_file_info']; ?>
                                        </div>
                                    </div>
                                </div>
								<?php } 
									foreach ($student_details as $row2){
								?>
								<div class="col-md-6 profile-info">
									<div class="row">
										<div class="col-md-6">
											<img src="assets/uploads/<?php echo $row2['student_photo']; ?>" style="height:150px;width:150px">
										</div>
									</div>
									<h4>Student Details</h4>
									<div class="row">
                                        <div class="col-sm-4 col-xs-6 detailsEvent">
											Class
                                            <span>: </span>
                                        </div>
                                        <div class="col-sm-8 col-xs-6 detailsEvent">
                                            <?php echo $class_title; ?>
                                        </div>
                                    </div>
									<div class="row">
                                        <div class="col-sm-4 col-xs-6 detailsEvent">
											Roll No.
                                            <span>: </span>
                                        </div>
                                        <div class="col-sm-8 col-xs-6 detailsEvent">
                                            <?php echo $row2['roll_number']; ?>
                                        </div>
                                    </div>
									<div class="row">
                                        <div class="col-sm-4 col-xs-6 detailsEvent">
											Student Name
                                            <span>: </span>
                                        </div>
                                        <div class="col-sm-8 col-xs-6 detailsEvent">
                                            <?php echo $row2['student_nam']; ?>
                                        </div>
                                    </div>
									<div class="row">
                                        <div class="col-sm-4 col-xs-6 detailsEvent">
											Date of Birth
                                            <span>: </span>
                                        </div>
                                        <div class="col-sm-8 col-xs-6 detailsEvent">
                                            <?php echo $row2['birth_date']; ?>
                                        </div>
                                    </div>
									<div class="row">
                                        <div class="col-sm-4 col-xs-6 detailsEvent">
											Father Name
                                            <span>: </span>
                                        </div>
                                        <div class="col-sm-8 col-xs-6 detailsEvent">
                                            <?php echo $row2['farther_name']; ?>
                                        </div>
                                    </div>
									<div class="row">
                                        <div class="col-sm-4 col-xs-6 detailsEvent">
											Mother Name
                                            <span>: </span>
                                        </div>
                                        <div class="col-sm-8 col-xs-6 detailsEvent">
                                            <?php echo $row2['mother_name']; ?>
                                        </div>
                                    </div>
									<div class="row">
                                        <div class="col-sm-4 col-xs-6 detailsEvent">
											Present Address
                                            <span>: </span>
                                        </div>
                                        <div class="col-sm-8 col-xs-6 detailsEvent">
                                            <?php echo $row2['present_address']; ?>
                                        </div>
                                    </div>
									<div class="row">
                                        <div class="col-sm-4 col-xs-6 detailsEvent">
											Permanent Address
                                            <span>: </span>
                                        </div>
                                        <div class="col-sm-8 col-xs-6 detailsEvent">
                                            <?php echo $row2['permanent_address']; ?>
                                        </div>
                                    </div>
									<div class="row">
                                        <div class="col-sm-4 col-xs-6 detailsEvent">
											Phone
                                            <span>: </span>
                                        </div>
                                        <div class="col-sm-8 col-xs-6 detailsEvent">
                                            <?php echo $row2['phone']; ?>
                                        </div>
                                    </div>
								</div>
									<?php } ?>
                                <div class="col-md-offset-3 col-md-6">
                                    <a href="javascript:history.back()" class="btn blue btn-block classDetailsFont">
                                        <i class="fa fa-mail-reply-all"></i> <?php echo lang('back'); ?></a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        
                    </div>
                </div>
                <!-- END EXTRAS PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/global/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/admin/pages/scripts/components-dropdowns.js"></script>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<?php 
	function relation_name_returner($relation_num){
		switch ($relation_num) {
			case '1':
				echo 'Father';
				break;
			case '2':
				echo 'Mother';
				break;
			case '3':
				echo 'Brother';
				break;
			case '4':
				echo 'Sister';
				break;
			case '5':
				echo 'Spouse';
				break;
			case '6':
				echo 'Friend';
				break;
			case '7':
				echo 'Family Member';
				break;
			default:
				echo 'N/A';
				break;
		}
	}
?>
<script>
    // function dormitorysRoomAmount(str) {
        // var xmlhttp;
        // if (str.length === 0) {
            // document.getElementById("ajaxResult").innerHTML = "";
            // return;
        // }
        // if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            // xmlhttp = new XMLHttpRequest();
        // } else {
            // code for IE6, IE5
            // xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        // }
        // xmlhttp.onreadystatechange = function () {
            // if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                // document.getElementById("ajaxResult").innerHTML = xmlhttp.responseText;
            // }
        // };
        // xmlhttp.open("GET", "index.php/dormitory/ajaxDormitoryRoom?q=" + str, true);
        // xmlhttp.send();
    // }

//
//    function dormitorysRoomSeat(str) {
//        var sel = document.getElementById("dormitories");
//        var val = sel.options[sel.selectedIndex].text;
//        var xmlhttp;
//        if (str.length === 0) {
//            document.getElementById("ajaxResult_2").innerHTML = "";
//            return;
//        }
//        if (window.XMLHttpRequest) {
//            // code for IE7+, Firefox, Chrome, Opera, Safari
//            xmlhttp = new XMLHttpRequest();
//        }
//        else {
//            // code for IE6, IE5
//            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
//        }
//        xmlhttp.onreadystatechange = function() {
//            if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
//                document.getElementById("ajaxResult_2").innerHTML = xmlhttp.responseText;
//            }
//        };
//        xmlhttp.open("GET", "index.php/dormitory/ajaxSeatAmount?q=" + str + "&g=" + val, true);
//        xmlhttp.send();
//    }

</script>
<script>
    jQuery(document).ready(function () {
//here is auto reload after 1 second for time and date in the top
        jQuery(setInterval(function () {
            jQuery("#result").load("index.php/home/iceTime");
        }, 1000));
    });
</script>