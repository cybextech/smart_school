<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class AccountModel extends CI_Model {
    /**
     * This model is using into the students controller
     * Load : $this->load->model('account');
     */
    function __construct() {
        parent::__construct();
        $this->load->dbforge();
    }

    //This function will return all students paments information
    public function stud_payment($par="") {
        $data = array();
        $query = $this->db->query("SELECT * FROM slip WHERE parient_id = $par  ORDER BY id DESC");
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        return $data;
    }

    //This function will return full invoice information
    public function invoice($slipId) {
        $data = array();
        $query = $this->db->query("SELECT * FROM slip WHERE id=$slipId");
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        return $data;
    }

    //This function will return all income account title list
    public function inco_title($parient_id) {
        $data = array();
        $this->db->select('*');
        $this->db->where('category', 'Income');
        $this->db->where('parient_id', $parient_id);
        $query = $this->db->get('account_title');
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        return $data;
    }

    //This function will return all income account title list
    public function expa_title($parient_id) {
        $data = array();
        $this->db->select('*');
        $this->db->where('category', 'Expense');
        $this->db->where('parient_id', $parient_id);
        $query = $this->db->get('account_title');
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        return $data;
    }

    //This function will return Total amount in a transuctio slip
    //This function was used in "paySalary()"
    public function pre_balence($parient_id) {

        

        $data = array();
        // $this->db->select('sum(amount)');
        // $this->db->where('parient_id', $parient_id);
        // $this->db->where('category', $category);
        // $query = $this->db->get('transection');
	   $query = $this->db->query("SELECT * FROM transection where parient_id=$parient_id ORDER BY id DESC LIMIT 1");
        foreach ($query->result_array() as $row) {
          
            // $row['balance'] = $row ["sum(amount)"];
            $data[] = $row['balance'];
            // $data[] = $row['balance'];
        }
        // var_dump($this->db->last_query());
        // die;
        if (!empty($data)) {
            return $data[0];
        } else {
            return 0;
        }
    }

    //This function will reaturn only maximam slip_number
    function maxSlip() {
        $maxid = 0;
        $row = $this->db->query('SELECT MAX(slip_number) AS `maxid` FROM `slip_number`')->row();
        if ($row) {
            $maxid = $row->maxid;
        }return $maxid + 1;
    }

    //This function will chack that is ther any tranjection submited today or not.
    public function tran_check($acco_id) {
        $d = date('d-m-Y');
        $date = strtotime($d);
        $data = array();
        $query = $this->db->query("SELECT id,amount FROM transection WHERE date = $date AND acco_id=$acco_id");
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        if (!empty($data)) {
            return $data;
        } else {
            return 'no_entry';
        }
    }

    //This function will return all employ who will get government salary
    public function salaryEmployList($month ='',$par="",$year="") {
        $data = array();
        $this->db->select('employe_title,employ_user_id');
        $current_year = date('Y');
        $new_month = $month -1; 
        $new_year = $year - 1;
        if($month == '1'){
            $this->db->where('month', '12');
            $this->db->where('year', $new_year);
        }else{
            $this->db->where('month', $new_month);
            $this->db->where('year', $year);
        }
        $this->db->where('parient_id',$par);
        $query = $this->db->get("set_salary");
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        return $data;
    }

    //This function will return one employ salary info
    public function ajaxSalaryAmount($uId, $salType, $month, $year) {
		
		$salary = 0;
		$months = [31 , 28 , 31 , 30 , 31 , 30 , 31 , 31 , 30 , 31 , 30 , 31];
		$query = $this->db->query("SELECT total FROM set_salary WHERE employ_user_id='$uId'");
		foreach ($query->result_array() as $row) {
			$salary = $row['total'];
		}
		
		if($salType == 1){
			return $salary;
		}
		else{
			$this->db->where('year',$year);
			$this->db->where('month',$month);
			$this->db->where('present_or_absent',0);
			$this->db->where('employ_id',$uId);
			$days_absent=$this->db->get('teacher_attendance');
			$days_absent = $days_absent->num_rows();
            $days_attended = 30-$days_absent;
			$salary = ceil(($salary/$months[$month-1])* $days_attended);
			return $salary;
		}
        
    }

    //This function will return all employ salary list
    public function employee_salary($par="") {
        $data = array();
        $query = $this->db->query("SELECT * FROM salary WHERE parient_id=$par");
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        return $data;
    }

    //This function will return all employ salary list
    public function my_salary($par="",$m_id = "") {
        $data = array();
        $query = $this->db->query("SELECT * FROM salary WHERE parient_id=$par AND user_id=$m_id");
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        return $data;
    }

    //This function will return employ's previous advanced taken amount
    public function preAdvance($uid) {
        $data = array();
        $query = $this->db->query("SELECT advanced_taken FROM set_salary WHERE employ_user_id=$uid");
        foreach ($query->result_array() as $row) {
            $data = $row['advanced_taken'];
        }
        return $data;
    }

    //This function will show employe title
    public function semployTitle($uid) {
        $data = array();
        $query = $this->db->query("SELECT employe_title FROM set_salary WHERE employ_user_id=$uid");
        foreach ($query->result_array() as $row) {
            $data = $row['employe_title'];
        }
        return $data;
    }

    //This funtion will return all income's data from transection table
    public function income($parient_id) {
        $data = array();
        $query = $this->db->query("SELECT * FROM transection WHERE category='Income' AND parient_id=$parient_id");
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        return $data;
    }

    //This function will return account title by id
    public function acc_tit_id($acco_id) {
        $data = array();
        $query = $this->db->query("SELECT account_title FROM account_title WHERE id =$acco_id");
        foreach ($query->result_array() as $row) {
            $data = $row['account_title'];
        }
        return $data;
    }

    //This funtion will return all income's data from transection table
    public function expanse($parient_id) {
        $data = array();
        $query = $this->db->query("SELECT * FROM transection WHERE category='Expense' AND parient_id=$parient_id");
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        return $data;
    }

    //This function will return only one trangection information by trangection id
    public function single_tran($id) {
        $data = array();
        $query = $this->db->query("SELECT * FROM transection WHERE id='$id'");
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        return $data;
    }

    //This function will return only transection id list 
    public function id_list($id) {
        $data = array();
        $query = $this->db->query("SELECT id FROM transection WHERE id>'$id'");
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        return $data;
    }

    //This function will return all class's id and title
    public function all_class($par="") {
        $m = date('F');
        $data = array();
        $query = $this->db->query("SELECT id FROM class WHERE month_fee != '$m' AND parient_id = $par");
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }return $data;
    }

    //This function will return total month fee end of the month
    public function total_fee($class_id,$par="") {
        $data = array();
        $year = date('Y');
        $query = $this->db->query("SELECT id,amount FROM fee_item WHERE year=$year AND class_id=$class_id AND parient_id = $par");
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        return $data;     
    }

    //This function will return all students id
    public function all_students($class_id,$par="") {
        $data = array();
        $query = $this->db->query("SELECT student_id FROM class_students WHERE section=$class_id AND parient_id = $par");
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }return $data;
    }

    //This function will return student's last month due amount
    public function dues($student_id,$par) {
        $data = 0;
        $year = date('Y');
        $this->db->select("balance");
        $this->db->where("year", $year);
        $this->db->where("student_id", $student_id);
        $this->db->where("parient_id", $par);
        $this->db->order_by("id", "DESC");
        $this->db->LIMIT(1);
        $query = $this->db->get("slip");
        // $query = $this->db->query("SELECT  FROM slip WHERE  = $year AND student_id = $student_id ORDER BY id DESC LIMIT 1");
        // var_dump($this->db->last_query());
        // die;
        foreach ($query->result_array() as $row) {
            $data = $row['balance'];
        }
        if ($data > 0) {
            return $data;
        } else {
            $data = 0;
            return $data;
        }
    }

    //This function will return advanced paid amount for students fee
    public function advance($student_id,$par) {
        $data = 0;
        $year = date('Y');
        $this->db->select("advance");
        $this->db->where("year", $year);
        $this->db->where("student_id", $student_id);
        $this->db->where("parient_id", $par);
        $this->db->order_by("id", "DESC");
        $this->db->LIMIT(1);
        $query = $this->db->get("slip");
        foreach ($query->result_array() as $row) {
            $data = $row['advance'];
        }
        if ($data > 0) {
            return $data;
        } else {
            $data = 0;
            return $data;
        }
        return $data;
    }

    //This function will return item title by item id
    public function item_title($item_id) {
        $data = "";
        $query = $this->db->query("SELECT title FROM fee_item WHERE id=$item_id");
        foreach ($query->result_array() as $row) {
            $data = $row['title'];
        }
        return $data;
    }

    //This function will show item fee amount by item id
    public function item_amount($item_id) {
        $data = "";
        $query = $this->db->query("SELECT amount FROM fee_item WHERE id=$item_id");
        foreach ($query->result_array() as $row) {
            $data = $row['amount'];
        }
        return $data;
    }

    //This function will return payment slip info
    public function s_slip_info($s_id) {
        $query = $this->db->query("SELECT total FROM slip WHERE id=$s_id");
        foreach ($query->result_array() as $row) {
            $data = $row['total'];
        }return $data;
    }

    public function rDues($s_id) {
        $query = $this->db->query("SELECT dues FROM slip WHERE id=$s_id");
        foreach ($query->result_array() as $row) {
            $data = $row['dues'];
        }return $data;
    }

    //This function will return paid amount
    public function paid_amount($s_id) {
        $query = $this->db->query("SELECT paid FROM slip WHERE id=$s_id");
        foreach ($query->result_array() as $row) {
            $data = $row['paid'];
        }return $data;
    }

    //This function will return student's own slip
    public function own_slips($student_id) {
        $data = array();
        $this->db->where("year", date('Y'));
        $this->db->where("student_id", $student_id);
        $query = $this->db->get('slip');
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }return $data;
    }

    public function std_uid($student_id,$par) {
        $data = "";
        $this->db->where("parient_id", $par);
        $this->db->where("student_id", $student_id);
        $this->db->select("user_id");
        $query = $this->db->get('student_info');
        foreach ($query->result_array() as $row) {
            $data = $row['user_id'];
        }
        return $data;
    }

    public function libFine($student_id,$par) {
        $data = "";
        $this->db->where("parient_id", $par);
        $this->db->where("user_id", $student_id);
        $this->db->select("fine");
        $query = $this->db->get('library_member');
        foreach ($query->result_array() as $row) {
            $data = $row['fine'];
        }
        return $data;
    }

    public function scholarship1($student_id,$par) {
        $data = "";
        $this->db->where("parient_id", $par);
        $this->db->where("user_id", $student_id);
        $this->db->select("scholarship");
        $query = $this->db->get('class_students');
        foreach ($query->result_array() as $row) {
            $data = $row['scholarship'];
        }
        return $data;
    }

    //This function will return students of school
    public function all_student($par="") {
        $m = date('F');
        $data = array();
        $query = $this->db->query("SELECT * FROM class_students WHERE month_fee != '$m' AND parient_id = $par");
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }return $data;
    }

}
