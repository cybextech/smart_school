<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Account extends MX_Controller {
    /**
     * This controller is using for controlling account and tranjection
     *
     * Maps to the following URL
     * 		http://example.com/index.php/account
     * 	- or -  
     * 		http://example.com/index.php/account/<method_name>
     */
    function __construct() {
        parent::__construct();
        $this->load->model('accountmodel');
        $this->load->model('common');
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login');
        }
    }

    //This function is adding now account title
    public function addAccountTitle() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_accountant() && !$this->ion_auth->in_group(8) && !$this->ion_auth->is_admin())) {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        if ($this->input->post('submit', TRUE)) {
            $accuntInfo = array(
                'parient_id' => $this->db->escape_like_str($parient_id),
                'account_title' => $this->db->escape_like_str($this->input->post('accountTitle', TRUE)),
                'category' => $this->db->escape_like_str($this->input->post('type', TRUE)),
                'description' => $this->db->escape_like_str($this->input->post('description', TRUE))
            );
            if ($this->db->insert('account_title', $accuntInfo)) {
                $data['allAccount'] = $this->common->getAllData('account_title', $parient_id);
                $data['message'] = '<div class="alert alert-success alert-dismissable">
								<button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
								<strong>Success ! </strong>'.lang('accnt_ad_scsc').'
							</div>';
                $this->load->view('temp/header');
                $this->load->view('addAccountTitle', $data);
                $this->load->view('temp/footer');
            }
        } else {
            $data['allAccount'] = $this->common->getAllData('account_title', $parient_id);
            $this->load->view('temp/header');
            $this->load->view('addAccountTitle', $data);
            $this->load->view('temp/footer');
        }
    }

    //This function is using for show all account title view
    public function allAccount() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_accountant() && !$this->ion_auth->in_group(8) && !$this->ion_auth->is_admin())) {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $this->load->view('temp/header');
        $this->load->view('allAccount', $data);
        $this->load->view('temp/footer');
    }

    //This function will edit Account title information here.
    public function editAccountInfo() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_accountant() && !$this->ion_auth->in_group(8) && !$this->ion_auth->is_admin())) {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $id = $this->input->get('id', TRUE);
        if ($this->input->post('submit', TRUE)) {
            $accuntInfo = array(
                'account_title' => $this->db->escape_like_str($this->input->post('accountTitle', TRUE)),
                'category' => $this->db->escape_like_str($this->input->post('type', TRUE)),
                'description' => $this->db->escape_like_str($this->input->post('description', TRUE))
            );
            $this->db->where('id', $id);
            if ($this->db->update('account_title', $accuntInfo)) {
                $data['allAccount'] = $this->common->getAllData('account_title', $parient_id);
                $data['message'] = '<div class="alert alert-success alert-dismissable">
								<button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
								<strong>Success ! </strong> '.lang('acnt_titl_upd').'
							</div>';
                $this->load->view('temp/header');
                $this->load->view('addAccountTitle', $data);
                $this->load->view('temp/footer');
            }
        } else {
            $data['accountInfo'] = $this->common->getWhere('account_title', 'id', $id);
            $this->load->view('temp/header');
            $this->load->view('editAccount', $data);
            $this->load->view('temp/footer');
        }
    }

    //This function will delete Account Title.
    public function deleteAccount() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_accountant() && !$this->ion_auth->in_group(8) && !$this->ion_auth->is_admin())) {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $id = $this->input->get('id', TRUE);
        $this->db->delete('account_title', array('id' => $id));
        //After deleteing the account lode all Account info.
        $data['allAccount'] = $this->common->getAllData('account_title', $parient_id);
        $data['message'] = '<div class="alert alert-success alert-dismissable">
                                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                        <strong>Success ! </strong>'.lang('acnt_titl_dlt').'
                                                </div>';
        $this->load->view('temp/header');
        $this->load->view('addAccountTitle', $data);
        $this->load->view('temp/footer');
    }

    //This function will show students own due and pay
    public function due_pay() {
        if (!$this->ion_auth->logged_in()) {
            redirect('auth', 'refresh');
        }
        if($this->ion_auth->is_admin() || $this->ion_auth->in_group(8))
        {
            $user_id = $this->input->get('uisd');
        }
        else
        {
            $user = $this->ion_auth->user()->row();
            $user_id = $user->id;
        }
        $student_id = $this->common->student_id($user_id);
        $data['slips'] = $this->accountmodel->own_slips($student_id);
        $this->load->view('temp/header');
        $this->load->view('due_pay', $data);
        $this->load->view('temp/footer');
    }

    //This function will load all students trangections slips
    public function allSlips() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_accountant() && !$this->ion_auth->in_group(8) && !$this->ion_auth->is_admin())) {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $data['slips'] = $this->accountmodel->stud_payment($parient_id);
        $this->load->view('temp/header');
        $this->load->view('allSlips', $data);
        $this->load->view('temp/footer');
    }

    //Show invioce or students tranjection slips details
    public function view_invoice() {
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $slipId = $this->input->get('sid', TRUE);
        $data['invoice'] = $this->accountmodel->invoice($slipId);
        $data['schoolName'] = $this->common->schoolName($parient_id);
        $data['currency'] = $this->common->currencyClass($parient_id);
        $this->load->view('temp/header');
        $this->load->view('invoice', $data);
        $this->load->view('temp/footer');
    }

    //This function will pay students fees
    public function fee_pay() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_accountant() && !$this->ion_auth->in_group(8) && !$this->ion_auth->is_admin())) {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $date = strtotime(date('d-m-Y'));
        if ($this->input->post('submit', TRUE)) 
        {
            $sid = $this->input->get('sid');
            $total = $this->input->post('total', TRUE);
            $paid = $this->input->post('paid_amount', TRUE);
            $d1 = $this->accountmodel->invoice($sid);
            $paid = $paid + $d1[0]['paid'];
            $pre_balence = $this->accountmodel->pre_balence($parient_id);
            $balence = $pre_balence + $this->input->post('paid_amount', TRUE);
            $query123 = $this->db->query("SELECT time_zone FROM configuration WHERE parient_id = '$parient_id'");
            $timezone = "";
            foreach ($query123->result_array() as $row123) {
                $timezone = $row123['time_zone'];
            }
            $datestring = "%h:%i %a";
            $now = now();
            $time = gmt_to_local($now, $timezone);
            $compTime = mdate($datestring, $time);
            $t_id = "";
            $this->db->select('id');
            $this->db->where('account_title', 'Fee');
            $this->db->where('category', 'Income');
            $this->db->where('parient_id', $parient_id);
            $query2 = $this->db->get('account_title');
            foreach ($query2->result_array() as $row2)
            {
                $t_id = $row2['id'];
            }
            if ($paid > $total || $paid == $total)
            {
                $due = 0;
                $balance = $paid - $total;
                $status = 'Paid';
                //echo 'a';
            } 
            elseif ($paid < $total) 
            {
                $balance = 0;
                $due = $total - $paid;
                $status = 'Not Clear';
                //echo 'b';
            }
            $inco_data = array(
                        'parient_id' => $this->db->escape_like_str($parient_id),
                        'date' => $this->db->escape_like_str($date),
                        'time' => $compTime,
                        'type' => $this->db->escape_like_str('Fee'),
                        'acco_id' => $this->db->escape_like_str($t_id),
                        'category' => $this->db->escape_like_str('Income'),
                        'amount' => $this->db->escape_like_str($this->input->post('paid_amount', TRUE)),
                        'balance' => $this->db->escape_like_str($balence)
                    );
            $slip_data = array(
                'dues' => $this->db->escape_like_str($due),
                'total' => $this->db->escape_like_str($total),
                'paid' => $this->db->escape_like_str($paid),
                'balance' => $this->db->escape_like_str($balance),
                'status' => $this->db->escape_like_str($status),
                'date' => $this->db->escape_like_str($date),
                'mathod' => $this->db->escape_like_str($this->input->post('method', TRUE)),
            );
            $this->db->where('id', $sid);
            if ($this->db->update('slip', $slip_data))
            {
                if($this->db->insert('transection', $inco_data))
                {
                    $data['message'] = '<div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                <strong>WOW!</strong>'.lang('trasction_scscs').'
                            </div>';
                    $data['slips'] = $this->accountmodel->stud_payment($parient_id);
                    $this->load->view('temp/header');
                    $this->load->view('allSlips', $data);
                    $this->load->view('temp/footer');
                }
            }
        } 
        else 
        {
            $sid = $this->input->get('sid');
            $data['total'] = $this->accountmodel->s_slip_info($sid);
            $data['dues'] = $this->accountmodel->rDues($sid);
            $data['slip_id'] = $sid;
            $this->load->view('temp/header');
            $this->load->view('fee_pay', $data);
            $this->load->view('temp/footer');
        }
    }

    //This function will edit student payment information
    public function edit_fee_pay() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_accountant() && !$this->ion_auth->in_group(8) && !$this->ion_auth->is_admin())) {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        if ($this->input->post('submit', TRUE)) {
            $date = strtotime(date('d-m-Y'));
            $query123 = $this->db->query("SELECT time_zone FROM configuration WHERE parient_id = '$parient_id'");
            $timezone = "";
            foreach ($query123->result_array() as $row123) {
                $timezone = $row123['time_zone'];
            }
            $datestring = "%h:%i %a";
            $now = now();
            $time = gmt_to_local($now, $timezone);
            $compTime = mdate($datestring, $time);
            $pre_balence = $this->accountmodel->pre_balence($parient_id);
            $sid = $this->input->get('sid');
            $total = $this->input->post('total', TRUE);
            $paid = $this->input->post('paid_amount', TRUE);
            $prev_paid = $this->input->post('paid_amount1', TRUE);
            $balence = "";
            $amnt = "";
            if($prev_paid > $paid)
            {
                $balence = $prev_paid - $paid ;
                $amnt = $balence;
                $balence = $pre_balence - $balence;
                $inco_data = array(
                        'parient_id' => $this->db->escape_like_str($parient_id),
                        'date' => $this->db->escape_like_str($date),
                        'time' => $compTime,
                        'type' => $this->db->escape_like_str('Extra Fee Return'),
                        'category' => $this->db->escape_like_str('Expense'),
                        'amount' => $this->db->escape_like_str($amnt),
                        'balance' => $this->db->escape_like_str($balence)
                    );
                $this->db->insert('transection', $inco_data);
            }
            elseif($prev_paid < $paid)
            {
                $balence = $paid - $prev_paid ;
                $amnt = $balence;
                $balence = $pre_balence + $balence;
                $inco_data = array(
                        'parient_id' => $this->db->escape_like_str($parient_id),
                        'date' => $this->db->escape_like_str($date),
                        'time' => $compTime,
                        'type' => $this->db->escape_like_str('Extra Fee Submission'),
                        'category' => $this->db->escape_like_str('Income'),
                        'amount' => $this->db->escape_like_str($amnt),
                        'balance' => $this->db->escape_like_str($balence)
                    );
                $this->db->insert('transection', $inco_data);
            }
            if ($paid > $total || $paid == $total) {
                $due = 0;
                $balance = $paid - $total;
                $status = 'Paid';
                //echo 'a';
            } elseif ($paid < $total) {
                $balance = 0;
                $due = $total - $paid;
                $status = 'Not Clear';
                //echo 'b';
            }
            $slip_data = array(
                'dues' => $this->db->escape_like_str($due),
                'total' => $this->db->escape_like_str($total),
                'paid' => $this->db->escape_like_str($paid),
                'balance' => $this->db->escape_like_str($balance),
                'status' => $this->db->escape_like_str($status),
                'mathod' => $this->db->escape_like_str($this->input->post('method', TRUE)),
            );
            $this->db->where('id', $sid);
            if ($this->db->update('slip', $slip_data)) {
                $data['message'] = '<div class="alert alert-success alert-dismissable">
								<button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
								<strong>WOW!</strong> '.lang('trasction_scscs').'
							</div>';
                $data['slips'] = $this->accountmodel->stud_payment($parient_id);
                $this->load->view('temp/header');
                $this->load->view('allSlips', $data);
                $this->load->view('temp/footer');
            }
        } else {
            $sid = $this->input->get('sid');
            $data['paid_amount'] = $this->accountmodel->paid_amount($sid);
            $data['total'] = $this->accountmodel->s_slip_info($sid);
            $data['slip_id'] = $sid;
            $this->load->view('temp/header');
            $this->load->view('edit_fee_pay', $data);
            $this->load->view('temp/footer');
        }
    }
    
    //This function will give the student information from studentID
    public function studentInfoById() {
        $studentId = $this->input->get('q', TRUE);
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $query = $this->common->stuInfoId($studentId,$parient_id);
        if (empty($query)) {
            echo '<div class="form-group">
                    <label class="col-md-3 control-label"></label>
                        <div class="col-md-6">
                        <div class="alert alert-danger">
                            <strong>' . lang('tea_info') . ':</strong> ' . lang('teac_1') . ' <strong>' . $studentId . '</strong>' . lang('teac_2') . '
                    </div></div></div>';
        } else {
            echo '<div class="row"><div class="col-md-offset-2 col-md-7 stuInfoIdBox">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="col-md-4 control-label">' . lang('teac_3') . ' <span class="requiredStar">  </span></label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="studentName" value="' . $query->student_nam . '" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">' . lang('teac_4') . ' <span class="requiredStar">  </span></label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="class" value="' . $this->common->class_title($query->class_id) . '" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <img src="assets/uploads/' . $query->student_photo . '" class="img-responsive" alt=""><br>
                    </div>
                </div></div>';
        }
    }

    public function own_salary()
    {
        $parient_id = $this->session->userdata('parient_id');
        $my_id = $this->session->userdata('user_id');
        $data['salary_list'] = $this->accountmodel->my_salary($parient_id,$my_id);
        $this->load->view('temp/header');
        $this->load->view('own_salary', $data);
        $this->load->view('temp/footer');
    }
    //This function will work to pay salary to employes
    public function paySalary() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_accountant() && !$this->ion_auth->in_group(8) && !$this->ion_auth->is_admin())) {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        if ($this->input->post('submit', TRUE)) {
            $pre_balence = $this->accountmodel->pre_balence($parient_id);
            $total_amount = $this->input->post('totalSalary', TRUE);
            if ($pre_balence >= $total_amount) {
                $balence = $pre_balence - $total_amount;
                $employId = $this->input->post('employId', TRUE);
                if ($this->input->post('month', TRUE) == 1) {
                    $month = 'January';
                } elseif ($this->input->post('month', TRUE) == 2) {
                    $month = 'February';
                } elseif ($this->input->post('month', TRUE) == 3) {
                    $month = 'March';
                } elseif ($this->input->post('month', TRUE) == 4) {
                    $month = 'April';
                } elseif ($this->input->post('month', TRUE) == 5) {
                    $month = 'May';
                } elseif ($this->input->post('month', TRUE) == 6) {
                    $month = 'Jun';
                } elseif ($this->input->post('month', TRUE) == 7) {
                    $month = 'July';
                } elseif ($this->input->post('month', TRUE) == 8) {
                    $month = 'August';
                } elseif ($this->input->post('month', TRUE) == 9) {
                    $month = 'Septembore';
                } elseif ($this->input->post('month', TRUE) == 10) {
                    $month = 'October';
                } elseif ($this->input->post('month', TRUE) == 11) {
                    $month = 'November';
                } elseif ($this->input->post('month', TRUE) == 12) {
                    $month = 'December';
                }
                    $t_id = "";
                    $this->db->select('id');
                    $this->db->where('account_title', 'Salary');
                    $this->db->where('category', 'Expense');
                    $this->db->where('parient_id', $parient_id);
                    $query2 = $this->db->get('account_title');
                    foreach ($query2->result_array() as $row2) 
                    {
                        $t_id = $row2['id'];
                    }
                $salary = array(
                    'year' => $this->db->escape_like_str($this->input->post('year', TRUE)),
                    'parient_id' => $parient_id,
                    'date' => $this->db->escape_like_str(strtotime(date('d-m-Y'))),
                    'month' => $this->db->escape_like_str($month),
                    'total_amount' => $this->db->escape_like_str($total_amount),
                    'method' => $this->db->escape_like_str($this->input->post('method', TRUE)),
                    'user_id' => $this->db->escape_like_str($employId),
                    'employ_title' => $this->db->escape_like_str($this->input->post('employ_title', TRUE))
                );
                if ($this->db->insert('salary', $salary)) 
                {
                    $query123 = $this->db->query("SELECT time_zone FROM configuration WHERE parient_id = '$parient_id'");
                    $timezone = "";
                    foreach ($query123->result_array() as $row123) {
                        $timezone = $row123['time_zone'];
                    }
                   $datestring = "%h:%i %a";
                   $now = now();
                   $time = gmt_to_local($now, $timezone);
                   $compTime = mdate($datestring, $time);
                   $inco_data = array(
                            'date' => $this->db->escape_like_str(strtotime(date('d-m-Y'))),
                            'time' => $compTime,
                            'acco_id' => $this->db->escape_like_str($t_id),
                            'parient_id' => $this->db->escape_like_str($parient_id),
                            'category' => $this->db->escape_like_str('Expense'),
                            'type' => $this->db->escape_like_str('Salary'),
                            'amount' => $this->db->escape_like_str($total_amount),
                            'balance' => $this->db->escape_like_str($balence)
                        );
                        $this->db->insert('transection', $inco_data);
                }
                $satSalaryInfo = array(
                    'month' => $this->db->escape_like_str($this->input->post('month', TRUE)),
                );
                $this->db->where('employ_user_id', $employId);
                if ($this->db->update('set_salary', $satSalaryInfo)) {
                    redirect('account/paySalary', 'refresh');
                }
            } else {
                $data['message'] = '<div class="alert alert-block alert-danger fade in">
                                    <button data-dismiss="alert" class="close" type="button"></button>
                                    <h4 class="alert-heading">' . lang('error') . '</h4> ' . lang('teac_5') . '
                            </div>';
                $data['salary_list'] = $this->accountmodel->employee_salary($parient_id);
                $this->load->view('temp/header');
                $this->load->view('paySalary', $data);
                $this->load->view('temp/footer');
            }
        } else {
            $data['salary_list'] = $this->accountmodel->employee_salary($parient_id);
            $this->load->view('temp/header');
            $this->load->view('paySalary', $data);
            $this->load->view('temp/footer');
        }
    }

    //This function will show the employ who will get Government salary
    public function ajaxEmployInfo() {
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $month = $this->input->get('month');
        $yearr = $this->input->get('year');
        $query = $this->accountmodel->salaryEmployList($month,$parient_id,$yearr);
        echo '<div class="form-group">
		<label class="col-md-3 control-label">Salary Type<span class="requiredStar"> * </span></label>
            <div class="col-md-9">
                <select onchange="changeSalaryType(this.value)" class="form-control" name="employId" data-validation="required" data-validation-error-msg="' . lang('teac_11') . '">
                    <option value="1">Full Salary</option>
					<option value="2">Day Based Salary</option>
				</select>
			</div>
		</div>
		<div class="form-group">
            <label class="col-md-3 control-label">' . lang('teac_6') . ' <span class="requiredStar"> * </span></label>
            <div class="col-md-9">
                <select onchange="salaryAmount(this.value)" class="form-control" name="employId" data-validation="required" data-validation-error-msg="' . lang('teac_11') . '">
                    <option value="">' . lang('select') . '</option>';
        foreach ($query as $row) {
            echo '<option value="' . $row['employ_user_id'] . '">' . $row['employe_title'] . '</option>';
        }
        echo '</select>
            </div>
        </div>
		<div id="ajaxResult_2"></div>';
    }

    //This function will return one employe sallary amount
    public function ajaxSalaryAmount() 
    {
        $uId = $this->input->get('uId');
		$salType = $this->input->get('type');
		$month = $this->input->get('month');
		$year = $this->input->get('year');
        $query = $this->accountmodel->ajaxSalaryAmount($uId, $salType, $month, $year);
        echo '<div class="form-group">
            <label class="col-md-3 control-label"> ' . lang('teac_7') . ' <span class="requiredStar">  </span></label>
            <div class="col-md-9">
                <input type="text" readonly="" placeholder="Readonly" class="form-control" name="totalSalary" value="' . $query . '">
            </div>
        </div><input type="hidden" name="employ_title" value="' . $this->accountmodel->semployTitle($uId) . '">';
    }

    //this function will return employ title via user id
    public function SchEmploTItle() {
        $uId = $this->input->get('uId');
        echo '<input type="hidden" name="employ_title" value="' . $this->accountmodel->semployTitle($uId) . '">';
    }

    //This function will make transection
    public function transection() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_accountant() && !$this->ion_auth->in_group(8) && !$this->ion_auth->is_admin())) {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $query123 = $this->db->query("SELECT time_zone FROM configuration WHERE parient_id = '$parient_id'");
        $date = strtotime(date('d-m-Y'));
        $timezone = "";
            foreach ($query123->result_array() as $row123) {
                $timezone = $row123['time_zone'];
            }
        $datestring = "%h:%i %a";
        $now = now();
        $time = gmt_to_local($now, $timezone);
        $compTime = mdate($datestring, $time);
        if ($this->input->post('expense', TRUE)) 
        {
            $account_id = $this->input->post('account_id', TRUE);
            $amount = $this->input->post('amount', TRUE);
            $pre_balence = $this->accountmodel->pre_balence($parient_id);
            $ac_nam1 = "";
            $this->db->select('account_title');
            $this->db->where('id', $account_id);
            $this->db->where('parient_id', $parient_id);
            $query2 = $this->db->get('account_title');
            foreach ($query2->result_array() as $row2) {
                $ac_nam1 = $row2['account_title'];
            }
            if ($pre_balence >= $amount) 
            {
                $balence = $pre_balence - $amount;
                    $inco_data = array(
                        'parient_id' => $this->db->escape_like_str($parient_id),
                        'date' => $this->db->escape_like_str($date),
                        'time' => $compTime,
                        'type' => $this->db->escape_like_str($ac_nam1),
                        'acco_id' => $this->db->escape_like_str($account_id),
                        'category' => $this->db->escape_like_str('Expense'),
                        'amount' => $this->db->escape_like_str($amount),
                        'balance' => $this->db->escape_like_str($balence)
                    );
                    if ($this->db->insert('transection', $inco_data)) 
                    {
                        $data['message'] = '<div class="alert alert-block alert-success fade in">
                                            <button data-dismiss="alert" class="close" type="button"></button>
                                            <h4 class="alert-heading">' . lang('success') . ' </h4> ' . lang('teac_8') . ' 
                                    </div>';
                        $data['income'] = $this->accountmodel->income($parient_id);
                        $data['expanse'] = $this->accountmodel->expanse($parient_id);
                        $data['inco_title'] = $this->accountmodel->inco_title($parient_id);
                        $data['expa_title'] = $this->accountmodel->expa_title($parient_id);
                        $this->load->view('temp/header');
                        $this->load->view('transection', $data);
                        $this->load->view('temp/footer');
                    }
            } 
            else 
            {
                $data['message'] = '<div class="alert alert-block alert-danger fade in">
                                    <button data-dismiss="alert" class="close" type="button"></button>
                                    <h4 class="alert-heading">' . lang('error') . '</h4> ' . lang('teac_9') . '
                            </div>';
                $data['income'] = $this->accountmodel->income($parient_id);
                $data['expanse'] = $this->accountmodel->expanse($parient_id);
                $data['inco_title'] = $this->accountmodel->inco_title($parient_id);
                $data['expa_title'] = $this->accountmodel->expa_title($parient_id);
                $this->load->view('temp/header');
                $this->load->view('transection', $data);
                $this->load->view('temp/footer');
            }
        } 
        elseif ($this->input->post('income', TRUE)) 
        {
            $account_id = $this->input->post('account_id', TRUE);
            $ac_nam = "";
            $this->db->select('account_title');
            $this->db->where('id', $account_id);
            $this->db->where('parient_id', $parient_id);
            $query1 = $this->db->get('account_title');
            foreach ($query1->result_array() as $row1) 
            {
                $ac_nam = $row1['account_title'];
            }
            $amount = $this->input->post('amount', TRUE);
            $pre_balence = $this->accountmodel->pre_balence($parient_id);
            $balence = $pre_balence + $amount;
                $inco_data = array(
                    'parient_id' => $this->db->escape_like_str($parient_id),
                    'date' => $this->db->escape_like_str($date),
                    'time' => $compTime,
                    'acco_id' => $this->db->escape_like_str($account_id),
                    'type' => $this->db->escape_like_str($ac_nam),
                    'category' => $this->db->escape_like_str('Income'),
                    'amount' => $this->db->escape_like_str($amount),
                    'balance' => $this->db->escape_like_str($balence)
                );
                if ($this->db->insert('transection', $inco_data)) 
                {
                    $data['message_2'] = '<div class="alert alert-block alert-success fade in">
                                            <button data-dismiss="alert" class="close" type="button"></button>
                                            <h4 class="alert-heading">' . lang('success') . ' </h4> ' . lang('teac_10') . '
                                    </div>';
                    $data['income'] = $this->accountmodel->income($parient_id);
                    $data['expanse'] = $this->accountmodel->expanse($parient_id);
                    $data['inco_title'] = $this->accountmodel->inco_title($parient_id);
                    $data['expa_title'] = $this->accountmodel->expa_title($parient_id);
                    $this->load->view('temp/header');
                    $this->load->view('transection', $data);
                    $this->load->view('temp/footer');
                }
            }
            else 
            {
            $data['income'] = $this->accountmodel->income($parient_id);
            $data['expanse'] = $this->accountmodel->expanse($parient_id);
            $data['inco_title'] = $this->accountmodel->inco_title($parient_id);
            $data['expa_title'] = $this->accountmodel->expa_title($parient_id);
            $this->load->view('temp/header');
            $this->load->view('transection', $data);
            $this->load->view('temp/footer');
        }

    }

    //This function will show expanse list by date range 
    public function exp_list_da_ra() {
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $rngstrt = strtotime($this->input->post('rngstrt', TRUE));
        $rngfin = strtotime($this->input->post('rngfin', TRUE));
                $this->db->select('*');
                $this->db->where('date >=', $rngstrt);
                $this->db->where('date <=', $rngfin);
                $this->db->where('category', 'Expense');
                $this->db->where('parient_id', $parient_id);
                $query = $this->db->get('transection')->result_array();
        // $query = $this->db->query("SELECT * FROM transection WHERE date >='$rngstrt' AND date <= '$rngfin' AND category='Expense'");
        $i = 1;
        
        foreach ($query as $row) {
         
            echo '<tr>
                    <td>
                        ' . $i . '
                    </td>
                    <td>
                        ' . date("d-m-Y", $row['date']) . '
                    </td>
                    <td>
                        ' . $this->accountmodel->acc_tit_id($row['acco_id']) . '
                    </td>
                    <td>
                        ' . $row['amount'] . '
                    </td>
                    <td>
                        ' . $row['balance'] . '
                    </td>
                </tr>';
            $i++;
        }
    }

    //This function will show expanse list by date range 
    public function inc_list_da_ra() {
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $rngstrt = strtotime($this->input->post('rngstrt', TRUE));
        $rngfin = strtotime($this->input->post('rngfin', TRUE));
        $this->db->select('*');
                $this->db->where('date >=', $rngstrt);
                $this->db->where('date <=', $rngfin);
                $this->db->where('category', 'Income');
                $this->db->where('parient_id', $parient_id);
                $query = $this->db->get('transection')->result_array();
        // $query = $this->db->query("SELECT * FROM transection WHERE date >='$rngstrt' AND date <= '$rngfin' AND category='Income'");
        $i = 1;
        foreach ($query as $row) {
            echo '<tr>
                    <td>
                        ' . $i . '
                    </td>
                    <td>
                        ' . date("d-m-Y", $row['date']) . '
                    </td>
                    <td>
                        ' . $this->accountmodel->acc_tit_id($row['acco_id']) . '
                    </td>
                    <td>
                        ' . $row['amount'] . '
                    </td>
                    <td>
                        ' . $row['balance'] . '
                    </td>
                </tr>';
            $i++;
        }
    }

    //This function will make students month end slip by auto calculation
    public function end_stu_calcu() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_accountant() && !$this->ion_auth->in_group(8) && !$this->ion_auth->is_admin())) {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $id = $parient_id ;
        $lib_fine = 0;
        $class_students = $this->accountmodel->all_student($parient_id);
        foreach ($class_students as $row) 
        {
            $class_id = $row['section'];
            $m_t_fee = $this->accountmodel->total_fee($class_id,$parient_id);
            if (!empty($m_t_fee)) 
            {
                foreach ($m_t_fee as $row1) 
                {
                    $item_id[] = $row1['id'];
                    $money[] = $row1['amount'];
                }
                $id_text = implode(",", $item_id);
                $amount = array_sum($money);
                //$all_student = $this->accountmodel->all_students($class_id,$parient_id);
                // foreach ($all_student as $row2) 
                // {
                    $student_id = ($row['student_id']);
                    $std_uid = $this->accountmodel->std_uid($student_id,$parient_id);
                    $lib_fine = $this->accountmodel->libFine($std_uid,$parient_id);
                    $scholarship = $this->accountmodel->scholarship1($std_uid,$parient_id);
                    $amount = $amount + $lib_fine;
                    $dues = $this->accountmodel->dues($student_id,$parient_id);
                    if ($dues != 0) 
                    {
                        $total = $amount + $dues;
                    }
                    else 
                    {
                        $total = $amount;
                    }
                    $advance = $this->accountmodel->advance($student_id,$parient_id);
                    $advance = $advance + $scholarship;
                    if($scholarship > '0')
                    {
                        /////////Income////////////////////
                        $pre_balence = $this->accountmodel->pre_balence($parient_id);
                        $income1 = $pre_balence + $scholarship;
                        $query123 = $this->db->query("SELECT time_zone FROM configuration WHERE parient_id = '$parient_id'");
                        $timezone = "";
                        foreach ($query123->result_array() as $row123) {
                            $timezone = $row123['time_zone'];
                        }
                        $datestring = "%h:%i %a";
                        $now = now();
                        $time = gmt_to_local($now, $timezone);
                        $compTime = mdate($datestring, $time);
                        $inco_data = array(
                            'date' => $this->db->escape_like_str(strtotime(date('d-m-Y'))),
                            'time' => $compTime,
                            'parient_id' => $this->db->escape_like_str($parient_id),
                            'category' => $this->db->escape_like_str('Income'),
                            'type' => $this->db->escape_like_str('Fee'),
                            'amount' => $this->db->escape_like_str($scholarship),
                            'balance' => $this->db->escape_like_str($income1)
                        );
                        $this->db->insert('transection', $inco_data);
                        /////////Expense////////////////////
                        $pre_balence1 = $this->accountmodel->pre_balence($parient_id);
                        $expense1 = $pre_balence1 - $scholarship;
                        $inco_data1 = array(
                            'date' => $this->db->escape_like_str(strtotime(date('d-m-Y'))),
                            'time' => $compTime,
                            'parient_id' => $this->db->escape_like_str($parient_id),
                            'category' => $this->db->escape_like_str('Expense'),
                            'type' => $this->db->escape_like_str('Scholarship'),
                            'amount' => $this->db->escape_like_str($scholarship),
                            'balance' => $this->db->escape_like_str($expense1)
                        );
                        $this->db->insert('transection', $inco_data1);
                    }
                    $status = 'Unpaid';
                    $paid = 0;
                    $balanec = 0;
                    if ($advance != 0) 
                    {
                        if ($total > $advance) 
                        {
                            $total -= $advance;
                        } 
                        elseif ($advance == $total) 
                        {
                            $balanec = 0;
                            $paid = $total;
                            $status = 'Paid';
                        } 
                        elseif ($total < $advance) 
                        {
                            $paid = $total;
                            $balanec = $advance - $total;
                            $status = 'Paid';
                        }
                    }
                    $data = array(
                        'year' => $this->db->escape_like_str(date('Y')),
                        'month' => $this->db->escape_like_str(date('F')),
                        'class_id' => $this->db->escape_like_str($class_id),
                        'student_id' => $this->db->escape_like_str($student_id),
                        'lib_fine' => $this->db->escape_like_str($lib_fine),
                        'item_id' => $this->db->escape_like_str($id_text),
                        'amount' => $this->db->escape_like_str($amount),
                        'dues' => $this->db->escape_like_str($dues),
                        'advance' => $this->db->escape_like_str($advance),
                        'total' => $this->db->escape_like_str($total),
                        'paid' => $this->db->escape_like_str($paid),
                        'balance' => $this->db->escape_like_str($balanec),
                        'status' => $this->db->escape_like_str($status),
                        'parient_id' => $this->db->escape_like_str($parient_id),
                    );
                    $this->db->insert('slip', $data);
                    $fine = array(
                        'fine' => 0
                    );
                    $this->db->where('user_id', $std_uid);
                    $this->db->update('library_member', $fine);
                //}
                $item_id = array();
                $money = array();
                $class_com = array(
                    'month_fee' => $this->db->escape_like_str(date('F'))
                );
                $this->db->where('id', $row['id']);
                $this->db->update('class_students', $class_com);
            }
        }
        if(empty($class_students))
        {
            $this->session->set_userdata('fee_success', '1');
            redirect('account/allSlips');
        }
        else
        {
            $this->session->set_userdata('fee_error', '1');
            redirect('account/allSlips');
        }        
    }

}
