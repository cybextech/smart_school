<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Admins extends MX_Controller {

    /**
     * This controller is using for control teachers work
     * 
     * Maps to the following URL
     * 		http://example.com/index.php/teachers
     * 	- or -  
     * 		http://example.com/index.php/teachers/<method_name>
     */
    function __construct() {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login');
        }
        $this->load->model('adminsmodel');
    }

    //This function gives all teacher's short informattion in a table view
    public function allAdmins() {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_super()) {
            redirect('auth', 'refresh');
        }
        if($this->session->userdata('email_info')){
            $data['email_info'] = $this->session->userdata('email_info');
        }else{
            $data['email_info'] = '';
        }
        $data['admins'] = $this->adminsmodel->alladmins();
        $this->load->view('temp/header');
        $this->load->view('admins', $data);
        $this->load->view('temp/footer');
    }

    //This function gives all details about any teacher
    public function adminDetails() {
         if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_super()) {
            redirect('auth', 'refresh');
        }
        $id = $this->input->get('id');
        $userId = $this->input->get('uid');
        $data['admins'] = $this->common->getWhere('userinfo', 'id', $id);
        $data['user'] = $this->common->getWhere('users', 'id', $userId);
        $this->load->view('temp/header');
        $this->load->view('adminsdetails', $data);
        $this->load->view('temp/footer');
    }
    //This function is using for editing a teacher informations
    //And admin an select group  
    function edit_admin() {
         if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_super()) {
            redirect('auth', 'refresh');
        }
        $userId = $this->input->get('uid');
        $adminId = $this->input->get('id');
        if ($this->input->post('submit', TRUE)) {
            $username = strtolower($this->input->post('first_name', TRUE)) . ' ' . strtolower($this->input->post('last_name', TRUE));
            $additional_data = array(
                'username' => $this->db->escape_like_str($username),
                'email' => $this->db->escape_like_str($this->input->post('email', TRUE)),
                'first_name' => $this->db->escape_like_str($this->input->post('first_name', TRUE)),
                'last_name' => $this->db->escape_like_str($this->input->post('last_name', TRUE)),
                'phone' => $this->db->escape_like_str($this->input->post('phone1', TRUE)),
            );
            $this->db->where('id', $userId);
            $this->db->update('users', $additional_data);
            $adminInfo = array(
                'full_name' => $this->db->escape_like_str($username),
                'phone' => $this->db->escape_like_str($this->input->post('phone1', TRUE)),
                'present_address' => $this->db->escape_like_str($this->input->post('present_address', TRUE)),
                'farther_name' => $this->db->escape_like_str($this->input->post('branch_name', TRUE))
            );
            $this->db->where('id', $adminId);
            $this->db->update('userinfo', $adminInfo);
            redirect('admins/allAdmins', 'refresh');
        } else {
            //get all data about this teacher from the "user" table
            $data['userInfo'] = $this->common->getWhere('users', 'id', $userId);
            $data['adminInfo'] = $this->common->getWhere('userinfo', 'id', $adminId);
            //get all groupe information and current group information to view file by "$data" array.
            $this->load->view('temp/header');
            $this->load->view('editAdmins', $data);
            $this->load->view('temp/footer');
        }
    }
    //This function is use for delete a teacher.
    public function adminDelete() {
         if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_super()) {
            redirect('auth', 'refresh');
        }
        $Id = $this->input->get('id');
        $userId = $this->input->get('uid');
        $this->db->delete('config_week_day', array('parient_id' => $userId));
        $this->db->delete('userinfo', array('id' => $Id));
        $this->db->delete('users', array('id' => $userId));
        $this->db->delete('users_groups', array('user_id' => $userId));
        $this->db->delete('role_based_access', array('user_id' => $userId));
        redirect('admins/allAdmins', 'refresh');
    }
}
