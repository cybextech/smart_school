<link href="assets/admin/pages/css/profile.css" rel="stylesheet" type="text/css"/>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo lang('header_admin_info'); ?> <small></small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo lang('home'); ?>
                    </li>
                    <li>
                        <?php echo lang('header_admin'); ?>
                    </li>
                    <li>
                        <?php echo lang('header_admin_info'); ?>
                    </li>
                    <li>
                        <?php echo lang('details'); ?>
                    </li>
                    <li id="result" class="pull-right topClock"></li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-3">
                <?php
                $photo = $this->input->get('photo');
                $adminID = $this->input->get('id');
                $userId = $this->input->get('uid');
                ?>
                <ul class="ver-inline-menu tabbable margin-bottom-10">
                    <li class="detailsPicture">
                        <img alt="" class="img-responsive" src="assets/uploads/<?php echo $photo; ?>">
                    </li>
                    <li>
                        <a href="javascript:history.back()">
                            <i class="fa fa-mail-reply-all"></i> <?php echo lang('back'); ?></a>

                    </li>
                </ul>
            </div>
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-8 profile-info datilsBodyMB">
                        <?php
                        foreach ($admins as $row) {
                            $fullname = $row['full_name'];
                            $position = "Admin";
                            $branch = $row['present_address'];
                            $photo = $row['users_photo'];
                            $phone = $row['phone'];
                            $branch_name = $row['farther_name'];
                            foreach ($user as $row1) {
                                $email = $row1['email'];
                            }
                            ?>

                            <h1 class="teacherTitleFont"><?php echo $fullname; ?></h1>
                            <div class="row">
                                <div class="col-sm-4 col-xs-6 detailsEvent">
                                    <span>: </span>
                                    <?php echo lang('admin_position'); ?>
                                </div>
                                <div class="col-sm-6 col-xs-6 detailsEvent">
                                    <?php echo $position; ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 col-xs-6 detailsEvent">
                                    <?php echo lang('tea_email'); ?>
                                    <span>: </span>
                                </div>
                                <div class="col-sm-6 col-xs-6 detailsEvent">
                                    <?php echo $email; ?>
                                </div>
                            </div>
                           
                            <div class="row">
                                <div class="col-sm-4 col-xs-6 detailsEvent">
                                    <?php echo lang('tea_pn'); ?>
                                    <span>: </span>
                                </div>
                                <div class="col-sm-6 col-xs-6 detailsEvent">
                                    <?php echo $phone; ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 col-xs-6 detailsEvent">
                                    <?php echo lang('admin_brn'); ?>
                                    <span>: </span>
                                </div>
                                <div class="col-sm-6 col-xs-6 detailsEvent">
                                    <?php echo $branch_name; ?>
                                </div>
                                
                            </div>
                            <div class="row">
                                <div class="col-sm-4 col-xs-6 detailsEvent">
                                    <?php echo lang('admin_bd'); ?>
                                    <span>: </span>
                                </div>
                                <div class="col-sm-6 col-xs-6 detailsEvent">
                                    <?php echo $branch; ?>
                                </div>
                                
                            </div>
                            
                        <?php } ?>
                    </div>
                    <!--end col-md-8-->
                    <div class="col-md-4">
                        <div class="portlet sale-summary">
                            <div class="portlet-title">
                                <div class="caption">
                                    <?php echo lang('admin_tede'); ?>
                                </div>
                                <div class="tools">
                                    <a href="javascript:;" class="reload">
                                    </a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <ul class="list-unstyled">
                                    <li>
                                        <div class="alert alert-success marginBottomNone">
                                            <strong><?php echo $position; ?></strong>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--end col-md-4-->
                </div>
                
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->