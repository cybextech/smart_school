<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class Adminsmodel extends CI_Model {
    /**
     * This model is using into the students controller
     * Load : $this->load->model('studentmodel');
     */
    function __construct() {
        parent::__construct();
        $this->load->dbforge();
    }

    //This functiion will return all teacher information
    public function alladmins() {
        $data = array();
        $this->db->select("info.*, users.email as email");
        $this->db->from("userinfo as info");
        $this->db->where('group_id', '1');
        $this->db->join('users', 'info.user_id = users.id', 'left');
        $query = $this->db->get();
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        return $data;
    }
}
