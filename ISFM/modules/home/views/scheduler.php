<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/bootstrap-datepicker/css/datepicker3.css"/>
<link href="assets/global/plugins/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet"/>
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN CONTAINER -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <?php if(!empty($message)){echo $message;}?>
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo lang('scheduler'); ?>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i><?php echo lang('home');?>
                        
                    </li>
                    <li><?php echo lang('header_event_calan'); ?>
                        
                    </li>
                    <li>
                        <?php echo lang('scheduler'); ?>
                    </li>
                    <li id="result" class="pull-right topClock"></li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <?php
        $user = $this->ion_auth->user()->row();
        $userId = $user->id;

        ?>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <?php 
                    if(!empty($success))
                    {
                        echo $success;
                    }
                ?>
                <!-- BEGIN PORTLET-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i><?php echo lang('make_schedule'); ?>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                            <a href="javascript:;" class="reload">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <?php $form_attributs = array('name'=>'myForm','class' => 'form-horizontal', 'role' => 'form','onsubmit'=>'return validateForm()');
                        echo form_open('home/scheduler', $form_attributs);
                        ?>
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo lang('cal_title'); ?>  <span class="requiredStar"> * </span></label>
                                    <div class="col-md-3">
                                        <input class="form-control" data-validation="required alphanumeric" data-validation-allowing=" " type="text" name="title" size="10" data-validation-error-msg="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo lang('cal_start_date'); ?> <span class="requiredStar"> * </span></label>
                                    <div class="col-md-5">
                                        <div class="input-group input-medium date date-picker" data-date="" data-date-format="dd-mm-yyyy"  data-date-start-date="+0d">
                                            <input type="text" class="form-control" name="start_date" readonly data-validation="required" data-validation-error-msg="<?php echo lang('cal_p_s_d'); ?>">
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                            </span>
                                        </div>
                                        <!-- /input-group -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo lang('cal_end_date'); ?> <span class="requiredStar"> * </span></label>
                                    <div class="col-md-5">
                                        <div class="input-group input-medium date date-picker" data-date="" data-date-format="dd-mm-yyyy"  data-date-start-date="+0d">
                                            <input type="text" class="form-control" name="end_date" readonly data-validation="required" data-validation-error-msg="<?php echo lang('cal_p_e_d'); ?>">
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                            </span>
                                        </div>
                                        <!-- /input-group -->
                                    </div>
                                </div>
                                <div class="form-group">
                                        <label class="col-md-3 control-label"><?php echo lang('prior'); ?></label>
                                        <div class="col-md-9">
                                                <div class="radio-list">
                                                        <label class="radio-inline e_yellow">
                                                        <input type="radio" name="color" id="optionsRadios25" value="yellow"> <?php echo lang('cal_yello'); ?> </label>
                                                        <label class="radio-inline e_green">
                                                        <input type="radio" name="color" id="optionsRadios26" value="green"> <?php echo lang('cal_green'); ?> </label>                                                        
                                                        <label class="radio-inline e_red">
                                                        <input type="radio" name="color" id="optionsRadios25" value="red"> <?php echo lang('cal_red'); ?> </label>
                                                        <label class="radio-inline e_gray">
                                                        <input type="radio" name="color" id="optionsRadios26" value="grey"> <?php echo lang('cal_grey'); ?> </label>
                                                        <label class="radio-inline e_purple">
                                                        <input type="radio" name="color" id="optionsRadios26" value="purple" > <?php echo lang('cal_purple'); ?> </label>
                                                        
                                                </div>
                                        </div>
                                </div>
                                <div class="form-actions fluid">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button class="btn blue" type="submit" name="submit" value="submit"><?php echo lang('appc_1'); ?></button>
                                        <button class="btn default" type="reset"><?php echo lang('cal_refresh'); ?></button>
                                    </div>
                                </div>
                            </div>
                        <?php echo form_close(); ?>
                        <!-- END FORM-->
                    </div>
                </div>
                <!-- END PORTLET-->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box green-meadow calendar">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i><?php echo lang('cal_calender'); ?>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div id="calendar" class="has-toolbar">
                                </div>
                            </div>
                        </div>
                        <!-- END CALENDAR PORTLET-->
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->
<script src="assets/global/plugins/fullcalendar/fullcalendar/fullcalendar.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="assets/admin/pages/scripts/components-pickers.js"></script>
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script src="assets/admin/pages/scripts/table-advanced.js"></script>
<script src="assets/global/plugins/jquery.form-validator.min.js" type="text/javascript"></script>
<script> $.validate();</script>
<script>

    jQuery(document).ready(function() {
            if (!jQuery().fullCalendar) {
                return;
            }
            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();

            var h = {};


            $('#calendar').fullCalendar('destroy'); // destroy the calendar
            $('#calendar').fullCalendar({ //re-initialize the calendar
                header: h,
                defaultView: 'month', // change default view with available options from http://arshaw.com/fullcalendar/docs/views/Available_Views/ 
                slotMinutes: 15,
                editable: true,
                droppable: true, // this allows things to be dropped onto the calendar !!!
                drop: function (date, allDay) { // this function is called when something is dropped

                    // retrieve the dropped element's stored Event Object
                    var originalEventObject = $(this).data('eventObject');
                    // we need to copy it, so that multiple events don't have a reference to the same object
                    var copiedEventObject = $.extend({}, originalEventObject);

                    // assign it the date that was reported
                    copiedEventObject.start = date;
                    copiedEventObject.allDay = allDay;
                    copiedEventObject.className = $(this).attr("data-class");

                    // render the event on the calendar
                    // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                    $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

                    // is the "remove after drop" checkbox checked?
                    if ($('#drop-remove').is(':checked')) {
                        // if so, remove the element from the "Draggable Events" list
                        $(this).remove();
                    }
                },
                events: [
                    <?php
                    foreach ($event as $eve){
                        $title = $eve['title'];
                        $star_date = explode("-",$eve['start_date']);
                        $s_d = $star_date[0];
                        $s_m = $star_date[1]-1;
                        $s_y = $star_date[2];
                        $end_date =  explode("-",$eve['end_date']);
                        $e_d = $end_date[0];
                        $e_m = $end_date[1]-1;
                        $e_y = $end_date[2];
                        $color = $eve['color'];
                        $url = $eve['url'];
                    echo '{title: "'.$eve['title'].'",
                        start: new Date('.$s_y.','.$s_m.','. $s_d.'),
                        end: new Date('.$e_y.','.$e_m.','. $e_d.'),
                        backgroundColor: Metronic.getBrandColor("'.$color.'"),
                        url: "'.$url.'",},';
                    }
                    ?>
                ]
            });
    });
</script>
<script>
    jQuery(document).ready(function() {
//here is auto reload after 1 second for time and date in the top
        jQuery(setInterval(function() {
            jQuery("#result").load("index.php/home/iceTime");
        }, 1000));
        
    if (jQuery().datepicker)
    {
        $('.date-picker').datepicker({
            rtl: Metronic.isRTL(),
            orientation: "left",
            autoclose: true
        });
    }
    
    });
    
</script>
