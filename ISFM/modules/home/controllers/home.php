<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends MX_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/home
     * 	- or -  
     * 		http://example.com/index.php/home/index
     */
    function __construct() {
        parent::__construct();
        $this->load->model('common');
        $this->load->model('homeModel');
        $this->load->helper('file');
        $this->load->helper('form');
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login');
        }
    }

    //This function will show the users dashboard
    public function index() {
        $user = $this->ion_auth->user()->row();
        $id = $user->id;
        $my_id = $id;
        if ($this->ion_auth->is_teacher())
        {
            $id = $this->session->userdata("parient_id");
            $data['notice'] = $this->common->getTeacherNotice($id);
        }
        else if ($this->ion_auth->is_accountant())
        {
            $id = $this->session->userdata("parient_id");
            $data['notice'] = $this->common->getEANotice($id);
        }
        else if($this->ion_auth->is_parents() || $this->ion_auth->is_student())
        {
            $id = $this->session->userdata("parient_id");
            $data['notice'] = $this->common->getStudentNotice($id);
            
        }
        else if(!$this->ion_auth->is_admin() && !$this->ion_auth->is_super())
        {
            $id = $this->session->userdata("parient_id");
            $data['notice'] = $this->common->getEANotice($id);
        }
        else
        {
            $data['notice'] = $this->common->getAllData('notice_board',$id);
        }
        if($this->ion_auth->is_super())
        {
            $data['massage'] = $this->common->getWhere22('massage', 'receiver_id', $my_id, 'receiver_delete', 1);
            $data['totalStudent'] = $this->common->totalStudent1();
            $data['totalTeacher'] = $this->common->totalTeacher1();
            $data['totalStaff'] = $this->common->getWhere('users','user_status','Employee');
            $data['totalAttendStudent'] = $this->common->totalAttendStudent1();
            $data['allAdmins'] = $this->common->getWhere22('userinfo','parient_id',$id,'group_id','1');
        }
        else
        {
            $data['massage'] = $this->common->getWhere22('massage', 'receiver_id', $my_id, 'receiver_delete', 1);
            $data['totalStudent'] = $this->common->totalStudent($id);
            $data['totalTeacher'] = $this->common->totalTeacher($id);
            $data['totalStaff'] = $this->common->getWhere22('users','parient_id',$id,'user_status','Employee');
            $data['totalAttendStudent'] = $this->common->totalAttendStudent($id);
            $data['teacherAttendance'] = $this->common->teacherAttendance($id);
            $data['presentEmploy'] = $this->homeModel->presentEmploy($id);
            $data['absentEmploy'] = $this->homeModel->absentEmploy($id);
            $data['leaveEmploy'] = $this->homeModel->leaveEmploy($id);
            $data['event'] = $this->homeModel->all_event($id);
            $data['classAttendance'] = $this->homeModel->atten_chart($id);
            $data['classInfo'] = $this->common->classInfo($id);
        }
        if ($this->ion_auth->is_student()) 
        {
            $query = $this->common->getWhere('class_students', 'user_id', $my_id);
            $class_id = "";
            foreach ($query as $row) {
                $class_id = $row['section'];
            }
            $data['class_id'] = $class_id;
            $data['day'] = $this->common->getAllData('config_week_day',$id);
            $data['subject'] = $this->common->getWhere22('class_subject', 'section_id', $class_id,'parient_id',$id);
            $data['teacher'] = $this->common->getAllData('teachers_info',$id);
        }
        $this->load->view('temp/header', $data);
        $this->load->view('dashboard', $data);
        $this->load->view('temp/footer');
    }

     public function branch() {
        $my_id = $this->session->userdata('user_id');
        $id = $this->input->post('admins', TRUE);
        $data['notice'] = $this->common->getAllData('notice_board',$id);
        $data['admin_id'] = $id;
        $data['totalStaff'] = $this->common->getWhere22('users','parient_id',$id,'user_status','Employee');
        $data['massage'] = $this->common->getWhere22('massage', 'receiver_id', $my_id, 'receiver_delete', 1);
        $data['totalStudent'] = $this->common->totalStudent($id);
        $data['totalTeacher'] = $this->common->totalTeacher($id);
        $data['totalParents'] = $this->common->totalParents($id);
        $data['totalAttendStudent'] = $this->common->totalAttendStudent($id);
        $data['teacherAttendance'] = $this->common->teacherAttendance($id);
        $data['presentEmploy'] = $this->homeModel->presentEmploy($id);
        $data['absentEmploy'] = $this->homeModel->absentEmploy($id);
        $data['leaveEmploy'] = $this->homeModel->leaveEmploy($id);
        $data['event'] = $this->homeModel->all_event($id);
        $data['classAttendance'] = $this->homeModel->atten_chart($id);
        $data['classInfo'] = $this->common->classInfo($id);
        $this->load->view('temp/header', $data);
        $this->load->view('superDashboard', $data);
        $this->load->view('temp/footer');
        
    }

    public function profileView() {
        $user = $this->ion_auth->user()->row();
        $data['userprofile'] = $this->common->getWhere('users', 'id', $user->id);
        if ($this->input->post('submit', TRUE)) {
            $data_up = array(
                'first_name' => $this->db->escape_like_str($this->input->post('firstName', TRUE)),
                'last_name' => $this->db->escape_like_str($this->input->post('lastName', TRUE)),
                'username' => $this->db->escape_like_str($this->input->post('userName', TRUE)),
                'phone' => $this->db->escape_like_str($this->input->post('mobileNumber', TRUE)),
                'email' => $this->db->escape_like_str($this->input->post('email', TRUE)),
            );
            $this->db->where('id', $user->id);
            if ($this->db->update('users', $data_up)) {
                redirect('home/profileView', 'refresh');
            }
        } else {
            $this->load->view('temp/header');
            $this->load->view('profileView', $data);
            $this->load->view('temp/footer');
        }
    }

    public function profileImage() {
        $user = $this->ion_auth->user()->row();
        if ($this->ion_auth->is_admin()) {
            if (empty($user->profile_image)) {
                //Here is uploading the student's photo.
                $config['upload_path'] = './assets/uploads/';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = '10000';
                $config['max_width'] = '10240';
                $config['max_height'] = '7680';
                $config['encrypt_name'] = TRUE;
                $this->load->library('upload', $config);
                $this->upload->do_upload();
                $uploadFileInfo = $this->upload->data();
                $data_update = array(
                    'profile_image' => $this->db->escape_like_str($uploadFileInfo['file_name'])
                );
                $this->db->where('id', $user->id);
                if ($this->db->update('users', $data_update)) {
                    redirect('home/profileView', 'refresh');
                }
            } else {
                $path = 'assets/uploads/' . $user->profile_image;
                //$userprofile = $this->common->getWhere('users', 'id',$user->id);
                if (unlink($path)) {
                    //Here is uploading the student's photo.
                    $config['upload_path'] = './assets/uploads/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = '10000';
                    $config['max_width'] = '10240';
                    $config['max_height'] = '7680';
                    $config['encrypt_name'] = TRUE;

                    $this->load->library('upload', $config);
                    $this->upload->do_upload();
                    $uploadFileInfo = $this->upload->data();
                    $data_update = array(
                        'profile_image' => $this->db->escape_like_str($uploadFileInfo['file_name'])
                    );

                    $this->db->where('id', $user->id);
                    if ($this->db->update('users', $data_update)) {
                        redirect('home/profileView', 'refresh');
                    }
                } else {
                    echo lang('desc_1');
                }
            }
        } elseif ($this->ion_auth->is_teacher()) {
            if (empty($user->profile_image)) {
                //Here is uploading the student's photo.
                $config['upload_path'] = './assets/uploads/';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = '10000';
                $config['max_width'] = '10240';
                $config['max_height'] = '7680';
                $config['encrypt_name'] = TRUE;
                $this->load->library('upload', $config);
                $this->upload->do_upload();
                $uploadFileInfo = $this->upload->data();
                $data_update = array(
                    'profile_image' => $this->db->escape_like_str($uploadFileInfo['file_name'])
                );
                $this->db->where('id', $user->id);
                if ($this->db->update('users', $data_update)) {
                    redirect('home/profileView', 'refresh');
                }
            } else {
                $path = 'assets/uploads/' . $user->profile_image;
                //$userprofile = $this->common->getWhere('users', 'id',$user->id);
                if (unlink($path)) {
                    //Here is uploading the student's photo.
                    $config['upload_path'] = './assets/uploads/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = '10000';
                    $config['max_width'] = '10240';
                    $config['max_height'] = '7680';
                    $config['encrypt_name'] = TRUE;

                    $this->load->library('upload', $config);
                    $this->upload->do_upload();
                    $uploadFileInfo = $this->upload->data();
                    $data_update = array(
                        'profile_image' => $this->db->escape_like_str($uploadFileInfo['file_name'])
                    );
                    $this->db->where('id', $user->id);
                    if ($this->db->update('users', $data_update)) {
                        $data_update_2 = array(
                            'teachers_photo' => $this->db->escape_like_str($uploadFileInfo['file_name'])
                        );
                        $this->db->where('user_id', $user->id);
                        if ($this->db->update('teachers_info', $data_update_2)) {
                            redirect('home/profileView', 'refresh');
                        }
                    }
                } else {
                    echo lang('desc_1');
                }
            }
        } elseif ($this->ion_auth->is_student()) {
            if (empty($user->profile_image)) {
                //Here is uploading the student's photo.
                $config['upload_path'] = './assets/uploads/';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = '10000';
                $config['max_width'] = '10240';
                $config['max_height'] = '7680';
                $config['encrypt_name'] = TRUE;
                $this->load->library('upload', $config);
                $this->upload->do_upload();
                $uploadFileInfo = $this->upload->data();
                $data_update = array(
                    'profile_image' => $this->db->escape_like_str($uploadFileInfo['file_name'])
                );
                $this->db->where('id', $user->id);
                if ($this->db->update('users', $data_update)) {
                    redirect('home/profileView', 'refresh');
                }
            } else {
                $path = 'assets/uploads/' . $user->profile_image;
                //$userprofile = $this->common->getWhere('users', 'id',$user->id);
                if (unlink($path)) {
                    //Here is uploading the student's photo.
                    $config['upload_path'] = './assets/uploads/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = '10000';
                    $config['max_width'] = '10240';
                    $config['max_height'] = '7680';
                    $config['encrypt_name'] = TRUE;
                    $this->load->library('upload', $config);
                    $this->upload->do_upload();
                    $uploadFileInfo = $this->upload->data();
                    $data_update = array(
                        'profile_image' => $this->db->escape_like_str($uploadFileInfo['file_name'])
                    );
                    $this->db->where('id', $user->id);
                    if ($this->db->update('users', $data_update)) {
                        $data_update_3 = array(
                            'student_photo' => $this->db->escape_like_str($uploadFileInfo['file_name'])
                        );
                        $this->db->where('user_id', $user->id);
                        if ($this->db->update('student_info', $data_update_3)) {
                            redirect('home/profileView', 'refresh');
                        }
                    }
                } else {
                    echo lang('desc_1');
                }
            }
        } elseif ($this->ion_auth->is_parents()) {
            if (empty($user->profile_image)) {
                //Here is uploading the student's photo.
                $config['upload_path'] = './assets/uploads/';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = '10000';
                $config['max_width'] = '10240';
                $config['max_height'] = '7680';
                $config['encrypt_name'] = TRUE;
                $this->load->library('upload', $config);
                $this->upload->do_upload();
                $uploadFileInfo = $this->upload->data();
                $data_update = array(
                    'profile_image' => $this->db->escape_like_str($uploadFileInfo['file_name'])
                );
                $this->db->where('id', $user->id);
                if ($this->db->update('users', $data_update)) {
                    redirect('home/profileView', 'refresh');
                }
            } else {
                $path = 'assets/uploads/' . $user->profile_image;
                //$userprofile = $this->common->getWhere('users', 'id',$user->id);
                if (unlink($path)) {
                    //Here is uploading the student's photo.
                    $config['upload_path'] = './assets/uploads/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = '10000';
                    $config['max_width'] = '10240';
                    $config['max_height'] = '7680';
                    $config['encrypt_name'] = TRUE;
                    $this->load->library('upload', $config);
                    $this->upload->do_upload();
                    $uploadFileInfo = $this->upload->data();
                    $data_update = array(
                        'profile_image' => $this->db->escape_like_str($uploadFileInfo['file_name'])
                    );
                    $this->db->where('id', $user->id);
                    if ($this->db->update('users', $data_update)) {
                        redirect('home/profileView', 'refresh');
                    }
                } else {
                    echo lang('desc_1');
                }
            }
        }
    }

    //Thid function will show the calender with event
    public function calender() {
        $user = $this->ion_auth->user()->row();
        $userId = $user->id;
        $parient_id = $userId;
        // var_dump($parient_id);
        // die;
        if ($this->ion_auth->is_student() || $this->ion_auth->is_parents() || $this->ion_auth->is_teacher()) 
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        if ($this->input->post('submit', TRUE)) {
            $title = $this->input->post('title', TRUE);
            $start_date = $this->input->post('start_date', TRUE);
            $end_date = $this->input->post('end_date', TRUE);
            $color = $this->input->post('color', TRUE);
            $url = $this->input->post('url', TRUE);
            $event_info = array(
                'title' => $this->db->escape_like_str($title),
                'start_date' => $this->db->escape_like_str($start_date),
                'end_date' => $this->db->escape_like_str($end_date),
                'color' => $this->db->escape_like_str($color),
                'url' => $this->db->escape_like_str($url),
                'user_id' => $userId
            );
            if ($this->db->insert('calender_events', $event_info)) {
                redirect('home/calender', 'refresh');
            }
        } else {
            $data['event'] = $this->homeModel->all_event($parient_id);
            $this->load->view('temp/header');
            $this->load->view('calender', $data);
            $this->load->view('temp/footer');
        }
    }

    public function addEvent() {
        $user = $this->ion_auth->user()->row();
        $userId = $user->id;
        $parient_id = $userId;
        if (!$this->ion_auth->is_admin()) 
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        if ($this->input->post('submit', TRUE)) {
            $title = $this->input->post('title', TRUE);
            $start_date = $this->input->post('start_date', TRUE);
            $end_date = $this->input->post('end_date', TRUE);
            if(strtotime($start_date) >  strtotime($end_date))
            {
                $data['success'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                                                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                                        <strong>Error!</strong> Sorry Wrong date entry. Ending date must be greater then starting date
                                                                </div>';
                $data['id'] = $userId;
                $data['event'] = $this->homeModel->all_event($parient_id);
                $this->load->view('temp/header');
                $this->load->view('events', $data);
                $this->load->view('temp/footer');   
            }
            else
            {
                $color = $this->input->post('color', TRUE);
                $url = $this->input->post('url', TRUE);
                $user = $this->ion_auth->user()->row();
                $userId = $user->id;
                $event_info = array(
                    'title' => $this->db->escape_like_str($title),
                    'start_date' => $this->db->escape_like_str($start_date),
                    'end_date' => $this->db->escape_like_str($end_date),
                    'color' => $this->db->escape_like_str($color),
                    'url' => $this->db->escape_like_str($url),
                    'user_id' => $this->db->escape_like_str($userId)
                );
                if ($this->db->insert('calender_events', $event_info)) {
                    redirect('home/addEvent', 'refresh');
                }
            }
            
        } else {
            $data['id'] = $userId;
            $data['event'] = $this->homeModel->all_event($parient_id);
            $this->load->view('temp/header');
            $this->load->view('events', $data);
            $this->load->view('temp/footer');
        }
    }

    //This function will edit events information
    public function edit_event() {
        if ($this->input->post('submit', TRUE)) {
            $eve_id = $this->input->post('eve_id', TRUE);
            $title = $this->input->post('title', TRUE);
            $start_date = $this->input->post('start_date', TRUE);
            $end_date = $this->input->post('end_date', TRUE);
            $color = $this->input->post('color', TRUE);
            $url = $this->input->post('url', TRUE);
            $user = $this->ion_auth->user()->row();
            $userId = $user->id;
            $event_info = array(
                'title' => $this->db->escape_like_str($title),
                'start_date' => $this->db->escape_like_str($start_date),
                'end_date' => $this->db->escape_like_str($end_date),
                'color' => $this->db->escape_like_str($color),
                'url' => $this->db->escape_like_str($url),
                'user_id' => $this->db->escape_like_str($userId)
            );
            $this->db->where('id', $eve_id);
            if ($this->db->update('calender_events', $event_info)) {
                redirect('home/addEvent', 'refresh');
            }
        } else {
            $event_id = $this->input->get('eve_id');
            $data['event'] = $this->homeModel->single_event($event_id);
            $this->load->view('temp/header');
            $this->load->view('edit_event', $data);
            $this->load->view('temp/footer');
        }
    }

    public function iceTime() {
        $time = $this->common->iceTime();
    }
    //This function will delete clender event
    public function delete_event() {
        $id = $this->input->get('eve_id');
        $parient_id = $this->session->userdata('user_id');
        if (!$this->ion_auth->is_admin()) 
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        if ($this->db->delete('calender_events', array('id' => $id))) {
            $data['event'] = $this->homeModel->all_event($parient_id);
            $data['message'] = '<div class="alert alert-success alert-dismissable">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
								<strong>Success!</strong> The event was deleted successfully.
							</div>';
            $this->load->view('temp/header');
            $this->load->view('events', $data);
            $this->load->view('temp/footer');
        }
    }

    public function scheduler() {
        $user = $this->ion_auth->user()->row();
        $userId = $user->id;
        $parient_id = $this->session->userdata('user_id');
        if (!$this->ion_auth->is_admin()) 
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        if ($this->input->post('submit', TRUE)) {
            $title = $this->input->post('title', TRUE);
            $start_date = $this->input->post('start_date', TRUE);
            $end_date = $this->input->post('end_date', TRUE);
            if(strtotime($start_date) >  strtotime($end_date))
            {
                $data['success'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                                                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                                        <strong>Error!</strong> Sorry Wrong date entry. Ending date must be greater then starting date
                                                                </div>';
                $data['id'] = $userId;
                $data['event'] = $this->homeModel->schedul($userId);
                $this->load->view('temp/header');
                $this->load->view('events', $data);
                $this->load->view('temp/footer');   
            }
            else
            {
                $color = $this->input->post('color', TRUE);
                $user = $this->ion_auth->user()->row();
                $userId = $user->id;
                $event_info = array(
                    'title' => $this->db->escape_like_str($title),
                    'start_date' => $this->db->escape_like_str($start_date),
                    'end_date' => $this->db->escape_like_str($end_date),
                    'color' => $this->db->escape_like_str($color),
                    'user_id' => $this->db->escape_like_str($userId)
                );
                if ($this->db->insert('scheduler', $event_info)) {
                    redirect('home/scheduler', 'refresh');
                }
            }
            
        } else {
            $data['id'] = $userId;
            $data['event'] = $this->homeModel->schedul($userId);
            $this->load->view('temp/header');
            $this->load->view('scheduler', $data);
            $this->load->view('temp/footer');
        }
    }
}
