<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class HomeModel extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
    }

    //This function will daily present employe info.
    public function presentEmploy($id="") {
        $data = array();
        $date = strtotime(date('d-m-Y'));
        $query = $this->db->query("SELECT id FROM teacher_attendance WHERE date='$date' AND present_or_absent='1' AND parient_id='$id'");
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }return count($data);
    }

    //This function will daily present employe info.
    public function absentEmploy($id="") {
        $data = array();
        $date = strtotime(date('d-m-Y'));
        $query = $this->db->query("SELECT id FROM teacher_attendance WHERE date='$date' AND present_or_absent='0' AND parient_id='$id'");
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }return count($data);
    }

    //This function will daily present employe info.
    public function leaveEmploy($id="") {
        $data = array();
        $date = strtotime(date('d-m-Y'));
        $query = $this->db->query("SELECT id FROM users WHERE user_status='Employ' AND leave_status='Leave' AND leave_start<='$date' AND leave_end>='$date' AND parient_id='$id'");
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }return count($data);
    }

    //This function will show daily attendance percentise
    public function atten_chart($id="") {
        $data = array();
        $query = $this->db->query("SELECT class_title,section,attendance_percentices_daily FROM class WHERE parient_id = '$id'");
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        return $data;
    }

    //This function will return all events
    public function all_event($userId) {
        $data = array();
        $query = $this->db->query("SELECT * FROM calender_events WHERE user_id='$userId' ORDER BY start_date");
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        return $data;
    }

    //This function will return all schedule events
    public function schedul($userId) {
        $data = array();
        $query = $this->db->query("SELECT * FROM scheduler WHERE user_id='$userId' ORDER BY start_date");
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        return $data;
    }


    //This function will return single events
    public function single_event($eve_id) {
        $data = array();
        $query = $this->db->query("SELECT * FROM calender_events WHERE id='$eve_id'");
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        return $data;
    }
}
