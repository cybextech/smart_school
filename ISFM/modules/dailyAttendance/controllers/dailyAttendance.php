<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class DailyAttendance extends MX_Controller {
//    private $input;

    /**
     * This is DailyAttendance Controller in Class Module.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/dailyAttendance
     * 	- or -  
     * 		http://example.com/index.php/dailyAttendance/index
     */
    function __construct() {
        parent::__construct();
        $this->load->model('attendancemodule');
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login');
        }
    }

    //This function show the class & section for selecting class to take attendance 
    public function selectClassAttendance() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_teacher() && !$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8))) {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        if ($this->ion_auth->is_teacher()) 
        {
            $my_id = $this->session->userdata('user_id');
            $t_id = "";
            $this->db->where('user_id',$my_id);
            $this->db->select('id');
            $res1 = $this->db->get('teachers_info');
            foreach ($res1->result_array() as $key) {
                $t_id = $key['id'];
            }
            $data['s_class'] = $this->common->getWhere22('class','parient_id',$parient_id,'incharge',$t_id);
        }
        else
        {
            $data['s_class'] = $this->common->getAllData('class',$parient_id);
        }
        $this->load->view('temp/header');
        $this->load->view('selectClassAttendance', $data);
        $this->load->view('temp/footer');
    }

    //This function is used for take attendence to class students
    public function attendance() {
         if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_teacher() && !$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8))) {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if (!$this->ion_auth->is_admin()) 
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        if ($this->input->post('submit', TRUE)) {
            //Whene submit the attendence information after takeing the attendence
            $i = $this->input->post('in_velu', TRUE);
            $day = date("d-m-Y");
            $date = strtotime($day);
            $acad_year = $this->session->userdata('s_from')."-".$this->session->userdata('s_to');
            $classTitle = $this->input->post('classTitle', TRUE);;
            for ($x = 1; $x <= $i; $x++) {
                $roll = $this->input->post("roll_$x", TRUE);
                $name = $this->input->post("atudentName_$x", TRUE);
                $present = "";
                if ($this->input->post("action_$x", TRUE)) {
                    if ($this->input->post("action_$x", TRUE) === 'P') {
                        $present = "P";
                    } else {
                        $present = "A";
                    }
                }
                $userId = $this->input->post("userId_$x", TRUE);
                $studentInfoId = $this->input->post("studentInfoId_$x", TRUE);
                $section = $this->input->post("section_$x", TRUE);
                $data = array(
                    'date' => $this->db->escape_like_str($date),
                    'user_id' => $this->db->escape_like_str($userId),
                    'student_id' => $this->db->escape_like_str($studentInfoId),
                    'class_title' => $this->db->escape_like_str($classTitle),
                    'present_or_absent' => $this->db->escape_like_str($present),
                    'section' => $this->db->escape_like_str($section),
                    'roll_no' => $this->db->escape_like_str($roll),
                    'student_title' => $this->db->escape_like_str($name),
                    'parient_id' => $this->db->escape_like_str($parient_id),
                    'academic_year' => $acad_year
                );
                //insert the $data information into "daily_attendance" database.
                $this->db->insert('daily_attendance', $data);
                //Take class and attend amount monthly and make the attendence percintise monthly 
                $classAmountMonthly = $this->attendancemodule->classAmountMonthly($studentInfoId,$parient_id);
                if ($this->input->post("action_$x", TRUE) === 'P') {
                    $attendAmountMonthly = $this->attendancemodule->attendAmountMonthly($studentInfoId,$parient_id);
                } else {
                    $previousAttendAmountM = $this->attendancemodule->attendAmountMonthly($studentInfoId,$parient_id);
                    $todayAttendAmountM = 1;
                    $attendAmountMonthly = $previousAttendAmountM - $todayAttendAmountM;
                }
                $attendencePercenticeMonthly = $this->attendancemodule->attendPercentiseMonthly($attendAmountMonthly, $classAmountMonthly);
                //Take class and attend amount yearly and make the attendence percintise yearly 
                $classAmountYearly = $this->attendancemodule->classAmountYearly($studentInfoId,$parient_id);
                if ($this->input->post("action_$x", TRUE) === 'P') {
                    $attendAmountYearly = $this->attendancemodule->attendAmountYearly($studentInfoId,$parient_id);
                } else {
                    $previousAttendAmountY = $this->attendancemodule->attendAmountYearly($studentInfoId,$parient_id);
                    $todayAttendAmountY = 1;
                    $attendAmountYearly = $previousAttendAmountY - $todayAttendAmountY;
                }
                $attendencePercenticeYearly = $this->attendancemodule->attendPercentiseYearly($attendAmountYearly, $classAmountYearly);
                $data_1 = array(
                    'class_amount_monthly' => $this->db->escape_like_str($classAmountMonthly),
                    'class_amount_yearly' => $this->db->escape_like_str($classAmountYearly),
                    'attend_amount_monthly' => $this->db->escape_like_str($attendAmountMonthly),
                    'attend_amount_yearly' => $this->db->escape_like_str($attendAmountYearly),
                    'percentise_month' => $this->db->escape_like_str($attendencePercenticeMonthly),
                    'percentise_year' => $this->db->escape_like_str($attendencePercenticeYearly),
                );
                $this->db->update('daily_attendance', $data_1, array('student_id' => $studentInfoId, 'date' => $date));
                $data_2 = array(
                    'attendance_percentices_daily' => $this->db->escape_like_str($attendencePercenticeMonthly)
                );
                $this->db->update('class_students', $data_2, array('student_id' => $studentInfoId, 'section' => $classTitle));
            }
            $dailyClassAttendencePercentise = $this->attendancemodule->allStudentsDailyAttendence($date, $classTitle);
            $yearClassAttendencePercentise = $this->attendancemodule->allStudentsYearlyAttendence($date, $classTitle);
            $data_3 = array(
                'attendance_percentices_daily' => $this->db->escape_like_str($dailyClassAttendencePercentise),
                'attend_percentise_yearly' => $this->db->escape_like_str($yearClassAttendencePercentise)
            );
            $this->db->where('id', $classTitle);
            $this->db->where('parient_id', $parient_id);
            $this->db->update('class', $data_3);
            redirect('dailyAttendance/attendanceCompleteMessage', 'refresh');
        } else {
            //Load attendence view before takeing attendence with class,All section and specific class section
            $classTitle = $this->input->post('class_title', TRUE);
            $queryData = array();
            $query = $this->db->get_where('class_students', array('section' => $classTitle,'parient_id' => $parient_id));
            foreach ($query->result_array() as $row) {
                $queryData[] = $row;
            }
            $data['students'] = $queryData;
            if (!empty($data['students'])) {
                $this->load->view('temp/header');
                $this->load->view('attendance', $data);
                $this->load->view('temp/footer');
            } else {
                $data['success'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                                                                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                                    <strong>Error!</strong> Sorry there is no student in this class
                                                            </div>';
                    $data['s_class'] = $this->common->getAllData('class',$parient_id);
                    $this->load->view('temp/header');
                    $this->load->view('selectClassAttendance', $data);
                    $this->load->view('temp/footer');
            }
            
        }
    }

    //This function send a message that Attendance Was completed.
    //And gives two link for re-check and can edit the attendance.
    public function attendanceCompleteMessage() {
         if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_teacher() && !$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8))) {
            redirect('auth', 'refresh');
        }
        $this->load->view('temp/header');
        $this->load->view('attendenceCompleateMessage');
        $this->load->view('temp/footer');
    }

    //This function is used for filtering to get students information(which class and which section if the section in that class)
    public function ajaxClassAttendanceSection() {
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $classTitle = $this->input->get('q');
        $query = $this->common->getWhere22('class', 'class_title', $classTitle,'parient_id',$parient_id);
        foreach ($query as $row) {
            $data = $row;
        }
        echo '<input type="hidden" name="class" value="' . $classTitle . '">';
        if (!empty($data['section'])) {
            $section = $data['section'];
            $sectionArray = explode(",", $section);
            echo '<div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-4">
                            <select name="section" class="form-control">
                                <option value="all">' . lang('attc_1') . '</option>';
            foreach ($sectionArray as $sec) {
                echo '<option value="' . $sec . '">' . $sec . '</option>';
            }
            echo '</select></div>
                    </div>';
        } else {
            $section = 'This class has no section.';
            echo '<div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-6">
                        <div class="alert alert-warning">
                                <strong>Info!</strong> ' . $section . '
                        </div></div></div>';
        }
    }

    public function selectAttendancePreview() {
         if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_teacher() && !$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8))) {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        if ($this->ion_auth->is_teacher()) 
        {
            $my_id = $this->session->userdata('user_id');
            $t_id = "";
            $this->db->where('user_id',$my_id);
            $this->db->select('id');
            $res1 = $this->db->get('teachers_info');
            foreach ($res1->result_array() as $key) {
                $t_id = $key['id'];
            }
            $data['s_class'] = $this->common->getWhere22('class','parient_id',$parient_id,'incharge',$t_id);
        }
        else
        {
            $data['s_class'] = $this->common->getAllData('class',$parient_id);
        }
        $this->load->view('temp/header');
        $this->load->view('selectAttendencePreview', $data);
        $this->load->view('temp/footer');
    }

    public function attendencePreview() {
        //Load attendence view before takeing attendence with class,All section and specific class section
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_teacher() && !$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8))) {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        if ($this->ion_auth->is_teacher()) 
        {
            $my_id = $this->session->userdata('user_id');
            $t_id = "";
            $this->db->where('user_id',$my_id);
            $this->db->select('id');
            $res1 = $this->db->get('teachers_info');
            foreach ($res1->result_array() as $key) {
                $t_id = $key['id'];
            }
            $data['s_class'] = $this->common->getWhere22('class','parient_id',$parient_id,'incharge',$t_id);
        }
        else
        {
            $data['s_class'] = $this->common->getAllData('class',$parient_id);
        }
        $classTitle = $this->input->post('class_title', TRUE);
        $date = $this->input->post('date', TRUE);
        $intDate = strtotime($date);
        $queryData = array();
        $query = $this->db->get_where('daily_attendance', array('class_title' => $classTitle, 'date' => $intDate));
        foreach ($query->result_array() as $row) {
            $queryData[] = $row;
        }
        $data['students'] = $queryData;
        if (!empty($data['students'])) {
            $data['clas'] = $classTitle;
            $this->load->view('temp/header');
            $this->load->view('attendencePreview', $data);
            $this->load->view('temp/footer');
        } else {
           $data['success'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                            '.lang('clasc_10').'Sorry No Record Found!
                                    </div>';
                $this->load->view('temp/header');
                $this->load->view('selectAttendencePreview', $data);
                $this->load->view('temp/footer');
        }
    }

    //This function send class section to view by ajax. 
    public function ajaxAttendencePreview() {
        $classTitle = $this->input->get('q', TRUE);
        $query = $this->common->getWhere('class', 'class_title', $classTitle);
        foreach ($query as $row) {
            $data = $row;
        }
        echo '<input type="hidden" name="class" value="' . $classTitle . '">';
        if (!empty($data['section'])) {
            $section = $data['section'];
            $sectionArray = explode(",", $section);
            echo '<div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-4">
                            <select name="section" class="form-control">
                                <option value="all">' . lang('attc_1') . '</option>';
            foreach ($sectionArray as $sec) {
                echo '<option value="' . $sec . '">' . $sec . '</option>';
            }
            echo '</select></div>
                    </div>';
        } else {
            $section = 'This class has no section.';
            echo '<div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-6">
                        <div class="alert alert-warning">
                                <strong>' . lang('attc_3') . ' </strong> ' . $section . '
                        </div></div></div>';
        }
    }

    //This function can edit and update the related table's info.
    public function editAttendance() {
         if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_teacher() && !$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8))) {
            redirect('auth', 'refresh');
        }
        $id = $this->input->get('ghtnidjdfjkid', TRUE);
        $day = date("m/d/y");
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $date = strtotime($day);
        if ($this->input->post('submit', TRUE)) {
            $studentInfoId = $this->input->post('studentInfoId', TRUE);
            $pre_status = $this->input->post('status', TRUE);
            $classTitle = $this->input->post('classTitle', TRUE);
            $present = "";
            if ($this->input->post("action", TRUE)) {
                if ($this->input->post("action", TRUE) == 'P') {
                    $present = "P";
                } else {
                    $present = "A";
                }
            }
            if($pre_status != $present)
            {
                //Take class and attend amount monthly and make the attendence percintise monthly 
                $classAmountMonthly = $this->attendancemodule->classAmountMonthly($studentInfoId,$parient_id);
                $classAmountMonthly = $classAmountMonthly - 1;
                if ($this->input->post("action", TRUE) == 'P') {
                    $previousAttendAmountM = $this->attendancemodule->attendAmountMonthly($studentInfoId,$parient_id);
                    $previousAttendAmountM = $previousAttendAmountM - 1 ;
                    $todayAttendAmountM = 1;
                    $attendAmountMonthly = $previousAttendAmountM + $todayAttendAmountM;
                } else {
                    $previousAttendAmountM = $this->attendancemodule->attendAmountMonthly($studentInfoId,$parient_id);
                    $todayAttendAmountM = 2;
                    $attendAmountMonthly = $previousAttendAmountM - $todayAttendAmountM;
                }
                $attendencePercenticeMonthly = $this->attendancemodule->attendPercentiseMonthly($attendAmountMonthly, $classAmountMonthly);
                //Take class and attend amount yearly and make the attendence percintise yearly 
                $classAmountYearly = $this->attendancemodule->classAmountYearly($studentInfoId,$parient_id);
                $classAmountYearly = $classAmountYearly - 1 ;
                if ($this->input->post("action", TRUE) == 'P') {
                    $previousAttendAmountY = $this->attendancemodule->attendAmountYearly($studentInfoId,$parient_id);
                    $previousAttendAmountY = $previousAttendAmountY - 1 ;
                    $todayAttendAmountY = 1;
                    $attendAmountYearly = $previousAttendAmountY + $todayAttendAmountY;
                } else {
                    $previousAttendAmountY = $this->attendancemodule->attendAmountYearly($studentInfoId,$parient_id);
                    $todayAttendAmountY = 2;
                    $attendAmountYearly = $previousAttendAmountY - $todayAttendAmountY;
                }
                $attendencePercenticeYearly = $this->attendancemodule->attendPercentiseYearly($attendAmountYearly, $classAmountYearly);
                $tableData = array(
                    'present_or_absent' => $this->db->escape_like_str($present),
                    'class_amount_monthly' => $this->db->escape_like_str($classAmountMonthly),
                    'class_amount_yearly' => $this->db->escape_like_str($classAmountYearly),
                    'attend_amount_monthly' => $this->db->escape_like_str($attendAmountMonthly),
                    'attend_amount_yearly' => $this->db->escape_like_str($attendAmountYearly),
                    'percentise_month' => $this->db->escape_like_str($attendencePercenticeMonthly),
                    'percentise_year' => $this->db->escape_like_str($attendencePercenticeYearly),
                );
                $this->db->update('daily_attendance', $tableData, array('student_id' => $studentInfoId, 'date' => $date));
                $tableData_1 = array(
                    'attendance_percentices_daily' => $this->db->escape_like_str($attendencePercenticeMonthly)
                );
                $this->db->update('class_students', $tableData_1, array('student_id' => $studentInfoId, 'class_title' => $classTitle));
                redirect('dailyAttendance/attendanceEditCompleteMessage', 'refresh');
            }
            else
            {
                redirect('dailyAttendance/attendanceEditCompleteMessage', 'refresh');
            }   
        } else {
            $data['editInfo'] = $this->common->getWhere('daily_attendance', 'id', $id);
            $this->load->view('temp/header');
            $this->load->view('editAttendance', $data);
            $this->load->view('temp/footer');
        }
    }

    public function attendanceEditCompleteMessage() {
         if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_teacher() && !$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8))) {
            redirect('auth', 'refresh');
        }
        $this->load->view('temp/header');
        $this->load->view('attendanceEditCompleteMessage');
        $this->load->view('temp/footer');
    }

    //This functio check teacher security password
    public function t_a_s_p() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8))) {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        if ($this->input->post('submit', TRUE)) {
            $data="";
            $password = $this->input->post('password', TRUE);
            $this->db->select('t_a_s_p');
            $this->db->where('parient_id', $parient_id);
            $security = $this->db->get('configuration');
            foreach ($security->result_array() as $row) {
                $data = $row['t_a_s_p'];
            }
            if ($password == $data) {
                $today = strtotime(date('d-m-Y'));
                $month = date("m",$today);
                if ($this->attendancemodule->todayTeacherAtt($today,$parient_id) == 'Taken') {
                    redirect('dailyAttendance/teacherAttendance', 'refresh');
                } else {
                    $query = $this->db->query("SELECT id,username FROM users WHERE user_status='Employee' AND NOT (leave_status='Leave' AND leave_start<='$today' AND leave_end>='$today') and parient_id=$parient_id");
                    foreach ($query->result_array() as $row) {
                        $tData = array(
                            'parient_id' => $parient_id,
                            'year' => date('Y'),
                            'month' => $month,
                            'date' => $today,
                            'employ_id' => $row['id'],
                            'employ_title' => $row['username'],
                            'status' => '0'
                        );
                        $this->db->insert('teacher_attendance', $tData);
                    }
                    redirect('dailyAttendance/teacherAttendance', 'refresh');
                }
            } else {
                $dat['message'] = '<div class="alert alert-danger alert-dismissable">
								<button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
								<strong>' . lang('attc_4') . ' </strong> ' . lang('attc_5') . '
							</div>';
                $this->load->view('temp/header');
                $this->load->view('selcetTeacAttView', $dat);
                $this->load->view('temp/footer');
            }
        } else {
            $this->load->view('temp/header');
            $this->load->view('selcetTeacAttView');
            $this->load->view('temp/footer');
        }
    }

    //Start teacher attendance's function now.
    //take teacher attendance
    public function teacherAttendance() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8))) {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        if ($this->input->post('submit', TRUE)) {
            $query = $this->db->query("SELECT time_zone FROM configuration WHERE parient_id = '$parient_id'");
            $data = "";
            foreach ($query->result_array() as $row) {
                $data = $row['time_zone'];
            }
            $datestring = "%h:%i %a";
            $now = now();
            $timezone = $data;
            $time = gmt_to_local($now, $timezone);
            $compTime = mdate($datestring, $time);
            $teacherAttenId = $this->input->post('teacher', TRUE);
            $attendace = $this->input->post('presAbsent', TRUE);
            if($attendace == "Present")
            {
                $attendace = '1';
            }
            else if($attendace == "Absent")
            {
                $attendace = '0';
            }
            $insertData = array(
                'id' => $teacherAttenId,
                'present_or_absent' => $attendace,
                'attend_time' => $compTime,
                'status' => '1',
            );
            $this->db->where('id', $teacherAttenId);
            if ($this->db->update('teacher_attendance', $insertData)) {
                redirect('dailyAttendance/teacherAttendance', 'refresh');
            }
        } else {
            $data['teacher'] = $this->attendancemodule->teacherList();
            $this->load->view('temp/header');
            $this->load->view('teacherAttendanceView', $data);
            $this->load->view('temp/footer');
        }
    }

    //This function will return employee attendance information
    public function employe_atten_view() {
        if (!$this->ion_auth->logged_in() || $this->ion_auth->is_student()|| $this->ion_auth->is_parents()) {
            redirect('auth', 'refresh');
        }
        if($this->ion_auth->is_admin() || $this->ion_auth->in_group(8))
        {
            if($this->input->get('id'))
            {
                $data['employee'] = $this->attendancemodule->own_attendance($this->input->get('id'));
                $this->load->view('temp/header');
                $this->load->view('employe_atten_view', $data);
                $this->load->view('temp/footer');
            }
            else
            {
                $data['employee'] = $this->attendancemodule->attend_employe();
                $this->load->view('temp/header');
                $this->load->view('employe_atten_view', $data);
                $this->load->view('temp/footer');
            }
        }
        else
        {
            $data['employee'] = $this->attendancemodule->own_attendance($this->session->userdata('user_id'));
            $this->load->view('temp/header');
            $this->load->view('employe_atten_view', $data);
            $this->load->view('temp/footer');
        }
    }

    //take teacher attendance
    public function editTeacherAttendance() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8))) {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $id = $this->input->get('att_id', TRUE);
        if ($this->input->post('submit', TRUE)) 
        {
            $idd = $this->input->post('attnd_id', TRUE);
            $attendace = $this->input->post('presAbsent', TRUE);
            if($attendace == "Present")
            {
                $attendace = '1';
            }
            else if($attendace == "Absent")
            {
                $attendace = '0';
            }
            $insertData = array(
                'present_or_absent' => $attendace,
            );
            $this->db->where('id', $idd);
            if ($this->db->update('teacher_attendance', $insertData)) {
                redirect('dailyAttendance/employe_atten_view', 'refresh');
            }
        } 
        else 
        {
            $data['editInfo'] = $this->common->getWhere('teacher_attendance', 'id', $id);
            $this->load->view('temp/header');
            $this->load->view('editTeacherAttendance', $data);
            $this->load->view('temp/footer');
        }
    }

}
