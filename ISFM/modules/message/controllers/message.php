<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class Message extends MX_Controller {
    /**
     * This controller is using for controlling message 
     *
     * Maps to the following URL
     * 		http://example.com/index.php/message
     * 	- or -  
     * 		http://example.com/index.php/message/<method_name>
     */
    function __construct() {
        parent::__construct();
        $this->lang->load('auth');
        $this->load->model('messagemodel');
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login');
        }
    }
    //This function can send message.
    public function sendMessage() {
        if (!$this->ion_auth->logged_in() || $this->ion_auth->is_student())
        {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if ($this->ion_auth->is_teacher()) 
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        if ($this->input->post('submit', TRUE)) {
            var_dump("msg1");
            die;
            $receiver = $this->input->post('receiver', TRUE);
            $group = $this->input->post('receiverGroup', TRUE);
            $date = date("d/m/y");
            if ($group == 'Student') {
                //if this message's receipent will students then work here
                if ($receiver == 'AllStudentSchool') {
                    $query = $this->common->getAllData('student_info',$parient_id);
                    foreach ($query as $row) {
                        $message = array(
                            'sender_id' => $this->db->escape_like_str($this->input->post('senderId', TRUE)),
                            'receiver_id' => $this->db->escape_like_str($row['user_id']),
                            'subject' => $this->db->escape_like_str($this->input->post('subject', TRUE)),
                            'message' => $this->db->escape_like_str($this->input->post('message', TRUE)),
                            'read_unread' => $this->db->escape_like_str('0'),
                            'date' => $this->db->escape_like_str($date),
                            'sender_delete' => 1,
                            'receiver_delete' => 1
                        );
                        $this->db->insert('massage', $message);
                    }
                    $data['message'] = lang('mesc_1');
                    $this->load->view('temp/header');
                    $this->load->view('message', $data);
                    $this->load->view('temp/footer');
                } else {
                    $receiver_2 = $this->input->post('receiver_2', TRUE);
                    if ($receiver_2 == 'AllStudentsClass') {
                        $query = $this->common->getWhere22('student_info', 'class_title', $receiver,'parient_id',$parient_id);
                        foreach ($query as $row) {
                            $message = array(
                                'sender_id' => $this->db->escape_like_str($this->input->post('senderId', TRUE)),
                                'receiver_id' => $this->db->escape_like_str($row['user_id']),
                                'subject' => $this->db->escape_like_str($this->input->post('subject', TRUE)),
                                'message' => $this->db->escape_like_str($this->input->post('message', TRUE)),
                                'read_unread' => $this->db->escape_like_str('0'),
                                'date' => $this->db->escape_like_str($date),
                                'sender_delete' => 1,
                                'receiver_delete' => 1
                            );
                            $this->db->insert('massage', $message);
                        }
                        $data['message'] = lang('mesc_1');
                        $this->load->view('temp/header');
                        $this->load->view('message', $data);
                        $this->load->view('temp/footer');
                    } else {
                        $message = array(
                            'sender_id' => $this->db->escape_like_str($this->input->post('senderId', TRUE)),
                            'receiver_id' => $this->db->escape_like_str($receiver_2),
                            'subject' => $this->db->escape_like_str($this->input->post('subject', TRUE)),
                            'message' => $this->db->escape_like_str($this->input->post('message', TRUE)),
                            'read_unread' => $this->db->escape_like_str('0'),
                            'date' => $this->db->escape_like_str($date),
                            'sender_delete' => 1,
                            'receiver_delete' => 1
                        );
                        if ($this->db->insert('massage', $message)) {
                            $data['message'] = lang('mesc_1');
                            $this->load->view('temp/header');
                            $this->load->view('message', $data);
                            $this->load->view('temp/footer');
                        }
                    }
                }
            } elseif ($group == 'Teacher') {
                //if this message's receipent will Teacher then work here
                if ($receiver == 'AllTeacher') {
                    $query = $this->common->getAllData('teachers_info',$parient_id);
                    foreach ($query as $row) {
                        $message = array(
                            'sender_id' => $this->db->escape_like_str($this->input->post('senderId', TRUE)),
                            'receiver_id' => $this->db->escape_like_str($row['user_id']),
                            'subject' => $this->db->escape_like_str($this->input->post('subject', TRUE)),
                            'message' => $this->db->escape_like_str($this->input->post('message', TRUE)),
                            'read_unread' => $this->db->escape_like_str('0'),
                            'date' => $this->db->escape_like_str($date),
                            'sender_delete' => 1,
                            'receiver_delete' => 1
                        );
                        $this->db->insert('massage', $message);
                    }
                    $data['message'] = lang('mesc_1');
                    $this->load->view('temp/header');
                    $this->load->view('message', $data);
                    $this->load->view('temp/footer');
                } else {
                    $message = array(
                        'sender_id' => $this->db->escape_like_str($this->input->post('senderId', TRUE)),
                        'receiver_id' => $this->db->escape_like_str($receiver),
                        'subject' => $this->db->escape_like_str($this->input->post('subject', TRUE)),
                        'message' => $this->db->escape_like_str($this->input->post('message', TRUE)),
                        'read_unread' => $this->db->escape_like_str('0'),
                        'date' => $this->db->escape_like_str($date),
                        'sender_delete' => 1,
                        'receiver_delete' => 1
                    );
                    if ($this->db->insert('massage', $message)) {
                        $data['message'] = lang('mesc_1');
                        $this->load->view('temp/header');
                        $this->load->view('message', $data);
                        $this->load->view('temp/footer');
                    }
                }
            } elseif ($group == 'Parents') {
                //if this message's receipent will Parents then work here
                if ($receiver == 'AllParentsSchool') {
                    $query = $this->common->getAllData('parents_info',$parient_id);
                    foreach ($query as $row) {
                        $message = array(
                            'sender_id' => $this->db->escape_like_str($this->input->post('senderId', TRUE)),
                            'receiver_id' => $this->db->escape_like_str($row['user_id']),
                            'subject' => $this->db->escape_like_str($this->input->post('subject', TRUE)),
                            'message' => $this->db->escape_like_str($this->input->post('message', TRUE)),
                            'read_unread' => $this->db->escape_like_str('0'),
                            'date' => $this->db->escape_like_str($date),
                            'sender_delete' => 1,
                            'receiver_delete' => 1
                        );
                        $this->db->insert('massage', $message);
                    }
                    $data['message'] = lang('mesc_1');
                    $this->load->view('temp/header');
                    $this->load->view('message', $data);
                    $this->load->view('temp/footer');
                } else {
                    $receiver_2 = $this->input->post('receiver_2', TRUE);
                    if ($receiver_2 == 'AllParentsClass') {
                        $query = $this->common->getWhere22('parents_info', 'class_id', $receiver,'parient_id',$parient_id);
                        foreach ($query as $row) {
                            $message = array(
                                'sender_id' => $this->db->escape_like_str($this->input->post('senderId', TRUE)),
                                'receiver_id' => $this->db->escape_like_str($row['user_id']),
                                'subject' => $this->db->escape_like_str($this->input->post('subject', TRUE)),
                                'message' => $this->db->escape_like_str($this->input->post('message', TRUE)),
                                'read_unread' => $this->db->escape_like_str('0'),
                                'date' => $this->db->escape_like_str($date),
                                'sender_delete' => 1,
                                'receiver_delete' => 1
                            );
                            $this->db->insert('massage', $message);
                        }
                        $data['message'] = lang('mesc_1');
                        $this->load->view('temp/header');
                        $this->load->view('message', $data);
                        $this->load->view('temp/footer');
                    } else {
                        $message = array(
                            'sender_id' => $this->db->escape_like_str($this->input->post('senderId', TRUE)),
                            'receiver_id' => $this->db->escape_like_str($receiver_2),
                            'subject' => $this->db->escape_like_str($this->input->post('subject', TRUE)),
                            'message' => $this->db->escape_like_str($this->input->post('message', TRUE)),
                            'read_unread' => $this->db->escape_like_str('0'),
                            'date' => $this->db->escape_like_str($date),
                            'sender_delete' => 1,
                            'receiver_delete' => 1
                        );
                        if ($this->db->insert('massage', $message)) {
                            $data['message'] = lang('mesc_1');
                            $this->load->view('temp/header');
                            $this->load->view('message', $data);
                            $this->load->view('temp/footer');
                        }
                    }
                }
            }
        } else {
            if($this->ion_auth->is_super())
            {
                $data['allAdmins'] = $this->common->getWhere22('userinfo','parient_id',$parient_id,'group_id','1');
            }
            $data['message'] = lang('mesc_1');
            $this->load->view('temp/header');
            $this->load->view('message', $data);
            $this->load->view('temp/footer');
        }
    }

    //This function can send message.
    public function sendMessage2() {
        if (!$this->ion_auth->logged_in() || $this->ion_auth->is_student())
        {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if (!$this->ion_auth->is_admin()) 
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $group = $this->input->post('receiverGroup', TRUE);
        $receiver = $this->input->post('receiver', TRUE);
        $receiver2 = $this->input->post('receiver_2', TRUE);
        $date = date("d/m/y");
        if ($this->input->post('submit', TRUE)) {
            if ($this->input->post('msgType') == 'smsapi') {
                $query = $this->db->query('SELECT msg_apai_email,msg_hash_number,msg_sender_title FROM configuration')->row();

                // Textlocal account details
                $username = $query->msg_apai_email;
                $hash = $query->msg_hash_number;

                // Message details
                $numbers = $this->messagemodel->studentNumber($group, $receiver, $receiver2);
                $sender = urlencode($query->msg_sender_title);
                $message = rawurlencode($query->msg_sender_title);

                $numbers = implode(',', $numbers);

                // Prepare data for POST request
                $data = array('username' => $username, 'hash' => $hash, 'numbers' => $numbers, "sender" => $sender, "message" => $message);

                // Send the POST request with cURL
                $ch = curl_init('http://api.txtlocal.com/send/');
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $response = curl_exec($ch);
                curl_close($ch);

                // Process your response here
                $json = json_decode($response);

                if ($json->status == 'success') {
                    $data['Success'] = lang('mesc_1');
                } else {
                    $data['Error'] = lang('mesc_2');
                }
                $this->load->view('temp/header');
                $this->load->view('message', $data);
                $this->load->view('temp/footer');
            } else {
                if ($group == 'Student') {
                    //if this message's receipent will students then work here
                    if ($receiver == 'AllStudentSchool') {
                        $query = $this->common->getAllData('student_info',$parient_id);
                        foreach ($query as $row) {
                            $message = array(
                                'sender_id' => $this->db->escape_like_str($this->input->post('senderId', TRUE)),
                                'receiver_id' => $this->db->escape_like_str($row['user_id']),
                                'subject' => $this->db->escape_like_str($this->input->post('subject', TRUE)),
                                'message' => $this->db->escape_like_str($this->input->post('message', TRUE)),
                                'read_unread' => $this->db->escape_like_str('0'),
                                'date' => $date,
                                'sender_delete' => 1,
                                'receiver_delete' => 1
                            );
                            $this->db->insert('massage', $message);
                        }
                        $data['message'] = lang('mesc_1');
                        $this->load->view('temp/header');
                        $this->load->view('message', $data);
                        $this->load->view('temp/footer');
                    } else {
                        $receiver_2 = $this->input->post('receiver_2', TRUE);
                        if ($receiver_2 == 'AllStudentsClass') {
                            $query = $this->common->getWhere22('student_info', 'class_id', $receiver,'parient_id',$parient_id);
                            foreach ($query as $row) {
                                $message = array(
                                    'sender_id' => $this->db->escape_like_str($this->input->post('senderId', TRUE)),
                                    'receiver_id' => $this->db->escape_like_str($row['user_id']),
                                    'subject' => $this->db->escape_like_str($this->input->post('subject', TRUE)),
                                    'message' => $this->db->escape_like_str($this->input->post('message', TRUE)),
                                    'read_unread' => $this->db->escape_like_str('0'),
                                    'date' => $date,
                                    'sender_delete' => 1,
                                    'receiver_delete' => 1
                                );
                                $this->db->insert('massage', $message);
                            }
                            $data['message'] = lang('mesc_1');
                            $this->load->view('temp/header');
                            $this->load->view('message', $data);
                            $this->load->view('temp/footer');
                        } else {
                            $message = array(
                                'sender_id' => $this->db->escape_like_str($this->input->post('senderId', TRUE)),
                                'receiver_id' => $this->db->escape_like_str($receiver_2),
                                'subject' => $this->db->escape_like_str($this->input->post('subject', TRUE)),
                                'message' => $this->db->escape_like_str($this->input->post('message', TRUE)),
                                'read_unread' => $this->db->escape_like_str('0'),
                                'date' => $date,
                                'sender_delete' => 1,
                                'receiver_delete' => 1
                            );
                            if ($this->db->insert('massage', $message)) {
                                $data['message'] = lang('mesc_1');
                                $this->load->view('temp/header');
                                $this->load->view('message', $data);
                                $this->load->view('temp/footer');
                            }
                        }
                    }
                } elseif ($group == 'Teacher') {
                    //if this message's receipent will Teacher then work here
                    if ($receiver == 'AllTeacher') {
                        $query = $this->common->getAllData('teachers_info',$parient_id);
                        foreach ($query as $row) {
                            $message = array(
                                'sender_id' => $this->db->escape_like_str($this->input->post('senderId', TRUE)),
                                'receiver_id' => $this->db->escape_like_str($row['user_id']),
                                'subject' => $this->db->escape_like_str($this->input->post('subject', TRUE)),
                                'message' => $this->db->escape_like_str($this->input->post('message', TRUE)),
                                'read_unread' => $this->db->escape_like_str('0'),
                                'date' => $date,
                                'sender_delete' => 1,
                                'receiver_delete' => 1
                            );
                            $this->db->insert('massage', $message);
                        }
                        $data['message'] = lang('mesc_1');
                        $this->load->view('temp/header');
                        $this->load->view('message', $data);
                        $this->load->view('temp/footer');
                    } else {
                        $message = array(
                            'sender_id' => $this->db->escape_like_str($this->input->post('senderId', TRUE)),
                            'receiver_id' => $this->db->escape_like_str($receiver),
                            'subject' => $this->db->escape_like_str($this->input->post('subject', TRUE)),
                            'message' => $this->db->escape_like_str($this->input->post('message', TRUE)),
                            'read_unread' => $this->db->escape_like_str('0'),
                            'date' => $date,
                            'sender_delete' => 1,
                            'receiver_delete' => 1
                        );
                        if ($this->db->insert('massage', $message)) {
                            $data['message'] = lang('mesc_1');
                            $this->load->view('temp/header');
                            $this->load->view('message', $data);
                            $this->load->view('temp/footer');
                        }
                    }
                }elseif ($group == 'Super Admin')
                {
                    $myId = $this->session->userdata('user_id');
                    $reciver = $this->messagemodel->parentId($myId);
                    $message = array(
                                'sender_id' => $this->db->escape_like_str($this->input->post('senderId', TRUE)),
                                'receiver_id' => $this->db->escape_like_str($reciver),
                                'subject' => $this->db->escape_like_str($this->input->post('subject', TRUE)),
                                'message' => $this->db->escape_like_str($this->input->post('message', TRUE)),
                                'read_unread' => $this->db->escape_like_str('0'),
                                'date' => $date,
                                'sender_delete' => 1,
                                'receiver_delete' => 1
                            );
                    if ($this->db->insert('massage', $message)) {
                                $data['message'] = lang('mesc_1');
                                $this->load->view('temp/header');
                                $this->load->view('message', $data);
                                $this->load->view('temp/footer');
                            }
                }
                elseif($group =='all')
                {
                    $myId = $this->session->userdata('user_id');
                    $query = $this->common->getWhere22('userinfo', 'parient_id', $myId,'group_id','1');
                    foreach ($query as $row) 
                    {
                        $message = array(
                                    'sender_id' => $this->db->escape_like_str($this->input->post('senderId', TRUE)),
                                    'receiver_id' => $this->db->escape_like_str($row['user_id']),
                                    'subject' => $this->db->escape_like_str($this->input->post('subject', TRUE)),
                                    'message' => $this->db->escape_like_str($this->input->post('message', TRUE)),
                                    'read_unread' => $this->db->escape_like_str('0'),
                                    'date' => $date,
                                    'sender_delete' => 1,
                                    'receiver_delete' => 1
                                );
                        $this->db->insert('massage', $message);
                    }
                    $data['message'] = lang('mesc_1');
                    $this->load->view('temp/header');
                    $this->load->view('message', $data);
                    $this->load->view('temp/footer');
                            
                }
                 elseif ($group == 'Parents') 
                {
                    //if this message's receipent will Parents then work here
                    if ($receiver == 'AllParentsSchool') 
                    {
                        $query = $this->common->getAllData('parents_info',$parient_id);
                        foreach ($query as $row) {
                            $message = array(
                                'sender_id' => $this->db->escape_like_str($this->input->post('senderId', TRUE)),
                                'receiver_id' => $this->db->escape_like_str($row['user_id']),
                                'subject' => $this->db->escape_like_str($this->input->post('subject', TRUE)),
                                'message' => $this->db->escape_like_str($this->input->post('message', TRUE)),
                                'read_unread' => $this->db->escape_like_str('0'),
                                'date' => $date,
                                'sender_delete' => 1,
                                'receiver_delete' => 1
                            );
                            $this->db->insert('massage', $message);
                        }
                        $data['message'] = lang('mesc_1');
                        $this->load->view('temp/header');
                        $this->load->view('message', $data);
                        $this->load->view('temp/footer');
                    } 
                    else 
                    {
                        $receiver_2 = $this->input->post('receiver_2', TRUE);
                        if ($receiver_2 == 'AllParentsClass') {
                            $query = $this->common->getWhere22('parents_info', 'class_id', $receiver,'parient_id',$parient_id);
                            foreach ($query as $row) {
                                $message = array(
                                    'sender_id' => $this->db->escape_like_str($this->input->post('senderId', TRUE)),
                                    'receiver_id' => $this->db->escape_like_str($row['user_id']),
                                    'subject' => $this->db->escape_like_str($this->input->post('subject', TRUE)),
                                    'message' => $this->db->escape_like_str($this->input->post('message', TRUE)),
                                    'read_unread' => $this->db->escape_like_str('0'),
                                    'date' => $date,
                                    'sender_delete' => 1,
                                    'receiver_delete' => 1
                                );
                                $this->db->insert('massage', $message);
                            }
                            $data['message'] = lang('mesc_1');
                            $this->load->view('temp/header');
                            $this->load->view('message', $data);
                            $this->load->view('temp/footer');
                        } else {
                            $message = array(
                                'sender_id' => $this->db->escape_like_str($this->input->post('senderId', TRUE)),
                                'receiver_id' => $this->db->escape_like_str($receiver_2),
                                'subject' => $this->db->escape_like_str($this->input->post('subject', TRUE)),
                                'message' => $this->db->escape_like_str($this->input->post('message', TRUE)),
                                'read_unread' => $this->db->escape_like_str('0'),
                                'date' => $date,
                                'sender_delete' => 1,
                                'receiver_delete' => 1
                            );
                            if ($this->db->insert('massage', $message)) {
                                $data['message'] = lang('mesc_1');
                                $this->load->view('temp/header');
                                $this->load->view('message', $data);
                                $this->load->view('temp/footer');
                            }
                        }
                    }
                }
                elseif($group == 'incharge')
                {
                    $rec = $this->input->post('recev_me', TRUE);
                    $message = array(
                                    'sender_id' => $this->db->escape_like_str($this->input->post('senderId', TRUE)),
                                    'receiver_id' => $this->db->escape_like_str($rec),
                                    'subject' => $this->db->escape_like_str($this->input->post('subject', TRUE)),
                                    'message' => $this->db->escape_like_str($this->input->post('message', TRUE)),
                                    'read_unread' => $this->db->escape_like_str('0'),
                                    'date' => $date,
                                    'sender_delete' => 1,
                                    'receiver_delete' => 1
                                );
                    if($this->db->insert('massage', $message))
                    {
                        $data['message'] = lang('mesc_1');
                        $this->load->view('temp/header');
                        $this->load->view('message', $data);
                        $this->load->view('temp/footer');
                    }   
                }
                else
                {
                    $message = array(
                                    'sender_id' => $this->db->escape_like_str($this->input->post('senderId', TRUE)),
                                    'receiver_id' => $this->db->escape_like_str($group),
                                    'subject' => $this->db->escape_like_str($this->input->post('subject', TRUE)),
                                    'message' => $this->db->escape_like_str($this->input->post('message', TRUE)),
                                    'read_unread' => $this->db->escape_like_str('0'),
                                    'date' => $date,
                                    'sender_delete' => 1,
                                    'receiver_delete' => 1
                                );
                    if($this->db->insert('massage', $message))
                    {
                        $data['message'] = lang('mesc_1');
                        $this->load->view('temp/header');
                        $this->load->view('message', $data);
                        $this->load->view('temp/footer');
                    }   
                }
            }
        } else {
            //If the massage is not set oe not submit it will load at first view for sending massage
            $data['message'] = lang('mesc_1');
            $this->load->view('temp/header');
            $this->load->view('message', $data);
            $this->load->view('temp/footer');
        }
    }

    //This function will return all receiver whene give/select any user group.
    public function ajaxSelectReciver() {
        $parient_id = $this->session->userdata('user_id');
        if ($this->ion_auth->is_teacher()) 
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $group = $this->input->get('q');
        if ($group == 'Student') {
            //If the student's group was selected thene work here
            $query = $this->common->getClass('classes','sections',0,'parient_id', $parient_id);
            foreach ($query as $row) {
                $data[] =$row;
            }
            if(!empty($data))
            {
                echo '<div class="form-group">
                                <label class="control-label col-md-3">Student Class<span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-cubes"></i>
                                        </span>
                                        <select onchange="classStuAndPar(this.value)" name="receiver" class="form-control"  data-validation="required">
                                        <option value="">' . "--Select Class--" . '</option>
                                        <option value="AllStudentSchool">' . "All Students in this School" . '</option>';
                foreach ($data as $sec) {
                    echo '<option value="' . $sec['id'] . '">' . $sec['class_title'] . '</option>';
                }
                echo '</select></div></div></div>';
            }
           
        } elseif ($group == 'Teacher') {

            $query = $this->common->getAllData('teachers_info',$parient_id);
            foreach ($query as $row) {
                $data[] =$row;
            }
            if(!empty($data))
            {
                echo '<div class="form-group">
                                <label class="control-label col-md-3">Teacher Name<span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-cubes"></i>
                                        </span>
                                        <select onchange="classStuAndPar(this.value)" name="receiver" class="form-control"  data-validation="required">
                                        <option value="">' . "--Select Teacher--" . '</option>
                                        <option value="AllTeacher">' . "All Teachers in this School" . '</option>';
                foreach ($data as $sec) {
                    echo '<option value="' . $sec['user_id'] . '">' . $sec['fullname'] . '</option>';
                }
                echo '</select></div></div></div>';
            }
        } elseif ($group == 'Parents') {
            //If the parent's group was selected thene work here
            $query = $this->common->getClass('classes','sections',0,'parient_id', $parient_id);
            foreach ($query as $row) {
                $data[] =$row;
            }
            if(!empty($data))
            {
                echo '<div class="form-group">
                                <label class="control-label col-md-3">Parent\'s Child Class<span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-cubes"></i>
                                        </span>
                                        <select onchange="classStuAndPar(this.value)" name="receiver" class="form-control"  data-validation="required">
                                        <option value="">' . "--Select Class--" . '</option>
                                        <option value="AllParentsSchool">' . "All Parents in this School" . '</option>';
                foreach ($data as $sec) {
                    echo '<option value="' . $sec['id'] . '">' . $sec['class_title'] . '</option>';
                }
                echo '</select></div></div></div>';
            }
        }
    }

    //This function will return all receiver whene give/select any user group.
    public function ajaxClassStuAndPar() {
        $group = $this->input->get('g');
        $recInfo = $this->input->get('p');
        if ($group == 'Students') 
        {

            $query = $this->common->getWhere('student_info', 'class_id', $recInfo);
            foreach ($query as $row) {
                $data[] = $row;
            }

            // var_dump($data);
            // die;
            if(!empty($data))
            {
            echo '<div class="form-group">
                                    <label class="control-label col-md-3">Student Name<span class="requiredStar"> * </span></label>
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-user"></i>
                                            </span>
                                            <select name="receiver_2" class="form-control" >
                                            <option value="">' . "--Select Student--" . '</option>
                                            <option value="AllStudentsClass">' . "All students in this class" . '</option>';
            foreach ($data as $sec) {
                    echo '<option value="' . $sec['user_id'] . '">' . $sec['student_id'] . '-'.$sec['student_nam'].'</option>';
                }
                echo '</select></div></div></div>';
            }
        } 
        elseif ($group == 'Guardians')
        {
            // $query = $this->common->getWhere('parents_info', 'class_id', $recInfo);
            // foreach ($query as $row)
            // {
            //     echo '<option value="' . $row['user_id'] . '">' . $row['parents_name'] . ' - StudentID : ' . $row['student_id'] . ' </option>';
            // }
            $query = $this->common->getWhere('parents_info', 'class_id', $recInfo);
            foreach ($query as $row) {
                $data[] = $row;
            }
            if(!empty($data))
            {
            echo '<div class="form-group">
                                    <label class="control-label col-md-3">Guardian Name<span class="requiredStar"> * </span></label>
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-user"></i>
                                            </span>
                                            <select name="receiver_2" class="form-control" >
                                            <option value="">' . "--Select Guardian--" . '</option>
                                            <option value="AllParentsClass">' . "All Parents in this class" . '</option>';
            foreach ($data as $sec) {
                    echo '<option value="' . $sec['user_id'] . '">' . $sec['parents_name'] . ' - StudentID :'.$sec['student_id'].'</option>';
                }
                echo '</select></div></div></div>';
            }
        }
    }

    //This function will return all inbox read and unread massage
    public function inbox() {
        $user = $this->ion_auth->user()->row();
        $id = $user->id;
        $data['massage'] = $this->common->getWhere22('massage', 'receiver_id', $id, 'receiver_delete', 1);
        $this->load->view('temp/header');
        $this->load->view('inbox', $data);
        $this->load->view('temp/footer');
    }

    //This function will return all sent read and unread massage
    public function sent_box() {
        $user = $this->ion_auth->user()->row();
        $id = $user->id;
        $data['massage'] = $this->common->getWhere22('massage', 'sender_id', $id, 'sender_delete', 1);
        $this->load->view('temp/header');
        $this->load->view('sent', $data);
        $this->load->view('temp/footer');
    }

    //This function will return all inbox read and unread massage
    public function sentMessage() {
        $user = $this->ion_auth->user()->row();
        $id = $user->id;
        $data['massage'] = $this->common->getWhere22('massage', 'sender_id', $id, 'sender_delete', 1);
        $this->load->view('temp/header');
        $this->load->view('sent', $data);
        $this->load->view('temp/footer');
    }

    //This function can return unread massage in the inbox. 
    public function unreadMassage() {
        $user = $this->ion_auth->user()->row();
        $id = $user->id;
        $data['unreadMassage'] = $this->messagemodel->unReadMassage($id);
    }

    //user can read the message by this function
    public function readMassage() {
        $id = $this->input->get('id');
        $data['massage'] = $this->common->getWhere('massage', 'id', $id);
        $update = array(
            'read_unread' => $this->db->escape_like_str(1)
        );
        if ($this->db->update('massage', $update, array('id' => $this->db->escape_like_str($id)))) {
            if($this->input->get('sent'))
            {
                $sflag = $this->input->get('sent');
                $data['sflag'] = $sflag;
                $this->load->view('temp/header');
                $this->load->view('readmassage', $data);
                $this->load->view('temp/footer');
            }
            else
            {
                $data['sflag'] = '0';
                $this->load->view('temp/header');
                $this->load->view('readmassage', $data);
                $this->load->view('temp/footer');
            }
        }
    }

    //This function can check at first thet is the sender want to delete it.
    //If the sender delete the message befor the receiver delte then this function can delete this message from databae.
    //Or remove this message only from the inbox item.
    public function deleteInboxMassage() {
        $id = $this->input->get('id');
        $query = $this->common->getWhere('massage', 'id', $id);
        foreach ($query as $row) {
            $senderDelete = $row['sender_delete'];
        }
        if ($senderDelete == '0') {
            if ($this->db->delete('massage', array('id' => $id))) {
                redirect('message/inbox', 'refresh');
            }
        } else {
            $this->db->where('id', $id);
            $data = array('receiver_delete' => 0);
            if ($this->db->update('massage', $data)) {
                redirect('message/inbox', 'refresh');
            }
        }
    }

    //This function can check at first thet is the receiver want to delete it.
    //If the receiver delete the message befor the sender delte then this function can delete this message from databae.
    //Or remove this message only from the sent message item.
    public function deleteSentMassage() {
        $id = $this->input->get('id');
        $query = $this->common->getWhere('massage', 'id', $id);
        foreach ($query as $row) {
            $receiverDelete = $row['receiver_delete'];
        }
        if ($receiverDelete == '0') {
            if ($this->db->delete('massage', array('id' => $id))) {
                redirect('message/sent_box', 'refresh');
            }
        } else {
            $this->db->where('id', $id);
            $data = array('sender_delete' => 0);
            if ($this->db->update('massage', $data)) {
                redirect('message/sent_box', 'refresh');
            }
        }
    }

}
