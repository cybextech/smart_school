<div class="page-content-wrapper">
    <div class="page-content">
        <!--BEGIN PAGE Content-->
        <div class="row">
            <?php
            foreach ($massage as $row) {
                $userId = $row['sender_id'];
                $userId1 = $row['receiver_id'];
                $query = $this->common->getWhere('users', 'id', $userId);
                foreach ($query as $row1) {
                    $senderName = $row1['username'];
                }
                if($sflag == '1')
                {
                    $query321 = $this->common->getWhere('users', 'id', $userId1);
                    foreach ($query321 as $row321) {
                        $recieverName = $row321['username'];
                    }
                }
                ?>
                <div class="col-md-12">
                    <div class="inbox-content">
                        <div class="alert alert-success">
                            <?php if($sflag == '0') { ?>
                            <strong><?php echo lang('mes_from'); ?> : </strong> <?php echo $senderName; ?><br> <?php echo $row['date']; }else { ?>
                            <strong><?php echo lang('mes_to'); ?> : </strong> <?php echo $recieverName; ?><br> <?php echo $row['date']; } ?>
                        </div>
                        <div class="alert alert-info">
                            <strong><?php echo lang('mes_sub'); ?> : </strong> <?php echo $row['subject']; ?>
                            <hr>
                            <p><?php echo str_replace(['\\r', '\\n'],'',$row['message']); ?></p>
                            <hr>
                        </div>
                        <div class="form-actions fluid">
                            <div class="col-md-offset-3 col-md-6">
                                <button onclick="location.href = 'javascript:history.back()'" class="btn green"> &nbsp;&nbsp;&nbsp;&nbsp; <?php echo lang('back'); ?> &nbsp;&nbsp;&nbsp;&nbsp; </button>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        <!--End PAGE Content-->
    </div>
</div>