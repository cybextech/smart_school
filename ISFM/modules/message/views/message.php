<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="assets/global/plugins/select2/select2.css"/>
<link href="assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
<!-- BEGIN THEME STYLES -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo lang('header_sent_message'); ?> <small></small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo lang('home'); ?>
                    </li>
                    <li>
                        <?php echo lang('header_message'); ?>
                    </li>
                    <li>
                        <?php echo lang('header_sent_message'); ?>
                    </li>
                    <li id="result" class="pull-right topClock"></li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <div class="col-md-12">
            <?php if (!empty($Success)) { ?>
                <div class="alert alert-success">
                    <h1><strong><?php echo lang('success'); ?></strong> <?php echo $Success; ?></h1>
                </div>
                <?php
            }
            if (!empty($Error)) {
                ?>
                <div class="alert alert-danger">
                    <h1><strong><?php echo lang('error'); ?></strong> <?php echo $Error; ?></h1>
                </div>
            <?php } ?>
        </div>
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXTRAS PORTLET-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo lang('mes_sym'); ?>
                        </div>
                    </div>
                    <div class="portlet-body form">

                        <!-- BEGIN FORM-->
                        <?php
                        $form_attributs = array('class' => 'form-horizontal', 'role' => 'form');
                        echo form_open('message/sendMessage2', $form_attributs);
                        ?>
                        <div class="form-body">
                            <div class="form-group">
                                <label class="col-md-3"><div class="alert alert-success">
                                        <strong><?php echo lang('mes_symt'); ?></strong> 
                                    </div></label>
                                <div class="col-md-6">
                                    <div class="checkbox-list">
                                        <label>
                                            <input type="radio" checked="" name="msgType" value="internal"> <?php echo lang('mes_ims'); ?>  </label>
                                        <!-- <label>
                                            <input type="radio" name="msgType" value="smsapi"> <?php echo lang('mes_bss'); ?> </label> -->
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <br>
                            <?php if(!$this->ion_auth->is_admin() && !$this->ion_auth->is_super() && !$this->ion_auth->is_parents() && !$this->ion_auth->in_group(8))
                            {
                                $pid = $this->session->userdata('parient_id');
                                ?>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('mes_sg'); ?></label>
                                <div class="col-md-6">
                                    <select id="reseverGroup" onchange="selectReceiver(this.value)" class="form-control" name="receiverGroup" required>
                                        <option value=""><?php echo lang('select'); ?></option>
                                        <option value="Student"><?php echo lang('mes_stu')."s"; ?></option>
                                        <!-- <option value="Teacher"><?php echo lang('mes_tea'); ?></option> -->
                                        <option value="Parents"><?php echo lang('mes_par'); ?></option>
                                        <option value="<?php echo $pid; ?>"><?php echo lang('prin'); ?></option>
                                    </select>
                                </div>
                            </div>
                            <?php } elseif($this->ion_auth->is_super()){ ?>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo "Select Admin"; ?></label>
                                <div class="col-md-6">
                                    <select id="reseverGroup" class="form-control" name="receiverGroup" required>
                                        <option value=""><?php echo lang('select'); ?></option>
                                        <option value="all"><?php echo "All Admins"; ?></option>
                                        <?php foreach ($allAdmins as $row) { ?>
                                                <option value="<?php echo $row['user_id']; ?>"><?php echo $row['full_name']; ?></option>
                                            <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <?php } elseif($this->ion_auth->is_admin()) {  ?>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('mes_sg'); ?></label>
                                <div class="col-md-6">
                                    <select id="reseverGroup" onchange="selectReceiver(this.value)" class="form-control" name="receiverGroup" required>
                                        <option value=""><?php echo lang('select'); ?></option>
                                        <option value="Student"><?php echo lang('mes_stu')."s"; ?></option>
                                        <option value="Teacher"><?php echo lang('mes_tea'); ?></option>
                                        <option value="Parents"><?php echo lang('mes_par'); ?></option>
                                        <option value="Super Admin"><?php echo "Super Admin"; ?></option>
                                    </select>
                                </div>
                            </div>
                            <?php } elseif($this->ion_auth->in_group(8)) { $pid = $this->session->userdata('parient_id'); ?>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('mes_sg'); ?></label>
                                <div class="col-md-6">
                                    <select id="reseverGroup" onchange="selectReceiver(this.value)" class="form-control" name="receiverGroup" required>
                                        <option value=""><?php echo lang('select'); ?></option>
                                        <option value="<?php echo $pid; ?>"><?php echo lang('prin'); ?></option>
                                    </select>
                                </div>
                            </div>
                              <?php } elseif($this->ion_auth->is_parents()) { 
                                $pid = $this->session->userdata('parient_id');
                                $mid = $this->session->userdata('user_id');
                                $clas = "";
                                $this->db->where('user_id',$mid);
                                $this->db->select('section');
                                $q1 = $this->db->get('parents_info');
                                foreach ($q1->result_array() as $r1) {
                                    $clas = $r1['section'];
                                }
                                $incharg = "";
                                $this->db->where('id',$clas);
                                $this->db->select('incharge');
                                $q2 = $this->db->get('class');
                                foreach ($q2->result_array() as $r2) {
                                    $incharg = $r2['incharge'];
                                }
                                if($incharg != '0')
                                {
                                    $teacher_nam = "";
                                    $t_uid = "";
                                    $this->db->where('id',$incharg);
                                    $this->db->select('fullname');
                                    $this->db->select('user_id');
                                    $q3 = $this->db->get('teachers_info');
                                    foreach ($q3->result_array() as $r3) {
                                        $teacher_nam = $r3['fullname'];
                                        $t_uid = $r3['user_id'];
                                    }
                                }
                            ?>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('recv1'); ?></label>
                                <div class="col-md-6">
                                    <select id="reseverGroup" onchange="selectReceiver(this.value)" class="form-control" name="receiverGroup" required>
                                        <option value=""><?php echo lang('select'); ?></option>
                                        <?php if($incharg != '0') {?> 
                                        <option value="incharge"><?php echo $teacher_nam; ?></option>
                                        <?php } ?>
                                        <option value="<?php echo $pid; ?>"><?php echo lang('prin'); ?></option>
                                    </select>
                                </div>
                            </div>
                            <input type="hidden" name="recev_me" value="<?php echo $t_uid; ?>">
                            <?php } ?>
                            <?php $user = $this->ion_auth->user()->row(); ?>
                            <input type="hidden" name="senderId" value="<?php echo $user->id; ?>">
                            <div id="ajaxResult">
                             </div>
                             <div id="ajaxStuAndPar">
                             </div>
    
                            <div class="form-group">
                                <label class="control-label col-md-3"><?php echo lang('mes_sub'); ?></label>
                                <div class="col-md-6">
                                    <input class="form-control"  type="text" name="subject" required>
                                </div>
                            </div>
                            <div class="form-group last">
                                <label class="control-label col-md-3"><?php echo lang('mes_mas'); ?></label>
                                <div class="col-md-9">
                                    <textarea class="ckeditor form-control" name="message" rows="6" required></textarea>
                                </div>
                            </div>

                        </div>
                        <div class="form-actions fluid">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green" name="submit" value="Submit"><i class="fa fa-send"></i> &nbsp;&nbsp;<?php echo lang('mes_senbutt'); ?> </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                        <!-- END FORM-->
                    </div>
                </div>
                <!-- END EXTRAS PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/global/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/admin/pages/scripts/components-dropdowns.js"></script>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/global/plugins/ckeditor/ckeditor.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<script>
    function selectReceiver(str) {
        
        if(str == "Teacher")
        {
            document.getElementById("ajaxStuAndPar").style.display = 'none';
        }
        else if(str == "Student")
        {
            document.getElementById("ajaxStuAndPar").style.display = 'inline-block';
        }
        else if(str == "Parents")
        {
            document.getElementById("ajaxStuAndPar").style.display = 'inline-block';
        }
        else if(str == "Super Admin")
        {
            document.getElementById("ajaxStuAndPar").style.display = 'none';
        }
        var xmlhttp;
        if (str.length === 0) {
            document.getElementById("ajaxResult").innerHTML = "";
            return;
        }
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                document.getElementById("ajaxResult").innerHTML = xmlhttp.responseText;
            }
        };
        xmlhttp.open("GET", "index.php/message/ajaxSelectReciver?q=" + str, true);
        xmlhttp.send();
    }
    function classStuAndPar(str) {
        var xmlhttp;
        var sel = document.getElementById("reseverGroup");
        var val = sel.options[sel.selectedIndex].text;
        if (str.length === 0) {
            document.getElementById("ajaxStuAndPar").innerHTML = "";
            return;
        }
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                document.getElementById("ajaxStuAndPar").innerHTML = xmlhttp.responseText;
            }
        };
        xmlhttp.open("GET", "index.php/message/ajaxClassStuAndPar?p=" + str + "&g=" + val, true);
        xmlhttp.send();
    }
                                            
    jQuery(document).ready(function () {
//here is auto reload after 1 second for time and date in the top
        jQuery(setInterval(function () {
            jQuery("#result").load("index.php/home/iceTime");
        }, 1000));
    });
</script>

