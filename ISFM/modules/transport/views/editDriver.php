
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo "Edit Vehicle Information"; ?><small></small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo lang('home'); ?>
                    </li>
                    <li>
                        <?php echo lang('header_transpo'); ?>
                    </li>
                    <li>
                        <?php echo "Drivers"; ?>
                    </li>
                    <li>
                        <?php echo "Edit Drivers"; ?>
                    </li>
                    <li id="result" class="pull-right topClock"></li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12 ">
                <?php 
                    if(!empty($success))
                    {
                        echo $success;
                    }
                ?>
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box green ">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo "Give Information To Edit Driver"; ?>
                        </div>
                        <div class="tools">
                            <a href="" class="collapse">
                            </a>
                            <a href="" class="reload">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <?php $form_attributs = array('class' => 'form-horizontal', 'role' => 'form');
                        $id = $this->input->get('id');
                        echo form_open("transport/editDriver?id=$id", $form_attributs);
                        ?>
                            <?php foreach ($drivers as $row) { 
                                if($row['id']==$id){
                                ?>    
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"><?php echo "Vehicle No"; ?> <span class="requiredStar"> * </span></label>
                                        
                                        <div class="col-md-6">
                                            <select class="form-control" name="vehicleNo" required>
                                                <option value="<?php echo $row['vehicle_no']; ?>"><?php echo $row['vehicle_no']; ?></option>
                                                    <?php foreach ($vehicles as $row1) { ?>
                                                            <option value="<?php echo $row1['vehicle_no']; ?>"><?php echo $row1['vehicle_no']; ?></option>
                                                    <?php } ?>
                                            </select>
                                                    
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"><?php echo "Driver Name"; ?> <span class="requiredStar"> * </span></label>
                                        <div class="col-md-6">
                                            <input  class="form-control" type="text" data-validation="alphanumeric" data-validation-allowing=" " name="name" placeholder="" value="<?php echo $row['driver_name']; ?>" required>
                                            <input class="form-control" type="hidden" name="v_no" value="<?php echo $row['vehicle_no']; ?>" readonly="">
                                            <input class="form-control" type="hidden" name="l_no" value="<?php echo $row['license_no']; ?>" readonly="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"><?php echo "Present Address"; ?> <span class="requiredStar"> * </span></label>
                                        <div class="col-md-6">
                                            <textarea class="form-control" rows="3" required name="address"><?php echo $row['present_address']; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"><?php echo "Permanent Address"; ?><span class="requiredStar"></span></label>
                                        <div class="col-md-6">
                                            <textarea class="form-control" rows="3" name="address2"><?php echo $row['permanent_address']; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3"><?php echo lang('admi_DateOfBirth'); ?> <span class="requiredStar"> * </span></label>
                                        <div class="col-md-4">
                                            <input class="form-control" required name="birthdate" value="<?php echo $row['birth_date']; ?>" id="mask_date2" type="text" >
                                            <span class="help-block">
                                                Date Type  DD/MM/YYYY </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"> <?php echo "Mobile Number"; ?> <span class="requiredStar"> * </span></label>
                                        
                                            <div class="col-md-4">
                                                <input type="text" min="0" maxlength="11" value="<?php echo $row['phone']; ?>" class="form-control" name="phone1" placeholder="03001234567" required>
                                            </div>
                                            
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"> <?php echo "License Number"; ?> <span class="requiredStar"> * </span></label>
                                        
                                            <div class="col-md-4">
                                                <input type="text" data-validation="alphanumeric" data-validation-allowing=" " value="<?php echo $row['license_no']; ?>"  class="form-control" name="license" placeholder="" required>
                                            </div>
                                            
                                    </div>
                                </div>
                            <?php }}?>
                            <div class="form-actions fluid">
                                <div class="col-md-offset-3 col-md-6">
                                    <button type="submit" class="btn green" name="submit" value="Submit"><?php echo "Update"; ?></button>
                                    <button type="reset" class="btn default"><?php echo lang('refresh'); ?></button>
                                </div>
                            </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->
<script src="assets/global/plugins/jquery.form-validator.min.js" type="text/javascript"></script>
<script> $.validate(); </script>
<script>
    jQuery(document).ready(function() {
//here is auto reload after 1 second for time and date in the top
        jQuery(setInterval(function() {
            jQuery("#result").load("index.php/home/iceTime");
        }, 1000));
    });
</script>

