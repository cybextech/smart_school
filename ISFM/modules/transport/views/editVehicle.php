
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo "Edit Vehicle Information"; ?><small></small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo lang('home'); ?>
                    </li>
                    <li>
                        <?php echo lang('header_transpo'); ?>
                    </li>
                    <li>
                        <?php echo "Vehicles"; ?>
                    </li>
                    <li>
                        <?php echo "Edit Vehicles"; ?>
                    </li>
                    <li id="result" class="pull-right topClock"></li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12 ">
                <?php 
                    if(!empty($success))
                    {
                        echo $success;
                    }
                ?>
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box green ">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo "Give Information for Edit Vehicle"; ?>
                        </div>
                        <div class="tools">
                            <a href="" class="collapse">
                            </a>
                            <a href="" class="reload">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <?php $form_attributs = array('class' => 'form-horizontal', 'role' => 'form');
                        $id = $this->input->get('id');
                        echo form_open("transport/editVehicle?id=$id", $form_attributs);
                        ?>
                            <?php foreach ($vehicles as $row) { 
                                if($row['id']==$id){
                                ?>    
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"><?php echo "Vehicle No"; ?><span class="requiredStar"> * </span></label>
                                        <div class="col-md-6">
                                            <input  class="form-control" type="text" data-validation="alphanumeric" data-validation-allowing=" " name="vehicleNo" placeholder="" value="<?php echo $row['vehicle_no']; ?>" required >
                                            <input class="form-control" type="hidden" name="v_no" value="<?php echo $row['vehicle_no']; ?>" readonly="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"><?php echo "Vehicle type"; ?> <span class="requiredStar"> * </span></label>
                                        <div class="col-md-6">
                                            <input  class="form-control" data-validation="alphanumeric" data-validation-allowing=" " type="text" name="vehicleType" placeholder="" value="<?php echo $row['vehicle_type']; ?>" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"><?php echo "No of seats"; ?> <span class="requiredStar"> * </span></label>
                                        <div class="col-md-6">
                                            <input  class="form-control" type="number" name="seatAmount" placeholder="" value="<?php echo $row['total_seats']; ?>" required min="0">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"><?php echo "Vehicle Information"; ?><span class="requiredStar"></span></label>
                                        <div class="col-md-6">
                                            <textarea class="form-control" rows="3" name="description" required> <?php echo $row['vehicle_info']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            <?php }}?>
                            <div class="form-actions fluid">
                                <div class="col-md-offset-3 col-md-6">
                                    <button type="submit" class="btn green" name="submit" value="Submit"><?php echo "Update"; ?></button>
                                    <button type="reset" class="btn default"><?php echo lang('refresh'); ?></button>
                                </div>
                            </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->
<script src="assets/global/plugins/jquery.form-validator.min.js" type="text/javascript"></script>
<script> $.validate(); </script>
<script>
    jQuery(document).ready(function() {
//here is auto reload after 1 second for time and date in the top
        jQuery(setInterval(function() {
            jQuery("#result").load("index.php/home/iceTime");
        }, 1000));
    });
</script>

