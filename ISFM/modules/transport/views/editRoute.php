
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo "Edit Route Information"; ?><small></small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo lang('home'); ?>
                    </li>
                    <li>
                        <?php echo lang('header_transpo'); ?>
                    </li>
                    <li>
                        <?php echo "Routes"; ?>
                    </li>
                    <li>
                        <?php echo "Edit Routes"; ?>
                    </li>
                    <li id="result" class="pull-right topClock"></li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12 ">
                <?php 
                    if(!empty($success))
                    {
                        echo $success;
                    }
                ?>
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box green ">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo "Give Information To Edit Route"; ?>
                        </div>
                        <div class="tools">
                            <a href="" class="collapse">
                            </a>
                            <a href="" class="reload">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <?php $form_attributs = array('class' => 'form-horizontal', 'role' => 'form');
                        $id = $this->input->get('id');
                        echo form_open("transport/editRoute?id=$id", $form_attributs);
                        ?>
                            <?php foreach ($routes as $row) { 
                                if($row['id']==$id){
                                ?>    
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"><?php echo "Vehicle No"; ?> <span class="requiredStar"> * </span></label>
                                        
                                        <div class="col-md-6">
                                            <select class="form-control" name="vehicleNo" required>
                                                <option value="<?php echo $row['vehicle_no']; ?>"><?php echo $row['vehicle_no']; ?></option>
                                                    <?php foreach ($vehicles as $row1) { ?>
                                                            <option value="<?php echo $row1['vehicle_no']; ?>"><?php echo $row1['vehicle_no']; ?></option>
                                                    <?php } ?>
                                            </select>
                                                    
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"><?php echo "Route Code"; ?> <span class="requiredStar"> * </span></label>
                                        <div class="col-md-6">
                                            <input  class="form-control" type="text" data-validation="alphanumeric" data-validation-allowing=" " name="routeCode" value="<?php echo $row['route_code']; ?>" placeholder="" required>
                                            <input class="form-control" type="hidden" name="v_no" value="<?php echo $row['vehicle_no']; ?>" readonly="">
                                            <input class="form-control" type="hidden" name="r_code" value="<?php echo $row['route_code']; ?>" readonly="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"><?php echo "Route Start Place"; ?> <span class="requiredStar"> * </span></label>
                                        <div class="col-md-6">
                                            <input  class="form-control" value="<?php echo $row['route_start']; ?>" type="text" name="start" data-validation="alphanumeric" data-validation-allowing=" " placeholder="" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"><?php echo "Route End Place"; ?><span class="requiredStar"> * </span></label>
                                        <div class="col-md-6">
                                            <input  class="form-control" value="<?php echo $row['route_end']; ?>" type="text" name="stop" placeholder="" data-validation="alphanumeric" data-validation-allowing=" " required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"><?php echo "Route Fare"; ?><span class="requiredStar"> * </span></label>
                                        <div class="col-md-6">
                                            <input  class="form-control" value="<?php echo $row['fare']; ?>" min="0" type="number" name="fare" placeholder="" required>
                                        </div>
                                    </div>
                                </div>
                            <?php }}?>
                            <div class="form-actions fluid">
                                <div class="col-md-offset-3 col-md-6">
                                    <button type="submit" class="btn green" name="submit" value="Submit"><?php echo "Update"; ?></button>
                                    <button type="reset" class="btn default"><?php echo lang('refresh'); ?></button>
                                </div>
                            </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->
<script src="assets/global/plugins/jquery.form-validator.min.js" type="text/javascript"></script>
<script> $.validate(); </script>
<script>
    jQuery(document).ready(function() {
//here is auto reload after 1 second for time and date in the top
        jQuery(setInterval(function() {
            jQuery("#result").load("index.php/home/iceTime");
        }, 1000));
    });
</script>

