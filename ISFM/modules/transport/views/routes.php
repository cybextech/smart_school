<link rel="stylesheet" type="text/css" href="assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo "Add New Route"; ?><small></small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo lang('home'); ?>
                    </li>
                    <li>
                        <?php echo lang('header_transpo'); ?>
                    </li>
                    <li>
                        <?php echo "Routes"; ?>
                    </li>
                    <li id="result" class="pull-right topClock"></li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12 ">
                <?php
                    if (!empty($success)) 
                    {
                        echo '<br>' . $success;
                    }
                ?>
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box green ">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo "Give The Information For New Route"; ?>
                        </div>
                        <div class="tools">
                            <a href="" class="collapse">
                            </a>
                            <a href="" class="reload">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <?php $form_attributs = array('class' => 'form-horizontal', 'role' => 'form');
                        echo form_open('transport/routes', $form_attributs);
                        ?>
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo "Vehicle No"; ?> <span class="requiredStar"> * </span></label>
                                    
                                    <div class="col-md-6">
                                        <select class="form-control" name="vehicleNo" required>
                                            <option value=""><?php echo "--Select Vehicle--"; ?></option>
                                                <?php foreach ($vehicles as $row) { ?>
                                                        <option value="<?php echo $row['vehicle_no']; ?>"><?php echo $row['vehicle_no']; ?></option>
                                                <?php } ?>
                                        </select>
                                                
                                    </div>
                                </div>
                                <!-- <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo "Route Code"; ?> <span class="requiredStar"> * </span></label>
                                    <div class="col-md-6">
                                        <input  class="form-control" type="text" data-validation="alphanumeric" data-validation-allowing=" " name="routeCode" placeholder="" required>
                                    </div>
                                </div> -->
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo "Route Start Place"; ?> <span class="requiredStar"> * </span></label>
                                    <div class="col-md-6">
                                        <input  class="form-control" type="text" data-validation="alphanumeric" data-validation-allowing=" " name="start" placeholder="" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo "Route End Place"; ?><span class="requiredStar"> * </span></label>
                                    <div class="col-md-6">
                                        <input  class="form-control" type="text" data-validation="alphanumeric" data-validation-allowing=" " name="stop" placeholder="" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo "Route Fare"; ?><span class="requiredStar"> * </span></label>
                                    <div class="col-md-6">
                                        <input  class="form-control" min="0" type="number" name="fare" placeholder="" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions fluid">
                                <div class="col-md-offset-3 col-md-6">
                                    <button type="submit" class="btn green" name="submit" value="Submit"><?php echo lang('save'); ?></button>
                                    <button type="reset" class="btn default"><?php echo lang('refresh'); ?></button>
                                </div>
                            </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 >
                  <b>  <?php echo "All Routes"; ?></b>
                </h3>
                
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo "Routes Information"; ?>
                        </div>
                        <div class="tools">
                            <a class="collapse" href="javascript:;">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>
                                <tr>
                                    <th>
                                        Sr. No
                                    </th>
                                    <th>
                                        Vehicle Number
                                    </th>
                                    <!-- <th>
                                       Route Code
                                    </th> -->
                                    <th>
                                       Starting point
                                    </th>
                                    <th>
                                       Ending Point
                                    </th>
                                    <th>
                                       Monthy Fare
                                    </th>
                                        <th>
                                            Management
                                        </th>
                                   
                                </tr>
                            </thead>
                            <tbody>
                               <?php $i = 1; foreach ($routes as $row) { ?>
                                    <tr>
                                        <td>
                                           <?php echo $i; ?>
                                        </td>
                                        <td>
                                           <?php echo $row['vehicle_no']; ?>
                                        </td>
                                        <!-- <td>
                                            <?php echo $row['route_code']; ?>
                                        </td> -->
                                        <td>
                                         <?php echo $row['route_start']; ?>
                                        </td>
                                        <td>
                                           <?php echo $row['route_end']; ?>
                                        </td>
                                        <td>
                                           <?php echo $row['fare']; ?>
                                        </td>
                                        <td>
                                            <a class="btn btn-xs default" href="index.php/transport/editRoute?id=<?php echo $row['id']; ?>"> <i class="fa fa-pencil-square-o"></i> <?php echo lang('edit'); ?> </a>
                                            <a class="btn btn-xs red" href="index.php/transport/deleteRoute?id=<?php echo $row['id']; ?>&v_no=<?php echo $row['vehicle_no']; ?>" onClick="javascript:return confirm('<?php echo "Are you sure you want to delte this Route"; ?>')"> <i class="fa fa-trash-o"></i> <?php echo lang('delete'); ?> </a>
                                        </td>
                                    </tr>
                                 <?php $i++; } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>

        <!-- BEGIN PAGE CONTENT-->
        
        
<!-- END CONTENT -->
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script src="assets/admin/pages/scripts/table-advanced.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script src="assets/admin/pages/scripts/components-dropdowns.js"></script>
<script src="assets/global/plugins/jquery.form-validator.min.js" type="text/javascript"></script>
<script> $.validate(); </script>
<script>
    jQuery(document).ready(function() {
//here is auto reload after 1 second for time and date in the top
        jQuery(setInterval(function() {
            jQuery("#result").load("index.php/home/iceTime");
        }, 1000));
    });
</script>
