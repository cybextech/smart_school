<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->

<?php $user = $this->ion_auth->user()->row(); $userId = $user->id;?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo lang('tra_at'); ?> <small></small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo lang('home'); ?>
                    </li>
                    <li>
                        <?php echo lang('header_transpo'); ?>
                    </li>
                    <li>
                        <?php echo lang('tra_at'); ?>
                    </li>
                    <li id="result" class="pull-right topClock"></li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->

        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo lang('tra_ti'); ?>
                        </div>
                        <div class="tools">
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>
                                <tr>
                                    <th>
                                        Sr. No
                                    </th>
                                    <th>
                                        Vehicle Type
                                    </th>
                                    <th>
                                        Vehicle No
                                    </th>
                                    <th>
                                       Route
                                    </th>
                                    <th>
                                        Fare
                                    </th>
                                    
                                        <th>
                                            Total Seats
                                        </th>
                                        <th>
                                            Available Seats
                                        </th>
                                   
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; $type = ""; $t_seats=""; $free_seats=""; foreach ($routes as $row) {    
                                    foreach ($vehicles as $row1)
                                    {
                                        if($row['vehicle_no'] == $row1['vehicle_no'])
                                        {
                                            $type = $row1['vehicle_type'];
                                            $t_seats = $row1['total_seats'];
                                            $free_seats = $row1['total_seats']-$row1['fill_seats'];
                                            break;
                                        }
                                    }
                                 ?> 
                                    <tr>
                                        <td>
                                            <?php echo $i; ?>
                                        </td>
                                        <td>
                                           <?php echo $type; ?> 
                                        </td>
                                        <td>
                                             <?php echo $row['vehicle_no']; ?> 
                                        </td>
                                        <td>
                                             <?php echo $row['route_start']; ?> &nbsp TO &nbsp <?php echo $row['route_end']; ?>                                 
                                         </td>
                                        <td>
                                             <?php echo $row['fare']; ?>
                                        </td>
                                        
                                            <td>
                                                <?php echo $t_seats; ?> 
                                            </td>
                                            <td>
                                                <?php echo $free_seats; ?> 
                                            </td>
                                        
                                    </tr>
                                <?php $i++;} ?>

                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->


<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<script src="assets/admin/pages/scripts/table-advanced.js"></script>
<script>
    jQuery(document).ready(function() {
    //here is auto reload after 1 second for time and date in the top
        jQuery(setInterval(function() {
            jQuery("#result").load("index.php/home/iceTime");
        }, 1000));
    });
</script>


