<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/bootstrap-datepicker/css/datepicker3.css"/>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo "Add New Driver"; ?><small></small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo lang('home'); ?>
                    </li>
                    <li>
                        <?php echo lang('header_transpo'); ?>
                    </li>
                    <li>
                        <?php echo "Drivers"; ?>
                    </li>
                    <li id="result" class="pull-right topClock"></li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12 ">
                <?php
                    if (!empty($success)) 
                    {
                        echo '<br>' . $success;
                    }
                ?>
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box green ">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo "Give Information For New Driver"; ?>
                        </div>
                        <div class="tools">
                            <a href="" class="collapse">
                            </a>
                            <a href="" class="reload">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <?php $form_attributs = array('class' => 'form-horizontal','id' => 'validate_form', 'role' => 'form');
                        echo form_open('transport/drivers', $form_attributs);
                        ?>
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo "Vehicle No"; ?> <span class="requiredStar"> * </span></label>
                                    
                                    <div class="col-md-6">
                                        <select class="form-control" name="vehicleNo" required>
                                            <option value=""><?php echo "--Select Vehicle--"; ?></option>
                                                <?php foreach ($vehicles as $row) { ?>
                                                        <option value="<?php echo $row['vehicle_no']; ?>"><?php echo $row['vehicle_no']; ?></option>
                                                <?php } ?>
                                        </select>
                                                
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo "Driver Name"; ?> <span class="requiredStar"> * </span></label>
                                    <div class="col-md-6">
                                        <input data-validation="alphanumeric" data-validation-allowing=" "  class="form-control" type="text" name="name" placeholder="" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo "Present Address"; ?> <span class="requiredStar"> * </span></label>
                                    <div class="col-md-6">
                                        <textarea class="form-control" rows="3" required name="address"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo "Permanent Address"; ?><span class="requiredStar"></span></label>
                                    <div class="col-md-6">
                                        <textarea class="form-control" rows="3" name="address2"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo lang('admi_DateOfBirth'); ?> <span class="requiredStar"> * </span></label>
                                    <div class="col-md-5">
                                        <div class="input-group input-medium date date-picker" data-date="" data-date-format="dd/mm/yyyy">
                                            <input type="text" class="form-control" id="mask_date2" name="birthdate" readonly >
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                            </span>
                                        </div>
                                        <!-- /input-group -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"> <?php echo "Mobile Number"; ?> <span class="requiredStar"> * </span></label>
                                    
                                        <div class="col-md-4">
                                            <input type="text" min="0" maxlength="11"  class="form-control" name="phone1" placeholder="03001234567" required>
                                        </div>
                                        
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"> <?php echo "License Number"; ?> <span class="requiredStar"> * </span></label>
                                    
                                        <div class="col-md-4">
                                            <input type="text" data-validation="alphanumeric" data-validation-allowing=" " class="form-control" name="license" placeholder="" required>
                                        </div>
                                        
                                </div>
                            </div>
                            <div class="form-actions fluid">
                                <div class="col-md-offset-3 col-md-6">
                                    <button type="submit" class="btn green" name="submit" value="Submit"><?php echo lang('save'); ?></button>
                                    <button type="reset" class="btn default"><?php echo lang('refresh'); ?></button>
                                </div>
                            </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 >
                  <b>  <?php echo "All Drivers"; ?></b>
                </h3>
                
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo "Drivers Information"; ?>
                        </div>
                        <div class="tools">
                            <a class="collapse" href="javascript:;">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>
                                <tr>
                                    <th>
                                        Sr. No
                                    </th>
                                    <th>
                                        Driver Name
                                    </th>
                                    <th>
                                       Vehicle Number
                                    </th>
                                    <th>
                                       Phone Number
                                    </th>
                                    <th>
                                       License Number
                                    </th>
                                    
                                        <th>
                                           Management
                                        </th>
                                   
                                </tr>
                            </thead>
                            <tbody>
                               <?php $i = 1; foreach ($drivers as $row) { ?>
                                    <tr>
                                        <td>
                                           <?php echo $i; ?>
                                        </td>
                                        <td>
                                           <?php echo $row['driver_name']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['vehicle_no']; ?>
                                        </td>
                                        <td>
                                         <?php echo $row['phone']; ?>
                                        </td>
                                        <td>
                                           <?php echo $row['license_no']; ?>
                                        </td>
                                        <td>
                                           <a class="btn btn-xs default" href="index.php/transport/editDriver?id=<?php echo $row['id']; ?>"> <i class="fa fa-pencil-square-o"></i> <?php echo lang('edit'); ?> </a>
                                            <a class="btn btn-xs red" href="index.php/transport/deleteDriver?id=<?php echo $row['id']; ?>&v_no=<?php echo $row['vehicle_no']; ?>" onClick="javascript:return confirm('<?php echo "Are you sure you want to delte this Driver"; ?>')"> <i class="fa fa-trash-o"></i> <?php echo lang('delete'); ?> </a>
                                        </td>
                                    </tr>
                                 <?php $i++; } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>

        <!-- BEGIN PAGE CONTENT-->
        
        
<!-- END CONTENT -->
<!-- BEGIN PAGE LEVEL script -->
<script type="text/javascript" src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="assets/admin/pages/scripts/components-pickers.js"></script>
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script src="assets/admin/pages/scripts/table-advanced.js"></script>

<script type="text/javascript" src="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script type="text/javascript" src="assets/global/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
<script src="assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>
<script src="assets/global/plugins/typeahead/handlebars.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/typeahead/typeahead.bundle.min.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/components-form-tools.js"></script>
<script src="assets/global/plugins/jquery.form-validator.min.js" type="text/javascript"></script>
<script> $.validate(); </script>
<script>
    jQuery(document).ready(function() {
        ComponentsFormTools.init();
         if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: Metronic.isRTL(),
                orientation: "left",
                autoclose: true
            });
        }
    });
</script>
<script type="text/javascript">
    var RecaptchaOptions = {
        theme: 'custom',
        custom_theme_widget: 'recaptcha_widget'
    };
</script>
<script>
    jQuery(document).ready(function() {
//here is auto reload after 1 second for time and date in the top
        jQuery(setInterval(function() {
            jQuery("#result").load("index.php/home/iceTime");
        }, 1000));
    });
</script>

