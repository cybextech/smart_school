<link rel="stylesheet" type="text/css" href="assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo "Add New Vehicle"; ?><small></small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo lang('home'); ?>
                    </li>
                    <li>
                        <?php echo lang('header_transpo'); ?>
                    </li>
                    <li>
                        <?php echo "Vehicle"; ?>
                    </li>
                    <li id="result" class="pull-right topClock"></li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12 ">
                <?php
                    if (!empty($success)) 
                    {
                        echo '<br>' . $success;
                    }
                ?>
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box green ">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo "Give The Information For New Vehicle"; ?>
                        </div>
                        <div class="tools">
                            <a href="" class="collapse">
                            </a>
                            <a href="" class="reload">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <?php $form_attributs = array('class' => 'form-horizontal', 'role' => 'form' ,'id' => 'validate_form');
                        echo form_open('transport/vehicle', $form_attributs);
                        ?>
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo "Vehicle No"; ?> <span class="requiredStar"> * </span></label>
                                    <div class="col-md-6">
                                        <input data-validation="alphanumeric" data-validation-allowing=" " class="form-control" type="text" name="vehicleNo" placeholder="e.g LEC4031" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo "Vehicle type"; ?> <span class="requiredStar"> * </span></label>
                                    <div class="col-md-6">
                                        <input data-validation="alphanumeric" data-validation-allowing=" " class="form-control" type="text" name="vehicleType" placeholder="e.g pickup, Bus, Van" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo "No of seats"; ?> <span class="requiredStar"> * </span></label>
                                    <div class="col-md-6">
                                        <input  class="form-control" type="number" name="seatAmount" placeholder="" required min="0">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo "Vehicle Information"; ?><span class="requiredStar">*</span></label>
                                    <div class="col-md-6">
                                        <textarea class="form-control" rows="3" required name="description"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions fluid">
                                <div class="col-md-offset-3 col-md-6">
                                    <button type="submit" class="btn green" name="submit" value="Submit"><?php echo lang('save'); ?></button>
                                    <button type="reset" class="btn default"><?php echo lang('refresh'); ?></button>
                                </div>
                            </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 >
                  <b>  <?php echo "All Vehicles"; ?></b>
                </h3>
                
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo "Vehicles Information"; ?>
                        </div>
                        <div class="tools">
                            <a class="collapse" href="javascript:;">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>
                                <tr>
                                    <th>
                                        Sr. No
                                    </th>
                                    <th>
                                        Vehicle Number
                                    </th>
                                    <th>
                                       Vehicle Type
                                    </th>
                                    <th>
                                       Total Seats
                                    </th>
                                    <th>
                                       Empty Seats
                                    </th>
                                    
                                        <th>
                                           Management
                                        </th>
                                   
                                </tr>
                            </thead>
                            <tbody>
                               <?php $i = 1; foreach ($vehicles as $row) { ?>
                                    <tr>
                                        <td>
                                           <?php echo $i; ?>
                                        </td>
                                        <td>
                                           <?php echo $row['vehicle_no']; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['vehicle_type']; ?>
                                        </td>
                                        <td>
                                         <?php echo $row['total_seats']; ?>
                                        </td>
                                        <td>
                                           <?php echo ($row['total_seats']-$row['fill_seats']); ?>
                                        </td>
                                        <td>
                                           <a class="btn btn-xs default" href="index.php/transport/editVehicle?id=<?php echo $row['id']; ?>"> <i class="fa fa-pencil-square-o"></i> <?php echo lang('edit'); ?> </a>
                                            <a class="btn btn-xs red" href="index.php/transport/deleteVehicle?id=<?php echo $row['id']; ?>&v_no=<?php echo $row['vehicle_no']; ?>" onClick="javascript:return confirm('<?php echo "Are you sure you want to delte this vehicle"; ?>')"> <i class="fa fa-trash-o"></i> <?php echo lang('delete'); ?> </a>
                                        </td>
                                    </tr>
                                 <?php $i++; } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>

        <!-- BEGIN PAGE CONTENT-->
        
        
<!-- END CONTENT -->
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script src="assets/admin/pages/scripts/table-advanced.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script src="assets/admin/pages/scripts/components-dropdowns.js"></script>
<script src="assets/global/plugins/jquery.form-validator.min.js" type="text/javascript"></script>
<script> $.validate(); </script>
<script>
    jQuery(document).ready(function() {
//here is auto reload after 1 second for time and date in the top
        jQuery(setInterval(function() {
            jQuery("#result").load("index.php/home/iceTime");
        }, 1000));
    });
</script>
