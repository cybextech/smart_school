<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

Class Transport extends CI_Controller {

    /**
     * This controller is using for control transport
     *
     * Maps to the following URL
     * 		http://example.com/index.php/transport
     * 	- or -  
     * 		http://example.com/index.php/transport/<method_name>
     */
    public function __construct() {
        parent::__construct();
        $this->lang->load('auth');
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login');
        }
    }

    //This function is using for adding Drivers and their information.
    public function drivers() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8)))
        {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        if ($this->input->post('submit', TRUE)) {
            $driverInfo = array(
                'vehicle_no' => $this->db->escape_like_str($this->input->post('vehicleNo', TRUE)),
                'license_no' => $this->db->escape_like_str($this->input->post('license', TRUE)),
                'driver_name' => $this->db->escape_like_str($this->input->post('name', TRUE)),
                'present_address' => $this->db->escape_like_str($this->input->post('address', TRUE)),
                'permanent_address' => $this->db->escape_like_str($this->input->post('address2', TRUE)),
                'birth_date' => $this->db->escape_like_str($this->input->post('birthdate', TRUE)),
                'phone' => $this->db->escape_like_str($this->input->post('phone1', TRUE)),
                'parient_id' => $this->db->escape_like_str($parient_id),
            );
            //now submit data into database and load view.
            $exit = $this->common->getWhere('drivers','license_no', $this->input->post('license', TRUE));
            if(empty($exit))
            {
                if ($this->db->insert('drivers', $driverInfo)) 
                {
                    $status = array(
                        'status' => $this->db->escape_like_str('1'),
                    );
                    $this->db->where('vehicle_no', $this->input->post('vehicleNo', TRUE));
                    if ($this->db->update('vehicle', $status)) 
                    {
                        $data['success'] = '<div class="alert alert-info alert-dismissable admisionSucceassMessageFont">
                                                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                                        <strong>Success!</strong> '.lang('trans_1').'
                                                                </div>';
                        $data['vehicles'] = $this->common->getWhere22('vehicle','parient_id',$parient_id,'status','0');
                        $data['drivers'] = $this->common->getAllData('drivers',$parient_id);
                        $this->load->view('temp/header');
                        $this->load->view('drivers',$data);
                        $this->load->view('temp/footer');
                    }
                }
            }
            else
            {
                $data['success'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                                                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                                        <strong>Error!</strong>'.lang('trans_2').'.
                                                                </div>';
                        $data['vehicles'] = $this->common->getWhere22('vehicle','parient_id',$parient_id,'status','0');
                        $data['drivers'] = $this->common->getAllData('drivers',$parient_id);
                        $this->load->view('temp/header');
                        $this->load->view('drivers',$data);
                        $this->load->view('temp/footer');
            }
        } else {
            $data['vehicles'] = $this->common->getWhere22('vehicle','parient_id',$parient_id,'status','0');
            $data['drivers'] = $this->common->getAllData('drivers',$parient_id);
            $this->load->view('temp/header');
            $this->load->view('drivers',$data);
            $this->load->view('temp/footer');
        }
    }

    //This function is using for adding Vehicles and it's informations.
    public function vehicle() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8)))
        {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        if ($this->input->post('submit', TRUE)) {
            $exit = $this->common->getWhere('vehicle','vehicle_no', $this->input->post('vehicleNo', TRUE));
            if(empty($exit))
            {
               $vehicleInfo = array(
                'vehicle_no' => $this->db->escape_like_str($this->input->post('vehicleNo', TRUE)),
                'vehicle_type' => $this->db->escape_like_str($this->input->post('vehicleType', TRUE)),
                'total_seats' => $this->db->escape_like_str($this->input->post('seatAmount', TRUE)),
                'vehicle_info' => $this->db->escape_like_str($this->input->post('description', TRUE)),
                'parient_id' => $this->db->escape_like_str($parient_id),
                );
                //now submit data into database and load view.
                if ($this->db->insert('vehicle', $vehicleInfo)) {
                    $data['success'] = '<div class="alert alert-info alert-dismissable admisionSucceassMessageFont">
                                                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                                        <strong>Success!</strong>'.lang('trans_3').'
                                                                </div>';
                    $data['vehicles'] = $this->common->getAllData('vehicle',$parient_id);
                    $this->load->view('temp/header');
                    $this->load->view('vehicle',$data);
                    $this->load->view('temp/footer');
                } 
            }
            else
            {
                $data['success'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                                                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                                        <strong>Error!</strong> '.lang('trans_4').'
                                                                </div>';
                $data['vehicles'] = $this->common->getAllData('vehicle',$parient_id);
                $this->load->view('temp/header');
                $this->load->view('vehicle',$data);
                $this->load->view('temp/footer');
            }
            
        } else {
            $data['vehicles'] = $this->common->getAllData('vehicle',$parient_id);
            $this->load->view('temp/header');
            $this->load->view('vehicle',$data);
            $this->load->view('temp/footer');
        }
    }

    //This function is using for adding Routes and thier information.
    public function routes() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8)))
        {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        if ($this->input->post('submit', TRUE)) {
            //$exit = $this->common->getWhere('routes','route_code', $this->input->post('routeCode', TRUE));
            // if(empty($exit))
            // {
                $routeInfo = array(
                    'vehicle_no' => $this->db->escape_like_str($this->input->post('vehicleNo', TRUE)),
                    'route_code' => $this->db->escape_like_str($this->input->post('routeCode', TRUE)),
                    'route_start' => $this->db->escape_like_str($this->input->post('start', TRUE)),
                    'route_end' => $this->db->escape_like_str($this->input->post('stop', TRUE)),
                    'fare' => $this->db->escape_like_str($this->input->post('fare', TRUE)),
                    'parient_id' => $this->db->escape_like_str($parient_id),
                );
                //now submit data into database and load view.
                if ($this->db->insert('routes', $routeInfo)) {
                    $status = array(
                        'on_route' => $this->db->escape_like_str('1'),
                    );
                    $this->db->where('vehicle_no', $this->input->post('vehicleNo', TRUE));
                    if ($this->db->update('vehicle', $status)) 
                    {
                        $data['success'] = '<div class="alert alert-info alert-dismissable admisionSucceassMessageFont">
                                                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                                        <strong>Success!</strong> '.lang('trans_5').'
                                                                </div>';
                       $data['vehicles'] = $this->common->getWhere22('vehicle','parient_id',$parient_id,'on_route','0');
                        $data['routes'] = $this->common->getAllData('routes',$parient_id);
                        $this->load->view('temp/header');
                        $this->load->view('routes',$data);
                        $this->load->view('temp/footer');
                    }
                }
            // }
            // else
            // {
            //     $data['success'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
            //                                                             <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
            //                                                             <strong>Error!</strong>'.lang('trans_6').'
            //                                                     </div>';
            //            $data['vehicles'] = $this->common->getWhere22('vehicle','parient_id',$parient_id,'on_route','0');
            //             $data['routes'] = $this->common->getAllData('routes',$parient_id);
            //             $this->load->view('temp/header');
            //             $this->load->view('routes',$data);
            //             $this->load->view('temp/footer');
            // }
            
        } else {
            $data['vehicles'] = $this->common->getWhere22('vehicle','parient_id',$parient_id,'on_route','0');
            $data['routes'] = $this->common->getAllData('routes',$parient_id);
            $this->load->view('temp/header');
            $this->load->view('routes',$data);
            $this->load->view('temp/footer');
        }
    }
    // This function is showing all transport
    public function allTransport() {
        $parient_id = $this->session->userdata("user_id");
        if(!$this->ion_auth->is_admin() )
        {
            $parient_id = $this->session->userdata("parient_id");
        }
        $data['vehicles'] = $this->common->getWhere('vehicle','parient_id',$parient_id);
        $data['routes'] = $this->common->getAllData('routes',$parient_id);
        // $data['transport'] = $this->common->getAllData('transport');
        $this->load->view('temp/header');
        $this->load->view('allTransport', $data);
        $this->load->view('temp/footer');
    }

    //To edit vehicle information
    public function editVehicle() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8)))
        {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $id = $this->input->get('id');
        if ($this->input->post('submit', TRUE)) {
            $vehicle_nmbr_upd = $this->input->post('vehicleNo', TRUE);
            $exit = $this->common->getWhere('vehicle','vehicle_no', $this->input->post('vehicleNo', TRUE));
            $v_nmbr = $this->input->post('v_no', TRUE);
            if(empty($exit) || $v_nmbr==$this->input->post('vehicleNo', TRUE))
            {
               $vehicleInfo = array(
                'vehicle_no' => $this->db->escape_like_str($this->input->post('vehicleNo', TRUE)),
                'vehicle_type' => $this->db->escape_like_str($this->input->post('vehicleType', TRUE)),
                'total_seats' => $this->db->escape_like_str($this->input->post('seatAmount', TRUE)),
                'vehicle_info' => $this->db->escape_like_str($this->input->post('description', TRUE)),
                'parient_id' => $this->db->escape_like_str($parient_id),
                );
               $nmbrinfo = array('vehicle_no' => $this->db->escape_like_str($this->input->post('vehicleNo', TRUE)),);
                //now submit data into database and load view.
                $this->db->where('id', $id);
                if ($this->db->update('vehicle', $vehicleInfo)) 
                {
                    $this->db->where('vehicle_no', $v_nmbr);
                    if($this->db->update('routes', $nmbrinfo))
                    {
                        $this->db->where('vehicle_no', $v_nmbr);
                        if($this->db->update('drivers', $nmbrinfo))
                        {
                            redirect('transport/vehicle', 'refresh');
                        }  
                    }   
                }
            }
            else
            {
                $data['success'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                                                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                                        <strong>Error!</strong>'.lang('trans_4').'
                                                                </div>';
                $data['vehicles'] = $this->common->getAllData('vehicle',$parient_id);
                $this->load->view('temp/header');
                $this->load->view('editVehicle',$data);
                $this->load->view('temp/footer');
            }
        } else {
            $data['vehicles'] = $this->common->getWhere('vehicle','parient_id',$parient_id);
            $this->load->view('temp/header');
            $this->load->view('editVehicle', $data);
            $this->load->view('temp/footer');
        }
    }

    //THis function is using to delete Transport
    public function deleteVehicle() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8)))
        {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $id = $this->input->get('id');
        $v_no = $this->input->get('v_no');
        if ($this->db->delete('vehicle', array('id' => $id))) 
        {
            if($this->db->delete('drivers', array('vehicle_no' => $v_no)))
            {
                if($this->db->delete('routes', array('vehicle_no' => $v_no)))
                {
                    redirect('transport/vehicle', 'refresh');
                } 
            }          
        }
    }

    //To edit Drivers information
    public function editDriver() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8)))
        {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $id = $this->input->get('id');
        if ($this->input->post('submit', TRUE)) {
            $vehicle_nmbr_upd = $this->input->post('vehicleNo', TRUE);
            $exit = $this->common->getWhere('drivers','license_no', $this->input->post('license', TRUE));
            $l_nmbr = $this->input->post('l_no', TRUE);
            $v_nmbr = $this->input->post('v_no', TRUE);
            if(empty($exit) || $l_nmbr == $this->input->post('license', TRUE))
            {
               $driverInfo = array(
                    'vehicle_no' => $this->db->escape_like_str($this->input->post('vehicleNo', TRUE)),
                    'license_no' => $this->db->escape_like_str($this->input->post('license', TRUE)),
                    'driver_name' => $this->db->escape_like_str($this->input->post('name', TRUE)),
                    'present_address' => $this->db->escape_like_str($this->input->post('address', TRUE)),
                    'permanent_address' => $this->db->escape_like_str($this->input->post('address2', TRUE)),
                    'birth_date' => $this->db->escape_like_str($this->input->post('birthdate', TRUE)),
                    'phone' => $this->db->escape_like_str($this->input->post('phone1', TRUE)),
                    'parient_id' => $this->db->escape_like_str($parient_id),
                );
               $nmbrinfo = array('vehicle_no' => $this->db->escape_like_str($this->input->post('vehicleNo', TRUE)),);
                //now submit data into database and load view.
                $this->db->where('id', $id);
                if ($this->db->update('drivers', $driverInfo)) 
                {
                   if($v_nmbr != $vehicle_nmbr_upd)
                   {
                        $status = array(
                            'status' => $this->db->escape_like_str('1'),
                        );
                        $status1 = array(
                            'status' => $this->db->escape_like_str('0'),
                        );
                        $this->db->where('vehicle_no', $this->input->post('vehicleNo', TRUE));
                        if($this->db->update('vehicle', $status))
                        {
                            $this->db->where('vehicle_no', $v_nmbr);
                            if($this->db->update('vehicle', $status1))
                            {
                               redirect('transport/drivers', 'refresh');
                            }
                        }
                   }
                   else
                   {
                        redirect('transport/drivers', 'refresh');
                   }
                }
            }
            else
            {
                $data['success'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                                                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                                        <strong>Error!</strong>'.lang('trans_2').'
                                                                </div>';
                $data['vehicles'] = $this->common->getWhere22('vehicle','parient_id',$parient_id,'status','0');
                $data['drivers'] = $this->common->getAllData('drivers',$parient_id);
                $this->load->view('temp/header');
                $this->load->view('editDriver',$data);
                $this->load->view('temp/footer');
            }
        } else {
            $data['vehicles'] = $this->common->getWhere22('vehicle','parient_id',$parient_id,'status','0');
            $data['drivers'] = $this->common->getAllData('drivers',$parient_id);
            $this->load->view('temp/header');
            $this->load->view('editDriver', $data);
            $this->load->view('temp/footer');
        }
    }

    public function deleteDriver() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8)))
        {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $id = $this->input->get('id');
        $v_no = $this->input->get('v_no');
        if ($this->db->delete('drivers', array('id' => $id))) 
        {
            $status = array('status' => $this->db->escape_like_str('0'));
            $this->db->where('vehicle_no', $v_no);
            if($this->db->update('vehicle', $status))
            {
                redirect('transport/drivers', 'refresh');
            }   
        }
    }

    //To edit Drivers information
    public function editRoute() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8)))
        {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $id = $this->input->get('id');
        if ($this->input->post('submit', TRUE)) {
            $vehicle_nmbr_upd = $this->input->post('vehicleNo', TRUE);
            $exit = $this->common->getWhere('routes','route_code', $this->input->post('routeCode', TRUE));
            $r_code = $this->input->post('r_code', TRUE);
            $v_nmbr = $this->input->post('v_no', TRUE);
            if(empty($exit) || $r_code == $this->input->post('routeCode', TRUE))
            {
               $routeInfo = array(
                    'vehicle_no' => $this->db->escape_like_str($this->input->post('vehicleNo', TRUE)),
                    'route_code' => $this->db->escape_like_str($this->input->post('routeCode', TRUE)),
                    'route_start' => $this->db->escape_like_str($this->input->post('start', TRUE)),
                    'route_end' => $this->db->escape_like_str($this->input->post('stop', TRUE)),
                    'fare' => $this->db->escape_like_str($this->input->post('fare', TRUE)),
                    'parient_id' => $this->db->escape_like_str($parient_id),
                );
               $nmbrinfo = array('vehicle_no' => $this->db->escape_like_str($this->input->post('vehicleNo', TRUE)),);
                //now submit data into database and load view.
                $this->db->where('id', $id);
                if ($this->db->update('routes', $routeInfo)) 
                {
                   if($v_nmbr != $vehicle_nmbr_upd)
                   {
                        $status = array(
                            'on_route' => $this->db->escape_like_str('1'),
                        );
                        $status1 = array(
                            'on_route' => $this->db->escape_like_str('0'),
                        );
                        $this->db->where('vehicle_no', $this->input->post('vehicleNo', TRUE));
                        if($this->db->update('vehicle', $status))
                        {
                            $this->db->where('vehicle_no', $v_nmbr);
                            if($this->db->update('vehicle', $status1))
                            {
                               redirect('transport/routes', 'refresh'); 
                            }
                        }
                   }
                   else
                   {
                        redirect('transport/routes', 'refresh');
                   }
                }
            }
            else
            {
                $data['success'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                                                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                                        <strong>Error!</strong> '.lang('trans_6').'
                                                                </div>';
                $data['vehicles'] = $this->common->getWhere22('vehicle','parient_id',$parient_id,'on_route','0');
                $data['routes'] = $this->common->getAllData('routes',$parient_id);
                $this->load->view('temp/header');
                $this->load->view('editRoute',$data);
                $this->load->view('temp/footer');
            }
        } else {
            $data['vehicles'] = $this->common->getWhere22('vehicle','parient_id',$parient_id,'on_route','0');
            $data['routes'] = $this->common->getAllData('routes',$parient_id);
            $this->load->view('temp/header');
            $this->load->view('editRoute', $data);
            $this->load->view('temp/footer');
        }
    }

    public function deleteRoute() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8)))
        {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $id = $this->input->get('id');
        $v_no = $this->input->get('v_no');
        if ($this->db->delete('routes', array('id' => $id))) 
        {
            $status = array('on_route' => $this->db->escape_like_str('0'));
            $this->db->where('vehicle_no', $v_no);
            if($this->db->update('vehicle', $status))
            {
                redirect('transport/routes', 'refresh');
            }   
        }
    }

}
