<!-- BEGIN CONTENT -->
<link rel="stylesheet" type="text/css" href="assets/global/plugins/timepickerr/wickedpicker.css"/>
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo lang('header_set_time'); ?><small></small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo lang('home'); ?>
                    </li>
                    <li>
                        <?php echo lang('header_academic'); ?>
                    </li>
                    <li>
                        <?php echo lang('header_routin'); ?>
                        
                    </li>
                    <li>
                        <?php echo lang('header_set_time'); ?>
                    </li>
                    <li id="result" class="pull-right topClock"></li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12 ">
            <?php
                if (!empty($success)) {
                    echo $success;
                }
                if (!empty($success1)) {
                    echo $success1;
                }
                ?>
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box green ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-bars"></i> <?php echo lang('clas_rout_for'); ?> <?php echo $classTile." ".$sectionTitle; ?>
                        </div>
                        <div class="tools">
                            <a href="" class="collapse">
                            </a>
                            <a href="" class="reload">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <?php $form_attributs = array('class' => 'form-horizontal', 'role' => 'form');
                        echo form_open('sclass/addClassRoutin', $form_attributs);
                        ?>
                            <div class="form-body">
                                <div class="alert alert-info marginBottomNone">
                                    <div class="form-group marginBottomNone">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <input type="hidden" name="class" value="<?php echo $class_id; ?>">
                                                <input type="hidden" name="section" value="<?php echo $section_id; ?>">
                                                <div class="col-md-2 routineFildMarginBottom">
                                                    <select class="form-control" name="day" data-validation="required" data-validation-error-msg="">
                                                        <option value=""><?php echo lang('clas_select_day'); ?></option>
                                                            <option <?php if($day[0]['monday'] == "Holyday"){echo 'disabled=""';}?> class="<?php echo $day[0]['monday']; ?>">Monday</option>
                                                            <option <?php if($day[0]['tuesday'] == "Holyday"){echo 'disabled=""';}?> class="<?php echo $day[0]['tuesday']; ?>">Tuesday</option>
                                                            <option <?php if($day[0]['wednesday'] == "Holyday"){echo 'disabled=""';}?> class="<?php echo $day[0]['wednesday']; ?>">Wednesday</option>
                                                            <option <?php if($day[0]['thursday'] == "Holyday"){echo 'disabled=""';}?> class="<?php echo $day[0]['thursday']; ?>">Thursday</option>
                                                            <option <?php if($day[0]['friday'] == "Holyday"){echo 'disabled=""';}?> class="<?php echo $day[0]['friday']; ?>">Friday</option>
                                                            <option <?php if($day[0]['saturday'] == "Holyday"){echo 'disabled=""';}?> class="<?php echo $day[0]['saturday']; ?>">Saturday</option>
                                                            <option <?php if($day[0]['sunday'] == "Holyday"){echo 'disabled=""';}?> class="<?php echo $day[0]['sunday']; ?>">Sunday</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-2 routineFildMarginBottom">
                                                    <select class="form-control" name="subject" data-validation="required" data-validation-error-msg="">
                                                        <option value=""><?php echo lang('clas_select_subject'); ?></option>
                                                        <?php foreach ($subject as $row1) { ?>
                                                            <option><?php echo $row1['subject_title'] ?></option>
<?php } ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-2 routineFildMarginBottom">
                                                    <select class="form-control" name="teacher" data-validation="required" data-validation-error-msg="">
                                                        <option value=""><?php echo lang('clas_select_teacher'); ?></option>
                                                        <?php foreach ($teacher as $row2) { ?>
                                                            <option value="<?php echo $row2['id'] ?>"><?php echo $row2['fullname'] ?></option>
<?php } ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-2 routineFildMarginBottom" >
                                                    <input type="time" id="timepicker1" class="form-control" placeholder="<?php echo lang('clas_start_time_plas'); ?>" name="startTime" data-validation="required" data-validation-error-msg="">
                                                </div>
                                                <div class="col-md-2 routineFildMarginBottom">
                                                    <input type="time" id="timepicker2" class="form-control input-small" placeholder="<?php echo lang('clas_end_time_plas'); ?>" name="endTime" data-validation="required" data-validation-error-msg="">
                                                </div>
                                                <div class="col-md-2 routineFildMarginBottom">
                                                    <input type="text" data-validation="alphanumeric" data-validation-allowing=" " class="form-control" placeholder="<?php echo lang('clas_roon_no_plas'); ?>" name="roomNumber" data-validation="required" data-validation-error-msg="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions fluid marginTopNone">
                                <div class="col-md-offset-3 col-md-9">
                                    <button class="btn blue" type="submit" name="submit2" value="Add Routine Subject"><?php echo lang('clas_add_button'); ?></button>
                                    <button class="btn default" type="reset"><?php echo lang('clas_rest_button'); ?></button>
                                </div>
                            </div>
                        <?php echo form_close(); ?>

                        <div class="alert alert-warning">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <?php echo $classTile." ".$sectionTitle;?> <?php echo lang('clas_routine'); ?>
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse">
                                        </a>
                                        <a href="javascript:;" class="reload">
                                        </a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <?php
                                    $monday  = $day[0]['monday'];
                                    $tuesday = $day[0]['tuesday'];
                                    $wednesday = $day[0]['wednesday'];
                                    $thursday = $day[0]['thursday'];
                                    $friday = $day[0]['friday'];
                                    $saturday = $day[0]['saturday'];
                                    $sunday = $day[0]['sunday'];
                                        ?>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="col-sm-2 day <?php echo $monday; ?>">
                                                Monday
                                                </div>
                                                <?php
                                                //$query = array();
                                                $query = $this->classmodel->getWhere('class_routine', 'day_title', 'Monday','section',$section_id);
                                                foreach ($query as $row4) {
                                                    ?>
                                                    <div class="col-sm-2 subject">
                                                        <p><?php echo $row4['subject']; ?></p>
                                                        <p><?php echo $this->common->teacher_title($row4['subject_teacher']); ?></p>
                                                        <p class="pFontSize"><?php echo $row4['start_time']; ?> - <?php echo $row4['end_time']; ?></p>
                                                        <p class="pFontSize"><?php echo lang('dor_room').": ". $row4['room_number']; ?></p>
                                                    </div>
                                        <?php } ?>
                                            </div>
                                        </div>
                                         <div class="row">
                                            <div class="col-md-12">
                                                <div class="col-sm-2 day <?php echo $tuesday; ?>">
                                                Tuesday
                                                </div>
                                                <?php
                                                //$query = array();
                                                $query = $this->classmodel->getWhere('class_routine', 'day_title', 'Tuesday','section', $section_id);
                                                foreach ($query as $row4) {
                                                    ?>
                                                    <div class="col-sm-2 subject">
                                                        <p><?php echo $row4['subject']; ?></p>
                                                        <p><?php echo $this->common->teacher_title($row4['subject_teacher']); ?></p>
                                                        <p class="pFontSize"><?php echo $row4['start_time']; ?> - <?php echo $row4['end_time']; ?></p>
                                                        <p class="pFontSize"><?php echo lang('dor_room').": ". $row4['room_number']; ?></p>
                                                    </div>
                                        <?php } ?>
                                            </div>
                                        </div>
                                         <div class="row">
                                            <div class="col-md-12">
                                                <div class="col-sm-2 day <?php echo $wednesday; ?>">
                                                Wednesday
                                                </div>
                                                <?php
                                                //$query = array();
                                                $query = $this->classmodel->getWhere('class_routine', 'day_title', 'Wednesday','section', $section_id);
                                                foreach ($query as $row4) {
                                                    ?>
                                                    <div class="col-sm-2 subject">
                                                        <p><?php echo $row4['subject']; ?></p>
                                                        <p><?php echo $this->common->teacher_title($row4['subject_teacher']); ?></p>
                                                        <p class="pFontSize"><?php echo $row4['start_time']; ?> - <?php echo $row4['end_time']; ?></p>
                                                        <p class="pFontSize"><?php echo lang('dor_room').": ".  $row4['room_number']; ?></p>
                                                    </div>
                                        <?php } ?>
                                            </div>
                                        </div>
                                         <div class="row">
                                            <div class="col-md-12">
                                                <div class="col-sm-2 day <?php echo $thursday; ?>">
                                                Thursday
                                                </div>
                                                <?php
                                                //$query = array();
                                                $query = $this->classmodel->getWhere('class_routine', 'day_title', 'Thursday','section', $section_id);
                                                foreach ($query as $row4) {
                                                    ?>
                                                    <div class="col-sm-2 subject">
                                                        <p><?php echo $row4['subject']; ?></p>
                                                        <p><?php echo $this->common->teacher_title($row4['subject_teacher']); ?></p>
                                                        <p class="pFontSize"><?php echo $row4['start_time']; ?> - <?php echo $row4['end_time']; ?></p>
                                                        <p class="pFontSize"><?php echo lang('dor_room').": ".  $row4['room_number']; ?></p>
                                                    </div>
                                        <?php } ?>
                                            </div>
                                        </div>
                                         <div class="row">
                                            <div class="col-md-12">
                                                <div class="col-sm-2 day <?php echo $friday; ?>">
                                                Friday
                                                </div>
                                                <?php
                                                //$query = array();
                                                $query = $this->classmodel->getWhere('class_routine', 'day_title', 'Friday', 'section', $section_id);
                                                foreach ($query as $row4) {
                                                    ?>
                                                    <div class="col-sm-2 subject">
                                                        <p><?php echo $row4['subject']; ?></p>
                                                        <p><?php echo $this->common->teacher_title($row4['subject_teacher']); ?></p>
                                                        <p class="pFontSize"><?php echo $row4['start_time']; ?> - <?php echo $row4['end_time']; ?></p>
                                                        <p class="pFontSize"><?php echo lang('dor_room').": ".  $row4['room_number']; ?></p>
                                                    </div>
                                        <?php } ?>
                                            </div>
                                        </div>
                                         <div class="row">
                                            <div class="col-md-12">
                                                <div class="col-sm-2 day <?php echo $saturday; ?>">
                                                Saturday
                                                </div>
                                                <?php
                                                //$query = array();
                                                $query = $this->classmodel->getWhere('class_routine', 'day_title', 'Saturday', 'section', $section_id);
                                                foreach ($query as $row4) {
                                                    ?>
                                                    <div class="col-sm-2 subject">
                                                        <p><?php echo $row4['subject']; ?></p>
                                                        <p><?php echo $this->common->teacher_title($row4['subject_teacher']); ?></p>
                                                        <p class="pFontSize"><?php echo $row4['start_time']; ?> - <?php echo $row4['end_time']; ?></p>
                                                        <p class="pFontSize"><?php echo lang('dor_room').": ".  $row4['room_number']; ?></p>
                                                    </div>
                                        <?php } ?>
                                            </div>
                                        </div>
                                         <div class="row">
                                            <div class="col-md-12">
                                                <div class="col-sm-2 day <?php echo $sunday; ?>">
                                                Sunday
                                                </div>
                                                <?php
                                                //$query = array();
                                                $query = $this->classmodel->getWhere('class_routine', 'day_title', 'Sunday', 'section', $section_id);
                                                foreach ($query as $row4) {
                                                    ?>
                                                    <div class="col-sm-2 subject">
                                                        <p><?php echo $row4['subject']; ?></p>
                                                        <p><?php echo $this->common->teacher_title($row4['subject_teacher']); ?></p>
                                                        <p class="pFontSize"><?php echo $row4['start_time']; ?> - <?php echo $row4['end_time']; ?></p>
                                                        <p class="pFontSize"><?php echo lang('dor_room').": ".  $row4['room_number']; ?></p>
                                                    </div>
                                        <?php } ?>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->
<!-- BEGIN PAGE LEVEL PLUGINS --><!-- 
<script type="text/javascript" src="assets/global/plugins/timepickerr/wickedpicker.js"></script> -->
<script type="text/javascript" src="assets/global/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
<script src="assets/global/plugins/jquery.form-validator.min.js" type="text/javascript"></script><!-- 
<script type="text/javascript" src="assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> -->
<script> $.validate(); </script>
<script type="text/javascript">
           //  $('#timepicker1').timepicker();
           // $('#timepicker2').timepicker();
           // $('#timepicker1').wickedpicker();
           // $('#timepicker2').wickedpicker();
           // var options = {twentyFour: false, upArrow: 'wickedpicker__controls__control-up',  downArrow: 'wickedpicker__controls__control-down',  close: 'wickedpicker__close', hoverState:'hover-state',title: 'Timepicker',  showSeconds: false, secondsInterval: 1, minutesInterval: 1,  beforeShow: null,  show: null,  clearable: true,  }; 
           // $('#timepicker1').wickedpicker(options);
           // $('#timepicker2').wickedpicker(options);
    </script>
<script>
    jQuery(document).ready(function() {
//here is auto reload after 1 second for time and date in the top
        jQuery(setInterval(function() {
            jQuery("#result").load("index.php/home/iceTime");
        }, 1000));
    });
</script>