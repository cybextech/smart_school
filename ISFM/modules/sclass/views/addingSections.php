<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo lang('clas_add_section'); ?> <small></small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo lang('home'); ?>
                        
                    </li>
                    <li>
                        <?php echo lang('header_academic'); ?>
                        
                    </li>
                    <li>
                        <?php echo lang('header_cor_clas'); ?>
                    </li>
                    <li>
                        <?php echo lang('clas_add_section'); ?>
                    </li>
                    <li id="result" class="pull-right topClock"></li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12 ">
              <?php
              $cls_title = $this->common->class_title1($classId);
                if (!empty($success)) {
                    echo $success;
                }
                ?>
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box green ">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo lang('add_new_section')." Of ".$cls_title; ?>
                        </div>
                        <div class="tools">
                            <a href="" class="collapse">
                            </a>
                            <a href="" class="reload">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <?php $form_attributs = array('class' => 'form-horizontal', 'role' => 'form');
                        echo form_open('sclass/addSection', $form_attributs);
                        ?>
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-3 control-label"> <?php echo lang('clas_section'); ?> <span class="requiredStar">*</span></label>
                                    <div class="col-md-6">
                                        <div id="section_div">
                                            <input type="text" data-validation="required" data-validation-error-msg="" pattern="[A-Za-z0-9\s]{1,20}" title="alphanumerics and spaces only" name="section" class="form-control classGroupInput" placeholder="">
                                             <input type="hidden" name="class_id" value="<?php echo $classId; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo lang('clas_sec_capaci'); ?><span class="requiredStar"> * </span></label>
                                    <div class="col-md-6">
                                        <select name="capicity" class="form-control"  data-validation="required" data-validation-error-msg="Select section's student capacity.">
                                            <option value=""><?php echo lang('select'); ?></option>
                                            <option value="10"><?php echo lang('clas_10stu'); ?></option>
                                            <option value="20"><?php echo lang('clas_20stu'); ?></option>
                                            <option value="30"><?php echo lang('clas_30stu'); ?></option>
                                            <option value="40"><?php echo lang('clas_40stu'); ?></option>
                                            <option value="50"><?php echo lang('clas_50stu'); ?></option>
                                            <option value="60"><?php echo lang('clas_60stu'); ?></option>
                                            <option value="70"><?php echo lang('clas_70stu'); ?></option>
                                            <option value="80"><?php echo lang('clas_80stu'); ?></option>
                                            <option value="90"><?php echo lang('clas_90stu'); ?></option>
                                            <option value="100"><?php echo lang('clas_100stu'); ?></option>
                                            <option value="150"><?php echo lang('clas_150stu'); ?></option>
                                            <option value="200"><?php echo lang('clas_200stu'); ?></option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions fluid">
                                <div class="col-md-offset-3 col-md-6">
                                    <button type="submit" name="submit" class="btn green" value="Add Class"><?php echo lang('clas_add_section'); ?></button>
                                    <button type="reset" class="btn default"><?php echo lang('refresh'); ?></button>
                                </div>
                            </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->
<script>
   
    jQuery(document).ready(function() {
//here is auto reload after 1 second for time and date in the top
        jQuery(setInterval(function() {
            jQuery("#result").load("index.php/home/iceTime");
        }, 1000));
    });
</script>
<script src="assets/global/plugins/jquery.form-validator.min.js" type="text/javascript"></script>
<script> $.validate(); </script>