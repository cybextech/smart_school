<!-- BEGIN CONTENT --><!-- 
<link rel="stylesheet" type="text/css" href="assets/global/plugins/timepickerr/wickedpicker.css"/> -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo lang('clas_edi_cla_rout'); ?><small></small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo lang('home'); ?>
                        
                    </li>
                    <li>
                        <?php echo lang('header_academic'); ?>
                        
                    </li>
                    <li>
                        <?php echo lang('header_routin'); ?>
                        
                    </li>
                    <li>
                        <?php echo lang('header_stu_timble'); ?>
                    </li>

                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12 ">
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box green ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-bars"></i> <?php echo lang('clas_edi_rou_for');
                            $class_id = $this->input->get('class');
                            echo " ".$this->common->class_title($class_id)." ".$this->common->section_title($class_id);
                            ?>
                        </div>
                        <div class="tools">
                            <a href="" class="collapse">
                            </a>
                            <a href="" class="reload">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <?php
                        $form_attributs = array('class' => 'form-horizontal', 'role' => 'form');
                        $id = $this->input->get('id');
                        echo form_open("sclass/editRoutine?id=$id&class=$class_id", $form_attributs);
                        ?>
                        <div class="form-body">
                            <div class="alert alert-info marginBottomNone">
                                <div class="form-group marginBottomNone">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <?php
                                            if (!empty($message)) {
                                                echo $message;
                                            }
                                            if (!empty($success1)) {
                                                echo $success1;
                                            }
                                            ?>
                                            <?php foreach ($previousRoutin as $routin) { ?>
                                                <input type="hidden" name="class" value="<?php
                                            echo $this->common->class_title($class_id);
                                            ?>">
                                                <div class="col-md-2 routineFildMarginBottom">
                                                    <select class="form-control" name="day" required="">
                                                    <option <?php if($day[0]['monday'] == "Holyday"){echo 'disabled=""';}?>  class="<?php echo $day[0]['monday']; ?>" value="Monday"  <?php if($routin['day_title'] == 'Monday') echo 'selected'; ?>>Monday</option>
                                                            <option <?php if($day[0]['tuesday'] == "Holyday"){echo 'disabled=""';}?>  value="Tuesday" class="<?php echo $day[0]['tuesday']; ?>" <?php if($routin['day_title'] == 'Tuesday') echo 'selected'; ?>>Tuesday</option>
                                                            <option <?php if($day[0]['wednesday'] == "Holyday"){echo 'disabled=""';}?> value="Wednesday" class="<?php echo $day[0]['wednesday']; ?>" <?php if($routin['day_title'] == 'Wednesday') echo 'selected'; ?>>Wednesday</option>
                                                            <option <?php if($day[0]['thursday'] == "Holyday"){echo 'disabled=""';}?> class="<?php echo $day[0]['thursday']; ?>" value="Thursday"  <?php if($routin['day_title'] == 'Thursday') echo 'selected'; ?>>Thursday</option>
                                                            <option <?php if($day[0]['friday'] == "Holyday"){echo 'disabled=""';}?> class="<?php echo $day[0]['friday']; ?>" value="Friday"  <?php if($routin['day_title'] == 'Friday') echo 'selected'; ?>>Friday</option>
                                                            <option <?php if($day[0]['saturday'] == "Holyday"){echo 'disabled=""';}?> class="<?php echo $day[0]['saturday']; ?>" value="Saturday"  <?php if($routin['day_title'] == 'Saturday') echo 'selected'; ?>>Saturday</option>
                                                            <option <?php if($day[0]['sunday'] == "Holyday"){echo 'disabled=""';}?> class="<?php echo $day[0]['sunday']; ?>" value="Sunday"  <?php if($routin['day_title'] == 'Sunday') echo 'selected'; ?>>Sunday</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-2 routineFildMarginBottom">
                                                    <select class="form-control" name="subject" required="">
                                                        <?php foreach ($subject as $row1) { ?>
                                                            <option value="<?php echo $row1['subject_title'] ?>" <?php if($routin['subject'] == $row1['subject_title']) echo 'selected'; ?> ><?php echo $row1['subject_title'] ?></option>
                                                         <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-2 routineFildMarginBottom">
                                                    <select class="form-control" name="teacher" required="">
                                                    <?php foreach ($teacher as $row2) { ?>
                                                    <option value="<?php echo $row2['id'] ?>" <?php if($routin['subject_teacher'] == $row2['id']) echo 'selected'; ?> ><?php echo $row2['fullname'] ?></option>
                                                                        <?php } ?>
                                                       <!--  <option>Sunday</option> -->
                                                    </select>
                                                </div>
                                                <div class="col-md-2 routineFildMarginBottom">
                                                    <input type="time" id="timepicker1" class="form-control input-small" placeholder="Start Time" value="<?php echo $routin['start_time']; ?>" name="startTime" required="">
                                                </div>
                                                <div class="col-md-2 routineFildMarginBottom">
                                                    <input type="time" id="timepicker2" class="form-control input-small" placeholder="End Time" value="<?php echo $routin['end_time']; ?>" name="endTime" required="">
                                                </div>
                                                <div class="col-md-2 routineFildMarginBottom">
                                                    <input type="text" data-validation="required alphanumeric" data-validation-allowing=" " class="form-control" placeholder="Rome No" value="<?php echo $routin['room_number']; ?>" name="roomNumber" required="">
                                                </div>
<?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions fluid marginTopNone">
                            <div class="col-md-offset-3 col-md-9">
                                <button class="btn blue" type="submit" name="update" value="Update"><?php echo lang('clas_up_button'); ?></button>

                            </div>
                        </div>
<?php echo form_close(); ?>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->
</div>

<!-- <script type="text/javascript" src="assets/global/plugins/timepickerr/wickedpicker.js"></script> -->
<script type="text/javascript">
           // $('#timepicker1').wickedpicker();
           // $('#timepicker2').wickedpicker();
           // var options = { now: "12:35", twentyFour: false, upArrow: 'wickedpicker__controls__control-up',  downArrow: 'wickedpicker__controls__control-down',  close: 'wickedpicker__close', hoverState:'hover-state',title: 'Timepicker',  showSeconds: false, secondsInterval: 1, minutesInterval: 1,  beforeShow: null,  show: null,  clearable: false,  }; 
           // $('#timepicker1').wickedpicker(options);
           // $('#timepicker2').wickedpicker(options);
    </script>
