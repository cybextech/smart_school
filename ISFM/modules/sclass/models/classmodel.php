<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Classmodel extends CI_Model {
    /**
     * This model is using into the sclass controller
     * Load : $this->load->model('classmodel');
     */
    function __construct() {
        parent::__construct();
        $this->load->dbforge();
    }

    //This function sent an array to sclass controller's "addClassRoutin" function.
    public function classSubject($a) {
        $data = array();
        $query = $this->db->get_where('class', array('class_title' => $a));
        foreach ($query->result_array() as $row) {
            $data = $row;
        }return $data;
    }

    public function check_code($parient_id) {
        $data = "";
        $query = $this->db->query("SELECT classCode FROM classes where parient_id=$parient_id ORDER BY id DESC LIMIT 1");
        foreach ($query->result_array() as $row) {
            $data = $row['classCode'];
        }
        return $data;
    }

    //This functionn is for get data from database with two condition.
    public function getWhere($a, $b, $c, $d, $e) {
        $data = array();
        $query = $this->db->get_where($a, array($b => $c, $d => $e));
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }return $data;
    }

     public function getWheree1($a, $b, $c, $d, $e, $f, $g) {
        $data = array();
        $query = $this->db->get_where($a, array($b => $c, $d => $e, $f => $g));
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }return $data;
    }

     public function getWheree2($a, $b, $c) {
        // var_dump($g);
        // die;
        $data = array();
        $query = $this->db->get_where($a, array($b => $c));
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }return $data;
    }

    //  public function getWheree($a, $b, $c, $d, $e) {
    //     $data = array();
    //     $query = $this->db->get_where($a, array($b => $c, $d => $e));
    //     foreach ($query->result_array() as $row) {
    //         $data[] = $row;
    //     }return $data;
    // }

    //This function return total student amount in a class
    public function totalClassStudent($classTitle) {
        $data = array();
        $query = $this->db->get_where('class_students', array('class_title' => $classTitle));
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }return count($data);
    }

    //This function return section amount in a class
    public function totalClassSection($c_id) {
        $data = array();
        $query = $this->db->get_where('class', array('id' => $c_id));
        foreach ($query->result_array() as $row) {
            $data = $row['section'];
        }
        if (!empty($data)) {
            $section = explode(',', $data);
            return count($section);
        } else {
            return 'No Section';
        }
    }

    public function classCodeCheck($a) {
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $data = array();
        $this->db->where('parient_id', $parient_id);
        $query = $this->db->get('class');
        foreach ($query->result_array() as $row) {
            $data[] = $row['classCode'];
        }
        if (in_array($a, $data)) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    //This function will return true or false final exam and result compleate or not
    public function chFiExRe($class_id) {
        $data = array();
        $this->db->WHERE('class_id',$class_id);
        $this->db->WHERE('final','Final');
        $this->db->WHERE('publish','Publish');
        $query = $this->db->get('add_exam');
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        return $data;
    }

    //This function will make marite list 
    public function meritList($examId) {
        $data = array();
        $query = $this->db->query("SELECT student_id,total_mark FROM final_result WHERE exam_id='$examId'");
        foreach ($query->result_array() as $row) {
            $index = $row['student_id'];
            $data["$index"] = $row['total_mark'];
        }
        arsort($data);
        $ri = 1;
        foreach ($data as $key => $value) {
            $meritList = array(
                'maride_list' => $ri
            );
            $this->db->where('student_id', $key);
            $this->db->update('final_result', $meritList);
            $ri++;
        }
    }

    //This function will return class section student capacity
    public function sectionCap($classId) {
        $data = "";
        $query = $this->db->query("SELECT student_amount FROM class WHERE id='$classId'");
        foreach ($query->result_array() as $row) {
            $data =  $row['student_amount'];
        }
        return $data;
    }

    //This function will return student new class section by his studentid
    public function sectionSelect($studentId, $i) {
        $data = array();
        $query = $this->db->query("SELECT class_title,sex FROM class WHERE student_id='$studentId'");
        foreach ($query->result_array() as $row) {
            
        }
    }

    //This function will return next class
    public function nextClass($classId) {
        $data = "";
        $this->db->where('id', $classId);
        $query = $this->db->get('classes');
        foreach ($query->result_array() as $row) 
        {
            $data = $row['promoted_class'];
        }
        return $data;
    }

    //This function will return all sections
    public function getSections($classId) {
        $data = array();
        $this->db->where('class_id', $classId);
        $query = $this->db->get('class');
        foreach ($query->result_array() as $row) 
        {
            $data[] = $row;
        }
        return $data;
    }

    //This function will tell about next class promotion
    public function is_next_promoted($classId) {
        $data = array();
        $this->db->where('id', $classId);
        $query = $this->db->get('classes');
        foreach ($query->result_array() as $row) 
        {
            $data[] = $row;
        }
        return $data;
    }
}
