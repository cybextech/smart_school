<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class Sclass extends MX_Controller {
    /**
     * This controller is use for add class and maintain class
     *
     * Maps to the following URL
     * 		http://example.com/index.php/Sclass
     * 	- or -  
     * 		http://example.com/index.php/Sclass/<method_name>
     */
    function __construct() {
        parent::__construct();
        $this->load->model('classmodel');
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login');
        }
    }

    //This function is useing for add a new class
    public function addClass() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8)))
        {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        if ($this->input->post('submit', TRUE)) {
            $classTitle = $this->input->post('class_title', TRUE);
            $c_code = $this->classmodel->check_code($parient_id);
            $classCode = "";
            if(empty($c_code))
            {
                $classCode = 'c1' ;
            }
            else
            {
                $classCode = substr($c_code,1);
                $classCode = $classCode+1;
                $classCode = 'c'.$classCode;
            }
            $nexxt = $this->input->post('next_class');
            $tableData = array(
                'parient_id' => $this->session->userdata('user_id'),
                'class_title' => $this->db->escape_like_str($classTitle),
                'sections' => '0',
                'classCode' => ($classCode),
                'promoted_class' => ($nexxt)
            );
            $clas_exist = $this->common->getWhere22('classes','class_title', $classTitle, 'parient_id', $this->session->userdata('user_id'));
            if(empty($clas_exist))
            {
                if ($this->db->insert('classes', $tableData)) 
                {
                    $data['success'] = '<div class="alert alert-info alert-dismissable admisionSucceassMessageFont">
                                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                <strong>'.lang('success').' '.'</strong>'. '" '.$classTitle.' "' .  lang('succ_aded').'
                                        </div>';
                    $data['classes'] = $this->common->getAllData('classes',$parient_id);
                    $this->load->view('temp/header');
                    $this->load->view('addClassSection',$data);
                    $this->load->view('temp/footer');
                }
            }
            else
            {
                $data['success'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                            '.lang('clasc_10').' "' . $classTitle . '" '.lang('clasc_name').' '.lang('clasc_14').'
                                    </div>';
                                    // lang('clasc_code').' "' . $classCode . '" '.lang('clasc_11').
                // $data['classInfo'] = $this->common->getAllData('class', $parient_id);
                $data['classes'] = $this->common->getAllData('classes',$parient_id);
                $this->load->view('temp/header');
                $this->load->view('addClassSection', $data);
                $this->load->view('temp/footer');
            }
        } 
        else 
        {
            // $data['teacher'] = $this->common->getAllData('teachers_info');
            $data['classes'] = $this->common->getAllData('classes',$parient_id);
            $this->load->view('temp/header');
            $this->load->view('addClassSection',$data);
            $this->load->view('temp/footer');
        }
    }
    
    //Function for adding sections into classes
    public function addSection() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8)))
        {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        if ($this->input->post('AddSection', TRUE))
        {
            $data['classId'] = $this->input->post('class', TRUE);
            $this->load->view('temp/header');
            $this->load->view('addingSections',$data);
            $this->load->view('temp/footer');
        }
        elseif($this->input->post('submit', TRUE))
        {
            $section_name = $this->input->post('section', TRUE);
            $std_amounts = $this->input->post('capicity', TRUE);
            $cls_id = $this->input->post('class_id', TRUE);
            $cls_name = $this->common->class_title1($cls_id);
            $section_record = $this->common->getWhere22('class','section',$section_name,'class_id',$cls_id);
            $abc = $this->common->getWhere22('classes','parient_id',$parient_id,'id',$cls_id);
            $sec_count = $abc[0]['sections'];
            $sec_count = $sec_count + 1 ;
            if(empty($section_record))
            {
                $tableData = array(
                    'parient_id' => $parient_id,
                    'class_title' => $this->db->escape_like_str($cls_name),
                    'class_id' => $this->db->escape_like_str($cls_id),
                    'section' => $this->db->escape_like_str($section_name),
                    'section_student_capacity' => $this->db->escape_like_str($std_amounts),
                    'student_amount' => '0',
                    'attendance_percentices_daily' => '0',
                    'attend_percentise_yearly' => '0',
                );
                if ($this->db->insert('class', $tableData)) 
                {
                    $this->db->where('id', $cls_id);
                    $clsData = array(
                        'sections' => $sec_count,
                    );
                    if($this->db->update('classes', $clsData))
                    {
                        $data['success'] = '<div class="alert alert-info alert-dismissable admisionSucceassMessageFont">
                                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                <strong>'.lang('success').' '.'</strong>'.lang('clas_setion').' '.lang('succ_aded').'
                                        </div>';
                        $data['classes'] = $this->common->getWhere('classes','parient_id',$parient_id);
                        $this->load->view('temp/header');
                        $this->load->view('addSection',$data);
                        $this->load->view('temp/footer');
                    }
                }
            }
            else
            {
                $data['success'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>Section with this name already exist for this class</div>';
                $data['classId'] = $cls_id;
                $this->load->view('temp/header');
                $this->load->view('addingSections',$data);
                $this->load->view('temp/footer');
            }
        }
        else
        {
            $data['classes'] = $this->common->getWhere('classes','parient_id',$parient_id);
            $this->load->view('temp/header');
            $this->load->view('addSection',$data);
            $this->load->view('temp/footer');
        }
    }

    //This function can edit class information
    public function deleteClass() {
        if((!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8)))
        {
            redirect('home/index');
        }
        $id = $this->input->get('id');
        if ($this->db->delete('class', array('id' => $id))) {
            $this->db->delete('class_subject', array('class_id' => $id));
            redirect('sclass/allClass/', 'refresh');
        }
    }

    //This function can edit class and section name
    public function editClass() 
    {
        if((!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8)))
        {
            redirect('home/index');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $data['id'] = $this->input->get('id');
        $data['c_name'] = $this->input->get('c_name');
        $data['s_name'] = $this->input->get('s_name');
        if ($this->input->post('submit', TRUE))
        {
            $c_id = $this->input->get('id');
            $c_title = $this->input->post('class_title', TRUE);
            $s_title = $this->input->post('section', TRUE);
            $pre_clasName = $this->input->post('pre_name', TRUE);
            $pre_secName = $this->input->post('pre_sec', TRUE);
            if($pre_clasName == $c_title && $pre_secName == $s_title)
            {
                redirect('sclass/allClass');
            }
            elseif($pre_clasName == $c_title)
            {
                $sec = array('section' => $s_title);
                $this->db->WHERE('id',$c_id);
                $this->db->update('class',$sec);
                redirect('sclass/allClass');
            }
            elseif($pre_secName == $s_title)
            {
                $cls = array('class_title' => $c_title);

                $this->db->WHERE('class_title',$pre_clasName);
                $this->db->WHERE('parient_id',$parient_id);
                $this->db->update('class',$cls);

                $this->db->WHERE('class_title',$pre_clasName);
                $this->db->WHERE('parient_id',$parient_id);
                $this->db->update('classes',$cls);

                $this->db->WHERE('class_title',$pre_clasName);
                $this->db->WHERE('parient_id',$parient_id);
                $this->db->update('class_students',$cls);

                redirect('sclass/allClass');
            }
            else
            {
                $cls1 = array('class_title' => $c_title);
                $sec1 = array('section' => $s_title );

                $this->db->WHERE('class_title',$pre_clasName);
                $this->db->WHERE('parient_id',$parient_id);
                $this->db->update('class',$cls1);

                $this->db->WHERE('class_title',$pre_clasName);
                $this->db->WHERE('parient_id',$parient_id);
                $this->db->update('classes',$cls1);

                $this->db->WHERE('class_title',$pre_clasName);
                $this->db->WHERE('parient_id',$parient_id);
                $this->db->update('class_students',$cls1);

                $this->db->WHERE('id',$c_id);
                $this->db->update('class',$sec1);

                redirect('sclass/allClass');
            }
            
        }
        else
        {
            $this->load->view('temp/header');
            $this->load->view('editClass', $data);
            $this->load->view('temp/footer');
        }
    }
    
    //This function is useing for geting all class short information
    public function allClass() 
    {
        if(!$this->ion_auth->is_teacher() && (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8)))
        {
             redirect('home/index');
        }
        $parient_id = $this->session->userdata('user_id');
        if (!$this->ion_auth->is_admin()) 
        {
           $parient_id = $this->session->userdata("parient_id");
        }
        $data['classInfo'] = $this->common->getAllData('class', $parient_id);
        $this->load->view('temp/header');
        $this->load->view('allClass', $data);
        $this->load->view('temp/footer');
    }
    
    //This function is useing for a class's full informtion
    public function classDetails() {
        if(!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8) && !$this->ion_auth->is_teacher())
        {
             redirect('home/index');
        }
        $parient_id = $this->session->userdata('user_id');
        if (!$this->ion_auth->is_admin()) 
        {
            $parient_id = $this->session->userdata("parient_id");
        }
        $class_id = $this->input->get('c_id');
        $data['class'] = $this->common->getWhere('class', 'id', $class_id);
        $data['day'] = $this->common->getAllData('config_week_day',$parient_id);
        $data['subject'] = $this->common->getWhere('class_subject', 'section_id', $class_id);
        $data['teacher'] = $this->common->getAllData('teachers_info',$parient_id);
        $data['classSection'] = $this->classmodel->totalClassSection($class_id);
        $this->load->view('temp/header');
        $this->load->view('classDetails', $data);
        $this->load->view('temp/footer');
    }
    //This function lode the view for select which class routine add or make
    public function selectClassRoutin() {
        if(!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8))
        {
             redirect('home/index');
        }
        $parient_id = $this->session->userdata('user_id');
        if (!$this->ion_auth->is_admin()) 
        {
            $parient_id = $this->session->userdata("parient_id");
        }
        $data['classTile'] = $this->common->getClass('classes','sections',0,'parient_id',$parient_id);
        $data['day'] = $this->common->getAllData('config_week_day', $parient_id);
        $this->load->view('temp/header');
        $this->load->view('selectClassRoutine', $data);
        $this->load->view('temp/footer');
    }
    //This function is useing for add new class routine
    public function addClassRoutin() {
        if(!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8))
        {
             redirect('home/index');
        }
        $parient_id = $this->session->userdata('user_id');
        if (!$this->ion_auth->is_admin()) 
        {
            $parient_id = $this->session->userdata("parient_id");
        }
        $count = 0;
        $class_id = $this->input->post('class', TRUE);
        if ($this->input->post('section', TRUE))
        {
            $classTitle = $this->common->class_title($this->input->post('section', TRUE));
            $section = $this->input->post('section', TRUE);
            $section_title = $this->common->section_title($section);
            if ($this->input->post('submit2', TRUE)) 
            {
                $flag1 = 0;
                $day = $this->input->post('day', TRUE);
                $subject = $this->input->post('subject', TRUE);
                $teacher = $this->input->post('teacher', TRUE);
                $startTime = $this->input->post('startTime', TRUE);
                $endTime = $this->input->post('endTime', TRUE);
                // $startTime = preg_replace('/\s+/', '', $startTime);
                // $endTime = preg_replace('/\s+/', '', $endTime);
                $startTime = date('h:i A', strtotime($startTime));
                $endTime = date('h:i A', strtotime($endTime));
                $roomNumber = $this->input->post('roomNumber', TRUE);
                if(strtotime($startTime) >= strtotime($endTime))
                {
                    $data['success1'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                                                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                                            <strong>Error!</strong> Sorry Wrong Time entry. Ending Time must be greater then starting Time
                                                                    </div>';
                    $data['classTile'] = $classTitle;
                    $data['class_id'] = $class_id;
                    $data['section_id'] = $section;
                    $data['sectionTitle'] = $section_title;
                    $data['day'] = $this->common->getAllData('config_week_day', $parient_id);
                    $data['subject'] = $this->common->getWhere('class_subject', 'section_id', $section);
                    $data['teacher'] = $this->common->getAllData('teachers_info', $parient_id);
                    $flag1 = 1;
                    $this->load->view('temp/header');
                    $this->load->view('addClassRoutin', $data);
                    $this->load->view('temp/footer');
                }
                else
                {
                    if($flag1 == 0)
                    {
                        $q1 = $this->classmodel->getWhere('class_routine', 'day_title', $day,'section', $section);
                        foreach ($q1 as $r1)
                        {
                            if(strtotime($startTime) < strtotime($r1['end_time']) && strtotime($startTime) >= strtotime($r1['start_time']))
                            {
                                $count++;
                                $data['success1'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                                                                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                                                <strong>'.lang('error'). '</strong>' .lang('t1_error').'
                                                                        </div>';
                                $data['classTile'] = $classTitle;
                                $data['class_id'] = $class_id;
                                $data['section_id'] = $section;
                                $data['sectionTitle'] = $section_title;
                                $data['day'] = $this->common->getAllData('config_week_day', $parient_id);
                                $data['subject'] = $this->common->getWhere('class_subject', 'section_id', $section);
                                $data['teacher'] = $this->common->getAllData('teachers_info', $parient_id);
                                $flag1 = 1;
                                $this->load->view('temp/header');
                                $this->load->view('addClassRoutin', $data);
                                $this->load->view('temp/footer');
                            }
                            elseif(strtotime($startTime) < strtotime($r1['start_time']))
                            {
                                
                                if(strtotime($endTime) > strtotime($r1['start_time']) )
                                {
                                    $count++;
                                    $data['success1'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                                                                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                                                <strong>'.lang('error'). '</strong>' .lang('t1_error').'
                                                                        </div>';
                                    $data['classTile'] = $classTitle;
                                    $data['class_id'] = $class_id;
                                    $data['section_id'] = $section;
                                    $data['sectionTitle'] = $section_title;
                                    $data['day'] = $this->common->getAllData('config_week_day', $parient_id);
                                    $data['subject'] = $this->common->getWhere('class_subject', 'section_id', $section);
                                    $data['teacher'] = $this->common->getAllData('teachers_info', $parient_id);
                                    $flag1 = 1;
                                    $this->load->view('temp/header');
                                    $this->load->view('addClassRoutin', $data);
                                    $this->load->view('temp/footer');
                                }
                            }
                        }
                    }
                    if($flag1 == 0)
                    {
                        $q2 = $this->classmodel->getWheree1('class_routine', 'day_title', $day,'parient_id', $parient_id,'subject_teacher',$teacher);
                        foreach ($q2 as $r2)
                        {
                            if(strtotime($startTime) < strtotime($r2['end_time']) && strtotime($startTime) >= strtotime($r2['start_time']))
                            {
                                $count++;
                                $data['success1'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                                                                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                                                <strong>'.lang('error'). '</strong>' .lang('t2_error').'
                                                                        </div>';
                                $data['classTile'] = $classTitle;
                                $data['class_id'] = $class_id;
                                $data['section_id'] = $section;
                                $data['sectionTitle'] = $section_title;
                                $data['day'] = $this->common->getAllData('config_week_day', $parient_id);
                                $data['subject'] = $this->common->getWhere('class_subject', 'section_id', $section);
                                $data['teacher'] = $this->common->getAllData('teachers_info', $parient_id);
                                $flag1 = 1;
                                $this->load->view('temp/header');
                                $this->load->view('addClassRoutin', $data);
                                $this->load->view('temp/footer');
                            }
                            elseif(strtotime($startTime) < strtotime($r2['start_time']))
                            {
                                
                                if(strtotime($endTime) > strtotime($r2['start_time']) )
                                {
                                    $count++;
                                    $data['success1'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                                                                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                                                <strong>'.lang('error'). '</strong>' .lang('t2_error').'
                                                                        </div>';
                                    $data['classTile'] = $classTitle;
                                    $data['class_id'] = $class_id;
                                    $data['section_id'] = $section;
                                    $data['sectionTitle'] = $section_title;
                                    $data['day'] = $this->common->getAllData('config_week_day', $parient_id);
                                    $data['subject'] = $this->common->getWhere('class_subject', 'section_id', $section);
                                    $data['teacher'] = $this->common->getAllData('teachers_info', $parient_id);
                                    $flag1 = 1;
                                    $this->load->view('temp/header');
                                    $this->load->view('addClassRoutin', $data);
                                    $this->load->view('temp/footer');
                                }
                            }
                        }
                    }
                    if($flag1 == 0)
                    {
                        $q3 = $this->classmodel->getWheree1('class_routine', 'day_title', $day,'parient_id', $parient_id,'room_number',$roomNumber);
                        foreach ($q3 as $r3)
                        {
                            if(strtotime($startTime) < strtotime($r3['end_time']) && strtotime($startTime) >= strtotime($r3['start_time']))
                            {
                                $count++;
                                $data['success1'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                                                                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                                                <strong>'.lang('error'). '</strong>' .lang('t3_error').'
                                                                        </div>';
                                $data['classTile'] = $classTitle;
                                $data['class_id'] = $class_id;
                                $data['section_id'] = $section;
                                $data['sectionTitle'] = $section_title;
                                $data['day'] = $this->common->getAllData('config_week_day', $parient_id);
                                $data['subject'] = $this->common->getWhere('class_subject', 'section_id', $section);
                                $data['teacher'] = $this->common->getAllData('teachers_info', $parient_id);
                                $flag1 =1;
                                $this->load->view('temp/header');
                                $this->load->view('addClassRoutin', $data);
                                $this->load->view('temp/footer');
                            }
                            elseif(strtotime($startTime) < strtotime($r3['start_time']))
                            {
                                
                                if(strtotime($endTime) > strtotime($r3['start_time']) )
                                {
                                    $count++;
                                    $data['success1'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                                                                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                                                <strong>'.lang('error'). '</strong>' .lang('t3_error').'
                                                                        </div>';
                                    $data['classTile'] = $classTitle;
                                    $data['class_id'] = $class_id;
                                    $data['section_id'] = $section;
                                    $data['sectionTitle'] = $section_title;
                                    $data['day'] = $this->common->getAllData('config_week_day', $parient_id);
                                    $data['subject'] = $this->common->getWhere('class_subject', 'section_id', $section);
                                    $data['teacher'] = $this->common->getAllData('teachers_info', $parient_id);
                                    $flag1 = 1;
                                    $this->load->view('temp/header');
                                    $this->load->view('addClassRoutin', $data);
                                    $this->load->view('temp/footer');
                                }
                            }
                        }
                    }
                    if($count == 0)
                    {
                        $tableData = array(
                            'class_id' => $this->db->escape_like_str($class_id),
                            'day_title' => $this->db->escape_like_str($day),
                            'section' => $this->db->escape_like_str($section),
                            'subject' => $this->db->escape_like_str($subject),
                            'subject_teacher' => $this->db->escape_like_str($teacher),
                            'start_time' => $this->db->escape_like_str($startTime),
                            'end_time' => $this->db->escape_like_str($endTime),
                            'room_number' => $this->db->escape_like_str($roomNumber),
                            'parient_id' => $this->db->escape_like_str($parient_id)
                        );
                        $tableData2 = array(
                            'subject_teacher' => $this->db->escape_like_str($teacher),
                        );
                        if ($this->db->insert('class_routine', $tableData) && $this->db->update('class_subject', $tableData2, array('section_id' => $section, 'subject_title' => $subject))) {
                            $data['classTile'] = $classTitle;
                            $data['class_id'] = $class_id;
                            $data['section_id'] = $section;
                            $data['sectionTitle'] = $section_title;
                            $data['day'] = $this->common->getAllData('config_week_day', $parient_id);
                            $data['subject'] = $this->common->getWhere('class_subject', 'section_id', $section);
                            $data['teacher'] = $this->common->getAllData('teachers_info', $parient_id);
                            $flag1 = 1;
                            $this->load->view('temp/header');
                            $this->load->view('addClassRoutin', $data);
                            $this->load->view('temp/footer');
                        }
                    }
                }
            } 
            else 
            {
                $data['classTile'] = $classTitle;
                $data['class_id'] = $class_id;
                $data['section_id'] = $section;
                $data['sectionTitle'] = $section_title;
                $data['day'] = $this->common->getAllData('config_week_day', $parient_id);
                $data['subject'] = $this->common->getWhere('class_subject', 'section_id', $section);
                $data['teacher'] = $this->common->getAllData('teachers_info', $parient_id);
                $this->load->view('temp/header');
                $this->load->view('addClassRoutin', $data);
                $this->load->view('temp/footer');
            }
        }
        else
        {
            redirect('sclass/selectClassRoutin');
        }
    }

    //This function gives us class section and class info.
    public function ajaxClassInfo() {
        $class_id = $this->input->get('q');
        $query = $this->common->getWhere('class', 'class_id', $class_id);
        echo '<input type="hidden" name="class_title" value="' . $class_id . '">';
        if(!empty($query))
        {
            echo '<div class="form-group">
                        <label class="col-md-3 control-label">Section <span class="requiredStar"> * </span></label>
                        <div class="col-md-6">
                            <select name="section" class="form-control" data-validation="required">
                                <option value="">Select one....</option>';
        foreach ($query as $sec) {
           echo "<option value=".$sec['id'].">".$sec['section']."</option>";
        }
         echo '</select></div>
                    </div>';
        }
    }
    //This function check class code data type and leanth
    public function ajaxClassCodeInfo() {
        $classCode = $this->input->get('q');
        // if ($classCode <= 99) {
            if ($this->classmodel->classCodeCheck($classCode) == TRUE) {
                echo '<input type="hidden" value="' . $classCode . '" name="class_code">';
            } else {
                echo ''.lang('clasc_7').' " ' . $classCode . ' " '.lang('clasc_8');
            }
        // } else {
        //     echo lang('clasc_9');
        // }
    }
    //This function gives a view for serlect class routine
    public function selectAllRoutine() {
        if(!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8) && !$this->ion_auth->is_teacher())
        {
             redirect('home/index');
        }
        $parient_id = $this->session->userdata('user_id');
        if (!$this->ion_auth->is_admin()) 
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $data['classTile'] = $this->common->getAllData('class', $parient_id);
        // $data['day'] = $this->common->getAllData('config_week_day', $parient_id);
        $this->load->view('temp/header');
        $this->load->view('selectAllRoutine', $data);
        $this->load->view('temp/footer');
    }
    //This function gives a class routine after selecting a class
    public function allClassRoutine() {
        if(!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8) && !$this->ion_auth->is_teacher())
        {
             redirect('home/index');
        }
        $parient_id = $this->session->userdata('user_id');
        if (!$this->ion_auth->is_admin()) 
        {
            $parient_id = $this->session->userdata('parient_id');
            
        }
        if ($this->input->post('submit', TRUE)) {
            $class_id = $this->input->post('class', TRUE);
            $data['class_id'] = $class_id;
            $data['day'] = $this->common->getAllData('config_week_day', $parient_id);
            $data['subject'] = $this->common->getWhere('class_subject', 'section_id', $class_id);
            // $data['teacher'] = $this->common->getWhere('teachers_info', 'user_id', $a);
            $data['teacher'] = $this->common->getAllData('teachers_info',$parient_id);
            $this->load->view('temp/header');
            $this->load->view('viewRoutine', $data);
            $this->load->view('temp/footer');
        }
    }

    public function selectTeacher() 
    {
        if(!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8) && !$this->ion_auth->is_teacher())
        {
            redirect('home/index');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        if ($this->ion_auth->is_teacher()) 
        {
            $my_id = $this->session->userdata('user_id');
            $t_id = "";
            $this->db->where('user_id',$my_id);
            $this->db->select('id');
            $res1 = $this->db->get('teachers_info');
            foreach ($res1->result_array() as $key) {
                $t_id = $key['id'];
            }
            $class_id = $this->common->getid($parient_id);
            $data['class_id'] = $class_id;
            $day = $this->common->getAllData('config_week_day', $parient_id);
            $data['day'] = $day;
            $subject = $this->common->getWheree('class_subject', 'section_id', $class_id);
            $data['subject'] = $subject;
            //$teacher = $this->common->getWhere('teachers_info', 'id', $a);
            $data['teacher'] = $t_id;
            // $data['teacher'] = $this->common->getAllData('teachers_info');
            $this->load->view('temp/header');
            $this->load->view('teahcerviewRoutine', $data);
            $this->load->view('temp/footer');
        }
        else
        {
            $data['teacher'] = $this->common->getAllData('teachers_info',$parient_id);
            $this->load->view('temp/header');
            $this->load->view('selectTeacher', $data);
            $this->load->view('temp/footer');
        }
    }
     public function teacherRoutine() {
        if(!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8) && !$this->ion_auth->is_teacher())
        {
             redirect('home/index');
        }
        $parient_id = $this->session->userdata('user_id');
        if (!$this->ion_auth->is_admin()) 
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        if ($this->input->post('submit', TRUE)) {
            $class_id = $this->common->getid($parient_id);
            $a = $this->input->post('class', TRUE);
            $data['class_id'] = $class_id;
            $day = $this->common->getAllData('config_week_day', $parient_id);
            $data['day'] = $day;
            $subject = $this->common->getWheree('class_subject', 'section_id', $class_id);
            $data['subject'] = $subject;
            //$teacher = $this->common->getWhere('teachers_info', 'id', $a);
            $data['teacher'] = $a;
            // $data['teacher'] = $this->common->getAllData('teachers_info');
            $this->load->view('temp/header');
            $this->load->view('teahcerviewRoutine', $data);
            $this->load->view('temp/footer');
        }
    }
    //By this function edit routine previous information 
    public function editRoutine() {
        if(!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8))
        {
             redirect('home/index');
        }
        $count = 0;
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $routinClassId = $this->input->get('id', TRUE);
        $class_id = $this->input->get('class', TRUE);
        if ($this->input->post('update', TRUE)) {
            $day = $this->input->post('day', TRUE);
            $subject = $this->input->post('subject', TRUE);
            $teacher = $this->input->post('teacher', TRUE);
            $startTime = $this->input->post('startTime', TRUE);
            $endTime = $this->input->post('endTime', TRUE);
            $startTime = date('h:i A', strtotime($startTime));
            $endTime = date('h:i A', strtotime($endTime));
            // $startTime = preg_replace('/\s+/', '', $startTime);
            // $endTime = preg_replace('/\s+/', '', $endTime);
            $roomNumber = $this->input->post('roomNumber', TRUE);
            if(strtotime($startTime) > strtotime($endTime))
            {
                        $data['success1'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                                                                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                                                <strong>Error!</strong> Sorry Wrong Time entry. Ending Time must be greater then starting Time
                                                                        </div>';
                        $data['class_id'] = $class_id;
                        $data['day'] = $this->common->getAllData('config_week_day', $parient_id);
                        $data['subject'] = $this->common->getWhere('class_subject', 'section_id', $class_id);
                        $data['previousRoutin'] = $this->common->getWhere('class_routine', 'id', $routinClassId);
                        $data['teacher'] = $this->common->getAllData('teachers_info', $parient_id);
                        $this->load->view('temp/header');
                        $this->load->view('editRoutine', $data);
                        $this->load->view('temp/footer');
            }
            else
            {
                $q1 = $this->common->cls_routine('class_routine','id', $routinClassId, 'day_title', $day,'section', $class_id);
                foreach ($q1 as $r1)
                {
                    if(strtotime($startTime) < strtotime($r1['end_time']) && strtotime($startTime) >= strtotime($r1['start_time']))
                    {
                        $count++;
                        $data['success1'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                                                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                                        <strong>'.lang('error'). '</strong>' .lang('t1_error').'
                                                                </div>';
                        $data['class_id'] = $class_id;
                        $data['day'] = $this->common->getAllData('config_week_day', $parient_id);
                        $data['subject'] = $this->common->getWhere('class_subject', 'section_id', $class_id);
                        $data['previousRoutin'] = $this->common->getWhere('class_routine', 'id', $routinClassId);
                        $data['teacher'] = $this->common->getAllData('teachers_info', $parient_id);
                        $this->load->view('temp/header');
                        $this->load->view('editRoutine', $data);
                        $this->load->view('temp/footer');
                    }
                    elseif(strtotime($startTime) < strtotime($r1['start_time']))
                    {
                        
                        if(strtotime($endTime) > strtotime($r1['start_time']) )
                        {
                            $count++;
                            $data['success1'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                                                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                                        <strong>'.lang('error'). '</strong>' .lang('t1_error').'
                                                                </div>';
                            $data['class_id'] = $class_id;
                            $data['day'] = $this->common->getAllData('config_week_day', $parient_id);
                            $data['subject'] = $this->common->getWhere('class_subject', 'section_id', $class_id);
                            $data['previousRoutin'] = $this->common->getWhere('class_routine', 'id', $routinClassId);
                            $data['teacher'] = $this->common->getAllData('teachers_info', $parient_id);
                            $this->load->view('temp/header');
                            $this->load->view('editRoutine', $data);
                            $this->load->view('temp/footer');
                        }
                    }
                }
                $q2 = $this->common->cls_routine('class_routine','id', $routinClassId, 'day_title', $day,'subject_teacher', $teacher);
                foreach ($q2 as $r2)
                {
                    if(strtotime($startTime) < strtotime($r2['end_time']) && strtotime($startTime) >= strtotime($r2['start_time']))
                    {
                        $count++;
                        $data['success1'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                                                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                                        <strong>'.lang('error'). '</strong>' .lang('t2_error').'
                                                                </div>';
                        $data['class_id'] = $class_id;
                        $data['day'] = $this->common->getAllData('config_week_day', $parient_id);
                        $data['subject'] = $this->common->getWhere('class_subject', 'section_id', $class_id);
                        $data['previousRoutin'] = $this->common->getWhere('class_routine', 'id', $routinClassId);
                        $data['teacher'] = $this->common->getAllData('teachers_info', $parient_id);
                        $this->load->view('temp/header');
                        $this->load->view('editRoutine', $data);
                        $this->load->view('temp/footer');
                    }
                    elseif(strtotime($startTime) < strtotime($r2['start_time']))
                    {
                        
                        if(strtotime($endTime) > strtotime($r2['start_time']) )
                        {
                            $count++;
                            $data['success1'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                                                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                                        <strong>'.lang('error'). '</strong>' .lang('t2_error').'
                                                                </div>';
                            $data['class_id'] = $class_id;
                            $data['day'] = $this->common->getAllData('config_week_day', $parient_id);
                            $data['subject'] = $this->common->getWhere('class_subject', 'section_id', $class_id);
                            $data['previousRoutin'] = $this->common->getWhere('class_routine', 'id', $routinClassId);
                            $data['teacher'] = $this->common->getAllData('teachers_info', $parient_id);
                            $this->load->view('temp/header');
                            $this->load->view('editRoutine', $data);
                            $this->load->view('temp/footer');
                        }
                    }
                }
                $q3 = $this->common->cls_routine('class_routine','id', $routinClassId, 'day_title', $day,'room_number', $roomNumber,'parient_id', $parient_id);
                foreach ($q3 as $r3)
                {
                    if(strtotime($startTime) < strtotime($r3['end_time']) && strtotime($startTime) >= strtotime($r3['start_time']))
                    {
                        $count++;
                        $data['success1'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                                                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                                        <strong>'.lang('error'). '</strong>' .lang('t3_error').'
                                                                </div>';
                        $data['class_id'] = $class_id;
                        $data['day'] = $this->common->getAllData('config_week_day', $parient_id);
                        $data['subject'] = $this->common->getWhere('class_subject', 'section_id', $class_id);
                        $data['previousRoutin'] = $this->common->getWhere('class_routine', 'id', $routinClassId);
                        $data['teacher'] = $this->common->getAllData('teachers_info', $parient_id);
                        $this->load->view('temp/header');
                        $this->load->view('editRoutine', $data);
                        $this->load->view('temp/footer');
                    }
                    elseif(strtotime($startTime) < strtotime($r3['start_time']))
                    {
                        
                        if(strtotime($endTime) > strtotime($r3['start_time']) )
                        {
                            $count++;
                            $data['success1'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                                                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                                        <strong>'.lang('error'). '</strong>' .lang('t3_error').'
                                                                </div>';
                            $data['class_id'] = $class_id;
                            $data['day'] = $this->common->getAllData('config_week_day', $parient_id);
                            $data['subject'] = $this->common->getWhere('class_subject', 'section_id', $class_id);
                            $data['previousRoutin'] = $this->common->getWhere('class_routine', 'id', $routinClassId);
                            $data['teacher'] = $this->common->getAllData('teachers_info', $parient_id);
                            $this->load->view('temp/header');
                            $this->load->view('editRoutine', $data);
                            $this->load->view('temp/footer');
                        }
                    }
                }
                if($count == 0)
                {
                    $tableData = array(
                        'day_title' => $this->db->escape_like_str($day),
                        'subject' => $this->db->escape_like_str($subject),
                        'subject_teacher' => $this->db->escape_like_str($teacher),
                        'start_time' => $this->db->escape_like_str($startTime),
                        'end_time' => $this->db->escape_like_str($endTime),
                        'room_number' => $this->db->escape_like_str($roomNumber)
                    );
                    $this->db->where('id', $routinClassId);
                    if ($this->db->update('class_routine', $tableData)) {
                        $data['class_id'] = $class_id;
                        $data['day'] = $this->common->getAllData('config_week_day', $parient_id);
                        $data['subject'] = $this->common->getWhere('class_subject', 'section_id', $class_id);
                        $data['teacher'] = $this->common->getAllData('teachers_info', $parient_id);
                        $this->load->view('temp/header');
                        $this->load->view('viewRoutine', $data);
                        $this->load->view('temp/footer');
                    }
                }
            }
        } else {
            $data['class_id'] = $class_id;
            $data['day'] = $this->common->getAllData('config_week_day', $parient_id);
            $data['subject'] = $this->common->getWhere('class_subject', 'section_id', $class_id);
            $data['previousRoutin'] = $this->common->getWhere('class_routine', 'id', $routinClassId);
            $data['teacher'] = $this->common->getAllData('teachers_info', $parient_id);
            $this->load->view('temp/header');
            $this->load->view('editRoutine', $data);
            $this->load->view('temp/footer');
        }
    }
    //By this function we can delet a class routine
    public function deleteRoutine() {
        if(!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8))
        {
             redirect('home/index');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $routinClassId = $this->input->get('id');
        $class_id = $this->input->get('class_id');
        $subject = $this->input->get('subject');
        $tableData2 = array(
                    'subject_teacher' => ''
                );
        if ($this->db->delete('class_routine', array('id' => $routinClassId)) && $this->db->update('class_subject', $tableData2, array('section_id' => $class_id, 'subject_title' => $subject))) {

            $data['class_id'] = $class_id;
            $data['day'] = $this->common->getAllData('config_week_day', $parient_id);
            $data['subject'] = $this->common->getWhere('class_subject', 'section_id', $class_id);
            $data['teacher'] = $this->common->getAllData('teachers_info', $parient_id);
            $data['message'] = '<div class="alert alert-warning alert-dismissable">
								<button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
								<strong>'.lang('clasc_10').'</strong> '.lang('clasc_11').'
							</div>';
            $this->load->view('temp/header');
            $this->load->view('viewRoutine', $data);
            $this->load->view('temp/footer');
        }
    }
    //This function will show student's and parent's own class routine
    public function ownClassRoutin() {
        // if(!$this->ion_auth->is_parents() && !$this->ion_auth->is_student())
        // {
        //      redirect('home/index');
        // }
        $class_id = array();
        $par = $this->session->userdata("user_id");
        if(!$this->ion_auth->is_admin())
        {
            $par = $this->session->userdata("parient_id");
        }
        $userId = $this->input->get('uisd');
        if ($this->ion_auth->is_student() || $this->ion_auth->is_admin() || $this->ion_auth->in_group(8)) 
        {
           $query = $this->db->query("SELECT section FROM class_students WHERE user_id='$userId'");
            foreach ($query->result_array() as $row) {
                $class_id = $row['section'];
            } 
        }
        else if($this->ion_auth->is_parents())
        {
            $query = $this->db->query("SELECT section FROM parents_info WHERE user_id='$userId'");
            foreach ($query->result_array() as $row) {
                $class_id = $row['section'];
            }
        }
        
        $data['class_id'] = $class_id;
        $data['day'] = $this->common->getAllData('config_week_day', $par);
        $data['subject'] = $this->common->getWhere('class_subject', 'section_id', $class_id);
        $data['teacher'] = $this->common->getAllData('teachers_info', $par);
        $this->load->view('temp/header');
        $this->load->view('viewRoutine', $data);
        $this->load->view('temp/footer');
    }
    //This function gives us class section and class info.
    public function ajaxpromotion() {
        $classTitle = $this->input->get('q');
        $query = $this->common->getWhere('class', 'class_title', $classTitle);
        foreach ($query as $row) {
            $data = $row;
        }
        echo '<input type="hidden" name="class" value="' . $classTitle . '">';
        if (!empty($data['section'])) {
            $section = $data['section'];
            $sectionArray = explode(",", $section);
            $i = 0;
            foreach ($sectionArray as $se) {
                $i++;
            }
            for ($a = 1; $a <= $i; $a++) {
                echo '<div class="form-group">
                        <label class="col-md-3 control-label">'.lang('clasc_3').' ' . $a . '<span class="requiredStar"> * </span></label>
                        <div class="col-md-6">
                            <select name="section_' . $a . '" class="form-control" data-validation="required" data-validation-error-msg="">
                                <option value="">'.lang('select').'</option>';
                foreach ($sectionArray as $sec) {
                    echo '<option value="' . $sec . '">' . $sec . '</option>';
                }
                echo '</select></div></div>';
            }
            $b = $a - 1;
            echo '<input type="hidden" name="sectionAmount" value="' . $b . '">';
        } else {
            echo '<div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-6">
                        <div class="alert alert-warning">
                                <strong>'.lang('clasc_5').'</strong> '.lang('clasc_6').'
                        </div></div></div>';
        }
    }
    //This function will work for promotion 
    public function promotion() 
    {
        if(!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8))
        {
             redirect('home/index');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        if ($this->input->post('submit', TRUE)) 
        {
            $classId = $this->input->post('class', TRUE);
            $nxt_clas = $this->classmodel->nextClass($classId);
            if($nxt_clas != "pass out")
            {
                $is_next_promoted = $this->classmodel->is_next_promoted($nxt_clas);
                $flag1 = 1;
                if($is_next_promoted[0]['promotion'] == 0)
                {
                    $data['message'] = lang('pro_err').' "'.$is_next_promoted[0]['class_title'].'" first';
                    $data['classTile'] = $this->common->getClass('classes','sections',0,'parient_id',$parient_id);
                    $this->load->view('temp/header');
                    $this->load->view('promotion', $data);
                    $this->load->view('temp/footer');
                }
                else
                {
                    $all_sections = $this->classmodel->getSections($classId);
                    $next_sections = $this->classmodel->getSections($nxt_clas);
                    $total_next_sections = count($next_sections);
                    $sec_count1 = 0;
                    foreach ($all_sections as $r1) 
                    {
                        $pre_sec = $r1['id'];
                        if($sec_count1 < $total_next_sections)
                        {
                            $nxt_sec = $next_sections[$sec_count1]['id'];
                        }
                        else
                        {
                            $sec_count1--;
                            $nxt_sec = $next_sections[$sec_count1]['id'];
                        }
                        if (!empty($this->classmodel->chFiExRe($pre_sec)))
                        {
                            $fExam = $this->classmodel->chFiExRe($pre_sec);
                            $examId = $fExam[0]['id'];
                            $query = $this->db->query("SELECT * FROM final_result WHERE exam_id='$examId'");
                            $t_students = $this->classmodel->sectionCap($nxt_sec);
                            $Rol_nmbr = $t_students+1;
                            $pre_year = date('Y')-1;
                            $curr_year = date('Y');
                            $pass = 0;
                            $fail = 1;
                            $sm = 1;
                            foreach ($query->result_array() as $row) 
                            {
                                $studentId = $row['student_id'];
                                $status = $row['status'];
                                //$newRoll = $row['maride_list'];
                                //Here is chacking this student is pass the exam or not.
                                if ($status == 'Pass') 
                                {
                                    $arrayClassStud = array(
                                        'year' => $curr_year,
                                        'roll_number' => $Rol_nmbr,
                                        'class_id' => $nxt_clas,
                                        'class_title' => $next_sections[0]['class_title'],
                                        'section' => $nxt_sec,
                                        'attendance_percentices_daily' => $this->db->escape_like_str(0),
                                        'optional_sub' => '',
                                    );
                                    $this->db->where('student_id', $studentId);
                                    $this->db->where('parient_id', $parient_id);
                                    if ($this->db->update('class_students', $arrayClassStud)) 
                                    {
                                        $dorm = $this->common->getWhere22('dormitory_bed','student_id',$studentId,'parient_id',$parient_id);
                                        if(!empty($dorm))
                                        {
                                            $arrayDormiBed = array(
                                                'class' => $this->db->escape_like_str($nxt_sec),
                                                'roll_number' => $this->db->escape_like_str($Rol_nmbr)
                                            );
                                            $this->db->where('student_id', $studentId);
                                            $this->db->where('parient_id', $parient_id);
                                            $this->db->update('dormitory_bed', $arrayDormiBed);
                                        }
                                        $abu = $this->common->getWhere22('parents_info','student_id',$studentId,'parient_id',$parient_id);
                                        if(!empty($abu))
                                        {
                                           $arrayParentsInfo = array(
                                                'class_id' => $nxt_clas,
                                                'section' => $nxt_sec,
                                            );
                                            $this->db->where('student_id', $studentId);
                                            $this->db->where('parient_id', $parient_id);
                                            $this->db->update('parents_info', $arrayParentsInfo); 
                                        }
                                        $arrrayStudInfo = array(
                                            'year' => $curr_year,
                                            'roll_number' => $Rol_nmbr,
                                            'class_id' => $nxt_clas,
                                        );
                                        $this->db->where('student_id', $studentId);
                                        $this->db->where('parient_id', $parient_id);
                                        $this->db->update('student_info', $arrrayStudInfo);
                                        $pass++;
                                    }
                                }
                                else
                                {
                                    $onFail_data = array(
                                        'roll_number' => $fail,
                                        'attendance_percentices_daily' => '0',
                                        'year' => $curr_year,
                                        'optional_sub' => '',
                                    );
                                    $this->db->where('student_id', $studentId);
                                    $this->db->where('parient_id', $parient_id);
                                    $this->db->update('class_students', $onFail_data);
                                    $fail++;
                                }
                            }
                            ////////////////////////////////
                            $totalStudents = $this->classmodel->sectionCap($pre_sec);
                            $totalStudents = $totalStudents - $pass;
                            $sectionData = array(
                                'student_amount' => $totalStudents,
                                'attend_percentise_yearly' => '0',
                                'attendance_percentices_daily' => '0',
                            );
                            $totalStudents1 = $t_students + $pass;
                            $sectionData1 = array(
                                'student_amount' => $totalStudents1,
                                'attend_percentise_yearly' => '0',
                                'attendance_percentices_daily' => '0',
                            );

                            $this->db->WHERE('id',$pre_sec);
                            $this->db->update('class', $sectionData);

                            $this->db->WHERE('id',$nxt_sec);
                            $this->db->update('class', $sectionData1);

                            $final_exam = array(
                                'final' => 'promoted',
                            );
                            $this->db->WHERE('id',$examId);
                            $this->db->update('add_exam', $final_exam);
                            ////////////////////////////
                        }
                        else 
                        {
                            $data['message'] = lang('clasc_13');
                            $data['classTile'] = $this->common->getClass('classes','sections',0,'parient_id',$parient_id);
                            $this->load->view('temp/header');
                            $this->load->view('promotion', $data);
                            $this->load->view('temp/footer');
                            $flag1 = 0;
                        }  
                    }
                    //////
                    if($flag1 != 0)
                    {
                        $classData = array(
                            'promotion' => '1',
                        );
                        $classData1 = array(
                            'promotion' => '0',
                        );

                        $this->db->WHERE('id',$classId);
                        $this->db->update('classes', $classData);

                        $this->db->WHERE('id',$nxt_clas);
                        $this->db->update('classes', $classData1);

                        $data['message'] = lang('clasc_12');
                        $data['classTile'] = $this->common->getClass('classes','sections',0,'parient_id',$parient_id);
                        $this->load->view('temp/header');
                        $this->load->view('promotion', $data);
                        $this->load->view('temp/footer');
                        }
                }
            }
            else
            {
                $all_sections = $this->classmodel->getSections($classId);
                $flag = 1;
                foreach ($all_sections as $key)
                {
                    $sec_id = $key['id'];
                    if (!empty($this->classmodel->chFiExRe($sec_id))) 
                    {
                        $fExam = $this->classmodel->chFiExRe($sec_id);
                        $examId = $fExam[0]['id'];
                        $quer1 = $this->db->query("SELECT * FROM final_result WHERE exam_id='$examId'");
                        $i = 1;
                        $pre_year = date('Y')-1;
                        $curr_year = date('Y');
                        $pass = 0;
                        $fail = 1;
                        $sm = 1;
                        foreach ($quer1->result_array() as $k1) 
                        {
                            $status = $k1['status'];
                            if($status == "Pass")
                            {
                                $record = $this->common->getWhere22('class_students','student_id',$k1['student_id'],'parient_id',$parient_id);
                                if(!empty($record))
                                {
                                    $passingData = array(
                                        'user_id' => $record[0]['user_id'],
                                        'student_id' => $k1['student_id'],
                                        'parient_id' => $parient_id,
                                        'class_id' => $record[0]['class_id'],
                                        'section_id' => $record[0]['section'],
                                        'prev_year' => $pre_year,
                                        'passing_year' => $curr_year,
                                        'final_examId' => $examId,
                                        'fee_status' => $record[0]['month_fee'],
                                    );
                                    if($this->db->insert('passout_students',$passingData))
                                    {
                                        $pass++;
                                        $this->db->delete('class_students', array('user_id' => $record[0]['user_id']));
                                        $lib = $this->common->getWhere('library_member','user_id',$record[0]['user_id']);
                                        if(!empty($lib))
                                        {
                                            $this->db->delete('library_member', array('user_id' => $record[0]['user_id']));
                                        }
                                        $this->db->delete('role_based_access', array('user_id' => $record[0]['user_id']));
                                        $abu = $this->common->getWhere22('parents_info','student_id',$k1['student_id'],'parient_id',$parient_id);
                                        if(!empty($abu))
                                        {
                                            $this->db->delete('role_based_access', array('user_id' => $abu[0]['user_id']));
                                        }
                                        $dorm = $this->common->getWhere22('dormitory_bed','student_id',$k1['student_id'],'parient_id',$parient_id);
                                        if(!empty($dorm))
                                        {
                                            $did = $dorm[0]['dormitory_id'];
                                            $room_number = $dorm[0]['room_number'];
                                            $dormInfo = array(
                                                'student_id' => 'NULL',
                                                'student_name' => '',
                                                'class' => '',
                                                'roll_number' => '0',
                                            );
                                            $this->db->WHERE('student_id',$k1['student_id']);
                                            $this->db->WHERE('parient_id',$parient_id);
                                            $this->db->update('dormitory_bed', $dormInfo);
                                            $bedAmount = $this->common->getWhere22('dormitory_room','dormitory_id',$did,'room',$room_number);
                                            $bedAmount = $bedAmount[0]['free_seat'];
                                            $bedAmount = $bedAmount+1;
                                            $bedInfo = array(
                                                'free_seat' => $bedAmount,
                                            );
                                            $this->db->WHERE('dormitory_id',$did);
                                            $this->db->WHERE('room',$room_number);
                                            $this->db->update('dormitory_room', $bedInfo);
                                        }
                                        $std_pasData = array(
                                            'roll_number' => 'pass',
                                            'class_id' => 'pass', 
                                            'passing_year' => $curr_year,
                                        );
                                        $this->db->WHERE('student_id',$k1['student_id']);
                                        $this->db->WHERE('parient_id',$parient_id);
                                        $this->db->update('student_info', $std_pasData);
                                    }
                                }
                                else
                                {
                                    $data['message'] = lang('clasc_14');
                                    $data['classTile'] = $this->common->getClass('classes','sections',0,'parient_id',$parient_id);
                                    $this->load->view('temp/header');
                                    $this->load->view('promotion', $data);
                                    $this->load->view('temp/footer');
                                    $flag = 0;
                                }
                            }
                            else
                            {
                                $record = $this->common->getWhere22('class_students','student_id',$k1['student_id'],'parient_id',$parient_id);
                                if(!empty($record))
                                {
                                    $onFail_data = array(
                                        'roll_number' => $fail,
                                        'attendance_percentices_daily' => '0',
                                        'year' => $curr_year,
                                        'optional_sub' => '',
                                    );
                                    $this->db->WHERE('user_id',$record[0]['user_id']);
                                    $this->db->update('class_students', $onFail_data);
                                    $fail++;
                                }
                            }
                        }
                        $secData = $this->common->getWhere('class','id',$sec_id);
                        $totalStudents = $secData[0]['student_amount'];
                        $totalStudents = $totalStudents - $pass;
                        $sectionData = array(
                            'student_amount' => $totalStudents,
                            'attend_percentise_yearly' => '0',
                            'attendance_percentices_daily' => '0',
                        );
                        $this->db->WHERE('id',$sec_id);
                        $this->db->update('class', $sectionData);
                        $final_exam = array(
                            'final' => 'promoted',
                        );
                        $this->db->WHERE('id',$examId);
                        $this->db->update('add_exam', $final_exam);
                    }
                    else 
                    {
                        $data['message'] = lang('clasc_13');
                        $data['classTile'] = $this->common->getClass('classes','sections',0,'parient_id',$parient_id);
                        $this->load->view('temp/header');
                        $this->load->view('promotion', $data);
                        $this->load->view('temp/footer');
                        $flag = 0;
                    }  
                }
                
                if($flag != 0)
                {
                    $current_year = date('Y');
                    $next_year = $current_year + 1;
                    $classData = array(
                        'promotion' => '1',
                    );
                    $acad_session = array(
                        'session_start' =>  $current_year,
                        'session_end' =>  $next_year,
                    );

                    $this->db->WHERE('id',$classId);
                    $this->db->update('classes', $classData);

                    $this->db->WHERE('parient_id',$parient_id);
                    $this->db->update('configuration', $acad_session);

                    $this->session->unset_userdata('s_from');
                    $this->session->unset_userdata('s_to');
                    $this->session->set_userdata('s_from', $current_year);
                    $this->session->set_userdata('s_to', $next_year);
                    
                    $data['message'] = lang('clasc_12');
                    $data['classTile'] = $this->common->getClass('classes','sections',0,'parient_id',$parient_id);
                    $this->load->view('temp/header');
                    $this->load->view('promotion', $data);
                    $this->load->view('temp/footer');
                }
            }
        } else {
            $data['classTile'] = $this->common->getClass('classes','sections',0,'parient_id',$parient_id);
            $this->load->view('temp/header');
            $this->load->view('promotion', $data);
            $this->load->view('temp/footer');
        }
    }
}
