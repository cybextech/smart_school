<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class Parents extends CI_Controller {
    /**
     * This controller is using for 
     *
     * Maps to the following URL
     * 		http://example.com/index.php/parents
     * 	- or -  
     * 		http://example.com/index.php/parents/<method_name>
     */
    function __construct() {
        parent::__construct();
        $this->load->model('parentmodel');
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login');
        }
    }
 
     public function parentsInformation() {
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        if($this->session->userdata('email_info')){
            $data['email_info'] = $this->session->userdata('email_info');
        }else{
            $data['email_info'] = '';
        }
        if($this->ion_auth->is_teacher())
        {
            $my_id = $this->session->userdata('user_id');
            $t_id = "";
            $this->db->where('user_id',$my_id);
            $this->db->select('id');
            $res1 = $this->db->get('teachers_info');
            foreach ($res1->result_array() as $key) {
                $t_id = $key['id'];
            }
            $c_id ="";
            $this->db->where('incharge',$t_id);
            $this->db->select('class_id');
            $res1 = $this->db->get('class');
            foreach ($res1->result_array() as $key) {
                $c_id = $key['class_id'];
            }
            $data['s_class'] = $this->common->getWhere22('classes','parient_id',$parient_id,'id',$c_id);
        }
        else
        {
            $data['s_class'] = $this->common->getClass('classes','sections',0,'parient_id', $parient_id);
        }
        if ($this->input->post('submit', TRUE) && $this->input->post('section', TRUE)) 
        {
            $class = $this->input->post('class', TRUE);
            $section = $this->input->post('section', TRUE);
            $data['classTitle'] = $this->common->class_title1($class);
            $data['section'] = $section;
            if ($section == 'all') 
            {
                $data['parents'] = $this->parentmodel->getparientByClassSection($class, $section);
                if (!empty($data['parents'])) {
                    //If the class have student then run here.
                    $this->load->view('temp/header');
                    $this->load->view('parentsInformation', $data);
                    $this->load->view('temp/footer');
                } else {
                    //If the class have no any student then print the massage in the view.
                    $data['message'] = lang('stuc_1');
                    $this->load->view('temp/header');
                    $this->load->view('parentsInformation', $data);
                    $this->load->view('temp/footer');
                }
            } 
            else 
            {

                $data['parents'] = $this->parentmodel->getparientByClassSection($class, $section);
                if (!empty($data['parents'])) 
                {
                    //If the class have student then run here.
                    $this->load->view('temp/header');
                    $this->load->view('parentsInformation', $data);
                    $this->load->view('temp/footer');
                } else {
                    //If the class have no any student then print the massage in the view.
                    $data['message'] = lang('stuc_1');
                    $this->load->view('temp/header');
                    $this->load->view('parentsInformation', $data);
                    $this->load->view('temp/footer');
                }
            }
        }
        else 
        {
            //First of all this method run here and load class selecting view.
            $this->load->view('temp/header');
            $this->load->view('parents', $data);
            $this->load->view('temp/footer');
        }
    }
    //This function is used for filtering to get students information(which class and which section if the section in that class)
    //If any one want to select class section for get that section's parents thene he can call this ajax function from view file.
    public function ajaxClassSection() {
        $classid = $this->input->get('q');
        if($this->ion_auth->is_teacher())
        {
            $my_id = $this->session->userdata('user_id');
            $t_id = "";
            $this->db->where('user_id',$my_id);
            $this->db->select('id');
            $res1 = $this->db->get('teachers_info');
            foreach ($res1->result_array() as $key) {
                $t_id = $key['id'];
            }
            $query = $this->common->getWhere22('class', 'class_id', $classid, 'incharge', $t_id);
            echo '<div class="form-group">
                    <label class="col-md-3 control-label"></label>
                    <div class="col-md-4">
                        <select name="section" class="form-control">
                            ';
            foreach ($query as $sec) {
                echo '<option value="' . $sec['id'] . '">' . $sec['section'] . '</option>';
            }
            echo '</select></div>
                    </div>';
            echo '<input type="hidden" name="class" value="' . $classid . '">';
        }
        else
        {
            $query = $this->common->getWhere('class', 'class_id', $classid);
            echo '<div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-4">
                            <select name="section" class="form-control">
                                <option value="all">' . lang('stu_sel_cla_velue_all') . '</option>';
            foreach ($query as $sec) {
                echo '<option value="' . $sec['id'] . '">' . $sec['section'] . '</option>';
            }
            echo '</select></div>
                    </div>';
            echo '<input type="hidden" name="class" value="' . $classid . '">';
        }
    }
    //This function will update the parents information.
    public function editParentsInfo() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8)))
        {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $userID = $this->input->get('puid');
        $parentsInfoId = $this->input->get('painid');
        if ($this->input->post('submit', TRUE)) {
            $username = $this->input->post('first_name', TRUE) . ' ' . $this->input->post('last_name', TRUE);
            $additional_data = array(
                'username' => $this->db->escape_like_str($username),
                'first_name' => $this->db->escape_like_str($this->input->post('first_name', TRUE)),
                'last_name' => $this->db->escape_like_str($this->input->post('last_name', TRUE)),
                'phone' => $this->db->escape_like_str($this->input->post('phone1', TRUE)),
                'email' => $this->db->escape_like_str($this->input->post('email', TRUE))
            );
            $this->db->where('id', $userID);
            $this->db->update('users', $additional_data);
            $additionalData1 = array(
                'parents_name' => $this->db->escape_like_str($username),
                'relation' => $this->db->escape_like_str($this->input->post('guardianRelation', TRUE)),
                'email' => $this->db->escape_like_str($this->input->post('email', TRUE)),
                'phone' => $this->db->escape_like_str($this->input->post('phone1', TRUE)),
            );
            $this->db->where('id', $parentsInfoId);
            $this->db->update('parents_info', $additionalData1);
            $data['s_class'] = $this->common->getClass('classes','sections',0,'parient_id', $parient_id);
            $data['success'] = '<br><div class="col-md-12"><div class="alert alert-info alert-dismissable admisionSucceassMessageFont">
                                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                <strong>' . lang('success') . '</strong>' . lang('parc_4') . '
                                        </div></div>';
            $this->load->view('temp/header');
            $this->load->view('parents', $data);
            $this->load->view('temp/footer');
        } else {
            $data['info'] = $this->common->getWhere('parents_info', 'id', $parentsInfoId);
            $this->load->view('temp/header');
            $this->load->view('editParents', $data);
            $this->load->view('temp/footer');
        }
    }
    //This function is using for delete any parents profile.
    public function deleteParents() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8)))
        {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $parentsInfoId = $this->input->get('painid');
        $userID = $this->input->get('puid');
        $this->db->delete('users', array('id' => $userID));
        $this->db->delete('parents_info', array('id' => $parentsInfoId));
        $this->db->delete('role_based_access', array('user_id' => $userID));
        $this->db->delete('users_groups', array('user_id' => $userID));
        redirect("parents/parentsInformation", 'refresh');
    }
}
