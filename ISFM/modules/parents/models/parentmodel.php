<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Parentmodel extends CI_Model {
    /**
     * This model is using into the students controller
     * Load : $this->load->model('studentmodel');
     */
    function __construct() {
        parent::__construct();
        $this->load->dbforge();
    }

    //This function return class section 
    
    public function getparientByClassSection($a, $b) {

        if ($b == 'all') {
            $query = $this->db->get_where('parents_info', array('class_id' => $a));
            $data = array();
            foreach ($query->result_array() as $row) {
                $data[] = $row;
            }return $data;
        } else {
            $query = $this->db->get_where('parents_info', array('class_id' => $a, 'section' => $b));
            $data = array();
            foreach ($query->result_array() as $row) {
                $data[] = $row;
            }return $data;
        }
    }

    //THis function return student's picture name from database.
}
