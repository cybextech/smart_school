<!-- BEGIN CONTENT -->
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    All Notice <small></small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo lang('home'); ?>

                    </li>
                    <li>
                        <?php echo lang('header_academic'); ?>

                    </li>
                    <li>
                        <?php echo lang('header_notic'); ?>

                    </li>
                    <li>
                        <?php echo lang('header_all_notice'); ?>
                    </li>
                    <li id="result" class="pull-right topClock"></li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-globe"></i><?php echo lang('not_nligb'); ?>  
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>
                                <tr>
                                    <th class="table-checkbox">
                                        <?php echo lang('srno'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('date'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('not_subject'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('not_mess'); ?>
                                    </th>
                                    <th>
                                        <?php echo lang('not_notfol'); ?>
                                    </th>
                                    <?php if($this->ion_auth->is_admin() || $this->ion_auth->in_group(8)){ ?>
                                    <th>
                                        <?php echo lang('des_stud'); ?>
                                    </th>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($notice as $row) {
                                    ?>
                                    <tr class="odd gradeX">
                                        <td>
                                            <?php echo $i; ?>
                                        </td>
                                        <td>
                                            <?php echo $row['date']; ?>
                                        </td>
                                        <td>
                                            <a href="index.php/notice/noticeDetails?id=<?php echo $row['id']; ?>"> <?php echo $row['subject']; ?> </a>
                                        </td>
                                        <td>
                                            <div id="ellipsis">
                                                    <?php 
                                                    $text = str_ireplace(array("\r","\n",'\r','\n'),'', $row['notice']);
                                                    echo $text; ?>
                                                </div>
                                        </td>
                                        <td>
                                            <span class="label label-sm label-success noticeFlower" >
                                                <?php echo $row['receiver']; ?> </span>
                                        </td>
                                        <?php if($this->ion_auth->is_admin() || $this->ion_auth->in_group(8)){ ?>
                                        <td>
                                            <input type="checkbox" <?php if($row['status'] == '1'){?> checked <?php } ?> data-toggle="toggle" data-onstyle="success" data-width="80%" data-on="Hide" class="toggle_event" data-off="Show" id="<?php echo $row['id']; ?>">
                                            <div id="txtHint"></div>
                                        </td>
                                    </tr>
                                    <?php }
                                    $i++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script>
    jQuery(document).ready(function () {
//here is auto reload after 1 second for time and date in the top
        jQuery(setInterval(function () {
            jQuery("#result").load("index.php/home/iceTime");
        }, 1000));
    });

    var $p = $('#ellipsis p');
    var divh = $('#ellipsis').height();
    while ($p.outerHeight() > divh) {
        $p.text(function (index, text) {
            return text.replace(/\W*\s(\S)*$/, '...');
        });
    }
</script>
<script>
    var id = "";
    var st = "";
    $(function() 
    {
        $('.toggle_event').change(function() {
            changeStatus($(this).prop('id'),$(this).prop('checked'));
        })
    })

    function changeStatus(str1,str2) {
        var xmlhttp;
        if (str1.length === 0) {
            document.getElementById("txtHint").innerHTML = "";
            return;
        }
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        }
        else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
            }
        };
        xmlhttp.open("GET", "index.php/notice/changeStatus?id=" + str1+"&st="+str2, true);
        xmlhttp.send();
    }
</script>