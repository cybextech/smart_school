<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class ExamModel extends CI_Model {
    /**
     * This model is using into the Examination controller
     * Load : $this->load->model('exammodel');
     */
    function __construct() {
        parent::__construct();
        $this->load->dbforge();
    }

    //This function is checking that have any exam in that date.
    public function checkExam($a, $b) {
        $data = "";
        $query = $this->db->get_where('exam_routine', array('exam_id' => $a, 'exam_date' => $b));
        foreach ($query->result_array() as $row) {
            $data = $row['id'];
        }
        // $data = array_filter($data);
        // if (!empty($data)) {
        //     return 'Have An Exam';
        // } else {
        //     return 'No Any Exam';
        // }
        return $data;
    }

    //This function will return only class title
    public function getClassTitle() {
        $data = array();
        $query = $this->db->query("SELECT id,class_title FROM class");
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }return $data;
    }

    //This function will return exam title which result is not compleated 
    public function examTitleRes($class_id) {
        $data = array();
        $acad_year = $this->session->userdata('s_from')."-".$this->session->userdata('s_to');
        $this->db->WHERE('class_id',$class_id);
        $this->db->WHERE('academic_year',$acad_year);
        $this->db->WHERE('status','NoResult');
        $this->db->select('id');
        $this->db->select('exam_title');
        $query = $this->db->get('add_exam');
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }return $data;
    }

    //This function will return which subject's result will not publish
    public function examResSubject($examDate="",$now="",$pid="",$cid="",$exmId="") {
        $data = array();
        $query = $this->db->query("SELECT DISTINCT subject_id,exam_subject FROM exam_attendanc WHERE (parient_id='$pid') AND (section='$cid') AND (date >= '$examDate' AND date <= '$now') AND (exam_title='$exmId')");
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }return $data;
    }

    //This function will return exam starting date
    public function examDate($examId) {
        $data = array();
        $query = $this->db->query("SELECT start_date FROM add_exam WHERE id='$examId' AND status='NoResult'");
        foreach ($query->result_array() as $row) {
            $data[] = $row['start_date'];
        }return $data;
    }

    //This function will check students optional subject
    public function che_opt_sub($student_id, $subjectTitle) {
        
        $data = array();
        $this->db->select('id');
        $this->db->where('student_id', $student_id);
        $this->db->where('optional_sub', $subjectTitle);
        $query = $this->db->get('class_students');
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        if (!empty($data)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //This function is returan Exam Title by ExamId.
    public function examTitle($id) {
        $data = array();
        $query2 = $this->db->query("SELECT exam_title FROM add_exam WHERE id=$id");
        foreach ($query2->result_array() as $row) {
            return $row['exam_title'];
        }
    }

    //This function is returan Exam  subject by ExamId.
    public function examSubjectTitle($id) {
        $data = array();
        $query = $this->db->query("SELECT exam_subject FROM exam_routine WHERE id='$id'");
        foreach ($query->result_array() as $row) {
            return $row['exam_subject'];
        }
    }

    //This function will return exam subject by exam id and date
    public function examSubject($a, $b) {
        $data = array();
        $query = $this->db->get_where('exam_routine', array('exam_id' => $a, 'exam_date' => $b));
        foreach ($query->result_array() as $row) {
            return $row['exam_subject'];
        }
    }

    //This function return previous attendance view
    public function previewAttendance($a, $b, $c) {
        $data = array();
        $query = $this->db->get_where('exam_attendanc', array('section' => $a, 'exam_title' => $b, 'subject_id' => $c));
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }return $data;
    }

    //This function gives tyeachers view.
    public function teacherInfo($userId) {
        $data = array();
        $query = $this->db->get_where('teachers_info', array('user_id' => $userId));
        foreach ($query->result_array() as $row) {
            $data = $row;
        }return $data;
    }

    //This function return result sheet
    public function checkResultShit($class_id, $examTitle, $subject) {
        $data = array();
        $query = $this->db->get_where('result_shit', array('class_id' => $class_id, 'exam_title' => $examTitle, 'exam_subject' => $subject));
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }return $data;
    }

    public function approuveSubjectAmount($class_id, $examTitle) {
        $data = array();
        $query = $this->db->get_where('result_submition_info', array('class_id' => $class_id, 'exam_id' => $examTitle, 'submited' => '1'));
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        $subjectAmount = count($data);
        return $subjectAmount;
    }

    //This function return class subject amount
    public function classSubjectAmount($class_id) {
        $subject = array();
        $query = $this->db->query("SELECT id FROM class_subject WHERE class_id=$class_id AND optional!=1");
        foreach ($query->result_array() as $row) {
            $subject[] = $row;
        }
        $subjectAmount = count($subject);
        return $subjectAmount;
    }

    //This function return exam subject amount
    public function examEubject($examId,$par="") {
        $subject = array();
        $query = $this->db->query("SELECT id FROM exam_routine WHERE exam_id=$examId AND parient_id=$par");
        foreach ($query->result_array() as $row) {
            $subject[] = $row;
        }
        $subjectAmount = count($subject);
        return $subjectAmount;
    }
    //This function return absent amountby any student id
    public function absent($studentId,$examId) {
        $data = array();
        $query = $this->db->get_where('result_shit', array('student_id' => $studentId, 'result' => 'Absent' , 'exam_id' => $examId));
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        if (!empty($data)) {
            $failAmount = count($data);
        } else {
            $failAmount = 0;
        }
        return $failAmount;
    }

    //This function return fail amount by any student Id.
    public function fail($studentId,$examId) {
        $data = array();
        $query = $this->db->get_where('result_shit', array('student_id' => $studentId, 'result' => 'Fail', 'exam_id' => $examId));
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        if (!empty($data)) {
            $failAmount = count($data);
        } else {
            $failAmount = 0;
        }
        return $failAmount;
    }

    //This function return a exam title in a class.
    public function examTitleForMarkshit($class_id,$acdmc_yr) {
        $data = array();
        $query = $this->db->get_where('add_exam', array('class_id' => $class_id, 'publish' => 'Publish','academic_year' =>$acdmc_yr ));
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }return $data;
    }

    //THis function count the total point avarage that point.
    public function pointAverage($studentId, $classSubject,$examID) {
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $data = array();
//      $query = $this->db->get_where('result_shit', array('student_id' => $studentId));
        $this->db->select('point');
        $this->db->where('student_id', $studentId);
        $this->db->where('exam_id', $examID);
        $this->db->where('parient_id', $parient_id);
        $query = $this->db->get('result_shit');
        foreach ($query->result_array() as $row) {
            $data[] = $row['point'];
        }
        $totalPoint = array_sum($data);
        $result = $totalPoint / $classSubject;
        return round($result, 2);
    }

    //This function select the grade by the average point.
    public function averageGrade($point,$par="") {  
        $data = "";
        $point = round($point);
        $this->db->select('grade_name');
        $this->db->where('parient_id', $par);
        $this->db->where('point', $point);
        $query = $this->db->get('exam_grade');
        foreach ($query->result_array() as $row) {
            $data = $row['grade_name'];
        }
        return $data;
    }

    //This function count the total mark for student's examination.
    public function totalMark($studentId,$examId="",$par="") {
        $data = array();
        $query = $this->db->get_where('result_shit', array('student_id' => $studentId,'parient_id' => $par,'exam_id' => $examId));
        foreach ($query->result_array() as $row) {
            $data[] = $row['obtained_marks'];
        }
        $totalPoint = array_sum($data);
        return $totalPoint;
    }

    public function totalMark1($studentId,$examId="",$par="") {
        $data = array();
        $query = $this->db->get_where('result_shit', array('student_id' => $studentId,'parient_id' => $par,'exam_id' => $examId));
        foreach ($query->result_array() as $row) {
            $data[] = $row['total_marks'];
        }
        $totalPoint = array_sum($data);
        return $totalPoint;
    }

    //This function return the data for final reslt.
    public function finalResultShow($class_id, $examId) {
        $data = array();
        $query = $this->db->get_where('final_result', array('class_id' => $class_id, 'exam_id' => $examId));
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        return $data;
    }

    public function finalResultShow1($class_id, $examId, $std, $par) {
        $data = array();
        $query = $this->db->get_where('final_result', array('class_id' => $class_id, 'exam_id' => $examId, 'student_id' => $std, 'parient_id' => $par));
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        return $data;
    }

    //This function help to publish the result.
    public function publish($a, $b, $c) {
        $data = array();
        $academic_session = $this->session->userdata('s_from')."-".$this->session->userdata('s_to');
        $query = $this->db->get_where('result_action', array('status' => $a, 'publish' => $b, 'parient_id' => $c, 'academic_year' => $academic_session ));
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }return $data;
    }

    //This function help to publish the result.
    public function publish1($a, $b, $c, $d) {
        $data = array();
        $academic_session = $this->session->userdata('s_from')."-".$this->session->userdata('s_to');
        $query = $this->db->get_where('result_action', array('status' => $a, 'publish' => $b, 'parient_id' => $c, 'class_id' => $d, 'academic_year' => $academic_session));
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }return $data;
    }

    //This function return markshit by examtitle,class title and student id.
    public function markshit($examId, $class_id, $studentId) {
        $data = array();
        $query = $this->db->get_where('result_shit', array('exam_id' => $examId, 'class_id' => $class_id, 'student_id' => $studentId));
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }return $data;
    }

    //This function will return student id by user id
    public function student_id($user_id) {
        $data = array();
        $query = $this->db->query("SELECT student_id FROM student_info WHERE user_id='$user_id'");
        foreach ($query->result_array() as $row) {
            $data = $row['student_id'];
        }
        return $data;
    }
}
