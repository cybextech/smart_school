<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class Examination extends MX_Controller {

    /**
     * This controller is using for 
     *
     * Maps to the following URL
     * 		http://example.com/index.php/examination
     * 	- or -  
     * 		http://example.com/index.php/examination/<method_name>
     */
    function __construct() {
        parent::__construct();
        $this->lang->load('auth');
        $this->load->model('exammodel');
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login');
        }
    }
    //This function load all exam grade and point
    public function examGread() {
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata("parient_id");
        }
        $data['grade'] = $this->common->getAllData('exam_grade', $parient_id);
        $this->load->view('temp/header');
        $this->load->view('examGread', $data);
        $this->load->view('temp/footer');
    }
    //THis function add new exam grade
    public function addExamGread() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8))) {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata("parient_id");
        }
        if ($this->input->post('submit', TRUE)) {
            $gradeName = $this->input->post('gradeName', TRUE);
            $gradePoint = $this->input->post('gradePoint', TRUE);
            $numberFrom = $this->input->post('numberFrom', TRUE);
            $nameTo = $this->input->post('nameTo', TRUE);

            $data = array(
                'parient_id' => $this->db->escape_like_str($parient_id),
                'grade_name' => $this->db->escape_like_str($gradeName),
                'point' => $this->db->escape_like_str($gradePoint),
                'number_form' => $this->db->escape_like_str($numberFrom),
                'number_to' => $this->db->escape_like_str($nameTo)
            );
            $this->db->insert('exam_grade', $data);

            redirect('examination/examGread', 'refresh');
        } else {
            $this->load->view('temp/header');
            $this->load->view('addExamGrade');
            $this->load->view('temp/footer');
        }
    }
    //This function will edit exam grade and point
    public function editGrade() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8))) {
            redirect('auth', 'refresh');
        }
        $id = $this->input->get('id');
        if ($this->input->post('submit', TRUE)) {
            $gradeName = $this->input->post('gradeName', TRUE);
            $gradePoint = $this->input->post('gradePoint', TRUE);
            $numberFrom = $this->input->post('numberFrom', TRUE);
            $nameTo = $this->input->post('nameTo', TRUE);

            $editData = array(
                'grade_name' => $this->db->escape_like_str($gradeName),
                'point' => $this->db->escape_like_str($gradePoint),
                'number_form' => $this->db->escape_like_str($numberFrom),
                'number_to' => $this->db->escape_like_str($nameTo)
            );

            $this->db->where('id', $id);
            if ($this->db->update('exam_grade', $editData)) {
                redirect('examination/examGread', 'refresh');
            }
        } else {
            $data['gradInfo'] = $this->common->getWhere('exam_grade', 'id', $id);
            $this->load->view('temp/header');
            $this->load->view('editGrade', $data);
            $this->load->view('temp/footer');
        }
    }
    //This function can delete exam grade in this system
    public function deleteGrade() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8))) {
            redirect('auth', 'refresh');
        }
        $id = $this->input->get('id');
        if ($this->db->delete('exam_grade', array('id' => $id))) {
            redirect('examination/examGread', 'refresh');
        }
    }
    //This function is using for decleration new examination for nay class.
    public function addExam() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8))) {
            redirect('auth', 'refresh');
        }
        $academic_session = $this->session->userdata('s_from')."-".$this->session->userdata('s_to');
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $data['s_class'] = $this->common->getAllData('class', $parient_id);
        if ($this->input->post('submit', TRUE)) {
            $examTitle = $this->input->post('examTitle', TRUE);
            $startDate = $this->input->post('startDate', TRUE);
            $class_id = $this->input->post('class_id', TRUE);
            $totleTime = $this->input->post('totleTime', TRUE);
            $examInfo = array(
                'parient_id' => $this->db->escape_like_str($parient_id),
                'year' => $this->db->escape_like_str(date('Y')),
                'exam_title' => $this->db->escape_like_str($examTitle),
                'start_date' => $this->db->escape_like_str($startDate),
                'class_id' => $this->db->escape_like_str($class_id),
                'total_time' => $this->db->escape_like_str($totleTime),
                'publish' => $this->db->escape_like_str('Not Publish'),
                'final' => $this->db->escape_like_str($this->input->post('final', TRUE)),
                'status' => $this->db->escape_like_str('NoResult'),
                'academic_year' => $academic_session
            );
            //Here is adding an exam information into database
            if ($this->db->insert('add_exam', $examInfo)) {
                $data['successMessage'] = '<div class="alert alert-success">
                                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                <strong>' . lang('success') . '</strong> ' . lang('exac_1') . '" ' . $examTitle . ' " ' . "for" . ' "' . $this->common->class_title($class_id)." ". $this->common->section_title($class_id). '" ' . lang('exac_3') . '
                                        </div>';
                $data['examInfo'] = $this->common->getAllData('add_exam', $parient_id);
                $data['subject'] = $this->common->getWhere('class_subject', 'section_id', $class_id);
                $data['weeklyDay'] = $this->common->getAllData('config_week_day', $parient_id);
                $data['class_id'] = $class_id;
                $this->load->view('temp/header');
                $this->load->view('addRutinSubject', $data);
                $this->load->view('temp/footer');
            }
        } else {
            $this->load->view('temp/header');
            $this->load->view('addExamRutine', $data);
            $this->load->view('temp/footer');
        }
    }
    //This function will complete an exam routine after decletration that exam.
    public function completExamRoutin() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8))) {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('user_id');
        }
        $count = 0;
        if ($this->input->post('submit', TRUE)) {
            $class_id = $this->input->post('clsId', TRUE);
            $examId = $this->input->post('examId', TRUE);
            //this is the 1st subject's informations for this exam rutine
            if ($this->input->post('examSunjectFild', TRUE)) {
                $examDate1 = $this->input->post('examDate', TRUE);
                $subject1 = $this->input->post('subject', TRUE);
                $romeNo1 = $this->input->post('romeNo', TRUE);
                $starTima1 = $this->input->post('starTima', TRUE);
                $endTima1 = $this->input->post('endTima', TRUE);
                // $starTima1 = preg_replace('/\s+/', '', $starTima1);
                // $endTima1 = preg_replace('/\s+/', '', $endTima1);
                $starTima1 = date('h:i A', strtotime($starTima1));
                $endTima1 = date('h:i A', strtotime($endTima1));
                $examShift1 = $this->input->post('examShift', TRUE);
                if(strtotime($starTima1) >= strtotime($endTima1))
                {
                    $data['successMessage'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                                                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                                            <strong>Error!</strong> Sorry Wrong Time entry. Ending Time must be greater then starting Time
                                                                    </div>';
                    $data['examInfo'] = $this->common->getAllData('add_exam', $parient_id);
                    $data['class_id'] = $class_id;
                    $data['subject'] = $this->common->getWhere('class_subject', 'section_id', $class_id);
                    $data['weeklyDay'] = $this->common->getAllData('config_week_day', $parient_id);
                    $this->load->view('temp/header');
                    $this->load->view('addRutinSubject', $data);
                    $this->load->view('temp/footer');
                }
                else
                {
                    $q3 = $this->common->getWhere2('exam_routine', 'exam_date', $examDate1,'parient_id', $parient_id,'rome_number', $romeNo1);
                    foreach ($q3 as $r3)
                    {
                        if(strtotime($starTima1) < strtotime($r3['end_time']) && strtotime($starTima1) >= strtotime($r3['start_time']))
                        {
                            $count++;
                            $data['successMessage'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                                                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                                            <strong>'.lang('error'). '</strong>' .lang('t3_error').'
                                                                    </div>';
                            $data['class_id'] = $class_id;
                            $data['examInfo'] = $this->common->getAllData('add_exam', $parient_id);
                            $data['subject'] = $this->common->getWhere('class_subject', 'section_id', $class_id);
                            $data['weeklyDay'] = $this->common->getAllData('config_week_day', $parient_id);
                            $this->load->view('temp/header');
                            $this->load->view('addRutinSubject', $data);
                            $this->load->view('temp/footer');
                        }
                        elseif(strtotime($starTima1) < strtotime($r3['start_time']))
                        {
                            
                            if(strtotime($endTima1) > strtotime($r3['start_time']) )
                            {
                                $count++;
                                $data['successMessage'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                                                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                                            <strong>'.lang('error'). '</strong>' .lang('t3_error').'
                                                                    </div>';
                                $data['examInfo'] = $this->common->getAllData('add_exam', $parient_id);
                                $data['subject'] = $this->common->getWhere('class_subject', 'section_id', $class_id);
                                $data['weeklyDay'] = $this->common->getAllData('config_week_day', $parient_id);
                                $data['class_id'] = $class_id;
                                $this->load->view('temp/header');
                                $this->load->view('addRutinSubject', $data);
                                $this->load->view('temp/footer');
                            }
                        }
                    }
                    $q1 = $this->common->getWhere22('exam_routine', 'exam_date', $examDate1,'class_id', $class_id);
                    foreach ($q1 as $r1)
                    {
                        if(strtotime($starTima1) < strtotime($r1['end_time']) && strtotime($starTima1) >= strtotime($r1['start_time']))
                        {
                            $count++;
                            $data['successMessage'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                                                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                                            <strong>'.lang('error'). '</strong>' .lang('t4_error').'
                                                                    </div>';
                            $data['examInfo'] = $this->common->getAllData('add_exam', $parient_id);
                            $data['subject'] = $this->common->getWhere('class_subject', 'section_id', $class_id);
                            $data['weeklyDay'] = $this->common->getAllData('config_week_day', $parient_id);
                            $data['class_id'] = $class_id;
                            $this->load->view('temp/header');
                            $this->load->view('addRutinSubject', $data);
                            $this->load->view('temp/footer');
                        }
                        elseif(strtotime($starTima1) < strtotime($r1['start_time']))
                        {
                            
                            if(strtotime($endTima1) > strtotime($r1['start_time']) )
                            {
                                $count++;
                                $data['successMessage'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                                                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                                            <strong>'.lang('error'). '</strong>' .lang('t4_error').'
                                                                    </div>';
                                $data['examInfo'] = $this->common->getAllData('add_exam', $parient_id);
                                $data['subject'] = $this->common->getWhere('class_subject', 'section_id', $class_id);
                                $data['weeklyDay'] = $this->common->getAllData('config_week_day', $parient_id);
                                $data['class_id'] = $class_id;
                                $this->load->view('temp/header');
                                $this->load->view('addRutinSubject', $data);
                                $this->load->view('temp/footer');
                            }
                        }
                    }
                    if($count == 0)
                    {
                        $routine1 = array(
                            'parient_id' => $this->db->escape_like_str($parient_id),
                            'exam_id' => $this->db->escape_like_str($examId),
                            'exam_date' => $this->db->escape_like_str($examDate1),
                            'exam_subject' => $this->db->escape_like_str($subject1),
                            'class_id' => $this->db->escape_like_str($class_id),
                            'rome_number' => $this->db->escape_like_str($romeNo1),
                            'start_time' => $this->db->escape_like_str($starTima1),
                            'end_time' => $this->db->escape_like_str($endTima1),
                            'exam_shift' => $this->db->escape_like_str($examShift1),
                            'status' => $this->db->escape_like_str('NoResult')
                        );
                        //insert this subject information into the database.
                        $this->db->insert('exam_routine', $routine1);
                    }
                }
            }
            //this is the 2nd subject's informations for this exam rutine
            if ($this->input->post('examSunjectFild_2', TRUE)) {
                $examDate2 = $this->input->post('examDate_2', TRUE);
                $subject2 = $this->input->post('subject_2', TRUE);
                $romeNo2 = $this->input->post('romeNo_2', TRUE);
                $starTima2 = $this->input->post('starTima_2', TRUE);
                $endTima2 = $this->input->post('endTima_2', TRUE);
                // $starTima2 = preg_replace('/\s+/', '', $starTima2);
                // $endTima2 = preg_replace('/\s+/', '', $endTima2);
                $starTima2 = date('h:i A', strtotime($starTima2));
                $endTima2 = date('h:i A', strtotime($endTima2));
                $examShift2 = $this->input->post('examShift_2', TRUE);
                if(strtotime($starTima2) >= strtotime($endTima2))
                {
                    $data['successMessage'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                                                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                                            <strong>Error!</strong> Sorry Wrong Time entry. Ending Time must be greater then starting Time
                                                                    </div>';
                    $data['examInfo'] = $this->common->getAllData('add_exam', $parient_id);
                    $data['class_id'] = $class_id;
                    $data['subject'] = $this->common->getWhere('class_subject', 'section_id', $class_id);
                    $data['weeklyDay'] = $this->common->getAllData('config_week_day', $parient_id);
                    $this->load->view('temp/header');
                    $this->load->view('addRutinSubject', $data);
                    $this->load->view('temp/footer');
                }
                else
                {
                    $q3 = $this->common->getWhere2('exam_routine', 'exam_date', $examDate2,'parient_id', $parient_id,'rome_number', $romeNo2);
                    foreach ($q3 as $r3)
                    {

                        if(strtotime($starTima2) < strtotime($r3['end_time']) && strtotime($starTima2) >= strtotime($r3['start_time']))
                        {
                            $count++;
                            $data['successMessage'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                                                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                                            <strong>'.lang('error'). '</strong>' .lang('t3_error').'
                                                                    </div>';
                            $data['class_id'] = $class_id;
                            $data['examInfo'] = $this->common->getAllData('add_exam', $parient_id);
                            $data['subject'] = $this->common->getWhere('class_subject', 'section_id', $class_id);
                            $data['weeklyDay'] = $this->common->getAllData('config_week_day', $parient_id);
                            $this->load->view('temp/header');
                            $this->load->view('addRutinSubject', $data);
                            $this->load->view('temp/footer');
                        }
                        elseif(strtotime($starTima2) < strtotime($r3['start_time']))
                        {
                            
                            if(strtotime($endTima2) > strtotime($r3['start_time']) )
                            {
                                $count++;
                                $data['successMessage'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                                                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                                            <strong>'.lang('error'). '</strong>' .lang('t3_error').'
                                                                    </div>';
                                $data['examInfo'] = $this->common->getAllData('add_exam', $parient_id);
                                $data['subject'] = $this->common->getWhere('class_subject', 'section_id', $class_id);
                                $data['weeklyDay'] = $this->common->getAllData('config_week_day', $parient_id);
                                $data['class_id'] = $class_id;
                                $this->load->view('temp/header');
                                $this->load->view('addRutinSubject', $data);
                                $this->load->view('temp/footer');
                            }
                        }
                    }
                    $q1 = $this->common->getWhere22('exam_routine', 'exam_date', $examDate2,'class_id', $class_id);
                    foreach ($q1 as $r1)
                    {
                        if(strtotime($starTima2) < strtotime($r1['end_time']) && strtotime($starTima2) >= strtotime($r1['start_time']))
                        {
                            $count++;
                            $data['successMessage'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                                                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                                            <strong>'.lang('error'). '</strong>' .lang('t4_error').'
                                                                    </div>';
                            $data['examInfo'] = $this->common->getAllData('add_exam', $parient_id);
                            $data['subject'] = $this->common->getWhere('class_subject', 'section_id', $class_id);
                            $data['weeklyDay'] = $this->common->getAllData('config_week_day', $parient_id);
                            $data['class_id'] = $class_id;
                            $this->load->view('temp/header');
                            $this->load->view('addRutinSubject', $data);
                            $this->load->view('temp/footer');
                        }
                        elseif(strtotime($starTima2) < strtotime($r1['start_time']))
                        {
                            
                            if(strtotime($endTima2) > strtotime($r1['start_time']) )
                            {
                                $count++;
                                $data['successMessage'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                                                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                                            <strong>'.lang('error'). '</strong>' .lang('t4_error').'
                                                                    </div>';
                                $data['examInfo'] = $this->common->getAllData('add_exam', $parient_id);
                                $data['subject'] = $this->common->getWhere('class_subject', 'section_id', $class_id);
                                $data['weeklyDay'] = $this->common->getAllData('config_week_day', $parient_id);
                                $data['class_id'] = $class_id;
                                $this->load->view('temp/header');
                                $this->load->view('addRutinSubject', $data);
                                $this->load->view('temp/footer');
                            }
                        }
                    }
                    if($count == 0)
                    {
                        $routine2 = array(
                            'parient_id' => $this->db->escape_like_str($parient_id),
                            'exam_id' => $this->db->escape_like_str($examId),
                            'exam_date' => $this->db->escape_like_str($examDate2),
                            'exam_subject' => $this->db->escape_like_str($subject2),
                            'rome_number' => $this->db->escape_like_str($romeNo2),
                            'class_id' => $this->db->escape_like_str($class_id),
                            'start_time' => $this->db->escape_like_str($starTima2),
                            'end_time' => $this->db->escape_like_str($endTima2),
                            'exam_shift' => $this->db->escape_like_str($examShift2),
                            'status' => $this->db->escape_like_str('NoResult')
                        );
                        //insert this subject information into the database.
                        $this->db->insert('exam_routine', $routine2);
                    }
                }
            }
            //this is the 3rd subject's informations for this exam rutine
            if ($this->input->post('examSunjectFild_3', TRUE)) {

                $examDate3 = $this->input->post('examDate_3', TRUE);
                $subject3 = $this->input->post('subject_3', TRUE);
                $romeNo3 = $this->input->post('romeNo_3', TRUE);
                $starTima3 = $this->input->post('starTima_3', TRUE);
                $endTima3 = $this->input->post('endTima_3', TRUE);
                // $starTima3 = preg_replace('/\s+/', '', $starTima3);
                // $endTima3 = preg_replace('/\s+/', '', $endTima3);
                $starTima3 = date('h:i A', strtotime($starTima3));
                $endTima3 = date('h:i A', strtotime($endTima3));
                $examShift3 = $this->input->post('examShift_3', TRUE);
                $routine3 = array(
                    'parient_id' => $this->db->escape_like_str($parient_id),
                    'exam_id' => $this->db->escape_like_str($examId),
                    'exam_date' => $this->db->escape_like_str($examDate3),
                    'exam_subject' => $this->db->escape_like_str($subject3),
                    'rome_number' => $this->db->escape_like_str($romeNo3),
                    'class_id' => $this->db->escape_like_str($class_id),
                    'start_time' => $this->db->escape_like_str($starTima3),
                    'end_time' => $this->db->escape_like_str($endTima3),
                    'exam_shift' => $this->db->escape_like_str($examShift3),
                    'status' => $this->db->escape_like_str('NoResult')
                );
                //insert this subject information into the database.
                $this->db->insert('exam_routine', $routine3);
            }
            //this is the 4th subject's informations for this exam rutine
            if ($this->input->post('examSunjectFild_4', TRUE)) {
                $examDate4 = $this->input->post('examDate_4', TRUE);
                $subject4 = $this->input->post('subject_4', TRUE);
                $romeNo4 = $this->input->post('romeNo_4', TRUE);
                $starTima4 = $this->input->post('starTima_4', TRUE);
                $endTima4 = $this->input->post('endTima_4', TRUE);
                // $starTima4 = preg_replace('/\s+/', '', $starTima4);
                // $endTima4 = preg_replace('/\s+/', '', $endTima4);
                $starTima4 = date('h:i A', strtotime($starTima4));
                $endTima4 = date('h:i A', strtotime($endTima4));
                $examShift4 = $this->input->post('examShift_4', TRUE);
                $routine4 = array(
                    'parient_id' => $this->db->escape_like_str($parient_id),
                    'exam_id' => $this->db->escape_like_str($examId),
                    'exam_date' => $this->db->escape_like_str($examDate4),
                    'exam_subject' => $this->db->escape_like_str($subject4),
                    'rome_number' => $this->db->escape_like_str($romeNo4),
                    'class_id' => $this->db->escape_like_str($class_id),
                    'start_time' => $this->db->escape_like_str($starTima4),
                    'end_time' => $this->db->escape_like_str($endTima4),
                    'exam_shift' => $this->db->escape_like_str($examShift4),
                    'status' => $this->db->escape_like_str('NoResult')
                );
                //insert this subject information into the database.
                $this->db->insert('exam_routine', $routine4);
            }
            //this is the 5th subject's informations for this exam rutine
            if ($this->input->post('examSunjectFild_5', TRUE)) {
                $examDate5 = $this->input->post('examDate_5', TRUE);
                $subject5 = $this->input->post('subject_5', TRUE);
                $romeNo5 = $this->input->post('romeNo_5', TRUE);
                $starTima5 = $this->input->post('starTima_5', TRUE);
                $endTima5 = $this->input->post('endTima_5', TRUE);
                // $starTima5 = preg_replace('/\s+/', '', $starTima5);
                // $endTima5 = preg_replace('/\s+/', '', $endTima5);
                $starTima5 = date('h:i A', strtotime($starTima5));
                $endTima5 = date('h:i A', strtotime($endTima5));
                $examShift5 = $this->input->post('examShift_5', TRUE);
                $routine5 = array(
                    'parient_id' => $this->db->escape_like_str($parient_id),
                    'exam_id' => $this->db->escape_like_str($examId),
                    'exam_date' => $this->db->escape_like_str($examDate5),
                    'exam_subject' => $this->db->escape_like_str($subject5),
                    'rome_number' => $this->db->escape_like_str($romeNo5),
                    'class_id' => $this->db->escape_like_str($class_id),
                    'start_time' => $this->db->escape_like_str($starTima5),
                    'end_time' => $this->db->escape_like_str($endTima5),
                    'exam_shift' => $this->db->escape_like_str($examShift5),
                    'status' => $this->db->escape_like_str('NoResult')
                );
                //insert this subject information into the database.
                $this->db->insert('exam_routine', $routine5);
            }
            //this is the 6th subject's informations for this exam rutine
            if ($this->input->post('examSunjectFild_6', TRUE)) {
                $examDate6 = $this->input->post('examDate_6', TRUE);
                $subject6 = $this->input->post('subject_6', TRUE);
                $romeNo6 = $this->input->post('romeNo_6', TRUE);
                $starTima6 = $this->input->post('starTima_6', TRUE);
                $endTima6 = $this->input->post('endTima_6', TRUE);
                // $starTima6 = preg_replace('/\s+/', '', $starTima6);
                // $endTima6 = preg_replace('/\s+/', '', $endTima6);
                $starTima6 = date('h:i A', strtotime($starTima6));
                $endTima6 = date('h:i A', strtotime($endTima6));
                $examShift6 = $this->input->post('examShift_6', TRUE);
                $routine6 = array(
                    'parient_id' => $this->db->escape_like_str($parient_id),
                    'exam_id' => $this->db->escape_like_str($examId),
                    'exam_date' => $this->db->escape_like_str($examDate6),
                    'exam_subject' => $this->db->escape_like_str($subject6),
                    'rome_number' => $this->db->escape_like_str($romeNo6),
                    'class_id' => $this->db->escape_like_str($class_id),
                    'start_time' => $this->db->escape_like_str($starTima6),
                    'end_time' => $this->db->escape_like_str($endTima6),
                    'exam_shift' => $this->db->escape_like_str($examShift6),
                    'status' => $this->db->escape_like_str('NoResult')
                );
                //insert this subject information into the database.
                $this->db->insert('exam_routine', $routine6);
            }
            //this is the 7th subject's informations for this exam rutine
            if ($this->input->post('examSunjectFild_7', TRUE)) {
                $examDate7 = $this->input->post('examDate_7', TRUE);
                $subject7 = $this->input->post('subject_7', TRUE);
                $romeNo7= $this->input->post('romeNo_7', TRUE);
                $starTima7 = $this->input->post('starTima_7', TRUE);
                $endTima7 = $this->input->post('endTima_7', TRUE);
                // $starTima7 = preg_replace('/\s+/', '', $starTima7);
                // $endTima7 = preg_replace('/\s+/', '', $endTima7);
                $starTima7 = date('h:i A', strtotime($starTima7));
                $endTima7 = date('h:i A', strtotime($endTima7));
                $examShift7 = $this->input->post('examShift_7', TRUE);
                $routine7 = array(
                    'parient_id' => $this->db->escape_like_str($parient_id),
                    'exam_id' => $this->db->escape_like_str($examId),
                    'exam_date' => $this->db->escape_like_str($examDate7),
                    'exam_subject' => $this->db->escape_like_str($subject7),
                    'rome_number' => $this->db->escape_like_str($romeNo7),
                    'start_time' => $this->db->escape_like_str($starTima7),
                    'end_time' => $this->db->escape_like_str($endTima7),
                    'class_id' => $this->db->escape_like_str($class_id),
                    'exam_shift' => $this->db->escape_like_str($examShift7),
                    'status' => $this->db->escape_like_str('NoResult')
                );
                //insert this subject information into the database.
                $this->db->insert('exam_routine', $routine7);
            }
            //this is the 8th subject's informations for this exam rutine
            if ($this->input->post('examSunjectFild_8', TRUE)) {

                $examDate8 = $this->input->post('examDate_8', TRUE);
                $subject8 = $this->input->post('subject_8', TRUE);
                $romeNo8 = $this->input->post('romeNo_8', TRUE);
                $starTima8 = $this->input->post('starTima_8', TRUE);
                $endTima8 = $this->input->post('endTima_8', TRUE);
                // $starTima8 = preg_replace('/\s+/', '', $starTima8);
                // $endTima8 = preg_replace('/\s+/', '', $endTima8);
                $starTima8 = date('h:i A', strtotime($starTima8));
                $endTima8 = date('h:i A', strtotime($endTima8));
                $examShift8 = $this->input->post('examShift_8', TRUE);
                $routine8 = array(
                    'parient_id' => $this->db->escape_like_str($parient_id),
                    'exam_id' => $this->db->escape_like_str($examId),
                    'exam_date' => $this->db->escape_like_str($examDate8),
                    'exam_subject' => $this->db->escape_like_str($subject8),
                    'rome_number' => $this->db->escape_like_str($romeNo8),
                    'start_time' => $this->db->escape_like_str($starTima8),
                    'end_time' => $this->db->escape_like_str($endTima8),
                    'class_id' => $this->db->escape_like_str($class_id),
                    'exam_shift' => $this->db->escape_like_str($examShift8),
                    'status' => $this->db->escape_like_str('NoResult')
                );
                //insert this subject information into the database.
                $this->db->insert('exam_routine', $routine8);
            }
            //this is the 9th subject's informations for this exam rutine
            if ($this->input->post('examSunjectFild_9', TRUE)) {
                $examDate9 = $this->input->post('examDate_9', TRUE);
                $subject9 = $this->input->post('subject_9', TRUE);
                $romeNo9 = $this->input->post('romeNo_9', TRUE);
                $starTima9 = $this->input->post('starTima_9', TRUE);
                $endTima9 = $this->input->post('endTima_9', TRUE);
                // $starTima9 = preg_replace('/\s+/', '', $starTima9);
                // $endTima9 = preg_replace('/\s+/', '', $endTima9);
                $starTima9 = date('h:i A', strtotime($starTima9));
                $endTima9 = date('h:i A', strtotime($endTima9));
                $examShift9 = $this->input->post('examShift_9', TRUE);
                $routine9 = array(
                    'parient_id' => $this->db->escape_like_str($parient_id),
                    'exam_id' => $this->db->escape_like_str($examId),
                    'class_id' => $this->db->escape_like_str($class_id),
                    'exam_date' => $this->db->escape_like_str($examDate9),
                    'exam_subject' => $this->db->escape_like_str($subject9),
                    'rome_number' => $this->db->escape_like_str($romeNo9),
                    'start_time' => $this->db->escape_like_str($starTima9),
                    'end_time' => $this->db->escape_like_str($endTima9),
                    'exam_shift' => $this->db->escape_like_str($examShift9),
                    'status' => $this->db->escape_like_str('NoResult')
                );
                //insert this subject information into the database.
                $this->db->insert('exam_routine', $routine9);
            }
            //this is the 10th subject's informations for this exam rutine
            if ($this->input->post('examSunjectFild_10', TRUE)) {
                $examDate10 = $this->input->post('examDate_10', TRUE);
                $subject10 = $this->input->post('subject_10', TRUE);
                $romeNo10 = $this->input->post('romeNo_10', TRUE);
                $starTima10 = $this->input->post('starTima_10', TRUE);
                $endTima10 = $this->input->post('endTima_10', TRUE);
                // $starTima10 = preg_replace('/\s+/', '', $starTima10);
                // $endTima10 = preg_replace('/\s+/', '', $endTima10);
                $starTima10 = date('h:i A', strtotime($starTima10));
                $endTima10 = date('h:i A', strtotime($endTima10));
                $examShift10 = $this->input->post('examShift_10', TRUE);
                $routine10 = array(
                    'parient_id' => $this->db->escape_like_str($parient_id),
                    'exam_id' => $this->db->escape_like_str($examId),
                    'class_id' => $this->db->escape_like_str($class_id),
                    'exam_date' => $this->db->escape_like_str($examDate10),
                    'exam_subject' => $this->db->escape_like_str($subject10),
                    'rome_number' => $this->db->escape_like_str($romeNo10),
                    'start_time' => $this->db->escape_like_str($starTima10),
                    'end_time' => $this->db->escape_like_str($endTima10),
                    'exam_shift' => $this->db->escape_like_str($examShift10),
                    'status' => $this->db->escape_like_str('NoResult')
                );
                //insert this subject information into the database.
                $this->db->insert('exam_routine', $routine10);
            }
            //this is the 11th subject's informations for this exam rutine
            if ($this->input->post('examSunjectFild_11', TRUE)) {
                $examDate11 = $this->input->post('examDate_11', TRUE);
                $subject11 = $this->input->post('subject_11', TRUE);
                $romeNo11 = $this->input->post('romeNo_11', TRUE);
                $starTima11 = $this->input->post('starTima_11', TRUE);
                $endTima11 = $this->input->post('endTima_11', TRUE);
                // $starTima11 = preg_replace('/\s+/', '', $starTima11);
                // $endTima11 = preg_replace('/\s+/', '', $endTima11);
                $starTima11 = date('h:i A', strtotime($starTima11));
                $endTima11 = date('h:i A', strtotime($endTima11));
                $examShift11 = $this->input->post('examShift_11', TRUE);
                $routine11 = array(
                    'parient_id' => $this->db->escape_like_str($parient_id),
                    'exam_id' => $this->db->escape_like_str($examId),
                    'class_id' => $this->db->escape_like_str($class_id),
                    'exam_date' => $this->db->escape_like_str($examDate11),
                    'exam_subject' => $this->db->escape_like_str($subject11),
                    'rome_number' => $this->db->escape_like_str($romeNo11),
                    'start_time' => $this->db->escape_like_str($starTima11),
                    'end_time' => $this->db->escape_like_str($endTima11),
                    'exam_shift' => $this->db->escape_like_str($examShift11),
                    'status' => $this->db->escape_like_str('NoResult')
                );
                //insert this subject information into the database.
                $this->db->insert('exam_routine', $routine11);
            }
            //this is the 12th subject's informations for this exam rutine
            if ($this->input->post('examSunjectFild_12', TRUE)) {
                $examDate12 = $this->input->post('examDate_12', TRUE);
                $subject12 = $this->input->post('subject_12', TRUE);
                $romeNo12 = $this->input->post('romeNo_12', TRUE);
                $starTima12 = $this->input->post('starTima_12', TRUE);
                $endTima12 = $this->input->post('endTima_12', TRUE);
                // $starTima12 = preg_replace('/\s+/', '', $starTima12);
                // $endTima12 = preg_replace('/\s+/', '', $endTima12);
                $starTima12 = date('h:i A', strtotime($starTima12));
                $endTima12 = date('h:i A', strtotime($endTima12));
                $examShift12 = $this->input->post('examShift_12', TRUE);
                $routine12 = array(
                    'parient_id' => $this->db->escape_like_str($parient_id),
                    'exam_id' => $this->db->escape_like_str($examId),
                    'class_id' => $this->db->escape_like_str($class_id),
                    'exam_date' => $this->db->escape_like_str($examDate12),
                    'exam_subject' => $this->db->escape_like_str($subject12),
                    'rome_number' => $this->db->escape_like_str($romeNo12),
                    'start_time' => $this->db->escape_like_str($starTima12),
                    'end_time' => $this->db->escape_like_str($endTima12),
                    'exam_shift' => $this->db->escape_like_str($examShift12),
                    'status' => $this->db->escape_like_str('NoResult')
                );
                //insert this subject information into the database.
                $this->db->insert('exam_routine', $routine12);
            }
            //this is the 13th subject's informations for this exam rutine
            if ($this->input->post('examSunjectFild_13', TRUE)) {
                $examDate13 = $this->input->post('examDate_13', TRUE);
                $subject13 = $this->input->post('subject_13', TRUE);
                $romeNo13= $this->input->post('romeNo_13', TRUE);
                $starTima13 = $this->input->post('starTima_13', TRUE);
                $endTima13 = $this->input->post('endTima_13', TRUE);
                // $starTima13 = preg_replace('/\s+/', '', $starTima13);
                // $endTima13 = preg_replace('/\s+/', '', $endTima13);
                $starTima13 = date('h:i A', strtotime($starTima13));
                $endTima13 = date('h:i A', strtotime($endTima13));
                $examShift13 = $this->input->post('examShift_13', TRUE);
                $routine13 = array(
                    'parient_id' => $this->db->escape_like_str($parient_id),
                    'exam_id' => $this->db->escape_like_str($examId),
                    'class_id' => $this->db->escape_like_str($class_id),
                    'exam_date' => $this->db->escape_like_str($examDate13),
                    'exam_subject' => $this->db->escape_like_str($subject13),
                    'rome_number' => $this->db->escape_like_str($romeNo13),
                    'start_time' => $this->db->escape_like_str($starTima13),
                    'end_time' => $this->db->escape_like_str($endTima13),
                    'exam_shift' => $this->db->escape_like_str($examShift13),
                    'status' => $this->db->escape_like_str('NoResult')
                );
                //insert this subject information into the database.
                $this->db->insert('exam_routine', $routine13);
            }
            //this is the 14th subject's informations for this exam rutine
            if ($this->input->post('examSunjectFild_14', TRUE)) {
                $examDate = $this->input->post('examDate_14', TRUE);
                $subject = $this->input->post('subject_14', TRUE);
                $romeNo = $this->input->post('romeNo_14', TRUE);
                $starTima = $this->input->post('starTima_14', TRUE);
                $endTima = $this->input->post('endTima_14', TRUE);
                // $starTima = preg_replace('/\s+/', '', $starTima);
                // $endTima = preg_replace('/\s+/', '', $endTima);
                $starTima = date('h:i A', strtotime($starTima));
                $endTima = date('h:i A', strtotime($endTima));
                $examShift = $this->input->post('examShift_14', TRUE);
                $routine = array(
                    'parient_id' => $this->db->escape_like_str($parient_id),
                    'exam_id' => $this->db->escape_like_str($examId),
                    'exam_date' => $this->db->escape_like_str($examDate),
                    'exam_subject' => $this->db->escape_like_str($subject),
                    'rome_number' => $this->db->escape_like_str($romeNo),
                    'start_time' => $this->db->escape_like_str($starTima),
                    'end_time' => $this->db->escape_like_str($endTima),
                    'exam_shift' => $this->db->escape_like_str($examShift),
                    'status' => $this->db->escape_like_str('NoResult')
                );
                //insert this subject information into the database.
                $this->db->insert('exam_routine', $routine);
            }
            //this is the 15th subject's informations for this exam rutine
            if ($this->input->post('examSunjectFild_15', TRUE)) {
                $examDate = $this->input->post('examDate_15', TRUE);
                $subject = $this->input->post('subject_15', TRUE);
                $romeNo = $this->input->post('romeNo_15', TRUE);
                $starTima = $this->input->post('starTima_15', TRUE);
                $endTima = $this->input->post('endTima_15', TRUE);
                // $starTima = preg_replace('/\s+/', '', $starTima);
                // $endTima = preg_replace('/\s+/', '', $endTima);
                $starTima = date('h:i A', strtotime($starTima));
                $endTima = date('h:i A', strtotime($endTima));
                $examShift = $this->input->post('examShift_15', TRUE);
                $routine = array(
                    'parient_id' => $this->db->escape_like_str($parient_id),
                    'exam_id' => $this->db->escape_like_str($examId),
                    'exam_date' => $this->db->escape_like_str($examDate),
                    'exam_subject' => $this->db->escape_like_str($subject),
                    'rome_number' => $this->db->escape_like_str($romeNo),
                    'start_time' => $this->db->escape_like_str($starTima),
                    'end_time' => $this->db->escape_like_str($endTima),
                    'exam_shift' => $this->db->escape_like_str($examShift),
                    'status' => $this->db->escape_like_str('NoResult')
                );
                //insert this subject information into the database.
                $this->db->insert('exam_routine', $routine);
            }
            $data['rutineInfo'] = $this->common->getWhere('exam_routine', 'exam_id', $examId);
            $data['examInfo'] = $this->common->getWhere('add_exam', 'id', $examId);
            $data['schoolName'] = $this->common->schoolName($parient_id);
            $this->load->view('temp/header');
            $this->load->view('rutineSuccess', $data);
            $this->load->view('temp/footer');
        }
    }
    //This function can delete exam and exam routine
    public function deleteExamAndRoutine() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8))) {
            redirect('auth', 'refresh');
        }
        $examId = $this->input->get('examId', TRUE);
        if ($this->db->delete('add_exam', array('id' => $examId))) {
            if ($this->db->delete('exam_routine', array('exam_id' => $examId))) {
                redirect('examination/allExamRutine', 'refresh');
            }
        }
    }
    //This function will select that which exam routine 
    public function allExamRutine() {
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $data['s_class'] = $this->common->getAllData('class', $parient_id);
        $this->load->view('temp/header');
        $this->load->view('selectAllRoutine', $data);
        $this->load->view('temp/footer');
    }
    //This function load class's exam title which is declard previously by class title.
    public function ajaxClassExam() {
        $class_id = $this->input->get('q');
        $acad_year = $this->session->userdata('s_from')."-".$this->session->userdata('s_to');
        $this->db->WHERE('class_id',$class_id);
        $this->db->WHERE('academic_year',$acad_year);
        $query = $this->db->get('add_exam');
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        if (!empty($data)) {
            echo '<div class="form-group">
                        <label class="col-md-3 control-label">' . lang('exac_4') . '<span class="requiredStar"> * </span></label>
                        <div class="col-md-6">
                            <select name="examId" class="form-control">';
            foreach ($data as $sec) {
                echo '<option value="' . $sec['id'] . '">' . $sec['exam_title'] . '</option>';
            }
            echo '</select></div>
                    </div>';
        } else {
            echo '<div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-6">
                        <div class="alert alert-warning">
                                <strong>' . lang('exac_info') . '</strong> Sorry This class does not have any exam added yet
                        </div></div></div>';
        }
    }
    //This function show a success message when an Exam added and made this exam routine fully, with full rutine.
    public function routinView() {
        $parient_id = $this->session->userdata("user_id");
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata("parient_id");
        }
        if($this->input->get('eid'))
        {
            $examId = $this->input->get('eid');
            $data['rutineInfo'] = $this->common->getWhere('exam_routine', 'exam_id', $examId);
            $data['examInfo'] = $this->common->getWhere('add_exam', 'id', $examId);
            $data['schoolName'] = $this->common->schoolName($parient_id);
            $this->load->view('temp/header');
            $this->load->view('rutineSuccess', $data);
            $this->load->view('temp/footer') ;
        }
        elseif ($this->input->post('submit', TRUE) && $this->input->post('examId', TRUE)) {
            $examId = $this->input->post('examId', TRUE);
            $data['rutineInfo'] = $this->common->getWhere('exam_routine', 'exam_id', $examId);
            $data['examInfo'] = $this->common->getWhere('add_exam', 'id', $examId);
            $data['schoolName'] = $this->common->schoolName($parient_id);
            $this->load->view('temp/header');
            $this->load->view('rutineSuccess', $data);
            $this->load->view('temp/footer');
        } else {
            redirect('examination/allExamRutine');
        }
    }

    public function editExamAndRoutine() {
        $parient_id = $this->session->userdata("user_id");
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata("user_id");
        }
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8))) 
        {
            redirect('auth', 'refresh');
        }
        if($this->input->get('eid1') && $this->input->get('id1'))
        {
            $exam_id1 = $this->input->get('eid1');
            $sbjct_id = $this->input->get('id1');
            $chk = "";
            $this->db->WHERE('exam_title',$exam_id1);
            $this->db->select('id');
            $r1 = $this->db->get('exam_attendanc');
            foreach ($r1->result_array() as $k1) {
               $chk = $k1['id']; 
            }
            if(empty($chk))
            {
                $this->db->WHERE('id',$sbjct_id);
                $this->db->delete('exam_routine');
                $cls_id = $this->input->get('cls', TRUE);
                $scl_nam = $this->input->get('s_name', TRUE);
                $data['subject'] = $this->common->getWhere('class_subject', 'section_id', $cls_id);
                $data['rutineInfo'] = $this->common->getWhere('exam_routine', 'exam_id', $exam_id1);
                $data['examInfo'] = $this->common->getWhere('add_exam', 'id', $exam_id1);
                $data['schoolName'] = $scl_nam;
                $this->load->view('temp/header');
                $this->load->view('editExamRoutine', $data);
                $this->load->view('temp/footer');
            }
            else{
                $data['successMessage'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                        <strong>'.lang('error'). '</strong>' .lang('edit_err1').'
                </div>';
                $cls_id = $this->input->get('cls', TRUE);
                $scl_nam = $this->input->get('s_name', TRUE);
                $data['subject'] = $this->common->getWhere('class_subject', 'section_id', $cls_id);
                $data['rutineInfo'] = $this->common->getWhere('exam_routine', 'exam_id', $exam_id1);
                $data['examInfo'] = $this->common->getWhere('add_exam', 'id', $exam_id1);
                $data['schoolName'] = $scl_nam;
                $this->load->view('temp/header');
                $this->load->view('editExamRoutine', $data);
                $this->load->view('temp/footer');
            }
        }
        elseif($this->input->post('submit', TRUE))
        {
            $examId = $this->input->post('exam_id', TRUE);
            $cls_id = $this->input->post('cls', TRUE);
            $scl_nam = $this->input->post('scol', TRUE);
            $chk = "";
            $this->db->WHERE('exam_title',$examId);
            $this->db->select('id');
            $r2 = $this->db->get('exam_attendanc');
            foreach ($r2->result_array() as $k2) {
               $chk = $k2['id'];
            }
            if(empty($chk))
            {
                $total_papers = $this->input->post('count', TRUE);
                $exam_startDate = $this->input->post('start_dat', TRUE);
                $total_papers = $total_papers - 1;
                for ($x = 1; $x <= $total_papers; $x++) {
                    $subjct_id = $this->input->post("id_$x", TRUE);
                    $date = $this->input->post("date_$x", TRUE);
                    $subject = $this->input->post("subject_$x", TRUE);
                    $room = $this->input->post("romeNo_$x", TRUE);
                    $s_time = $this->input->post("startTime_$x", TRUE);
                    $end_time = $this->input->post("endTime_$x", TRUE);
                    $s_time = date('h:i A', strtotime($s_time));
                    $end_time = date('h:i A', strtotime($end_time));
                    $shift = $this->input->post("examShift_$x", TRUE);
                    $data = array(
                        'exam_date' => $date,
                        'exam_subject' => $subject,
                        'rome_number' => $room,
                        'start_time' => $s_time,
                        'end_time' => $end_time,
                        'exam_shift' => $shift,
                    );
                    $data1 = array(
                        'start_date' => $exam_startDate
                    );
                    $this->db->WHERE('id',$subjct_id);
                    $this->db->update('exam_routine', $data);

                    $this->db->WHERE('id',$examId);
                    $this->db->update('add_exam', $data1);
                    // $data['rutineInfo'] = $this->common->getWhere('exam_routine', 'exam_id', $examId);
                    // $data['examInfo'] = $this->common->getWhere('add_exam', 'id', $examId);
                    // $data['schoolName'] = $this->common->schoolName($parient_id);
                    // $this->load->view('temp/header');
                    // $this->load->view('rutineSuccess', $data);
                    // $this->load->view('temp/footer');
                }
                redirect('examination/routinView?eid='.$examId);
            }
            else
            {
                $data['successMessage'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                        <strong>'.lang('error'). '</strong>' .lang('edit_err1').'
                </div>';
                $data['subject'] = $this->common->getWhere('class_subject', 'section_id', $cls_id);
                $data['rutineInfo'] = $this->common->getWhere('exam_routine', 'exam_id', $examId);
                $data['examInfo'] = $this->common->getWhere('add_exam', 'id', $examId);
                $data['schoolName'] = $scl_nam;
                $this->load->view('temp/header');
                $this->load->view('editExamRoutine', $data);
                $this->load->view('temp/footer');
            }
        }
        else
        {
            $examId = $this->input->get('examId', TRUE);
            $cls_id = $this->input->get('cls', TRUE);
            $scl_nam = $this->input->get('scol_nam', TRUE);
            $data['subject'] = $this->common->getWhere('class_subject', 'section_id', $cls_id);
            $data['rutineInfo'] = $this->common->getWhere('exam_routine', 'exam_id', $examId);
            $data['examInfo'] = $this->common->getWhere('add_exam', 'id', $examId);
            $data['schoolName'] = $scl_nam;
            $this->load->view('temp/header');
            $this->load->view('editExamRoutine', $data);
            $this->load->view('temp/footer');
        }
    }
    //This function is using for select class and students for exam attendance.
    public function selectExamAttendance() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8) && !$this->ion_auth->is_teacher())) {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        if($this->ion_auth->is_teacher())
        {
            $my_id = $this->session->userdata('user_id');
            $t_id = "";
            $this->db->where('user_id',$my_id);
            $this->db->select('id');
            $res1 = $this->db->get('teachers_info');
            foreach ($res1->result_array() as $key) {
                $t_id = $key['id'];
            }
            $data['s_class'] = $this->common->getWhere22('class','parient_id',$parient_id,'incharge',$t_id);
            $this->load->view('temp/header');
            $this->load->view('selectExamAttendance', $data);
            $this->load->view('temp/footer');
        }
        else
        {
            $data['s_class'] = $this->common->getAllData('class', $parient_id);
            $this->load->view('temp/header');
            $this->load->view('selectExamAttendance', $data);
            $this->load->view('temp/footer');
        }
    }
    //This function is using for taking students by class title for exam attendence
    public function examAttendance() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8) && !$this->ion_auth->is_teacher())) {
            redirect('auth', 'refresh');
        }
        $date = date("d/m/Y");
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        if ($this->input->post('submit', TRUE)) 
        {
            $cid = $this->input->post('class_id', TRUE);
            $examId = $this->input->get('id');
            if($cid == false)
            {
               redirect('examination/selectExamAttendance');
            }
            $examTitle = $this->exammodel->examTitle($examId);
            $examSubject = $this->exammodel->examSubject($examId, $date);
            $sbjctId = $this->input->post("sbjctId", TRUE);
            //Whene submit the attendence information after takeing the attendence
            $i = $this->input->post('in_velu', TRUE);
            $class_id = $this->input->post('class_id', TRUE);
            for ($x = 1; $x <= $i; $x++) {
                $roll = $this->input->post("roll_$x", TRUE);
                $name = $this->input->post("studentName_$x", TRUE);
                $present = $this->input->post("action_$x", TRUE);
                $userId = $this->input->post("userId_$x", TRUE);
                $studentInfoId = $this->input->post("studentInfoId_$x", TRUE);
                $section = $this->input->post("section", TRUE);
                $data = array(
                    'parient_id' => $this->db->escape_like_str($parient_id),
                    'date' => $this->db->escape_like_str($date),
                    'exam_title' => $this->db->escape_like_str($examId),
                    'exam_subject' => $this->db->escape_like_str($examSubject),
                    'subject_id' => $this->db->escape_like_str($sbjctId),
                    'user_id' => $this->db->escape_like_str($userId),
                    'student_id' => $this->db->escape_like_str($studentInfoId),
                    'roll_no' => $this->db->escape_like_str($roll),
                    'class_id' => $this->db->escape_like_str($class_id),
                    'section' => $this->db->escape_like_str($section),
                    'attendance' => $this->db->escape_like_str($present),
                    'student_title' => $this->db->escape_like_str($name),
                );
                //insert the $data information into "daily_attendance" database.
               $this->db->insert('exam_attendanc', $data);
            }
            //Whene Exam Attendance was full compleate then lode this page
            $data['p_id'] = $parient_id;
            $data['previerAttendance'] = $this->exammodel->previewAttendance($section, $examId, $sbjctId);
            $data['classTitle'] = $class_id;
            $data['sec'] = $section;
            $this->load->view('temp/header');
            $this->load->view('viewExamAttendance', $data);
            $this->load->view('temp/footer');
        } 
        else 
        {
            $examId = $this->input->post('examId', TRUE);
            if($examId == false){
                redirect('examination/selectExamAttendance');
            }
            $examTitle = $this->exammodel->examTitle($examId);
            $class_id = $this->input->post('class', TRUE);
            $check = $this->exammodel->checkExam($examId, $date);
            if (!empty($check)) 
            {
                //Here is loding student for exam attendance.
                //Get here students and informations by class title.
                $queryData = array();
                $query = $this->db->get_where('class_students', array('section' => $class_id));
                foreach ($query->result_array() as $row) 
                {
                    $queryData[] = $row;
                }
                $data['students'] = $queryData;
                $data['sbjctId'] = $check;
                $data['examId'] = $examId;
                $data['examTitle'] = $examTitle;
                $data['examSubject'] = $this->exammodel->examSubject($examId, $date);
                $data['classTitle'] = $this->common->class_title($class_id);
                $data['secTitle'] = $this->common->section_title($class_id);
                if (!empty($data['students'])) 
                {
                    $this->load->view('temp/header');
                    $this->load->view('examAttendance', $data);
                    $this->load->view('temp/footer');
                } 
                else 
                {
                    $data['success'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                                                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                                        <strong>Alert!</strong> Sorry This class does not have any student
                                                                </div>';
                    $data['s_class'] = $this->common->getAllData('class', $parient_id);
                    $this->load->view('temp/header');
                    $this->load->view('selectExamAttendance', $data);
                    $this->load->view('temp/footer');

                }
            } 
            else if(empty($check)) 
            {
                $class_id = $this->input->post('class', TRUE);
                $info['classTitle'] = $this->common->class_title($class_id);
                $this->load->view('temp/header');
                $this->load->view('attendanceFaild', $info);
                $this->load->view('temp/footer');
            }
        }
    }
    //This function load's exam attendance view
    public function viewExamAttendance() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8) && !$this->ion_auth->is_teacher())) {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        if($this->ion_auth->is_teacher())
        {
            $my_id = $this->session->userdata('user_id');
            $t_id = "";
            $this->db->where('user_id',$my_id);
            $this->db->select('id');
            $res1 = $this->db->get('teachers_info');
            foreach ($res1->result_array() as $key) {
                $t_id = $key['id'];
            }
            $data['s_class'] = $this->common->getWhere22('class','parient_id',$parient_id,'incharge',$t_id);
        }
        else
        {
            $data['s_class'] = $this->common->getAllData('class', $parient_id);
        }
        if ($this->input->post('submit', TRUE) && $this->input->post('class', TRUE) && $this->input->post('examTitle', TRUE) && $this->input->post('subjectTitle', TRUE)) {
            $classTitle = $this->input->post('class', TRUE);
            $examID = $this->input->post('examTitle', TRUE);
            $subjectID = $this->input->post('subjectTitle', TRUE);
            $cid ="";
            $this->db->WHERE('id',$classTitle);
            $this->db->select('class_id');
            $this->db->from('class');
            $q1 = $this->db->get();
            foreach ($q1->result_array() as $r1) {
                $cid = $r1['class_id'];
            }
            $data['classTitle'] = $cid;
            $data['p_id'] = $parient_id;
            $data['sec'] = $classTitle;
            $data['previerAttendance'] = $this->exammodel->previewAttendance($classTitle, $examID, $subjectID);
            $this->load->view('temp/header');
            $this->load->view('viewExamAttendance', $data);
            $this->load->view('temp/footer');
        } else {
            $this->load->view('temp/header');
            $this->load->view('allExamAttendanceView', $data);
            $this->load->view('temp/footer');
        }
    }
    //This function is called by ajax from view
    public function ajaxAttendanceView() {
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $class_id = $this->input->get('q');
        $acad_year = $this->session->userdata('s_from')."-".$this->session->userdata('s_to');
        $this->db->WHERE('class_id',$class_id);
        $this->db->WHERE('academic_year',$acad_year);
        $query = $this->db->get('add_exam');
        foreach ($query->result_array() as $row) {
            $data[] = $row;
        }
        if (!empty($data)) {
            echo '<div class="form-group">
                        <label class="col-md-3 control-label">' . lang('exac_4') . '<span class="requiredStar"> * </span></label>
                        <div class="col-md-6">
                            <select name="examTitle" data-validation="required" data-validation-error-msg="" onchange="examSubjct(this.value)" class="form-control">
                            <option value="">--Select Exam--</option>';
            foreach ($data as $sec) {
                echo '<option value="' . $sec['id'] . '">' . $sec['exam_title'] . '</option>';
            }
            echo '</select></div>
                        </div>';
        } else {
            echo '<div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-6">
                        <div class="alert alert-warning">
                                <strong>' . lang('exac_info') . '</strong> Sorry there is no exam added for this class.
                        </div></div></div>';
        }
    }

     //The exam attendance can edit by this function.
    public function ajaxSbjct() 
    {
        $exam_id = $this->input->get('e_id');
        $subject = $this->common->getWhere('exam_routine', 'exam_id', $exam_id);
        if (!empty($subject)) 
        {
            echo '<div class="form-group">
                        <label class="col-md-3 control-label">' . lang('exac_8') . ' <span class="requiredStar"> * </span></label>
                        <div class="col-md-6">
                            <select name="subjectTitle" class="form-control">';
            foreach ($subject as $sub) 
            {
                echo '<option value="' . $sub['id'] . '">' . $sub['exam_subject'] . '</option>';
            }
            echo '</select></div>
                        </div>';
        } else
         {
            echo '<div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-6">
                        <div class="alert alert-warning">
                                <strong>' . lang('exac_info') . '</strong> Sorry there is no subject routine added for this exam.
                        </div></div></div>';
        }
    }
    //The exam attendance can edit by this function.
    public function editExamAttendance() {
         if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8) && !$this->ion_auth->is_teacher())) {
            redirect('auth', 'refresh');
        }
        $id = $this->input->get('id');
        if ($this->input->post('submit', TRUE)) {
            $updateInfo = array(
                'attendance' => $this->db->escape_like_str($this->input->post('action', TRUE))
            );
            $this->db->where('id', $id);
            if ($this->db->update('exam_attendanc', $updateInfo)) {
                redirect('examination/viewExamAttendance', 'refresh');
            }
        }
        $data['examAttendanceInf'] = $this->common->getWhere('exam_attendanc', 'id', $id);
        $this->load->view('temp/header');
        $this->load->view('editExamAttendance', $data);
        $this->load->view('temp/footer');
    }
    //Here is first time select class for result.
    public function makingResult() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8) && !$this->ion_auth->is_teacher())) {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        if($this->ion_auth->is_teacher())
        { 
            $my_id = $this->session->userdata('user_id');
            $t_id = "";
            $this->db->where('user_id',$my_id);
            $this->db->select('id');
            $res1 = $this->db->get('teachers_info');
            foreach ($res1->result_array() as $key) {
                $t_id = $key['id'];
            }
            $data['s_class'] = $this->common->getWhere22('class','parient_id',$parient_id,'incharge',$t_id);
        }
        else{
            $data['s_class'] = $this->common->getAllData('class', $parient_id);
        }
        if ($this->input->post('submit', TRUE)) 
        {
            if($this->input->post('examID', TRUE))
            {
                if($this->input->post('subjRutID', TRUE))
                {
                    $class_id = $this->input->post('class_id', TRUE);
                    $data['class_id'] = $class_id;
                    $data['examId'] = $this->input->post('examID', TRUE);
                    $data['subjectTitle'] = $this->input->post('examSubjectTitle', TRUE);
                    $data['examRUtinID'] = $this->input->post('subjRutID', TRUE);
                    $query1 = $this->db->get_where('exam_routine', array('parient_id' => $parient_id,'exam_id' => $this->input->post('examID', TRUE),'exam_subject' => $this->input->post('examSubjectTitle', TRUE)));
                    foreach ($query1->result_array() as $row1) {
                        $queryData1 = $row1['status'];
                    }
                    if($queryData1 == 'Result')
                    {
                        $data['s_class'] = $this->common->getAllData('class', $parient_id);
                        $data['success1'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                                                                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                                                <strong>Error!</strong> Exam Result for this subject has been submitted.
                                                                        </div>';
                        $this->load->view('temp/header');
                        $this->load->view('selectClassResult', $data);
                        $this->load->view('temp/footer');
                    }
                    else
                    {
                        $queryData = array();
                        $query = $this->db->get_where('class_students', array('section' => $class_id));
                        foreach ($query->result_array() as $row) {
                            $queryData[] = $row;
                        }
                        $data['students'] = $queryData;
                        $data['gread'] = $this->common->getAllData('exam_grade', $parient_id);
                        $this->load->view('temp/header');
                        $this->load->view('makingResult', $data);
                        $this->load->view('temp/footer');
                    }
                }
                else
                {
                    $data['s_class'] = $this->common->getAllData('class', $parient_id);
                    $data['success1'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                                                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                                            <strong>Info!</strong> Sorry you can not submit result
                                                                    </div>';
                    $this->load->view('temp/header');
                    $this->load->view('selectClassResult', $data);
                    $this->load->view('temp/footer');
                }  
            }
            else
            {
                $data['s_class'] = $this->common->getAllData('class', $parient_id);
                $data['success'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                                                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                                        <strong>Error!</strong> Sorry This class does not have any exam
                                                                </div>';
                $this->load->view('temp/header');
                $this->load->view('selectClassResult', $data);
                $this->load->view('temp/footer');
            }
            
        } else {
            $this->load->view('temp/header');
            $this->load->view('selectClassResult', $data);
            $this->load->view('temp/footer');
        }
    }
    //This function will take class and give the class exam title and subject
    public function ajaxClassResult() {
        $class_id = $this->input->get('q');
        $data = $this->exammodel->examTitleRes($class_id);
        if (!empty($data)) {
            echo '<div class="form-group">
                        <label class="col-md-3 control-label">' . lang('exac_4') . ' <span class="requiredStar"> * </span></label>
                        <div class="col-md-6">
                            <select onchange="examSubject(this.value,'.$class_id.')" name="examID" class="form-control" data-validation="required" data-validation-error-msg="">
                            <option value="">' . lang('select') . '</option>';
            foreach ($data as $sec) {
                echo '<option value="' . $sec['id'] . '">' . $sec['exam_title'] . '</option>';
            }
            echo '</select></div>
                        </div><div id="ajaxResult_2"></div>';
        } else {
            echo '<div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-6">
                        <div class="alert alert-warning">
                                <strong>' . lang('exac_info') . '</strong> This class does not have any exam.
                        </div></div></div>';
        }
    }
    //This function will show via ajax examination subject which are not compleated result
    public function ajaxExamSubject() {
        $examID = $this->input->get('erid');
        $clasID = $this->input->get('cid');
        $examDate = $this->exammodel->examDate($examID);
        $examDate = $examDate[0];
        $now = date('d/m/Y');
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
             $parient_id = $this->session->userdata('parient_id');
        }
        $subject = $this->exammodel->examResSubject($examDate,$now,$parient_id,$clasID,$examID);
        if (!empty($subject)) {
            echo '<div class="form-group">
                        <label class="col-md-3 control-label">' . lang('exac_8') . ' <span class="requiredStar"> * </span></label>
                        <div class="col-md-6">
                            <select onchange="examSubjectTitle(this.value)" name="subjRutID" class="form-control" data-validation="required" data-validation-error-msg="">
                            <option value="">' . lang('select') . '</option>';
            foreach ($subject as $sub) {

                echo '<option value="' . $sub['subject_id'] . '">' . $sub['exam_subject'] . '</option>';
            }
            echo '</select></div>
                        </div><div id="ajaxResult_3"></div>';
        } else {
            echo '<div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-6">
                        <div class="alert alert-warning">
                                <strong>' . lang('exac_info') . '</strong>'.lang('rslt_info').'
                        </div></div></div>';
        }
    }
    //This function will returan exam subject title 
    public function ajaxExamSubTitle() {
        $examId = $this->input->get('erid');
        $query = $this->db->query("SELECT exam_subject FROM exam_routine WHERE id='$examId'");
        foreach ($query->result_array() as $row) {
            $data = $row['exam_subject'];
            echo '<input type="hidden" name="examSubjectTitle" value="' . $row['exam_subject'] . '">';
        }
    }
    //This function will submit result from teacher.
    public function submitResult() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8) && !$this->ion_auth->is_teacher())) {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $i = $this->input->post('ivalue', TRUE);
        $examID = $this->input->post('examId', TRUE);
        $examTitle = $this->exammodel->examTitle($examID);
        $examRuId = $this->input->post('examRutinID', TRUE);
        $examSubject = $this->exammodel->examSubjectTitle($examRuId);
        $class_id = $this->input->post('class_id', TRUE);
        $teacherName = $this->input->post('teacherName', TRUE);
        //here is checking this subject is optionakl or not
        $date = date('d/m/Y');
        for ($a = 1; $a <= $i; $a++) {
            $rollNumber = $this->input->post("rollNumber_$a", TRUE);
            $result = $this->input->post("result_$a", TRUE);
            //$greadInfo = $this->input->post("gread_$a", TRUE);
            $grade = $this->input->post("gread_$a", TRUE);
            $query = $this->db->query("SELECT point FROM exam_grade WHERE grade_name='$grade' AND parient_id='$parient_id'");
            foreach ($query->result_array() as $row) {
                $data1 = $row['point'];
            }
            $resultInfo = array(
                'parient_id' => $this->db->escape_like_str($parient_id),
                'exam_id' => $this->db->escape_like_str($examID),
                'exam_title' => $this->db->escape_like_str($examTitle),
                'class_id' => $this->db->escape_like_str($class_id),
                'student_name' => $this->db->escape_like_str($this->input->post("studentTitle_$a", TRUE)),
                'student_id' => $this->db->escape_like_str($this->input->post("studentId_$a", TRUE)),
                'roll_number' => $this->db->escape_like_str($this->input->post("rollNumber_$a", TRUE)),
                'exam_subject' => $this->db->escape_like_str($examSubject),
                'subject_id' => $this->db->escape_like_str($examRuId),
                'result' => $this->db->escape_like_str($this->input->post("result_$a", TRUE)),
                'total_marks' => $this->db->escape_like_str($this->input->post("totalMark_$a", TRUE)),
                'obtained_marks' => $this->db->escape_like_str($this->input->post("obtainedMarks$a", TRUE)),
                'grade' => $this->db->escape_like_str($grade),
                'point' => $this->db->escape_like_str($data1),
            );
            $this->db->insert('result_shit', $resultInfo);
        }
        $submitInfo = array(
            'parient_id' => $this->db->escape_like_str($parient_id),
            'class_id' => $this->db->escape_like_str($class_id),
            'exam_title' => $this->db->escape_like_str($examTitle),
            'exam_id' => $this->db->escape_like_str($examID),
            'date' => $this->db->escape_like_str($date),
            'subject' => $this->db->escape_like_str($examSubject),
            'subject_id' => $this->db->escape_like_str($examRuId),
            'submited' => $this->db->escape_like_str(0),
            'teacher' => $this->db->escape_like_str($teacherName),
        );
        $subjectStatus = array(
            'status' => $this->db->escape_like_str('Result')
        );
        $this->db->where('id', $examRuId);
        $this->db->update('exam_routine', $subjectStatus);
        if ($this->db->insert('result_submition_info', $submitInfo)) {
            $data['examTitle'] = $examTitle;
            $data['examSubject'] = $examSubject;
            $data['teacherName'] = $teacherName;
            $data['class_id'] = $class_id;
            $this->load->view('temp/header');
            $this->load->view('submitMessage', $data);
            $this->load->view('temp/footer');
        }
    }
    //This function work only for admin.
    //He can view how many result shit was submited for aprove by admin.
    public function aproveShitView() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8)))
        {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $data['shitList'] = $this->common->getWhere22('result_submition_info','parient_id',$parient_id, 'submited', 0);
        $academic_session = $this->session->userdata('s_from')."-".$this->session->userdata('s_to');
        $data['classAction'] = $this->common->getWhere2('result_action', 'status', 'Not Complete','academic_year',$academic_session,'parient_id',$parient_id);
        $this->load->view('temp/header');
        $this->load->view('aproveShitView', $data);
        $this->load->view('temp/footer');
    }
    //This function can exes only admin and he can edit and Approve exam rtesult shit
    public function checkResultShit() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8)))
        {
            redirect('auth', 'refresh');
        }
        $id = $this->input->get('id', TRUE);
        $query = $this->common->getWhere('result_submition_info', 'id', $id);
        $examTitle = $query[0]['exam_title'];
        $class_id = $query[0]['class_id'];
        $subject = $query[0]['subject'];
        $data['examId'] = $id;
        $data['examTitle'] = $query[0]['exam_title'];
        $data['class_id'] = $class_id;
        $data['teacher'] = $query[0]['teacher'];
        $data['subject'] = $query[0]['subject'];
        $data['resultShit'] = $this->exammodel->checkResultShit($class_id, $examTitle, $subject);
        $this->load->view('temp/header');
        $this->load->view('checkResultShit', $data);
        $this->load->view('temp/footer');
    }
    //This function will edit student's result,number and grade,point
    public function editResult() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8)))
        {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $id = $this->input->get('id');
        if ($this->input->post('submit', TRUE)) {
            $grade = $this->input->post('gread', TRUE);
            $query = $this->db->query("SELECT point FROM exam_grade WHERE grade_name='$grade' AND parient_id='$parient_id'");
            foreach ($query->result_array() as $row) {
                $data1 = $row['point'];
            }
            $updateData = array(
                'result' => $this->db->escape_like_str($this->input->post('result', TRUE)),
                'total_marks' => $this->db->escape_like_str($this->input->post('totalMark', TRUE)),
                'obtained_marks' => $this->db->escape_like_str($this->input->post('obtainedMarks', TRUE)),
                'point' => $this->db->escape_like_str($data1),
                'grade' => $this->db->escape_like_str($grade)
            );
            $this->db->where('id', $id);
            if ($this->db->update('result_shit', $updateData)) {
                redirect('examination/aproveShitView', 'refresh');
            }
        } else {
            $data['gread'] = $this->common->getAllData('exam_grade',$parient_id);
            $data['previousResult'] = $this->common->getWhere('result_shit', 'id', $id);
            $this->load->view('temp/header');
            $this->load->view('editResult', $data);
            $this->load->view('temp/footer');
        }
    }
    //This function will approuve result shit which is sent from teacher.
    public function approuveResultShit() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8)))
        {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $id = $this->input->get('id');
        $data = array(
            'submited' => $this->db->escape_like_str(1)
        );
        $this->db->where('id', $id);
        if ($this->db->update('result_submition_info', $data)) {
            $query = $this->common->getWhere('result_submition_info', 'id', $id);
            foreach ($query as $row) {
                $rowInfo = $row;
            }
            $class_id = $rowInfo['class_id'];
            $examTitle = $rowInfo['exam_title'];
            $examId = $rowInfo['exam_id'];
            $subject = $rowInfo['subject'];
            $approuveSubject = $this->exammodel->approuveSubjectAmount($class_id, $examId);
            $examSubjects = $this->exammodel->examEubject($examId,$parient_id);
            $academic_session = $this->session->userdata('s_from')."-".$this->session->userdata('s_to');
            // var_dump($id." ");
            // var_dump($class_id." ");
            // var_dump($examTitle." ");
            // var_dump($examId." ");
            // var_dump($subject." ");
            // var_dump($approuveSubject." ");
            // var_dump($examSubjects);
            // die;
            if ($approuveSubject == $examSubjects) {

                $actionArrayt = array(
                    'parient_id' => $this->db->escape_like_str($parient_id),
                    'class_id' => $this->db->escape_like_str($class_id),
                    'exam_title' => $this->db->escape_like_str($examTitle),
                    'exam_id' => $this->db->escape_like_str($examId),
                    'status' => $this->db->escape_like_str('Not Complete'),
                    'academic_year' => $academic_session,
                );
                if ($this->db->insert('result_action', $actionArrayt)) {
                    redirect('examination/aproveShitView', 'refresh');
                }
            } else {
                redirect('examination/aproveShitView', 'refresh');
            }
        }
    }
    //This function will make finalresult for class students
    public function finalResult() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8)))
        {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $examActionId = $this->input->get('id');
        $class_id = $this->input->get('class');
        $examTitle = $this->input->get('exam');
        $examId = $this->input->get('examId');
        //Here taking a students list by class title
        $studentQuery = $this->common->getWhere('class_students', 'section', $class_id);
        foreach ($studentQuery as $row) {
            $studentId = $row['student_id'];
            $absent = $this->exammodel->absent($studentId,$examId);
            if ($absent == 0) {
                $fail = $this->exammodel->fail($studentId,$examId);
                if ($fail == 0) {
                    //$classSubject = $this->exammodel->classSubjectAmount($class_id);
                    $examSubjects = $this->exammodel->examEubject($examId,$parient_id);
                    $finalPoint = $this->exammodel->pointAverage($studentId,$examSubjects,$examId);
                    $gradeAverage = $this->exammodel->averageGrade($finalPoint,$parient_id);
                    $obtMarks = $this->exammodel->totalMark($studentId,$examId,$parient_id);
                    $totalMarks = $this->exammodel->totalMark1($studentId,$examId,$parient_id);
                    // var_dump($obtMarks."/".$totalMarks);
                    // die;
                    $finalResultArray = array(
                        'parient_id' => $this->db->escape_like_str($parient_id),
                        'class_id' => $this->db->escape_like_str($class_id),
                        'exam_id' => $this->db->escape_like_str($examId),
                        'exam_title' => $this->db->escape_like_str($examTitle),
                        'student_id' => $this->db->escape_like_str($studentId),
                        'student_name' => $this->db->escape_like_str($row['student_title']),
                        'final_grade' => $this->db->escape_like_str($gradeAverage),
                        'point' => $this->db->escape_like_str($finalPoint),
                        'total_marks' => $this->db->escape_like_str($totalMarks),
                        'obtained_marks' => $this->db->escape_like_str($obtMarks),
                        'status' => $this->db->escape_like_str('Pass'),
                    );
                    $this->db->insert('final_result', $finalResultArray);
                } else {
                    $examSubjects = $this->exammodel->examEubject($examId,$parient_id);
                    //$classSubject = $this->exammodel->classSubjectAmount($class_id);
                    $pointAverage = $this->exammodel->pointAverage($studentId,$examSubjects,$examId);
                    $gradeAverage = $this->exammodel->averageGrade($pointAverage,$parient_id);
                    $obtMarks = $this->exammodel->totalMark($studentId,$examId,$parient_id);
                    $totalMarks = $this->exammodel->totalMark1($studentId,$examId,$parient_id);
                    $finalResultArray = array(
                        'parient_id' => $this->db->escape_like_str($parient_id),
                        'class_id' => $this->db->escape_like_str($class_id),
                        'exam_id' => $this->db->escape_like_str($examId),
                        'exam_title' => $this->db->escape_like_str($examTitle),
                        'student_id' => $this->db->escape_like_str($studentId),
                        'student_name' => $this->db->escape_like_str($row['student_title']),
                        'final_grade' => $this->db->escape_like_str($gradeAverage),
                        'point' => $this->db->escape_like_str($pointAverage),
                        'total_marks' => $this->db->escape_like_str($totalMarks),
                        'obtained_marks' => $this->db->escape_like_str($obtMarks),
                        'status' => $this->db->escape_like_str('Fail'),
                        'fail_amount' => $this->db->escape_like_str($fail)
                    );
                    $this->db->insert('final_result', $finalResultArray);
                }
            } else {
                $finalResultArray = array(
                    'parient_id' => $this->db->escape_like_str($parient_id),
                    'class_id' => $this->db->escape_like_str($class_id),
                    'exam_id' => $this->db->escape_like_str($examId),
                    'exam_title' => $this->db->escape_like_str($examTitle),
                    'student_id' => $this->db->escape_like_str($studentId),
                    'student_name' => $this->db->escape_like_str($row['student_title']),
                    'final_grade' => $this->db->escape_like_str('--'),
                    'point' => $this->db->escape_like_str('--'),
                    'total_marks' => $this->db->escape_like_str('--'),
                    'obtained_marks' => $this->db->escape_like_str('--'),
                    'status' => $this->db->escape_like_str('Absent'),
                    'fail_amount' => $this->db->escape_like_str('--')
                );
                $this->db->insert('final_result', $finalResultArray);
            }
        }
        $examActionArray = array(
            'status' => $this->db->escape_like_str('Complete'),
            'publish' => $this->db->escape_like_str('Not Publish')
        );
        $this->db->where('id', $examActionId);
        if ($this->db->update('result_action', $examActionArray)) {
            $data['massage'] = '<br><div class="alert alert-success">
                                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                <strong>' . lang('success') . '</strong> ' . lang('exac_12') . ' "' . $examTitle . '" ' . lang('exac_13') . ' "' . $this->common->class_title($class_id) . '"' . lang('exac_14') . '<br>
                                                <strong>' . lang('exac_info') . ' </strong>' . lang('exac_15') . '   
                                        </div>';
            $data['shitList'] = $this->common->getWhere('result_submition_info', 'submited', 0);
            $academic_session = $this->session->userdata('s_from')."-".$this->session->userdata('s_to');
            $data['classAction'] = $this->common->getWhere22('result_action', 'status', 'Not Complete','academic_year',$academic_session);
            $this->load->view('temp/header');
            $this->load->view('aproveShitView', $data);
            $this->load->view('temp/footer');
        }
    }
    //This function select class for results 
    public function selectResult() {
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata("parient_id");
        }
        if($this->ion_auth->is_student())
        {
            $id = $this->session->userdata("user_id");
            $class_id = "";
            $this->db->WHERE('user_id',$id);
            $this->db->SELECT('section');
            $this->db->FROM('class_students');
            $query = $this->db->get();
            foreach ($query->result_array() as $row) {
                $class_id = $row['section'];
            }
            $data['result'] = $this->exammodel->publish1('Complete', 'Publish', $parient_id , $class_id);
            $this->load->view('temp/header');
            $this->load->view('selectResult', $data);
            $this->load->view('temp/footer');
        }
        elseif($this->ion_auth->is_parents())
        {
            $id = $this->session->userdata("user_id");
            $class_id = "";
            $this->db->WHERE('user_id',$id);
            $this->db->SELECT('section');
            $this->db->FROM('parents_info');
            $query = $this->db->get();
            foreach ($query->result_array() as $row) {
                $class_id = $row['section'];
            }
            $data['result'] = $this->exammodel->publish1('Complete', 'Publish', $parient_id , $class_id);
            $this->load->view('temp/header');
            $this->load->view('selectResult', $data);
            $this->load->view('temp/footer');
        }
        else
        {
            $data['result'] = $this->exammodel->publish('Complete', 'Publish',$parient_id);
            $this->load->view('temp/header');
            $this->load->view('selectResult', $data);
            $this->load->view('temp/footer');
        }
    }
    //This function will show details result in a class 
    public function fullResult() {
        if($this->ion_auth->is_student())
        {
            $id = $this->session->userdata("user_id");
            $parient_id = $this->session->userdata("parient_id");
            $std_id = "";
            $this->db->WHERE('user_id',$id);
            $this->db->SELECT('student_id');
            $this->db->FROM('student_info');
            $query = $this->db->get();
            foreach ($query->result_array() as $row) {
                $std_id = $row['student_id'];
            }
            $class_id = $this->input->get('class');
            $examTitle = $this->input->get('exam');
            $examID = $this->input->get('exam_id');
            $data['result'] = $this->exammodel->finalResultShow1($class_id, $examID,$std_id,$parient_id);
            $data['class'] = $class_id;
            $data['examTitle'] = $examTitle;
            $data['examID'] = $examID;
            $this->load->view('temp/header');
            $this->load->view('fullResult', $data);
            $this->load->view('temp/footer');
        }
        elseif($this->ion_auth->is_parents())
        {
            $id = $this->session->userdata("user_id");
            $parient_id = $this->session->userdata("parient_id");
            $std_id = "";
            $this->db->WHERE('user_id',$id);
            $this->db->SELECT('student_id');
            $this->db->FROM('parents_info');
            $query = $this->db->get();
            foreach ($query->result_array() as $row) {
                $std_id = $row['student_id'];
            }
            $class_id = $this->input->get('class');
            $examTitle = $this->input->get('exam');
            $examID = $this->input->get('exam_id');
            $data['result'] = $this->exammodel->finalResultShow1($class_id, $examID,$std_id,$parient_id);
            $data['class'] = $class_id;
            $data['examTitle'] = $examTitle;
            $data['examID'] = $examID;
            $this->load->view('temp/header');
            $this->load->view('fullResult', $data);
            $this->load->view('temp/footer');
        }
        else
        {
            $class_id = $this->input->get('class');
            $examTitle = $this->input->get('exam');
            $examID = $this->input->get('exam_id');
            $data['result'] = $this->exammodel->finalResultShow($class_id, $examID);
            $data['class'] = $class_id;
            $data['examTitle'] = $examTitle;
            $data['examID'] = $examID;
            $this->load->view('temp/header');
            $this->load->view('fullResult', $data);
            $this->load->view('temp/footer');
        }
        
    }
    //By this function admin can publish exam result in day.
    public function publishResult() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8)))
        {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $query = $this->exammodel->publish('Complete', 'Not Publish', $parient_id);
        foreach ($query as $row) {
            $id = $row['id'];
            $examTitle = $row['exam_title'];
            $class_id = $row['class_id'];
            $array = array(
                'publish' => $this->db->escape_like_str('Publish')
            );
            $this->db->where('id', $id);
            if ($this->db->update('result_action', $array)) {
                $this->db->update('add_exam', $array, array('exam_title' => $this->db->escape_like_str($examTitle), 'class_id' => $this->db->escape_like_str($class_id)));
            }
        }
        redirect('examination/selectResult', 'refresh');
    }
    //This function will select studentfor know mark shit
    public function selectClassMarksheet() {
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        if($this->input->get('eid', TRUE) && $this->input->get('cid' , TRUE) && $this->input->get('sid' , TRUE))
        {
            $class_id = $this->input->get('cid');
            $examId = $this->input->get('eid');
            $studentId = $this->input->get('sid');
            $student_name = $this->input->get('sname');
            $data['markshit'] = $this->exammodel->markshit($examId, $class_id, $studentId);
            $data['examTitle'] = $this->exammodel->examTitle($examId);
            $data['studentId'] = $studentId;
            $data['studentName'] = $student_name;
            $data['section'] = $class_id;
            $this->load->view('temp/header');
            $this->load->view('marksheet', $data);
            $this->load->view('temp/footer');
        }
        else
        {
            if ($this->input->post('submit', TRUE)) 
            {
                if ($this->input->post('examId', TRUE) && $this->input->post('studentId', TRUE)) {
                    $class_id = $this->input->post('class_id', TRUE);
                    $examId = $this->input->post('examId', TRUE);
                    $studentId = $this->input->post('studentId', TRUE);
                    $data['markshit'] = $this->exammodel->markshit($examId, $class_id, $studentId);
                    $data['examTitle'] = $this->exammodel->examTitle($examId);
                    $data['studentId'] = $studentId;
                    $data['section'] = $class_id;
                    $data['studentName'] = $this->input->post('studentTitle', TRUE);
                    $this->load->view('temp/header');
                    $this->load->view('marksheet', $data);
                    $this->load->view('temp/footer');
                } else {
                    $data['success'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                                                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                                            <strong>Error!</strong> Sorry There is no marksheet for this class yet
                                                                    </div>';
                    $data['class'] = $this->common->getAllData('class', $parient_id);
                    $this->load->view('temp/header');
                    $this->load->view('selectClassMarksheet', $data);
                    $this->load->view('temp/footer');
                }
            } else {
                $data['class'] = $this->common->getAllData('class', $parient_id);
                $this->load->view('temp/header');
                $this->load->view('selectClassMarksheet', $data);
                $this->load->view('temp/footer');
            }
        }
    }

    //This function will called by ajax from view for load class markshit
    public function ajaxClassMarkshit() {
        $class_id = $this->input->get('q');
        $acad_year = $this->session->userdata('s_from')."-".$this->session->userdata('s_to');
        $query = $this->exammodel->examTitleForMarkshit($class_id,$acad_year);
        foreach ($query as $row) {
            $data[] = $row;
        }
        if (!empty($data)) {
            echo '<div class="form-group">
                        <label class="col-md-3 control-label">' . lang('exac_4') . ' <span class="requiredStar"> * </span></label>
                        <div class="col-md-6">
                            <select name="examId" class="form-control">';
            foreach ($data as $sec) {
                echo '<option value="' . $sec['id'] . '">' . $sec['exam_title'] . '</option>';
            }
            echo '</select></div>
                        </div>';
            $student = $this->common->getWhere('class_students', 'section', $class_id);
            if (!empty($student)) {
                echo '<div class="form-group">
                            <label class="col-md-3 control-label">' . lang('exac_17') . ' <span class="requiredStar"> * </span></label>
                            <div class="col-md-6">
                                <select name="studentId" class="form-control">';
                foreach ($student as $stu) {
                    echo '<option value="' . $stu['student_id'] . '">' . $stu['student_title'] . '</option>';
                }
                echo '</select></div>
                    <input type="hidden" name="studentTitle" value="' . $stu['student_title'] . '">
                            </div>';
            } else {
                echo '<div class="form-group">
                            <label class="col-md-3 control-label"></label>
                            <div class="col-md-6">
                            <div class="alert alert-warning">
                                    <strong>' . lang('exac_info') . '</strong> There is no student in this class sorry
                            </div></div></div>';
            }
        } else {
            echo '<div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-6">
                        <div class="alert alert-warning">
                                <strong>' . lang('exac_info') . '</strong> There is no marksheet for this class yet
                        </div></div></div>';
        }
    }
    //This function will select student's own marksheet
    public function sel_ow_ma() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_student() && !$this->ion_auth->is_parents()))
        {
            redirect('auth', 'refresh');
        }
        $userId = $this->session->userdata('user_id');
        $parient_id = $this->session->userdata('parient_id');
        if ($this->input->post('submit', TRUE)) {
            if ($this->input->post('examId', TRUE)) {
                $class_id = $this->input->post('class_id', TRUE);
                $examId = $this->input->post('examId', TRUE);
                if($this->ion_auth->is_parents())
                {
                    $data1 = "";$data2="";
                    $this->db->select('student_id');
                    $this->db->where('parient_id', $parient_id);
                    $this->db->where('user_id', $userId);
                    $query = $this->db->get('parents_info');
                    foreach ($query->result_array() as $row) {
                        $data1 = $row['student_id'];
                    }
                    $studentId = $data1;                
                }
                else
                {
                    $user = $this->ion_auth->user()->row();
                    $userId = $user->id;
                    $studentId = $this->exammodel->student_id($userId);
                    //$data["studentName"] = $this->input->post('studentTitle', TRUE);
                }
                $this->db->select('student_nam');
                $this->db->where('parient_id', $parient_id);
                $this->db->where('student_id', $studentId);
                $query1 = $this->db->get('student_info');
                foreach ($query1->result_array() as $row1) {
                    $data2 = $row1['student_nam'];
                }
                $data["studentName"] = $data2;
                $data["markshit"] = $this->exammodel->markshit($examId, $class_id, $studentId);
                $data["examTitle"] = $this->exammodel->examTitle($examId);
                $data["studentId"] = $studentId;
                $this->load->view('temp/header');
                $this->load->view('ow_marksheet', $data);
                $this->load->view('temp/footer');
            } else {
                $data['success'] = '<div class="alert alert-danger alert-dismissable admisionSucceassMessageFont">
                                                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                                        <strong>Error!</strong> Sorry There is no marksheet for this class yet
                                                                </div>';
                $data['class'] = $this->common->getAllData('class', $parient_id);
                $this->load->view('temp/header');
                $this->load->view('sel_ow_ma', $data);
                $this->load->view('temp/footer');
            }
        } else {
            $cid="";
            if($this->ion_auth->is_student())
            {
                $this->db->WHERE('user_id',$userId);
                $this->db->select('section');
                $quer1 = $this->db->get('class_students');
                foreach ($quer1->result_array() as $key) {
                    $cid = $key['section'];
                }
            }
            elseif ($this->ion_auth->is_parents()) {
                $this->db->WHERE('user_id',$userId);
                $this->db->select('section');
                $quer1 = $this->db->get('parents_info');
                foreach ($quer1->result_array() as $key) {
                    $cid = $key['section'];
                }
            }
            $data['class'] = $this->common->getWhere('class','id',$cid);
            $this->load->view('temp/header');
            $this->load->view('sel_ow_ma', $data);
            $this->load->view('temp/footer');
        }
    }
    //This function will called by ajax from view for load class markshit
    public function ajax_ow_ma() {
        $class_id = $this->input->get('q');
        $acad_year = $this->session->userdata('s_from')."-".$this->session->userdata('s_to');
        $query = $this->exammodel->examTitleForMarkshit($class_id,$acad_year);
        foreach ($query as $row) {
            $data[] = $row;
        }
        if (!empty($data)) {
            echo '<div class="form-group">
                        <label class="col-md-3 control-label">' . lang('exac_4') . ' <span class="requiredStar"> * </span></label>
                        <div class="col-md-6">
                            <select name="examId" class="form-control">';
            foreach ($data as $sec) {
                echo '<option value="' . $sec['id'] . '">' . $sec['exam_title'] . '</option>';
            }
            echo '</select></div>
                        </div>';
        } else {
            echo '<div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-6">
                        <div class="alert alert-warning">
                                <strong>' . lang('exac_info') . '</strong>  Sorry There is no marksheet for this class yet
                        </div></div></div>';
        }
    }

    public function own_examRoutine() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_student() && !$this->ion_auth->is_parents()))
        {
            redirect('auth', 'refresh');
        }
        $userId = $this->session->userdata('user_id');
        $parient_id = $this->session->userdata('parient_id');
        if ($this->input->post('submit', TRUE)) 
        {
            $examId = $this->input->post('examId', TRUE);
            $data['rutineInfo'] = $this->common->getWhere('exam_routine', 'exam_id', $examId);
            $data['examInfo'] = $this->common->getWhere('add_exam', 'id', $examId);
            $data['schoolName'] = $this->common->schoolName($parient_id);
            $this->load->view('temp/header');
            $this->load->view('rutineSuccess', $data);
            $this->load->view('temp/footer');
        }
        else 
        {
            $cid="";
            if($this->ion_auth->is_student())
            {
                $this->db->WHERE('user_id',$userId);
                $this->db->select('section');
                $quer1 = $this->db->get('class_students');
                foreach ($quer1->result_array() as $key) {
                    $cid = $key['section'];
                }
            }
            elseif ($this->ion_auth->is_parents()) {
                $this->db->WHERE('user_id',$userId);
                $this->db->select('section');
                $quer1 = $this->db->get('parents_info');
                foreach ($quer1->result_array() as $key) {
                    $cid = $key['section'];
                }
            }
            $acad_year = $this->session->userdata('s_from')."-".$this->session->userdata('s_to');
            $data['exams'] = $this->common->getWhere22('add_exam','class_id',$cid,'academic_year',$acad_year);
            $this->load->view('temp/header');
            $this->load->view('own_examRoutine', $data);
            $this->load->view('temp/footer');
        }
    }
}
