<!-- BEGIN CONTENT -->
<link rel="stylesheet" type="text/css" href="assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/bootstrap-datepicker/css/datepicker3.css"/>
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE CONTENT-->
        <?php
        foreach ($examInfo as $row) {
            $examID = $row['id'];
            $examTitle = $row['exam_title'];
            $startDate = $row['start_date'];
            $class_title = $this->common->class_title($row['class_id']);
            $sec_title = $this->common->section_title($row['class_id']);
            $total_time = $row['total_time'];
            // var_dump($examID."-".$examTitle."-".$startDate."-".$class_title."-".$total_time);
            // die;
        }
        ?>
        <div class="row">
            <div class="col-md-12 ">
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box green ">
                    <div class="portlet-title">
                        <div class="caption">
                             <?php echo $class_title." ".$sec_title; ?>  <?php echo lang('exa_rou_succ_1'); ?> " <?php echo $examTitle; ?> "
                        </div>
                        <div class="tools">
                            <a href="" class="collapse">
                            </a>
                            <a href="" class="reload">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <div class="alert alert-warning">
                            <?php if(!empty($successMessage)) {echo $successMessage;} ?>
                            <div>
                                <div class="row">
                                    <div class="col-md-12 textAlignCenter">
                                        <H2><?php echo $schoolName; ?></H2>
                                        <p>
                                        <h4 class="rtsh"><?php echo lang('exa_etit'); ?>: <?php echo $examTitle; ?></h4>
                                        <?php echo lang('exa_start_date'); ?> :
                                        <div style="display: inline;" class="date date-picker" data-date="" data-date-format="dd/mm/yyyy"  data-date-start-date="+0d">
                                                                    <input type="text" name="sdate" id="sdate" readonly data-validation="required" data-validation-error-msg="" value="<?php echo $startDate; ?>">
                                                                    <span style="display: inline;" class="input-group-btn">
                                                                        <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                                                    </span>
                                                                </div><br>
                                        <?php echo lang('exa_class'); ?> : <?php echo $class_title." ".$sec_title; ?><br>
                                        <?php echo lang('exa_total_time'); ?> : <?php echo $total_time; ?>
                                        </p>
                                    </div>
                                </div>
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <?php echo lang('exa_erdas'); ?>
                                        </div>
                                        <div class="tools">
                                            <a href="javascript:;" class="collapse">
                                            </a>
                                            <a href="javascript:;" class="reload">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <?php echo lang('srno'); ?>
                                                        </th>
                                                        <th>
                                                            <?php echo lang('date'); ?>
                                                        </th>
                                                        <th>
                                                            <?php echo lang('exa_sub_tit'); ?>
                                                        </th>
                                                        <th>
                                                            <?php echo lang('exa_rnih'); ?>
                                                        </th>
                                                        <th>
                                                            <?php echo lang('exa_start_time'); ?>
                                                        </th>
                                                        <th>
                                                            <?php echo lang('exa_end_time'); ?>
                                                        </th>
                                                        <th>
                                                            <?php echo lang('exa_shift'); ?>
                                                        </th>
                                                        <th>
                                                            <?php echo lang('exa_actd'); ?>
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <div class="form">
                                                        <?php $form_attributs = array('name'=>'myForm','class' => 'form-horizontal', 'role' => 'form','onsubmit'=>'return validateForm()');
                        echo form_open('examination/editExamAndRoutine', $form_attributs);
                        ?>
                                                        <div class="form-body">
                                                            <div class="form-group">
                                                                <div id="div_scents">
                                                                    <div class="row">
                                                    <?php
                                                    $i = 1;
                                                    foreach ($rutineInfo as $row1) {
                                                        ?>
                                                        <tr>
                                                            <td>
                                                                <?php echo $i; ?>
                                                            </td>
                                                            <td>
                                                                <div class="input-group input-medium date date-picker" data-date="" data-date-format="dd/mm/yyyy"  data-date-start-date="+0d">
                                                                    <input type="text" class="form-control" name="date_<?php echo $i; ?>" readonly data-validation="required" data-validation-error-msg="" value="<?php echo $row1['exam_date']; ?>">
                                                                    <span class="input-group-btn">
                                                                        <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                                                    </span>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="classGroupInput">
                                                                    <select class="form-control" name="subject_<?php echo $i; ?>" data-validation="required" data-validation-error-msg="">
                                                                        <option value="<?php echo $row1['exam_subject']; ?>"><?php echo $row1['exam_subject']; ?></option>
                                                                        <?php foreach ($subject as $row2) { 
                                                                            if($row1['exam_subject'] != $row2['subject_title']) { ?>
                                                                            <option value="<?php echo $row2['subject_title']; ?>"><?php echo $row2['subject_title']; ?></option>
                                                                        <?php }} ?>
                                                                    </select>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div classGroupInput">
                                                                    <input type="text" class="form-control input-small" placeholder="" name="romeNo_<?php echo $i; ?>" data-validation="required" data-validation-error-msg="" value="<?php echo $row1['rome_number']; ?>" >
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <!-- <?php echo $row1['start_time']; ?> -->
                                                                <div classGroupInput">
                                                                    <input type="time" class="form-control input-small" placeholder="" name="startTime_<?php echo $i; ?>" data-validation="required" data-validation-error-msg="" value="<?php echo $row1['start_time']; ?>">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <!-- <?php echo $row1['end_time']; ?> -->
                                                                <div classGroupInput">
                                                                    <input type="time" class="form-control input-small" placeholder="" name="endTime_<?php echo $i; ?>" data-validation="required" data-validation-error-msg="" value="<?php echo $row1['end_time']; ?>">
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="classGroupInput">
                                                                    <select class="form-control" name="examShift_<?php echo $i; ?>" data-validation="required" data-validation-error-msg="">
                                                                        <option value="<?php echo $row1['exam_shift']; ?>"><?php echo $row1['exam_shift']; ?></option>
                                                                        <?php if($row1['exam_shift'] != lang('exa_morn_shi')) { ?>
                                                                        <option value="<?php echo lang('exa_morn_shi'); ?>"><?php echo lang('exa_morn_shi'); ?></option><?php } ?>
                                                                        <?php if($row1['exam_shift'] != lang('exa_even_shi')) { ?>
                                                                        <option value="<?php echo lang('exa_even_shi'); ?>"><?php echo lang('exa_even_shi'); ?></option><?php } ?>
                                                                    </select>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <a class="btn btn-xs tableActionButtonMargin" href="index.php/examination/editExamAndRoutine?id1=<?php echo $row1['id']; ?>&eid1=<?php echo $row['id']; ?>&cls=<?php echo $row['class_id']; ?>&s_name=<?php echo $schoolName; ?>" onClick="javascript:return confirm('Are you sure you want to remove this?')"> <ul><i class="fa fa-trash-o fa-2x" aria-hidden="true" style="color:red;"></i></ul></a>
                                                            </td>
                                                            <input type="hidden" name="id_<?php echo $i; ?>" value="<?php echo $row1['id']; ?>">
                                                        </tr>
                                                        <?php
                                                        $i++;
                                                    }
                                                    ?>
                                                    <input type="hidden" id="count" name="count" value="<?php echo $i; ?>">
                                                    <input type="hidden" name="exam_id" value="<?php echo $examID; ?>">
                                                    <input type="hidden" id="start_dat" name="start_dat" value="">
                                                    <input type="hidden" name="cls" value="<?php echo $row['class_id']; ?>">
                                                    <input type="hidden" name="scol" value="<?php echo $schoolName; ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
        
                                
                                                </tbody>
                                            </table>
                                            <div class="form-actions fluid">
                                                <div class="col-md-offset-5 col-md-6">
                                                    <button type="submit" class="btn green" name="submit" value="Submit"><?php echo lang('edit'); ?></button>
                                                </div>
                                            </div>
                                            <?php echo form_close(); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->
<script type="text/javascript" src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="assets/admin/pages/scripts/components-pickers.js"></script>
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script src="assets/admin/pages/scripts/table-advanced.js"></script>
<script src="assets/global/plugins/jquery.form-validator.min.js" type="text/javascript"></script>
<script> $.validate();</script>
<script>
    jQuery(document).ready(function() {
//here is auto reload after 1 second for time and date in the top
        jQuery(setInterval(function() {
            jQuery("#result").load("index.php/home/iceTime");
        }, 1000));
        
    if (jQuery().datepicker) 
    {
        $('.date-picker').datepicker({
            rtl: Metronic.isRTL(),
            orientation: "left",
            autoclose: true
        });
    }
    
    });

    function validateForm() {
        var y = document.getElementById('count').value;
        y = y - 1 ;
        var count ;
        var n_date ;
        var s_date = document.getElementById('sdate').value;
        for(count = 1 ; count <= y ; count++)
        {
            n_date = document.forms["myForm"]["date_"+count].value;
            if(n_date < s_date)
            {
                alert("subject exam date must be greater then examination start date");
                return false;
            }
        }
        document.getElementById('start_dat').value = s_date;
    }
</script>