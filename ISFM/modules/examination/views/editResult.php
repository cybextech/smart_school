<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12 ">
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box green ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-bars"></i> <?php echo lang('exa_eprngap'); ?>
                        </div>
                        <div class="tools">
                            <a href="" class="collapse">
                            </a>
                            <a href="" class="reload">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <?php
                        $form_attributs = array('class' => 'form-horizontal', 'role' => 'form');
                        $id = $this->input->get('id');
                        echo form_open("examination/editResult?id=$id", $form_attributs);
                        ?>
                        <?php $user = $this->ion_auth->user()->row(); ?>
                        <div class="form-body">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <?php echo lang('exa_gtnagp'); ?>
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"></a>
                                        <a href="javascript:;" class="reload"></a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <?php echo lang('exa_ct_role'); ?>
                                                    </th>
                                                    <th>
                                                        <?php echo lang('exa_ct_sn'); ?>
                                                    </th>
                                                    <th>
                                                        <?php echo lang('exa_ct_sid'); ?>
                                                    </th>
                                                    <th>
                                                        <?php echo "Obtained Marks"; ?>
                                                    </th>
                                                    <th>
                                                        <?php echo lang('exa_ct_toma'); ?>
                                                    </th>
                                                    
                                                    <th>
                                                        <?php echo lang('exa_ct_grade'); ?>
                                                    </th>
                                                     <th>
                                                        <?php echo lang('exa_ct_result'); ?>
                                                    </th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $i = 0;
                                                foreach ($previousResult as $row) {
                                                    ?>
                                                    <tr>
                                                        <td>    <?php echo $row['roll_number']; ?>
                                                        </td>
                                                        <td>    <?php echo $row['student_name']; ?>
                                                        </td>
                                                        <td>    <?php echo $row['student_id']; ?>
                                                        </td>
                                                        <td>
                                                            <input class="form-control" type="text" value="<?php echo $row['obtained_marks']; ?>" id="textBox<?php echo $i; ?>" name="obtainedMarks" onchange="selectClass(<?php echo $i; ?>)" data-validation="required" data-validation-error-msg="">
                                                            
                                                        </td>
                                                        <td>
                                                            <input class="form-control" type="text" id="textBox1<?php echo $i; ?>" onchange="selectClass(<?php echo $i; ?>)" name="totalMark" value="<?php echo $row['total_marks']; ?>" data-validation="required" data-validation-error-msg="">
                                                            
                                                        </td>
                                                       
                                                        <td>
                                                            <input class="form-control" type="text" id="grade<?php echo $i; ?>" name="gread" value="<?php echo $row['grade']; ?>" readonly data-validation="required" data-validation-error-msg="">
                                                        </td>
                                                        <td>
                                                            <select class="form-control editresultSelect" name="result"  data-validation="required" data-validation-error-msg="">
                                                                <option value="Pass" <?php if($row['result'] == 'Pass'){ echo "selected"; }?>> <?php echo lang('exa_pass'); ?> </option>
                                                                <option value="Fail" <?php if($row['result'] == 'Fail'){ echo "selected"; }?>> <?php echo lang('exa_fail'); ?> </option>
                                                                <option value="Absent" <?php if($row['result'] == 'Absent'){ echo "selected"; }?>> <?php echo lang('exa_absent'); ?> </option>
                                                            </select>
                                                        </td>
                                                        
                                                    </tr>
                                                <?php } ?>
                                            <input class="form-control" type="hidden" name="ivalue" value="<?php echo $i; ?>"/>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions fluid">
                            <div class="col-md-offset-3 col-md-6">
                                <button type="submit" class="btn green" name="submit" name="Update" value="Update"><?php echo lang('exa_up_result'); ?></button>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->
<script src="assets/global/plugins/jquery.form-validator.min.js" type="text/javascript"></script>
<script> $.validate();</script>
<script>
    jQuery(document).ready(function () {

        jQuery(setInterval(function () {
            jQuery("#result").load("index.php/home/iceTime");
        }, 1000));
    });



    var grade = '<?php echo(json_encode($gread)); ?>';
    var json = $.parseJSON(grade);
   
    function selectClass(str) {
        var reslt = ($('#textBox'+str).val() / $('#textBox1'+str).val())*100;
        for(var i = 0 ; i < json.length ; i++)
        {
            
            if((reslt >= json[i].number_form) && (reslt <= json[i].number_to))
            {
                
                $("#grade"+str).val(json[i].grade_name);
                
                console.log(json[i].grade_name);
                break;
            }
        }
    }
</script>
