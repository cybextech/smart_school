<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12 ">
                <div class="alert alert-danger">
                    <h3><?php echo "Sorry "." ".lang('exa_today_a').$classTitle.'"'." does not has any exam. So you can not take exam attendance for this class."; ?></h3><hr>
                    <div class="clearfix">
                        <button class="btn blue btn-lg" type="button" onclick="location.href = 'javascript:history.back();'"><?php echo lang('back'); ?></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->

