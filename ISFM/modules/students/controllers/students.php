<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class Students extends MX_Controller {

    /**
     * This controller is using for controlling to students
     *
     * Maps to the following URL
     * 		http://example.com/index.php/users
     * 	- or -  
     * 		http://example.com/index.php/users/<method_name>
     */
    function __construct() {
        parent::__construct();
        $this->load->model('studentmodel');
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login');
        }
    }
    //This function is used for get all students in this system.
    public function allStudent() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8) && !$this->ion_auth->is_teacher()))
        {
            redirect('auth', 'refresh');
        }
        if($this->session->userdata('email_info')){
            $data['email_info'] = $this->session->userdata('email_info');
        }else{
            $data['email_info'] = '';
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        if ($this->ion_auth->is_teacher()) 
        {
            $my_id = $this->session->userdata('user_id');
            $t_id = "";
            $this->db->where('user_id',$my_id);
            $this->db->select('id');
            $res1 = $this->db->get('teachers_info');
            foreach ($res1->result_array() as $key) {
                $t_id = $key['id'];
            }
            $c_id ="";
            $this->db->where('incharge',$t_id);
            $this->db->select('class_id');
            $res1 = $this->db->get('class');
            foreach ($res1->result_array() as $key) {
                $c_id = $key['class_id'];
            }
            $data['s_class'] = $this->common->getWhere22('classes','parient_id',$parient_id,'id',$c_id);
        }
        else
        {
            $data['s_class'] =  $this->common->getClass('classes','sections',0,'parient_id', $parient_id);
        }
        if ($this->input->post('submit', TRUE) && $this->input->post('section', TRUE))
        {
            $data['class_id'] = $this->input->post('class', TRUE);
            $class = $this->input->post('class', TRUE);
            $section = $this->input->post('section', TRUE);
            $data['section'] = $section;
            if ($section == 'all') {
                $data['studentInfo'] = $this->studentmodel->getStudentByClassSection($class, $section);
                if (!empty($data['studentInfo'])) {
                    //If the class have student then run here.
                    $this->load->view('temp/header');
                    $this->load->view('studentclass', $data);
                    $this->load->view('temp/footer');
                } else {
                    //If the class have no any student then print the massage in the view.
                    $data['message'] = lang('stuc_1');
                    $this->load->view('temp/header');
                    $this->load->view('studentclass', $data);
                    $this->load->view('temp/footer');
                }
            } 
            else 
            {
                $data['studentInfo'] = $this->studentmodel->getStudentByClassSection($class, $section);
                if (!empty($data['studentInfo'])) 
                {
                    //If the class have student then run here.
                    $this->load->view('temp/header');
                    $this->load->view('studentclass', $data);
                    $this->load->view('temp/footer');
                } 
                else 
                {
                    //If the class have no any student then print the massage in the view.
                    $data['message'] = lang('stuc_1');
                    $this->load->view('temp/header');
                    $this->load->view('studentclass', $data);
                    $this->load->view('temp/footer');
                }
            }
        } else {
            //First of all this method run here and load class selecting view
            $this->load->view('temp/header');
            $this->load->view('slectStudent', $data);
            $this->load->view('temp/footer');
        }
    }

    //This function is used for filtering to get students information
    //Whene class and section gave in the frontend, if the class have section he cane select the section and get student information in the viwe.
    public function ajaxClassSection() {
        $classid = $this->input->get('id');
        if($this->ion_auth->is_teacher())
        {
            $my_id = $this->session->userdata('user_id');
            $t_id = "";
            $this->db->where('user_id',$my_id);
            $this->db->select('id');
            $res1 = $this->db->get('teachers_info');
            foreach ($res1->result_array() as $key) {
                $t_id = $key['id'];
            }
            $query = $this->common->getWhere22('class', 'class_id', $classid, 'incharge', $t_id);
            echo '<div class="form-group">
                    <label class="col-md-3 control-label"></label>
                    <div class="col-md-4">
                        <select name="section" class="form-control">
                            ';
            foreach ($query as $sec) {
                echo '<option value="' . $sec['id'] . '">' . $sec['section'] . '</option>';
            }
            echo '</select></div>
                    </div>';
        }
        else
        {
            $query = $this->common->getWhere('class', 'class_id', $classid);
            echo '<div class="form-group">
                    <label class="col-md-3 control-label"></label>
                    <div class="col-md-4">
                        <select name="section" class="form-control">
                            <option value="all">' . lang('stu_sel_cla_velue_all') . '</option>';
            foreach ($query as $sec) {
                echo '<option value="' . $sec['id'] . '">' . $sec['section'] . '</option>';
            }
            echo '</select></div>
                    </div>';
        }
    }

    //This function is giving a student's the full information. 
    public function students_details() 
    {
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $id = $this->input->get('id');
        $studentId = $this->input->get('sid');
        $userId = $this->input->get('userId');
        $studentClass = $this->input->get('id');
        $class_id = $this->input->get('class_id');
        $data['stdId'] = $studentId ;
        $data['cls'] = $class_id ;
        $data['parient_id'] = $parient_id ;
        $data['studentInfo'] = $this->studentmodel->studentDetails($id);
        $data['photo'] = $this->studentmodel->studentPhoto1($studentId,$class_id,$parient_id);
        $this->load->view('temp/header');
        $this->load->view('studentsDetails', $data);
        $this->load->view('temp/footer');
    }

    //This function is use for edit student's informations.
    public function editStudent() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8)))
        {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $userId = $this->input->get('userId');
        $studentInfoId = $this->input->get('sid');
        $studentClass = $this->input->get('id');
        $class_id = $this->input->get('class_id');
        if ($this->input->post('submit', TRUE)) {
            $section_id = $this->input->post('sectin_id', TRUE);
            $username = $this->input->post('first_name', TRUE) . ' ' . $this->input->post('last_name', TRUE);
            $additional_data = array(
                'first_name' => $this->db->escape_like_str($this->input->post('first_name', TRUE)),
                'last_name' => $this->db->escape_like_str($this->input->post('last_name', TRUE)),
                'username' => $this->db->escape_like_str($username),
                'phone' => $this->db->escape_like_str($this->input->post('phone1', TRUE)),
                'email' => $this->db->escape_like_str($this->input->post('email', TRUE))
            );
            $this->db->where('id', $userId);
            $this->db->update('users', $additional_data);


            $username = $this->input->post('first_name', TRUE) . ' ' . $this->input->post('last_name', TRUE);
            $studentsInfo = array(
                'student_id' => $this->db->escape_like_str($this->input->post('student_id', TRUE)),
                'student_nam' => $this->db->escape_like_str($username),
                'roll_number' => $this->db->escape_like_str($this->input->post('roll_number', TRUE)),
                'student_id' => $this->db->escape_like_str($this->input->post('student_id', TRUE)),
                'class_id' => $this->db->escape_like_str($this->input->post('class', TRUE)),
                'farther_name' => $this->db->escape_like_str($this->input->post('father_name', TRUE)),
                'birth_date' => $this->db->escape_like_str($this->input->post('birthdate', TRUE)),
                'sex' => $this->db->escape_like_str($this->input->post('sex', TRUE)),
                'present_address' => $this->db->escape_like_str($this->input->post('present_address', TRUE)),
                'permanent_address' => $this->db->escape_like_str($this->input->post('permanent_address', TRUE)),
                'father_occupation' => $this->db->escape_like_str($this->input->post('father_occupation', TRUE)),
                'father_incom_range' => $this->db->escape_like_str($this->input->post('father_incom_range', TRUE)),
                'last_class_certificate' => $this->db->escape_like_str($this->input->post('previous_certificate', TRUE)),
                't_c' => $this->db->escape_like_str($this->input->post('tc', TRUE)),
                'academic_transcription' => $this->db->escape_like_str($this->input->post('at', TRUE)),
                'national_birth_certificate' => $this->db->escape_like_str($this->input->post('nbc', TRUE)),
                'testimonial' => $this->db->escape_like_str($this->input->post('testmonial', TRUE)),
                'blood' => $this->db->escape_like_str($this->input->post('blood', TRUE)),
            );
            // $this->db->where('user_id', $studentInfoId);
            // $this->db->update('student_info', $studentsInfo);
            $a = $this->input->post('section');
            $query = $this->common->getWhere('class','id',$a);
            $additionalData3 = array(
                'class_title' => $this->db->escape_like_str($query[0] ["class_title"]),
                'roll_number' => $this->db->escape_like_str($this->input->post('roll_number', TRUE)),
                'student_id' => $this->db->escape_like_str($this->input->post('student_id', TRUE)),
                'class_id' => $this->db->escape_like_str($this->input->post('class', TRUE)),
                'student_title' => $this->db->escape_like_str($username),
                'section' => $this->db->escape_like_str($this->input->post('section'))
            );
            $this->db->where('id', $studentClass);
            $this->db->update('class_students', $additionalData3);
            if($this->input->post('section') != $section_id)
            {
                $studentAmount = $this->common->classStudentAmount($this->input->post('section'));
                $clas_info1 = array(
                            'student_amount' => $this->db->escape_like_str($studentAmount)
                        );
                        $this->db->where('id', $this->input->post('section'));
                        $this->db->update('class', $clas_info1);
                $studentAmount1 = $this->common->classStudentAmountdecrement($section_id);
                $clas_info2 = array(
                    'student_amount' => $this->db->escape_like_str($studentAmount1)
                );
                $this->db->where('id', $section_id);
                $this->db->update('class', $clas_info2);
            }
            

            $data['success'] = '<div class="alert alert-info alert-dismissable admisionSucceassMessageFont">
                                                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                                                    <strong>' . lang('success') . '</strong> ' . lang('stuc_2') . '
                                            </div>';
            $data['classStudents'] = $this->common->getWhere('class_students', 'id', $studentClass);
            $data['studentInfo'] = $this->common->getWhere('student_info', 'user_id', $studentInfoId);

            $data['users'] = $this->common->getWhere('users', 'id', $userId);
            $data['s_class'] = $this->common->getClass('classes','sections',0,'parient_id', $parient_id);
            $data['section'] = $this->studentmodel->section($class_id);
            $sec = $data['classStudents'][0]['section'];
            $cls = $data['classStudents'][0]['class_id'];
            $data['class_id'] = $cls;
            $data['section'] = $sec;
            $data['studentInfo'] = $this->studentmodel->getStudentByClassSection($cls, $sec);
            // var_dump($data['studentInfo']);
            // die;
            $this->load->view('temp/header');
            $this->load->view('studentclass', $data);
            $this->load->view('temp/footer');
        } else {
            //first here load the edit student view with student's previous value.
            $data['classStudents'] = $this->common->getWhere('class_students', 'id', $studentClass);
            $data['studentInfo'] = $this->common->getWhere('student_info', 'user_id', $studentInfoId);
            $data['users'] = $this->common->getWhere('users', 'id', $userId);
            $data['s_class'] = $this->common->getClass('classes','sections',0,'parient_id', $parient_id);
            $data['sectiond'] = $this->studentmodel->section($class_id);
            $this->load->view('temp/header');
            $this->load->view('editStudentInfo', $data);
            $this->load->view('temp/footer');
        }
    }
    //This function is use for delete a student.
    public function studentDelete() {
        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !$this->ion_auth->in_group(8)))
        {
            redirect('auth', 'refresh');
        }
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');
        }
        $id = $this->input->get('id');
        $count = 0;
        $studentInfoId = $this->input->get('sid');
        $userId = $this->input->get('userId');
        $class_id = $this->input->get('class_id');
        if ($this->db->delete('class_students', array('id' => $id)) && $this->db->delete('student_info', array('user_id' => $userId)) && $this->db->delete('users', array('id' => $userId)) && $this->db->delete('role_based_access', array('user_id' => $userId)))
        {
            $pid = "";
            $parents = array();
            $this->db->WHERE('student_id',$studentInfoId);
            $this->db->WHERE('parient_id',$parient_id);
            $this->db->select('user_id');
            $quer1 = $this->db->get('parents_info');
            foreach ($quer1->result_array() as $line1) 
            {
                $pid = $line1['user_id'];
            }
            if(!empty($pid))
            {
                if($this->db->delete('parents_info', array('student_id' => $studentInfoId,'parient_id' => $parient_id)))
                {
                    $this->db->WHERE('user_id',$studentInfoId);
                    $this->db->select('id');
                    $quer2 = $this->db->get('parents_info');
                    foreach ($quer2->result_array() as $line2) 
                    {
                        $parents[] = $line2['id'];
                    }
                    $count = count($parents);
                    if($count == 0)
                    {
                        $this->db->delete('users', array('id' => $pid));
                        $this->db->delete('role_based_access', array('user_id' => $pid));
                        $this->db->delete('users_groups', array('user_id' => $pid));
                    }
                }
                // var_dump($count);
                // die;
            }
            $this->db->delete('users_groups', array('user_id' => $userId));
            $studentAmount = $this->common->classStudentAmount($class_id);
            $studentAmount = $studentAmount - 2;
            $clas_info = array('student_amount' => $this->db->escape_like_str($studentAmount));
            $this->db->where('id', $class_id);
            $this->db->update('class', $clas_info);
            redirect('students/allStudent');
        }
    }
    //This function will return only logedin students information
    public function studentsInfo() {
        $parient_id = $this->session->userdata('parient_id');
        $uid = $this->input->get('uisd');
        if ($this->ion_auth->is_parents())
        {
            $std_id="";
            $this->db->WHERE('user_id',$uid);
            $this->db->SELECT('student_id');
            $this->db->FROM('parents_info');
            $query = $this->db->get();
            foreach ($query->result_array() as $row) {
                $std_id = $row['student_id'];
            }
            $sid = $std_id;
            $this->db->WHERE('student_id',$sid);
            $this->db->WHERE('parient_id',$parient_id);
            $this->db->SELECT('user_id');
            $this->db->FROM('student_info');
            $query1 = $this->db->get();
            foreach ($query1->result_array() as $row1) {
                $std_id = $row1['user_id'];
            }
            $uid = $std_id;
        }
        $data['studentInfo'] = $this->studentmodel->ownStudentDetails($uid);
        $data['photo'] = $this->studentmodel->ownStudentPhoto($uid);
        $this->load->view('temp/header');
        $this->load->view('studentsDetails', $data);
        $this->load->view('temp/footer');
    }

    public function myAttendance() {
        $parient_id = $this->session->userdata('user_id');
        if(!$this->ion_auth->is_admin())
        {
            $parient_id = $this->session->userdata('parient_id');    
        }
        $uid = $this->input->get('uisd');
        if ($this->ion_auth->is_parents())
        {
            $std_id="";
            $this->db->WHERE('user_id',$uid);
            $this->db->SELECT('student_id');
            $this->db->FROM('parents_info');
            $query = $this->db->get();
            foreach ($query->result_array() as $row) {
                $std_id = $row['student_id'];
            }
            $sid = $std_id;
            $this->db->WHERE('student_id',$sid);
            $this->db->WHERE('parient_id',$parient_id);
            $this->db->SELECT('user_id');
            $this->db->FROM('student_info');
            $query1 = $this->db->get();
            foreach ($query1->result_array() as $row1) {
                $std_id = $row1['user_id'];
            }
            $uid = $std_id;
        }
        $acad_year = $this->session->userdata('s_from')."-".$this->session->userdata('s_to');
        $data['attndnce'] = $this->studentmodel->myAttendance($uid,$parient_id,$acad_year);
        $this->load->view('temp/header');
        $this->load->view('myAttendance',$data);
        $this->load->view('temp/footer');
    }
}
