<link rel="stylesheet" type="text/css" href="assets/global/plugins/bootstrap-datepicker/css/datepicker3.css"/>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo lang('snd')." ".lang('des_daily_atten'); ?> <small></small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo lang('home'); ?>
                        
                    </li>
                    <li>
                        <?php echo lang('header_stu_paren'); ?>
                        
                    </li>
                    <li>
                        <?php echo lang('snd'); ?>
                        
                    </li>
                    <li>
                        <?php echo lang('des_daily_atten'); ?>
                        
                    </li>
                    <li id="result" class="pull-right topClock"></li>
                </ul>
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <?php if(!empty($success))
                {
                    echo $success;
                }
                ?>
                <div class="tab-content">
                    <div id="tab_0" class="tab-pane active">
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption">
                                    <?php echo lang('des_daily_atten'); ?>
                                </div>
                                <div class="tools">
                                    <a class="collapse" href="javascript:;">
                                    </a>
                                    <a class="reload" href="javascript:;">
                                    </a>
                                </div>
                            </div>
                            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>
                                   <?php echo lang('srno'); ?>
                                </th>
                                <th>
                                    <?php echo lang('date'); ?>
                                </th>
                                <th>
                                    <?php echo lang('des_stud'); ?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; foreach ($attndnce as $row) { ?>
                                <tr>
                                    <td>
                                        <?php echo $i; ?>
                                    </td>
                                    <td> 
                                         <?php echo date("d-m-Y", $row['date']) ?>
                                    </td>
                                    <td>
                                        <?php if($row['present_or_absent'] == 'P')
                                        {
                                            echo '<span class="label label-sm label-success">Present</span>';
                                        }
                                        else
                                        {
                                            echo '<span class="label label-sm label-danger">Absent</span>';
                                        } ?>
                                    </td>
                                </tr>
                            <?php $i++;} ?>
                        </tbody>
                    </table>
                </div>
            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
        </div>
        <div class="form-actions fluid marginTopNone">
            <div class="col-md-offset-3 col-md-9">
                <button class="btn white col-sm-9 col-xs-12 routineGoBack" type="button" onclick="window.location.href = 'javascript:history.back()'"></button>
            </div>
        </div>
        <div class="form-actions fluid marginTopNone">
            <div class="col-md-offset-3 col-md-9">
                <button class="btn blue col-sm-9 col-xs-12 routineGoBack" type="button" onclick="window.location.href = 'javascript:history.back()'"><?php echo lang('back'); ?></button>
            </div>
        </div>
    </div>

    <!-- END CONTENT -->
    <script type="text/javascript" src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script>
        jQuery(document).ready(function () {
            if (jQuery().datepicker) {
                $('.date-picker').datepicker({
                    rtl: Metronic.isRTL(),
                    orientation: "left",
                    autoclose: true
                });
                //$('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
            }

            //here is auto reload after 1 second for time and date in the top
            jQuery(setInterval(function () {
                jQuery("#result").load("index.php/home/iceTime");
            }, 1000));
        });

        function attendanceClassSection(str) {
            var xmlhttp;
            if (str.length == 0) {
                document.getElementById("ajaxResult").innerHTML = "";
                return;
            }
            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            } else {
                // code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                    document.getElementById("ajaxResult").innerHTML = xmlhttp.responseText;
                }
            }
            xmlhttp.open("GET", "index.php/dailyAttendance/ajaxAttendencePreview?q=" + str, true);
            xmlhttp.send();
        }
    </script>
