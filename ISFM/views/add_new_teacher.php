<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="assets/global/plugins/bootstrap-datepicker/css/datepicker3.css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo lang('tea_ant'); ?> <small></small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo lang('home'); ?>
                    </li>
                    <li>
                        <?php echo lang('header_teacher'); ?>
                    </li>
                    <li>
                        <?php echo lang('header_a_nteac'); ?>
                    </li>
                    <li id="result" class="pull-right topClock"></li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12 ">
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet box green ">
                    <div class="portlet-title">
                        <div class="caption">
                            <?php echo lang('tea_gtifnt'); ?>
                        </div>
                        <div class="tools">
                            <a href="" class="collapse">
                            </a>
                            <a href="" class="reload">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <?php
                        $form_attributs = array('class' => 'form-horizontal', 'id' => 'validate_form' , 'role' => 'form' );
                        echo form_open_multipart('users/addTeacher', $form_attributs);
                        ?>
                        <div class="form-body">
                            <?php
                            if (!empty($success)) {
                                echo $success;
                            }
                            ?>

                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo "First Name"; ?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                    <input type="text" data-validation="alphanumeric" data-validation-allowing=" " class="form-control" placeholder="First Name" name="first_name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('tea_ln'); ?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                    <input type="text" data-validation="alphanumeric" data-validation-allowing=" "  class="form-control" placeholder="Last Name" name="last_name" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('tea_fn'); ?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                    <input type="text" data-validation="alphanumeric" data-validation-allowing=" " class="form-control" name="father_name"  placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('tea_mn'); ?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                    <input type="text" data-validation="alphanumeric" data-validation-allowing=" " class="form-control" name="mother_name"  placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3"><?php echo lang('tea_dob'); ?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-5">
                                    <div class="input-group input-medium date date-picker" data-date="" data-date-format="dd/mm/yyyy">
                                        <input type="text" class="form-control" name="birthdate" readonly id="mask_date2">
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>
                                    <!-- /input-group -->
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('tea_sex'); ?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-6 marginLeftSex">
                                    <div class="radio-list">
                                        <label class="radio-inline">
                                            <input type="radio" name="sex" id="optionsRadios4" value="Male" ><?php echo lang('tea_male'); ?></label>
                                        <label class="radio-inline">
                                            <input type="radio" name="sex" id="optionsRadios5" value="Female"> <?php echo lang('tea_female'); ?> </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="sex" id="optionsRadios6" value="Other"> <?php echo lang('tea_other'); ?> </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('tea_prea'); ?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                    <textarea class="form-control" name="present_address" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('tea_per_add'); ?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                    <textarea class="form-control" name="permanent_address" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('tea_pn'); ?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-1">
                                    <input type="text" min="0" maxlength="4" class="form-control" name="phoneCode" placeholder="0300" required="">
                                </div>
                                <div class="col-md-4">
                                    <input type="text" min="0" maxlength="7" class="form-control" name="phone" placeholder="1234567">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('tea_email'); ?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                    <input type="email" data-validation="alphanumeric" data-validation-allowing="@ ."  class="form-control" onkeyup="checkEmail(this.value)" placeholder="demo@demo.com"name="email" placeholder="" >
                                    <div id="checkEmail" class="col-md-12"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"> <?php echo lang('tea_pass'); ?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                    <input type="password" data-validation="alphanumeric" data-validation-allowing=" " class="form-control" name="password" id="password" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('tea_con_pass'); ?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                    <input type="password" data-validation="alphanumeric" data-validation-allowing=" " class="form-control" name="password_confirm" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('tea_pp'); ?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                    <select class="form-control" name="position" data-validation="required" data-validation-error-msg="You have to select anyone.">
                                        <option value=""><?php echo lang('select'); ?> </option>
                                        <?php foreach ($positions as $key) {?>
                                        <option value="<?php echo $key['position'] ?>"><?php echo $key['position'] ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <!-- <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('tea_in'); ?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                    <input type="text" data-validation="alphanumeric" data-validation-allowing=" " class="form-control" name="indexNo" placeholder="">
                                </div>
                            </div> -->
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('tea_fs'); ?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                    <input type="text" data-validation="alphanumeric"  data-validation-allowing=", " class="form-control" name="facultiesSubject" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('tea_wh'); ?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-6">
                                    <select name="workingHoure" class="form-control">
                                        <option value=""><?php echo lang('select'); ?></option>
                                        <option value="Part time"><?php echo lang('tea_pt'); ?></option>
                                        <option value="Full time"><?php echo lang('tea_ft'); ?></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('tea_eq'); ?><span class="requiredStar"> * </span></label>
                                <div class="col-md-2">
                                    <H4 class="eduFormTitle"><?php echo lang('tea_dd'); ?></H4>
                                    <input class="form-control eduForm" name="dd_1" type="text" placeholder="" data-validation="required alphanumeric" data-validation-allowing=" " data-validation-error-msg="">
                                    <input class="form-control eduForm" name="dd_2" type="text" placeholder="">
                                    <input class="form-control eduForm" name="dd_3" type="text" placeholder="">
                                    <input class="form-control" name="dd_4" type="text" placeholder="" >
                                    <input class="form-control" name="dd_5" type="text" placeholder="" >
                                </div>
                                <div class="col-md-3">
                                    <H4 class="eduFormTitle"><?php echo lang('tea_scu'); ?></H4>
                                    <input class="form-control eduForm" name="scu_1" type="text" placeholder="" data-validation="required alphanumeric" data-validation-allowing=" " data-validation-error-msg="">
                                    <input class="form-control eduForm" name="scu_2" type="text" >
                                    <input class="form-control eduForm" name="scu_3" type="text" >
                                    <input class="form-control eduForm" name="scu_4" type="text" >
                                    <input class="form-control eduForm" name="scu_5" type="text" >
                                </div>
                                <div class="col-md-2">
                                    <H4 class="eduFormTitle"><?php echo lang('tea_result'); ?></H4>
                                    <select name="result_1" data-validation="required" class="form-control eduForm">
                                        <option value="">--Select--</option>
                                        <option value="Pass"><?php echo lang('pas'); ?></option>
                                        <option value="Fail"><?php echo lang('fal'); ?></option>
                                    </select>
                                    <select name="result_2" class="form-control eduForm">
                                        <option value="">--Select--</option>
                                        <option value="Pass"><?php echo lang('pas'); ?></option>
                                        <option value="Fail"><?php echo lang('fal'); ?></option>
                                    </select>
                                    <select name="result_3" class="form-control eduForm">
                                        <option value="">--Select--</option>
                                        <option value="Pass"><?php echo lang('pas'); ?></option>
                                        <option value="Fail"><?php echo lang('fal'); ?></option>
                                    </select>
                                    <select name="result_4" class="form-control eduForm">
                                        <option value="">--Select--</option>
                                        <option value="Pass"><?php echo lang('pas'); ?></option>
                                        <option value="Fail"><?php echo lang('fal'); ?></option>
                                    </select>
                                    <select name="result_5" class="form-control eduForm">
                                        <option value="">--Select--</option>
                                        <option value="Pass"><?php echo lang('pas'); ?></option>
                                        <option value="Fail"><?php echo lang('fal'); ?></option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <H4 class="eduFormTitle"><?php echo lang('tea_py'); ?></H4>
                                    <input class="form-control eduForm" name="paYear_1" type="text" placeholder="YYYY" data-validation="required alphanumeric" maxlength="4" data-validation-error-msg="">
                                    <input class="form-control eduForm" maxlength="4" name="paYear_2" type="text"  placeholder="YYYY" >
                                    <input  class="form-control eduForm" maxlength="4" name="paYear_3" type="text" placeholder="YYYY" >
                                    <input class="form-control eduForm" maxlength="4" name="paYear_4" type="text"  placeholder="YYYY" >
                                    <input class="form-control eduForm" maxlength="4" name="paYear_5" type="text"  placeholder="YYYY" >
                                </div>
                            </div>
                            <div class="form-group last">
                                <label class="control-label col-md-3"><?php echo "Select Image"; ?> <span class="requiredStar"> * </span></label>
                                <div class="col-md-9">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail">
                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail uploadImagePreview">
                                        </div>
                                        <div>
                                            <span class="btn default btn-file">
                                                <span class="fileinput-new"><?php echo lang('tea_sii'); ?> </span>
                                                <span class="fileinput-exists"><?php echo lang('tea_change'); ?> </span>
                                                <input type="file" name="userfile" data-validation="required" data-validation-error-msg="">
                                            </span>
                                            <a href="#" class="btn red fileinput-exists" data-dismiss="fileinput"><?php echo lang('tea_remove'); ?> </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="alert alert-success">
                                <strong><?php echo lang('tea_note'); ?> :</strong> <?php echo lang('tea_sadi'); ?>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"> </label>
                                <div class="col-md-9">
                                    <div class="checkbox-list">
                                        <label>
                                            <input name="cv" value="submited" type="checkbox"> <?php echo lang('tea_cv'); ?></label>
                                        <label>
                                            <input name="educational_certificat" value="submited" type="checkbox"> <?php echo lang('tea_ec'); ?> </label>
                                        <label>
                                            <input name="exc" value="submited" type="checkbox"> <?php echo lang('tea_exc'); ?></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions fluid">
                            <div class="col-md-offset-3 col-md-6">
                                <button type="submit" class="btn green" name="submit" value="submit"><?php echo lang('tea_si'); ?></button>
                                <button type="reset" class="btn default"><?php echo lang('refresh'); ?></button>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->
<!-- BEGIN PAGE LEVEL script -->
<script type="text/javascript" src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="assets/admin/pages/scripts/components-pickers.js"></script>
<script type="text/javascript" src="assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script src="assets/admin/pages/scripts/table-advanced.js"></script>

<script type="text/javascript" src="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script type="text/javascript" src="assets/global/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
<script src="assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>
<script src="assets/global/plugins/typeahead/handlebars.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/typeahead/typeahead.bundle.min.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/components-form-tools.js"></script>
<script src="assets/global/plugins/jquery.form-validator.min.js" type="text/javascript"></script>
<script> $.validate();</script>
<script>
    jQuery(document).ready(function () {
            ComponentsFormTools.init();
            if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: Metronic.isRTL(),
                orientation: "left",
                autoclose: true
            });
        }
        });
        function checkEmail(str) {
            var xmlhttp;
            if (str.length === 0) {
                document.getElementById("checkEmail").innerHTML = "";
                return;
            }
            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            } else {
                // code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                    document.getElementById("checkEmail").innerHTML = xmlhttp.responseText;
                }
            };
            xmlhttp.open("GET", "index.php/commonController/checkEmail?val=" + str, true);
            xmlhttp.send();
        }

    jQuery(document).ready(function () {
//here is auto reload after 1 second for time and date in the top
        jQuery(setInterval(function () {
            jQuery("#result").load("index.php/home/iceTime");
        }, 1000));
    });
</script>
<script type="text/javascript">
    var RecaptchaOptions = {
        theme: 'custom',
        custom_theme_widget: 'recaptcha_widget'
    };
</script>
<!-- END PAGE LEVEL script -->
