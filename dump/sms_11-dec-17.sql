-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 11, 2017 at 02:48 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sms`
--

-- --------------------------------------------------------

--
-- Table structure for table `account_title`
--

CREATE TABLE IF NOT EXISTS `account_title` (
  `id` int(11) NOT NULL,
  `parient_id` int(11) DEFAULT NULL,
  `account_title` varchar(100) NOT NULL,
  `category` varchar(20) NOT NULL,
  `description` varchar(300) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account_title`
--

INSERT INTO `account_title` (`id`, `parient_id`, `account_title`, `category`, `description`) VALUES
(1, NULL, 'Rent', 'Expense', ''),
(2, NULL, 'Student Fee', 'Income', ''),
(3, NULL, 'Teacher Salary', 'Expense', ''),
(4, 80, 'bill', 'Expense', 'abc'),
(6, 80, 'student fee', 'Income', ''),
(7, 80, 'teacher fee', 'Expense', ''),
(8, 80, 'funds', 'Income', ''),
(9, 81, 'bill', 'Expense', 'Eclectricity, gas and other bills ect'),
(10, 81, 'Fee', 'Income', 'Student fees'),
(11, 81, 'Salary', 'Expense', 'Salary for staff'),
(12, 81, 'transport', 'Income', 'something you don\\''t know ;)');

-- --------------------------------------------------------

--
-- Table structure for table `add_exam`
--

CREATE TABLE IF NOT EXISTS `add_exam` (
  `id` int(11) NOT NULL,
  `parient_id` int(11) DEFAULT NULL,
  `year` int(5) NOT NULL,
  `exam_title` varchar(100) NOT NULL,
  `start_date` varchar(30) NOT NULL,
  `class_id` int(11) NOT NULL,
  `total_time` varchar(10) NOT NULL,
  `publish` varchar(50) NOT NULL,
  `final` text NOT NULL,
  `status` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `add_exam`
--

INSERT INTO `add_exam` (`id`, `parient_id`, `year`, `exam_title`, `start_date`, `class_id`, `total_time`, `publish`, `final`, `status`) VALUES
(4, 0, 2016, 'Test Exam 1', '09/06/2016', 1, '1 Hour 30 ', 'Publish', 'NoFinal', 'NoResult'),
(5, 0, 2016, 'FInal Exam', '08/10/2016', 3, '30 Minute', 'Not Publish', 'Final', 'NoResult'),
(6, 0, 2017, 'kACHAY pAPER', '10/03/2017', 11, '1 Hour 30 ', 'Not Publish', 'NoFinal', 'NoResult'),
(12, 80, 2017, 'winter Term', '15/12/2017', 26, '1 Hour', 'Not Publish', 'NoFinal', 'NoResult'),
(13, 80, 2017, 'winter Term', '13/12/2017', 27, '1 Hour', 'Not Publish', 'NoFinal', 'NoResult'),
(14, 80, 2017, 'test exam', '18/11/2017', 26, '1 Hour', 'Publish', 'NoFinal', 'NoResult'),
(15, 80, 2017, 'test exam', '18/11/2017', 26, '1 Hour', 'Publish', 'NoFinal', 'NoResult'),
(20, 80, 2017, 'test exam', '18/11/2017', 26, '1 Hour 30 ', 'Publish', 'NoFinal', 'NoResult'),
(22, 81, 2017, 'Mids', '04/12/2017', 28, '1 Hour', 'Publish', 'NoFinal', 'NoResult');

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE IF NOT EXISTS `books` (
  `id` int(11) NOT NULL,
  `parient_id` int(11) DEFAULT NULL,
  `date` int(11) NOT NULL,
  `isbn_no` varchar(20) NOT NULL,
  `book_no` int(11) NOT NULL,
  `books_title` varchar(100) NOT NULL,
  `authore` varchar(150) NOT NULL,
  `category` varchar(100) NOT NULL,
  `parent_category` varchar(100) DEFAULT NULL,
  `edition` varchar(100) NOT NULL,
  `pages` int(11) NOT NULL,
  `language` varchar(30) NOT NULL,
  `uploderTitle` varchar(100) NOT NULL,
  `books_amount` int(11) NOT NULL,
  `status` varchar(50) NOT NULL,
  `issu_date` int(11) NOT NULL,
  `due_date` int(11) NOT NULL,
  `issu_member_no` int(11) NOT NULL,
  `cover_photo` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `parient_id`, `date`, `isbn_no`, `book_no`, `books_title`, `authore`, `category`, `parent_category`, `edition`, `pages`, `language`, `uploderTitle`, `books_amount`, `status`, `issu_date`, `due_date`, `issu_member_no`, `cover_photo`) VALUES
(1, 81, 1511910000, '1231232323', 2911171, 'urdu1', 'adcwt', '13', '10', '1st', 200, 'urdu', 'asif bilal', 1, 'Available', 0, 0, 0, '704d855f5c05fd560c19503ba69c75a5.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `books_category`
--

CREATE TABLE IF NOT EXISTS `books_category` (
  `id` int(11) NOT NULL,
  `parient_id` int(11) DEFAULT NULL,
  `category_creator` varchar(100) NOT NULL,
  `category_title` varchar(100) NOT NULL,
  `parent_category` varchar(100) NOT NULL,
  `description` varchar(500) NOT NULL,
  `books_amount` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `books_category`
--

INSERT INTO `books_category` (`id`, `parient_id`, `category_creator`, `category_title`, `parent_category`, `description`, `books_amount`) VALUES
(8, 81, 'junaid ahmad', 'Science', '0', 'Sciencie related books', 0),
(9, 81, 'junaid ahmad', 'Programming', '0', 'Programming related books', 0),
(10, 81, 'junaid ahmad', 'Arts', '0', 'Arts related subjects', 0),
(11, 81, 'junaid ahmad', 'maths', '8', 'math', 0),
(12, 81, 'junaid ahmad', 'physics', '8', 'phy', 0),
(13, 81, 'junaid ahmad', 'urdu', '10', 'urdu', 0),
(14, 81, 'junaid ahmad', 'english', '10', 'eng', 0),
(15, 81, 'junaid ahmad', 'c++', '9', 'c++', 0),
(16, 81, 'junaid ahmad', 'java', '9', 'java', 0);

-- --------------------------------------------------------

--
-- Table structure for table `calender_events`
--

CREATE TABLE IF NOT EXISTS `calender_events` (
  `id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `start_date` varchar(15) NOT NULL,
  `end_date` varchar(15) NOT NULL,
  `color` varchar(15) NOT NULL,
  `url` varchar(200) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `calender_events`
--

INSERT INTO `calender_events` (`id`, `title`, `start_date`, `end_date`, `color`, `url`, `user_id`) VALUES
(1, 'test', '05-05-2016', '06-05-2016', 'red', 'test.com', 1),
(2, 'exam', '08-07-2017', '31-07-2017', 'purple', '', 8),
(3, 'test', '24-11-2017', '25-11-2017', 'green', 'anything', 81),
(4, 'exams', '28-11-2017', '30-11-2017', 'red', '', 81);

-- --------------------------------------------------------

--
-- Table structure for table `class`
--

CREATE TABLE IF NOT EXISTS `class` (
  `id` int(11) NOT NULL,
  `parient_id` int(11) DEFAULT NULL,
  `class_title` varchar(50) NOT NULL,
  `class_group` varchar(150) NOT NULL,
  `section` varchar(100) NOT NULL,
  `section_student_capacity` varchar(5) NOT NULL,
  `classCode` varchar(40) NOT NULL,
  `student_amount` int(11) NOT NULL,
  `attendance_percentices_daily` int(11) NOT NULL,
  `attend_percentise_yearly` int(11) NOT NULL,
  `month_fee` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class`
--

INSERT INTO `class` (`id`, `parient_id`, `class_title`, `class_group`, `section`, `section_student_capacity`, `classCode`, `student_amount`, `attendance_percentices_daily`, `attend_percentise_yearly`, `month_fee`) VALUES
(1, 77, 'Class 1', '', 'A,B,C', '40', '1', 0, 50, 50, ''),
(2, 77, 'Class 2', '', 'A', '30', '2', 0, 0, 0, ''),
(3, 77, 'Class 2', '', 'B', '50', '3', 0, 0, 0, ''),
(4, 77, 'Class 3', 'A', '', '20', '5', 0, 0, 0, ''),
(26, 80, 'class 1', '', 'B', '20', 'cl1', 3, 50, 50, ''),
(27, 80, 'class 2', '', 'A,B,C', '80', 'cl2', 2, 0, 0, ''),
(28, 81, 'class 1', '', 'a,b', '50', '1', 2, 100, 100, 'December'),
(29, 81, 'class 2', '', 'a', '50', '2', 1, 100, 100, 'December'),
(30, 81, 'class 3', '', '', '50', '3', 0, 0, 0, 'December');

-- --------------------------------------------------------

--
-- Table structure for table `class_routine`
--

CREATE TABLE IF NOT EXISTS `class_routine` (
  `id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `section` varchar(100) NOT NULL,
  `day_title` varchar(50) NOT NULL,
  `subject` varchar(300) NOT NULL,
  `subject_teacher` varchar(200) NOT NULL,
  `start_time` varchar(30) NOT NULL,
  `end_time` varchar(30) NOT NULL,
  `room_number` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class_routine`
--

INSERT INTO `class_routine` (`id`, `class_id`, `section`, `day_title`, `subject`, `subject_teacher`, `start_time`, `end_time`, `room_number`) VALUES
(1, 1, '', 'Sunday', 'AMAR BANGLA BOI', 'Willie B. Quint', '9.00am', '9.30', '101'),
(2, 1, '', 'Sunday', 'ENGLISH FOR TODAY', 'Fredrick V. Keyes', '9.31am', '10.00am', '101'),
(3, 1, '', 'Sunday', 'PRIMARY MATHEMATICS', 'mumar abboud', '10.01am', '10.30am', '101'),
(4, 1, '', 'Sunday', 'PRIMARY MATHEMATICS', 'mumar abboud', '10.01am', '10.30am', '101'),
(5, 2, '', 'Sunday', 'AMAR BANGLA BOI', 'Inayah Asfour', '9.00am', '9.30am', '102'),
(6, 2, '', 'Sunday', 'ENGLISH FOR TODAY', 'mumar abboud', '9.31am', '10.00am', '102'),
(7, 2, '', 'Sunday', 'PRIMARY MATHEMATICS', 'Fredrick V. Keyes', '10.01am', '10.30am', '102'),
(8, 3, '', 'Sunday', 'AMAR BANGLA BOI', 'Willie B. Quint', '9.00am', '9.30am', '103'),
(9, 3, '', 'Sunday', 'ENGLISH FOR TODAY', 'Fredrick V. Keyes', '9.31am', '10.00am', '103'),
(10, 3, '', 'Sunday', 'PRIMARY MATHEMATICS', 'mumar abboud', '10.01am', '10.30am', '103'),
(11, 3, '', 'Sunday', 'BANGLADESH AND GLOBAL STUDIES', 'Inayah Asfour', '10.30am', '11.00am', '103'),
(12, 3, '', 'Monday', 'ENGLISH FOR TODAY', 'Fredrick V. Keyes', '9.00am', '9.30', '103'),
(13, 3, '', 'Monday', 'PRIMARY MATHEMATICS', 'mumar abboud', '9.31am', '10.00am', '103'),
(14, 11, '', 'Monday', 'Math', 'mumar abboud', '8:00', '8:30', 'A1'),
(15, 11, '', 'Monday', 'General Science', 'Inayah Asfour', '8:30', '9:00', 'A1'),
(16, 11, '', 'Monday', 'Islamiat', 'Inayah Asfour', '9:00', '9:30', 'A1'),
(17, 11, '', 'Monday', 'Social Studies', 'mumar abboud', '9:30', '10:00', 'A1'),
(18, 11, '', 'Monday', 'English', 'Inayah Asfour', '10:00', '10:30', 'A1'),
(19, 8, '', 'Monday', 'AMAR BANGLA BOI', 'mumar abboud', '8:00', '8:30', 'A1'),
(20, 14, '', 'Wednesday', 'Math', 'Willie B. Quint', '7:00 AM', '8:00 AM', '13'),
(21, 14, '', 'Wednesday', 'English', 'Fredrick V. Keyes', '8:15 AM', '9:00 AM', '4'),
(22, 11, '', 'Monday', 'Islamiat', 'Mudasir Murtaza', '11:00', '12:00', 'A1'),
(23, 11, '', 'Tuesday', 'Social Studies', 'Mudasir Murtaza', '8:00', '8:30', 'A1'),
(24, 15, '', 'Monday', 'Maths', 'Mr. Raheel naeem', '7:00 AM', '7:45 AM', '3'),
(25, 15, '', 'Monday', 'English', 'Willie B. Quint', '8:00 AM', '8:45 AM', '3'),
(26, 15, '', 'Tuesday', 'Maths', 'Mr. Raheel naeem', '8:00AM', '8:45 AM', '3'),
(27, 15, '', 'Tuesday', 'English', 'Willie B. Quint', '7:00 AM', '7:45', '3'),
(28, 28, '', 'Monday', 'English', 'asad amjad', '8', '9', '1'),
(29, 28, '', 'Monday', 'Urdu', 'asad amjad', '9', '10', '1'),
(30, 28, '', 'Monday', 'Math', 'asad amjad', '10', '11', '1');

-- --------------------------------------------------------

--
-- Table structure for table `class_students`
--

CREATE TABLE IF NOT EXISTS `class_students` (
  `id` int(11) NOT NULL,
  `parient_id` int(11) DEFAULT NULL,
  `year` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `roll_number` int(11) DEFAULT NULL,
  `student_id` varchar(100) NOT NULL,
  `class_id` int(11) NOT NULL,
  `class_title` varchar(50) NOT NULL,
  `section` varchar(150) NOT NULL,
  `student_title` varchar(100) NOT NULL,
  `attendance_percentices_daily` int(11) NOT NULL,
  `optional_sub` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class_students`
--

INSERT INTO `class_students` (`id`, `parient_id`, `year`, `user_id`, `roll_number`, `student_id`, `class_id`, `class_title`, `section`, `student_title`, `attendance_percentices_daily`, `optional_sub`) VALUES
(1, 80, 2017, 87, 5, '2017cl2005', 27, 'class 2', 'C', 'dsda dsad', 0, ''),
(4, 80, 2017, 90, 1, '2017cl1001', 26, 'class 1', 'B', 'student3 student3', 0, ''),
(5, 80, 2017, 91, 6, '2017cl2006', 27, 'class 2', 'C', 'student4 student4', 0, ''),
(6, 80, 2017, 99, 2, '2017cl1002', 26, 'class 1', 'B', 'studen01 studen01', 0, ''),
(7, 80, 2017, 100, 3, '2017cl1003', 26, 'class 1', 'B', 'studen02 studen02', 0, ''),
(8, 81, 2017, 104, 1, '20171001', 28, 'class 1', 'a', 'syed junaid', 100, ''),
(9, 81, 2017, 105, 2, '20171002', 28, 'class 1', 'b', 'ahmad asdf', 100, ''),
(10, 81, 2017, 108, 1, '20172001', 29, 'class 2', 'a', 'dum1 dumy', 100, '');

-- --------------------------------------------------------

--
-- Table structure for table `class_subject`
--

CREATE TABLE IF NOT EXISTS `class_subject` (
  `id` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `parient_id` int(11) DEFAULT NULL,
  `class_id` int(11) NOT NULL,
  `subject_title` varchar(100) NOT NULL,
  `group` varchar(100) NOT NULL,
  `subject_teacher` varchar(100) NOT NULL,
  `edition` varchar(100) NOT NULL,
  `writer_name` varchar(100) NOT NULL,
  `optional` int(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=123 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class_subject`
--

INSERT INTO `class_subject` (`id`, `year`, `parient_id`, `class_id`, `subject_title`, `group`, `subject_teacher`, `edition`, `writer_name`, `optional`) VALUES
(1, 2016, 0, 1, 'AMAR BANGLA BOI', '', 'Willie B. Quint', '2016', 'NCTB', 0),
(2, 2016, 0, 1, 'ENGLISH FOR TODAY', '', 'Fredrick V. Keyes', '2016', 'NCTB', 0),
(3, 2016, 0, 1, 'PRIMARY MATHEMATICS', '', 'mumar abboud', '2016', 'NCTB', 0),
(4, 2016, 0, 2, 'AMAR BANGLA BOI', '', 'Inayah Asfour', '2016', 'NCTB', 0),
(5, 2016, 0, 2, 'ENGLISH FOR TODAY', '', 'mumar abboud', '2016', 'NCTB', 0),
(6, 2016, 0, 2, 'PRIMARY MATHEMATICS', '', 'Fredrick V. Keyes', '2016', 'NCTB', 0),
(7, 2016, 0, 2, 'PRIMARY MATHEMATICS', '', 'Fredrick V. Keyes', '2016', 'NCTB', 0),
(8, 2016, 0, 3, 'AMAR BANGLA BOI', '', 'Willie B. Quint', '2016', 'NCTB', 0),
(9, 2016, 0, 3, 'ENGLISH FOR TODAY', '', 'Fredrick V. Keyes', '2016', 'NCTB', 0),
(10, 2016, 0, 3, 'PRIMARY MATHEMATICS', '', 'mumar abboud', '2016', 'NCTB', 0),
(11, 2016, 0, 3, 'BANGLADESH AND GLOBAL STUDIES', '', 'Inayah Asfour', '2016', 'NCTB', 0),
(12, 2016, 0, 3, 'PRIMARY SCIENCE', '', '', '2016', 'NCTB', 0),
(13, 2016, 0, 3, 'ISLAM AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(14, 2016, 0, 3, 'HINDU RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(15, 2016, 0, 3, 'BUDDIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(16, 2016, 0, 3, 'CHRIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(17, 2016, 0, 4, 'AMAR BANGLA BOI', '', '', '2016', 'NCTB', 0),
(18, 2016, 0, 4, 'ENGLISH FOR TODAY', '', '', '2016', 'NCTB', 0),
(19, 2016, 0, 4, 'PRIMARY MATHEMATICS', '', '', '2016', 'NCTB', 0),
(20, 2016, 0, 4, 'BANGLADESH AND GLOBAL STUDIES', '', '', '2016', 'NCTB', 0),
(21, 2016, 0, 4, 'PRIMARY SCIENCE', '', '', '2016', 'NCTB', 0),
(22, 2016, 0, 4, 'ISLAM AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(23, 2016, 0, 4, 'HINDU RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(24, 2016, 0, 4, 'BUDDIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(25, 2016, 0, 4, 'CHRIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(26, 2016, 0, 5, 'AMAR BANGLA BOI', '', '', '2016', 'NCTB', 0),
(27, 2016, 0, 5, 'ENGLISH FOR TODAY', '', '', '2016', 'NCTB', 0),
(28, 2016, 0, 5, 'PRIMARY MATHEMATICS', '', '', '2016', 'NCTB', 0),
(29, 2016, 0, 5, 'BANGLADESH AND GLOBAL STUDIES', '', '', '2016', 'NCTB', 0),
(30, 2016, 0, 5, 'PRIMARY SCIENCE', '', '', '2016', 'NCTB', 0),
(31, 2016, 0, 5, 'ISLAM AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(32, 2016, 0, 5, 'HINDU RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(33, 2016, 0, 5, 'BUDDIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(34, 2016, 0, 5, 'CHRIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(35, 2016, 0, 6, 'AMAR BANGLA BOI', '', '', '2016', 'NCTB', 0),
(36, 2016, 0, 6, 'ENGLISH FOR TODAY', '', '', '2016', 'NCTB', 0),
(37, 2016, 0, 6, 'PRIMARY MATHEMATICS', '', '', '2016', 'NCTB', 0),
(38, 2016, 0, 6, 'BANGLADESH AND GLOBAL STUDIES', '', '', '2016', 'NCTB', 0),
(39, 2016, 0, 6, 'PRIMARY SCIENCE', '', '', '2016', 'NCTB', 0),
(40, 2016, 0, 6, 'ISLAM AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(41, 2016, 0, 6, 'HINDU RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(42, 2016, 0, 6, 'BUDDIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(43, 2016, 0, 6, 'CHRIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(44, 2016, 0, 7, 'AMAR BANGLA BOI', '', '', '2016', 'NCTB', 0),
(45, 2016, 0, 7, 'ENGLISH FOR TODAY', '', '', '2016', 'NCTB', 0),
(46, 2016, 0, 7, 'PRIMARY MATHEMATICS', '', '', '2016', 'NCTB', 0),
(47, 2016, 0, 7, 'BANGLADESH AND GLOBAL STUDIES', '', '', '2016', 'NCTB', 0),
(48, 2016, 0, 7, 'PRIMARY SCIENCE', '', '', '2016', 'NCTB', 0),
(49, 2016, 0, 7, 'ISLAM AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(50, 2016, 0, 7, 'HINDU RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(51, 2016, 0, 7, 'BUDDIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(52, 2016, 0, 7, 'CHRIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(53, 2016, 0, 8, 'AMAR BANGLA BOI', '', 'mumar abboud', '2016', 'NCTB', 0),
(54, 2016, 0, 8, 'ENGLISH FOR TODAY', '', '', '2016', 'NCTB', 0),
(55, 2016, 0, 8, 'PRIMARY MATHEMATICS', '', '', '2016', 'NCTB', 0),
(56, 2016, 0, 8, 'ISLAM AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(57, 2016, 0, 8, 'HINDU RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(58, 2016, 0, 8, 'BUDDIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(59, 2016, 0, 8, 'CHRIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(60, 2016, 0, 8, 'BANGLADESH AND GLOBAL STUDIES', '', '', '2016', 'NCTB', 0),
(61, 2016, 0, 8, 'PRIMARY SCIENCE', '', '', '2016', 'NCTB', 0),
(62, 2016, 0, 9, 'AMAR BANGLA BOI', '', '', '2016', 'NCTB', 0),
(63, 2016, 0, 9, 'ENGLISH FOR TODAY', '', '', '2016', 'NCTB', 0),
(64, 2016, 0, 9, 'PRIMARY MATHEMATICS', '', '', '2016', 'NCTB', 0),
(65, 2016, 0, 9, 'BANGLADESH AND GLOBAL STUDIES', '', '', '2016', 'NCTB', 0),
(66, 2016, 0, 9, 'PRIMARY SCIENCE', '', '', '2016', 'NCTB', 0),
(67, 2016, 0, 9, 'ISLAM AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(68, 2016, 0, 9, 'HINDU RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(69, 2016, 0, 9, 'BUDDIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(70, 2016, 0, 9, 'CHRIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(71, 2016, 0, 10, 'AMAR BANGLA BOI', '', '', '2016', 'NCTB', 0),
(72, 2016, 0, 10, 'ENGLISH FOR TODAY', '', '', '2016', 'NCTB', 0),
(73, 2016, 0, 10, 'PRIMARY MATHEMATICS', '', '', '2016', 'NCTB', 0),
(74, 2016, 0, 10, 'BANGLADESH AND GLOBAL STUDIES', '', '', '2016', 'NCTB', 0),
(75, 2016, 0, 10, 'ISLAM AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(76, 2016, 0, 10, 'HINDU RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(77, 2016, 0, 10, 'BUDDIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(78, 2016, 0, 10, 'CHRIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(79, 2016, 0, 10, 'PRIMARY SCIENCE', '', '', '2016', 'NCTB', 0),
(80, 2016, 0, 9, 'AGRICULTURE STUDIES', '', '', '2016', 'NCTB', 1),
(81, 2016, 0, 10, 'AGRICULTURE STUDIES', '', '', '2016', 'NCTB', 1),
(82, 2016, 0, 10, 'ARTS & CRAFTS', '', '', '2016', 'NCTB', 1),
(83, 2016, 0, 10, 'ARTS & CRAFTS', '', '', '2016', 'NCTB', 1),
(84, 2016, 0, 9, 'MUSIC', '', '', '2016', 'NCTB', 1),
(85, 2016, 0, 10, 'MUSIC', '', '', '2016', 'NCTB', 1),
(86, 2017, 0, 11, 'Math', '', 'mumar abboud', 'Edition1', 'benjamin', 0),
(87, 2017, 0, 11, 'General Science', '', 'Inayah Asfour', 'Edition 1st', 'Suhail', 0),
(88, 2017, 0, 11, 'Islamiat', '', 'Mudasir Murtaza', '1st', 'Mohsin', 0),
(89, 2017, 0, 11, 'Social Studies', '', 'Mudasir Murtaza', '1st', 'Umair', 0),
(90, 2017, 0, 11, 'English', '', 'Inayah Asfour', '1st', 'Mudasir', 0),
(91, 2017, 0, 11, 'Naazra', '', '', '1st', 'Kashif Junaid', 0),
(92, 2017, 0, 14, 'Math', '', 'Willie B. Quint', '13', 'mr.sohail', 0),
(93, 2017, 0, 14, 'English', '', 'Fredrick V. Keyes', 'new15', 'Mohsin', 0),
(94, 2017, 0, 1, 'Maths', '', '', 'new15', 'kashif', 0),
(95, 2017, 0, 15, 'Maths', '', 'Mr. Raheel naeem', 'edition 15', 'Mohsin', 0),
(96, 2017, 0, 15, 'English', '', 'Willie B. Quint', 'edition 16', 'sohail', 0),
(97, 2017, 0, 15, 'Urdu', '', '', 'edition 17', 'umair', 0),
(98, 2017, 0, 15, 'English B', '', '', 'edition 18', 'ali', 0),
(99, 2017, 0, 16, 'Maths', '', '', 'edition 15', 'Mohsin', 0),
(100, 2017, 0, 16, 'english', '', '', 'edition 16', 'sohail', 0),
(101, 2017, 0, 16, 'Urdu', '', '', 'edition 17', 'Syed Umer', 0),
(102, 2017, 0, 16, 'English 2', '', '', 'edition 18', 'ali', 0),
(103, 2017, 80, 17, 'Math', '', '', '1st', 'abc', 0),
(104, 2017, 80, 17, 'asd', '', '', 'asd', 'asd', 0),
(105, 2017, 80, 17, 'english', '', '', '1st', 'abc', 0),
(106, 2017, 80, 17, 'pak study', '', '', '1st', 'asd', 1),
(113, 2017, 80, 27, 'english', '', '', '1', 'a', 0),
(114, 2017, 80, 27, 'math', '', '', '2', '', 0),
(115, 2017, 80, 27, 'urdu', '', '', '3', '', 0),
(116, 2017, 80, 26, 'abc', '', '', 'a', 'a', 0),
(117, 2017, 80, 26, 'math', '', '', 'a', 'a', 0),
(118, 2017, 80, 26, 'english', '', '', 'a', '', 0),
(119, 2017, 81, 28, 'English', '', 'asad amjad', '1st', 'Amjad salam', 0),
(120, 2017, 81, 28, 'Urdu', '', 'asad amjad', '1st', 'ahsan nadeem', 0),
(121, 2017, 81, 28, 'Math', '', 'asad amjad', '3rd', 'usman', 0),
(122, 2017, 81, 30, 'English', '', '', '3rd', 'benjimen', 0);

-- --------------------------------------------------------

--
-- Table structure for table `configuration`
--

CREATE TABLE IF NOT EXISTS `configuration` (
  `id` int(11) NOT NULL,
  `parient_id` int(11) DEFAULT NULL,
  `logo` varchar(150) NOT NULL,
  `time_zone` varchar(150) NOT NULL,
  `school_name` varchar(150) NOT NULL,
  `starting_year` varchar(50) NOT NULL,
  `headmaster_name` varchar(150) NOT NULL,
  `address` varchar(150) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `currenct` varchar(50) NOT NULL,
  `country` varchar(150) NOT NULL,
  `language` text NOT NULL,
  `msg_apai_email` varchar(100) NOT NULL,
  `msg_hash_number` varchar(100) NOT NULL,
  `msg_sender_title` varchar(100) NOT NULL,
  `countryPhonCode` varchar(5) NOT NULL,
  `t_a_s_p` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `configuration`
--

INSERT INTO `configuration` (`id`, `parient_id`, `logo`, `time_zone`, `school_name`, `starting_year`, `headmaster_name`, `address`, `phone`, `email`, `currenct`, `country`, `language`, `msg_apai_email`, `msg_hash_number`, `msg_sender_title`, `countryPhonCode`, `t_a_s_p`) VALUES
(2, 8, '', 'UP3', 'asda', '12/12/1922', 'sad', 'sdasd', 'asda', 'abvad@gmail.com', 'fa fa-money', 'Bangladesh', '', '', '', '', '', ''),
(3, 81, '', 'UP5', 'Cybex Public School', '12/12/1981', 'dfssdf', 'dfsdfsfs', 'sfsdf', 'sdfs@gmail.com', 'fa fa-money', 'Pakistan', '', '', '', '', '', '123456'),
(4, 80, '', 'UP5', 'CybexTech', '16/11/2017', 'Sohail Ahmd', 'Walton Lahore', '12345678', 'sohail9689@gmail.com', 'fa fa-money', 'Pakistan', '', '', '', '', '', '123456');

-- --------------------------------------------------------

--
-- Table structure for table `config_week_day`
--

CREATE TABLE IF NOT EXISTS `config_week_day` (
  `id` int(11) NOT NULL,
  `parient_id` int(11) DEFAULT NULL,
  `day_name` varchar(20) NOT NULL,
  `status` varchar(30) NOT NULL,
  `sunday` varchar(40) DEFAULT 'Holyday',
  `monday` varchar(40) DEFAULT 'Open',
  `tuesday` varchar(40) DEFAULT 'Open',
  `wednesday` varchar(40) DEFAULT 'Open',
  `thursday` varchar(40) DEFAULT 'Open',
  `friday` varchar(40) DEFAULT 'Open',
  `saturday` varchar(40) DEFAULT 'Holyday'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `config_week_day`
--

INSERT INTO `config_week_day` (`id`, `parient_id`, `day_name`, `status`, `sunday`, `monday`, `tuesday`, `wednesday`, `thursday`, `friday`, `saturday`) VALUES
(1, 80, '', '', 'Holyday', 'Open', 'Open', 'Open', 'Open', 'Open', 'Holyday'),
(2, 81, 'sunday', 'HOLIDAY', 'Holyday', 'Open', 'Open', 'Open', 'Open', 'Open', 'Holyday'),
(3, 86, '', '', 'Holyday', 'Open', 'Open', 'Open', 'Open', 'Open', 'Holyday'),
(4, 114, '', '', 'Holyday', 'Open', 'Open', 'Open', 'Open', 'Open', 'Holyday');

-- --------------------------------------------------------

--
-- Table structure for table `daily_attendance`
--

CREATE TABLE IF NOT EXISTS `daily_attendance` (
  `id` int(11) NOT NULL,
  `date` varchar(50) NOT NULL,
  `user_id` varchar(30) NOT NULL,
  `student_id` varchar(150) NOT NULL,
  `class_title` varchar(30) NOT NULL,
  `section` varchar(100) NOT NULL,
  `days_amount` varchar(20) NOT NULL,
  `roll_no` int(11) NOT NULL,
  `present_or_absent` varchar(2) NOT NULL,
  `student_title` varchar(100) NOT NULL,
  `class_amount_monthly` int(11) NOT NULL,
  `class_amount_yearly` int(11) NOT NULL,
  `attend_amount_monthly` int(11) NOT NULL,
  `attend_amount_yearly` int(11) NOT NULL,
  `percentise_month` int(11) NOT NULL,
  `percentise_year` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `daily_attendance`
--

INSERT INTO `daily_attendance` (`id`, `date`, `user_id`, `student_id`, `class_title`, `section`, `days_amount`, `roll_no`, `present_or_absent`, `student_title`, `class_amount_monthly`, `class_amount_yearly`, `attend_amount_monthly`, `attend_amount_yearly`, `percentise_month`, `percentise_year`) VALUES
(1, '1464991200', '4', '201601001', 'Class 1', 'Section A,Section B,Section C,Section D', '', 1, 'P', 'Benjamin D. Lampe', 1, 1, 1, 1, 100, 100),
(2, '1464991200', '12', '201601002', 'Class 1', 'Section A,Section B,Section C,Section D', '', 2, 'P', 'Rahim Hasan', 1, 1, 1, 1, 100, 100),
(3, '1464991200', '13', '201601003', 'Class 1', 'Section A,Section B,Section C,Section D', '', 3, 'P', 'Junayed Hak', 1, 1, 1, 1, 100, 100),
(4, '1464991200', '16', '201601004', 'Class 1', '', '', 4, 'A', 'Razia Akture', 1, 1, 0, 0, 0, 0),
(5, '1464991200', '23', '201601005', 'Class 1', '', '', 5, 'P', 'Polash Sarder', 1, 1, 1, 1, 100, 100),
(6, '1464991200', '24', '201601006', 'Class 1', '', '', 6, 'A', 'Sumon Akon', 1, 1, 0, 0, 0, 0),
(7, '1464991200', '17', '201602001', 'Class 2', '', '', 1, 'P', 'Abdullah  hossain', 1, 1, 1, 1, 100, 100),
(8, '1464991200', '18', '201602002', 'Class 2', '', '', 2, 'P', 'Sujana Ahmed', 1, 1, 1, 1, 100, 100),
(9, '1464991200', '19', '201602003', 'Class 2', '', '', 3, 'P', 'Mahmud Hasan', 1, 1, 1, 1, 100, 100),
(10, '1464991200', '20', '201602004', 'Class 2', '', '', 4, 'P', 'Mahbuba Akter', 1, 1, 1, 1, 100, 100),
(11, '1464991200', '21', '201602005', 'Class 2', '', '', 5, 'P', 'Irfan Hossain', 1, 1, 1, 1, 100, 100),
(12, '1464991200', '22', '201602006', 'Class 2', '', '', 6, 'P', 'Imran Hasan', 1, 1, 1, 1, 100, 100),
(13, '1464991200', '25', '201603001', 'Class 3', '', '', 1, 'A', 'Farjana Akter', 1, 1, 0, 0, 0, 0),
(14, '1464991200', '26', '201603002', 'Class 3', '', '', 2, 'P', 'Akram Hossain', 1, 1, 1, 1, 100, 100),
(15, '1464991200', '27', '201603003', 'Class 3', '', '', 3, 'P', 'Alamin Saeder', 1, 1, 1, 1, 100, 100),
(16, '1464991200', '28', '201603004', 'Class 3', '', '', 4, 'P', 'Sabina Sumi', 1, 1, 1, 1, 100, 100),
(17, '1464991200', '29', '201604001', 'Class 4', '', '', 1, 'P', 'Sanjida Hossain', 1, 1, 1, 1, 100, 100),
(18, '1464991200', '30', '201604002', 'Class 4', '', '', 2, 'P', 'Kawser  Shikder', 1, 1, 1, 1, 100, 100),
(19, '1464991200', '31', '201604003', 'Class 4', '', '', 3, 'P', 'Shohana Akter', 1, 1, 1, 1, 100, 100),
(20, '1464991200', '32', '201604004', 'Class 4', '', '', 4, 'P', 'Juthi Khanam', 1, 1, 1, 1, 100, 100),
(21, '1464991200', '33', '201605001', 'Class 5', '', '', 1, 'P', 'Tanjila Akter', 1, 1, 1, 1, 100, 100),
(22, '1464991200', '34', '201605002', 'Class 5', '', '', 2, 'P', 'Nusrat Jahan', 1, 1, 1, 1, 100, 100),
(23, '1464991200', '35', '201605003', 'Class 5', '', '', 3, 'P', 'Amina Akter', 1, 1, 1, 1, 100, 100),
(24, '1464991200', '36', '201605004', 'Class 5', '', '', 4, 'A', 'Ebrahim Khondokar', 1, 1, 0, 0, 0, 0),
(25, '1464991200', '37', '201605005', 'Class 5', '', '', 5, 'A', 'Mintu  Fokir', 1, 1, 0, 0, 0, 0),
(26, '1464991200', '38', '201606001', 'Class 6', 'Section A', '', 1, 'P', 'Shohid Islam', 1, 1, 1, 1, 100, 100),
(27, '1464991200', '39', '201606002', 'Class 6', 'Section B', '', 2, 'P', 'Khadija Akter', 1, 1, 1, 1, 100, 100),
(28, '1464991200', '40', '201606003', 'Class 6', 'Section A', '', 3, 'P', 'Maruf Hossain', 1, 1, 1, 1, 100, 100),
(29, '1464991200', '41', '201606004', 'Class 6', 'Section B', '', 4, 'P', 'Mitu  Akter', 1, 1, 1, 1, 100, 100),
(30, '1464991200', '42', '201606005', 'Class 6', 'Section A', '', 5, 'A', 'Rayhan  Kebria', 1, 1, 0, 0, 0, 0),
(31, '1496188800', '17', '201602001', 'Class 2', '', '', 1, 'P', 'Abdullah  hossain', 2, 2, 2, 2, 100, 100),
(32, '1496188800', '18', '201602002', 'Class 2', '', '', 2, 'P', 'Sujana Ahmed', 2, 2, 2, 2, 100, 100),
(33, '1496188800', '19', '201602003', 'Class 2', '', '', 3, 'P', 'Mahmud Hasan', 2, 2, 2, 2, 100, 100),
(34, '1496188800', '20', '201602004', 'Class 2', '', '', 4, 'A', 'Mahbuba Akter', 2, 2, 1, 1, 50, 50),
(35, '1496188800', '21', '201602005', 'Class 2', '', '', 5, 'A', 'Irfan Hossain', 2, 2, 1, 1, 50, 50),
(36, '1496188800', '22', '201602006', 'Class 2', '', '', 6, 'A', 'Imran Hasan', 2, 2, 1, 1, 50, 50),
(37, '1496275200', '59', '201700001', 'Shajar Nursery 1 ', 'section2015 A', '', 1, 'P', 'Syed Umer', 4, 4, 3, 3, 75, 75),
(38, '1496275200', '60', '201700002', 'Shajar Nursery 1 ', 'section2015 A', '', 2, 'P', 'usman ahmad', 3, 3, 1, 1, 33, 33),
(39, '1496275200', '59', '201700001', 'Shajar Nursery 1 ', 'section2015 A', '', 1, '', 'Syed Umer', 4, 4, 3, 3, 75, 75),
(40, '1496275200', '60', '201700002', 'Shajar Nursery 1 ', 'section2015 A', '', 2, '', 'usman ahmad', 3, 3, 1, 1, 33, 33),
(41, '1496275200', '59', '201700001', 'Shajar Nursery 1 ', 'section2015 A', '', 1, 'P', 'Syed Umer', 4, 4, 3, 3, 75, 75),
(42, '1496275200', '60', '201700002', 'Shajar Nursery 1 ', 'section2015 A', '', 2, 'A', 'usman ahmad', 3, 3, 1, 1, 33, 33),
(43, '1496275200', '59', '201700001', 'Shajar Nursery 1 ', 'section2015 A', '', 1, 'P', 'Syed Umer', 4, 4, 3, 3, 75, 75),
(46, '1512514800', '104', '20171001', 'class 1', 'a', '', 1, 'P', 'syed junaid', 1, 1, 1, 1, 100, 100),
(47, '1512514800', '108', '20172001', 'class 2', 'a', '', 1, 'P', 'dum1 dumy', 1, 1, 1, 1, 100, 100),
(48, '1512514800', '105', '20171002', 'class 1', 'b', '', 2, 'P', 'ahmad asdf', 1, 1, 1, 1, 100, 100);

-- --------------------------------------------------------

--
-- Table structure for table `dormitory`
--

CREATE TABLE IF NOT EXISTS `dormitory` (
  `id` int(11) NOT NULL,
  `parient_id` int(11) DEFAULT NULL,
  `dormitory_name` varchar(100) NOT NULL,
  `dormitory_for` varchar(100) NOT NULL,
  `room_amount` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dormitory`
--

INSERT INTO `dormitory` (`id`, `parient_id`, `dormitory_name`, `dormitory_for`, `room_amount`) VALUES
(1, 0, 'Boys Hostel', 'Only for male', 125),
(2, 0, 'Girls Hostel', 'Only for female', 40),
(3, 0, 'Teachers Dormitory', 'Only for teachers (Male Teacher)', 10),
(5, 81, 'Khalid bin waleed hostel', 'Only for male', 100);

-- --------------------------------------------------------

--
-- Table structure for table `dormitory_bed`
--

CREATE TABLE IF NOT EXISTS `dormitory_bed` (
  `id` int(11) NOT NULL,
  `parient_id` int(11) DEFAULT NULL,
  `dormitory_id` int(11) NOT NULL,
  `dormitory_name` varchar(100) NOT NULL,
  `room_number` varchar(50) NOT NULL,
  `bed_number` varchar(50) NOT NULL,
  `student_id` int(11) NOT NULL,
  `student_name` varchar(100) NOT NULL,
  `class` varchar(50) NOT NULL,
  `roll_number` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dormitory_bed`
--

INSERT INTO `dormitory_bed` (`id`, `parient_id`, `dormitory_id`, `dormitory_name`, `room_number`, `bed_number`, `student_id`, `student_name`, `class`, `roll_number`) VALUES
(96, 81, 5, 'Khalid bin waleed hostel', 'Room No: 1', 'Seat No: 1', 20172001, 'dum1 dumy', '29', 1),
(97, 81, 5, 'Khalid bin waleed hostel', 'Room No: 1', 'Seat No: 2', 0, '', '', 0),
(98, 81, 5, 'Khalid bin waleed hostel', 'Room No: 1', 'Seat No: 3', 0, '', '', 0),
(99, 81, 5, 'Khalid bin waleed hostel', 'Room No: 1', 'Seat No: 4', 0, '', '', 0),
(100, 81, 5, 'Khalid bin waleed hostel', 'Room No: 1', 'Seat No: 5', 0, '', '', 0),
(101, 81, 5, 'Khalid bin waleed hostel', 'Room No: 1', 'Seat No: 6', 0, '', '', 0),
(102, 81, 5, 'Khalid bin waleed hostel', 'Room No: 1', 'Seat No: 7', 0, '', '', 0),
(103, 81, 5, 'Khalid bin waleed hostel', 'Room No: 1', 'Seat No: 8', 0, '', '', 0),
(104, 81, 5, 'Khalid bin waleed hostel', 'Room No: 1', 'Seat No: 9', 0, '', '', 0),
(105, 81, 5, 'Khalid bin waleed hostel', 'Room No: 1', 'Seat No: 10', 0, '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `dormitory_room`
--

CREATE TABLE IF NOT EXISTS `dormitory_room` (
  `id` int(11) NOT NULL,
  `parient_id` int(11) DEFAULT NULL,
  `dormitory_id` int(11) NOT NULL,
  `dormitory_name` varchar(100) NOT NULL,
  `room` varchar(50) NOT NULL,
  `bed_amount` int(11) NOT NULL,
  `free_seat` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=476 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dormitory_room`
--

INSERT INTO `dormitory_room` (`id`, `parient_id`, `dormitory_id`, `dormitory_name`, `room`, `bed_amount`, `free_seat`) VALUES
(376, 81, 5, 'Khalid bin waleed hostel', 'Room No: 1', 10, 9),
(377, 81, 5, 'Khalid bin waleed hostel', 'Room No: 2', 0, 0),
(378, 81, 5, 'Khalid bin waleed hostel', 'Room No: 3', 0, 0),
(379, 81, 5, 'Khalid bin waleed hostel', 'Room No: 4', 0, 0),
(380, 81, 5, 'Khalid bin waleed hostel', 'Room No: 5', 0, 0),
(381, 81, 5, 'Khalid bin waleed hostel', 'Room No: 6', 0, 0),
(382, 81, 5, 'Khalid bin waleed hostel', 'Room No: 7', 0, 0),
(383, 81, 5, 'Khalid bin waleed hostel', 'Room No: 8', 0, 0),
(384, 81, 5, 'Khalid bin waleed hostel', 'Room No: 9', 0, 0),
(385, 81, 5, 'Khalid bin waleed hostel', 'Room No: 10', 0, 0),
(386, 81, 5, 'Khalid bin waleed hostel', 'Room No: 11', 0, 0),
(387, 81, 5, 'Khalid bin waleed hostel', 'Room No: 12', 0, 0),
(388, 81, 5, 'Khalid bin waleed hostel', 'Room No: 13', 0, 0),
(389, 81, 5, 'Khalid bin waleed hostel', 'Room No: 14', 0, 0),
(390, 81, 5, 'Khalid bin waleed hostel', 'Room No: 15', 0, 0),
(391, 81, 5, 'Khalid bin waleed hostel', 'Room No: 16', 0, 0),
(392, 81, 5, 'Khalid bin waleed hostel', 'Room No: 17', 0, 0),
(393, 81, 5, 'Khalid bin waleed hostel', 'Room No: 18', 0, 0),
(394, 81, 5, 'Khalid bin waleed hostel', 'Room No: 19', 0, 0),
(395, 81, 5, 'Khalid bin waleed hostel', 'Room No: 20', 0, 0),
(396, 81, 5, 'Khalid bin waleed hostel', 'Room No: 21', 0, 0),
(397, 81, 5, 'Khalid bin waleed hostel', 'Room No: 22', 0, 0),
(398, 81, 5, 'Khalid bin waleed hostel', 'Room No: 23', 0, 0),
(399, 81, 5, 'Khalid bin waleed hostel', 'Room No: 24', 0, 0),
(400, 81, 5, 'Khalid bin waleed hostel', 'Room No: 25', 0, 0),
(401, 81, 5, 'Khalid bin waleed hostel', 'Room No: 26', 0, 0),
(402, 81, 5, 'Khalid bin waleed hostel', 'Room No: 27', 0, 0),
(403, 81, 5, 'Khalid bin waleed hostel', 'Room No: 28', 0, 0),
(404, 81, 5, 'Khalid bin waleed hostel', 'Room No: 29', 0, 0),
(405, 81, 5, 'Khalid bin waleed hostel', 'Room No: 30', 0, 0),
(406, 81, 5, 'Khalid bin waleed hostel', 'Room No: 31', 0, 0),
(407, 81, 5, 'Khalid bin waleed hostel', 'Room No: 32', 0, 0),
(408, 81, 5, 'Khalid bin waleed hostel', 'Room No: 33', 0, 0),
(409, 81, 5, 'Khalid bin waleed hostel', 'Room No: 34', 0, 0),
(410, 81, 5, 'Khalid bin waleed hostel', 'Room No: 35', 0, 0),
(411, 81, 5, 'Khalid bin waleed hostel', 'Room No: 36', 0, 0),
(412, 81, 5, 'Khalid bin waleed hostel', 'Room No: 37', 0, 0),
(413, 81, 5, 'Khalid bin waleed hostel', 'Room No: 38', 0, 0),
(414, 81, 5, 'Khalid bin waleed hostel', 'Room No: 39', 0, 0),
(415, 81, 5, 'Khalid bin waleed hostel', 'Room No: 40', 0, 0),
(416, 81, 5, 'Khalid bin waleed hostel', 'Room No: 41', 0, 0),
(417, 81, 5, 'Khalid bin waleed hostel', 'Room No: 42', 0, 0),
(418, 81, 5, 'Khalid bin waleed hostel', 'Room No: 43', 0, 0),
(419, 81, 5, 'Khalid bin waleed hostel', 'Room No: 44', 0, 0),
(420, 81, 5, 'Khalid bin waleed hostel', 'Room No: 45', 0, 0),
(421, 81, 5, 'Khalid bin waleed hostel', 'Room No: 46', 0, 0),
(422, 81, 5, 'Khalid bin waleed hostel', 'Room No: 47', 0, 0),
(423, 81, 5, 'Khalid bin waleed hostel', 'Room No: 48', 0, 0),
(424, 81, 5, 'Khalid bin waleed hostel', 'Room No: 49', 0, 0),
(425, 81, 5, 'Khalid bin waleed hostel', 'Room No: 50', 0, 0),
(426, 81, 5, 'Khalid bin waleed hostel', 'Room No: 51', 0, 0),
(427, 81, 5, 'Khalid bin waleed hostel', 'Room No: 52', 0, 0),
(428, 81, 5, 'Khalid bin waleed hostel', 'Room No: 53', 0, 0),
(429, 81, 5, 'Khalid bin waleed hostel', 'Room No: 54', 0, 0),
(430, 81, 5, 'Khalid bin waleed hostel', 'Room No: 55', 0, 0),
(431, 81, 5, 'Khalid bin waleed hostel', 'Room No: 56', 0, 0),
(432, 81, 5, 'Khalid bin waleed hostel', 'Room No: 57', 0, 0),
(433, 81, 5, 'Khalid bin waleed hostel', 'Room No: 58', 0, 0),
(434, 81, 5, 'Khalid bin waleed hostel', 'Room No: 59', 0, 0),
(435, 81, 5, 'Khalid bin waleed hostel', 'Room No: 60', 0, 0),
(436, 81, 5, 'Khalid bin waleed hostel', 'Room No: 61', 0, 0),
(437, 81, 5, 'Khalid bin waleed hostel', 'Room No: 62', 0, 0),
(438, 81, 5, 'Khalid bin waleed hostel', 'Room No: 63', 0, 0),
(439, 81, 5, 'Khalid bin waleed hostel', 'Room No: 64', 0, 0),
(440, 81, 5, 'Khalid bin waleed hostel', 'Room No: 65', 0, 0),
(441, 81, 5, 'Khalid bin waleed hostel', 'Room No: 66', 0, 0),
(442, 81, 5, 'Khalid bin waleed hostel', 'Room No: 67', 0, 0),
(443, 81, 5, 'Khalid bin waleed hostel', 'Room No: 68', 0, 0),
(444, 81, 5, 'Khalid bin waleed hostel', 'Room No: 69', 0, 0),
(445, 81, 5, 'Khalid bin waleed hostel', 'Room No: 70', 0, 0),
(446, 81, 5, 'Khalid bin waleed hostel', 'Room No: 71', 0, 0),
(447, 81, 5, 'Khalid bin waleed hostel', 'Room No: 72', 0, 0),
(448, 81, 5, 'Khalid bin waleed hostel', 'Room No: 73', 0, 0),
(449, 81, 5, 'Khalid bin waleed hostel', 'Room No: 74', 0, 0),
(450, 81, 5, 'Khalid bin waleed hostel', 'Room No: 75', 0, 0),
(451, 81, 5, 'Khalid bin waleed hostel', 'Room No: 76', 0, 0),
(452, 81, 5, 'Khalid bin waleed hostel', 'Room No: 77', 0, 0),
(453, 81, 5, 'Khalid bin waleed hostel', 'Room No: 78', 0, 0),
(454, 81, 5, 'Khalid bin waleed hostel', 'Room No: 79', 0, 0),
(455, 81, 5, 'Khalid bin waleed hostel', 'Room No: 80', 0, 0),
(456, 81, 5, 'Khalid bin waleed hostel', 'Room No: 81', 0, 0),
(457, 81, 5, 'Khalid bin waleed hostel', 'Room No: 82', 0, 0),
(458, 81, 5, 'Khalid bin waleed hostel', 'Room No: 83', 0, 0),
(459, 81, 5, 'Khalid bin waleed hostel', 'Room No: 84', 0, 0),
(460, 81, 5, 'Khalid bin waleed hostel', 'Room No: 85', 0, 0),
(461, 81, 5, 'Khalid bin waleed hostel', 'Room No: 86', 0, 0),
(462, 81, 5, 'Khalid bin waleed hostel', 'Room No: 87', 0, 0),
(463, 81, 5, 'Khalid bin waleed hostel', 'Room No: 88', 0, 0),
(464, 81, 5, 'Khalid bin waleed hostel', 'Room No: 89', 0, 0),
(465, 81, 5, 'Khalid bin waleed hostel', 'Room No: 90', 0, 0),
(466, 81, 5, 'Khalid bin waleed hostel', 'Room No: 91', 0, 0),
(467, 81, 5, 'Khalid bin waleed hostel', 'Room No: 92', 0, 0),
(468, 81, 5, 'Khalid bin waleed hostel', 'Room No: 93', 0, 0),
(469, 81, 5, 'Khalid bin waleed hostel', 'Room No: 94', 0, 0),
(470, 81, 5, 'Khalid bin waleed hostel', 'Room No: 95', 0, 0),
(471, 81, 5, 'Khalid bin waleed hostel', 'Room No: 96', 0, 0),
(472, 81, 5, 'Khalid bin waleed hostel', 'Room No: 97', 0, 0),
(473, 81, 5, 'Khalid bin waleed hostel', 'Room No: 98', 0, 0),
(474, 81, 5, 'Khalid bin waleed hostel', 'Room No: 99', 0, 0),
(475, 81, 5, 'Khalid bin waleed hostel', 'Room No: 100', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `dormitory_visitors`
--

CREATE TABLE IF NOT EXISTS `dormitory_visitors` (
  `id` int(11) NOT NULL,
  `parient_id` int(11) DEFAULT NULL,
  `student_id` int(15) NOT NULL,
  `visitor_name` varchar(50) NOT NULL,
  `visitor_cnic` varchar(15) NOT NULL,
  `visitor_present_address` varchar(512) NOT NULL,
  `visitor_permanent_address` varchar(512) NOT NULL,
  `visitor_relation` int(11) NOT NULL,
  `visitor_gender` varchar(10) NOT NULL,
  `visitor_email` varchar(35) NOT NULL,
  `visitor_phone` varchar(15) NOT NULL,
  `visitor_cnic_copy` varchar(25) NOT NULL,
  `visitor_documents_file_info` varchar(50) NOT NULL,
  `visitor_photo` varchar(512) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dormitory_visitors`
--

INSERT INTO `dormitory_visitors` (`id`, `parient_id`, `student_id`, `visitor_name`, `visitor_cnic`, `visitor_present_address`, `visitor_permanent_address`, `visitor_relation`, `visitor_gender`, `visitor_email`, `visitor_phone`, `visitor_cnic_copy`, `visitor_documents_file_info`, `visitor_photo`) VALUES
(13, 76, 201702001, 'mohsin naeem', '4563215789789', 'hey there boyo', 'hey there boyo its permanent', 1, 'Male', 'mohsin@yahoo.com', '03018548764', 'submited', 'file 236794', '5f254a957c74f1d084e0d4ac94e397c7.png');

-- --------------------------------------------------------

--
-- Table structure for table `drivers`
--

CREATE TABLE IF NOT EXISTS `drivers` (
  `id` int(6) NOT NULL,
  `vehicle_no` varchar(10) NOT NULL,
  `driver_name` varchar(30) NOT NULL,
  `present_address` varchar(50) NOT NULL,
  `permanent_address` varchar(50) NOT NULL,
  `birth_date` varchar(100) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `parient_id` int(11) DEFAULT NULL,
  `license_no` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `employe`
--

CREATE TABLE IF NOT EXISTS `employe` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `father_name` varchar(150) NOT NULL,
  `mother_name` varchar(150) NOT NULL,
  `birth_date` varchar(100) NOT NULL,
  `sex` varchar(50) NOT NULL,
  `present_address` varchar(150) NOT NULL,
  `permanent_address` varchar(150) NOT NULL,
  `job_title_post` varchar(100) NOT NULL,
  `working_hour` varchar(20) NOT NULL,
  `salary_amount` varchar(100) NOT NULL,
  `educational_qualifation_1` varchar(300) NOT NULL,
  `educational_qualifation_2` varchar(300) NOT NULL,
  `educational_qualifation_3` varchar(300) NOT NULL,
  `educational_qualifation_4` varchar(300) NOT NULL,
  `educational_qualifation_5` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `exam_attendanc`
--

CREATE TABLE IF NOT EXISTS `exam_attendanc` (
  `id` int(11) NOT NULL,
  `date` varchar(50) NOT NULL,
  `parient_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `student_id` varchar(20) NOT NULL,
  `student_title` varchar(100) NOT NULL,
  `class_id` int(11) NOT NULL,
  `roll_no` varchar(11) NOT NULL,
  `section` varchar(100) NOT NULL,
  `exam_title` varchar(150) NOT NULL,
  `exam_subject` varchar(100) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `attendance` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_attendanc`
--

INSERT INTO `exam_attendanc` (`id`, `date`, `parient_id`, `user_id`, `student_id`, `student_title`, `class_id`, `roll_no`, `section`, `exam_title`, `exam_subject`, `subject_id`, `attendance`) VALUES
(23, '18/11/2017', 80, 90, '2017cl1001', 'student3 student3', 26, '1', 'B', 'test exam', 'abc', 0, 'A'),
(24, '18/11/2017', 80, 99, '2017cl1002', 'studen01 studen01', 26, '2', 'B', 'test exam', 'abc', 0, 'A'),
(25, '18/11/2017', 80, 100, '2017cl1003', 'studen02 studen02', 26, '3', 'B', 'test exam', 'abc', 0, 'A'),
(26, '18/11/2017', 80, 90, '2017cl1001', 'student3 student3', 26, '1', 'B', 'test exam', 'math', 0, 'A'),
(27, '18/11/2017', 80, 99, '2017cl1002', 'studen01 studen01', 26, '2', 'B', 'test exam', 'math', 0, 'P'),
(28, '18/11/2017', 80, 100, '2017cl1003', 'studen02 studen02', 26, '3', 'B', 'test exam', 'math', 0, 'P'),
(29, '18/11/2017', 80, 90, '2017cl1001', 'student3 student3', 26, '1', 'B', 'math', 'english', 0, 'P'),
(30, '18/11/2017', 80, 99, '2017cl1002', 'studen01 studen01', 26, '2', 'B', 'math', 'english', 0, 'P'),
(31, '18/11/2017', 80, 100, '2017cl1003', 'studen02 studen02', 26, '3', 'B', 'math', 'english', 0, 'P'),
(38, '04/12/2017', 81, 104, '20171001', 'syed junaid', 28, '1', 'a', 'Mids', 'English', 25, 'P'),
(39, '04/12/2017', 81, 105, '20171002', 'ahmad asdf', 28, '2', 'b', 'Mids', 'English', 25, 'P'),
(42, '05/12/2017', 81, 104, '20171001', 'syed junaid', 28, '1', 'a', 'Mids', 'Urdu', 26, 'P'),
(43, '05/12/2017', 81, 105, '20171002', 'ahmad asdf', 28, '2', 'b', 'Mids', 'Urdu', 26, 'P'),
(44, '06/12/2017', 81, 104, '20171001', 'syed junaid', 28, '1', 'a', 'Mids', 'Math', 27, 'P'),
(45, '06/12/2017', 81, 105, '20171002', 'ahmad asdf', 28, '2', 'b', 'Mids', 'Math', 27, 'P');

-- --------------------------------------------------------

--
-- Table structure for table `exam_grade`
--

CREATE TABLE IF NOT EXISTS `exam_grade` (
  `id` int(11) NOT NULL,
  `parient_id` int(11) DEFAULT NULL,
  `grade_name` varchar(30) NOT NULL,
  `point` varchar(4) NOT NULL,
  `number_form` varchar(5) NOT NULL,
  `number_to` varchar(5) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_grade`
--

INSERT INTO `exam_grade` (`id`, `parient_id`, `grade_name`, `point`, `number_form`, `number_to`) VALUES
(1, NULL, 'F', '0', '0', '32'),
(2, NULL, 'D', '1', '33', '39'),
(3, NULL, 'C', '2', '40', '49'),
(4, NULL, 'B', '3', '50', '59'),
(5, NULL, 'A-', '3.5', '60', '69'),
(6, NULL, 'A', '4', '70', '79'),
(7, NULL, 'A+', '5', '80', '100'),
(8, NULL, 'A+', '5', '90', '100'),
(9, NULL, 'A', '4', '80', '100'),
(11, 80, 'A', '4', '80', '90'),
(12, 80, 'A+', '5', '90', '100'),
(13, 80, 'B', '3', '70', '80'),
(14, 81, 'A+', '5', '90', '100'),
(15, 81, 'A', '4', '80', '89'),
(16, 81, 'B', '3', '70', '79'),
(17, 81, 'C', '2', '60', '69'),
(18, 81, 'D', '1', '50', '59'),
(19, 81, 'F', '0', '0', '49');

-- --------------------------------------------------------

--
-- Table structure for table `exam_routine`
--

CREATE TABLE IF NOT EXISTS `exam_routine` (
  `id` int(11) NOT NULL,
  `parient_id` int(11) DEFAULT NULL,
  `exam_id` int(11) NOT NULL,
  `exam_date` varchar(30) NOT NULL,
  `exam_subject` varchar(100) NOT NULL,
  `subject_code` varchar(15) NOT NULL,
  `rome_number` varchar(10) NOT NULL,
  `start_time` varchar(10) NOT NULL,
  `end_time` varchar(30) NOT NULL,
  `exam_shift` varchar(50) NOT NULL,
  `status` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_routine`
--

INSERT INTO `exam_routine` (`id`, `parient_id`, `exam_id`, `exam_date`, `exam_subject`, `subject_code`, `rome_number`, `start_time`, `end_time`, `exam_shift`, `status`) VALUES
(1, NULL, 4, '9/06/2016', 'AMAR BANGLA BOI', '101', '101', '09.00am', '10.30am', 'Morning shift', 'Result'),
(2, NULL, 4, '10/06/2016', 'ENGLISH FOR TODAY', '102', '101', '09.00am', '10.30am', 'Morning shift', 'Result'),
(3, NULL, 4, '11/06/2016', 'PRIMARY MATHEMATICS', '103', '101', '09.00am', '10.30am', 'Morning shift', 'Result'),
(4, NULL, 5, '08/10/2016', 'AMAR BANGLA BOI', '101', '101', '10.30am', '11.00am', 'Morning shift', 'Result'),
(5, NULL, 6, '10/03/2017', 'Math', 'math-01', 'A1', '9:00', '10:30', 'Morning shift', 'NoResult'),
(6, NULL, 6, '11/03/2017', 'General Science', 'english-01', 'A1', '9:00', '10:30', 'Morning shift', 'NoResult'),
(7, NULL, 6, '12/03/2017', 'Islamiat', 'science-01', 'A1', '9:00', '10:30', 'Morning shift', 'NoResult'),
(8, 80, 12, '15/12/2017', 'abc', '12', '1', '8', '9', 'Morning shift', 'NoResult'),
(9, 80, 12, '16/12/2017', 'math', '13', '1', '8', '9', 'Morning shift', 'NoResult'),
(10, 80, 12, '17/12/2017', 'english', '14', '1', '8', '9', 'Morning shift', 'NoResult'),
(11, 80, 13, '13/12/2017', 'english', '12', '1', '9', '10', 'Morning shift', 'NoResult'),
(12, 80, 13, '14/12/2017', 'math', '13', '1', '9', '10', 'Morning shift', 'NoResult'),
(13, 80, 13, '15/12/2017', 'urdu', '12', '1', '9', '10', 'Select Shift', 'NoResult'),
(14, 80, 14, '18/11/2017', 'abc', '123', '1', '8', '9', 'Morning shift', 'Result'),
(15, 80, 14, '18/11/2017', 'abc', '123', '1', '9', '10', 'Morning shift', 'NoResult'),
(16, 80, 15, '18/11/2017', 'math', '32', '1', '1', '1', 'Morning shift', 'Result'),
(21, 80, 20, '18/11/2017', 'english', '23', '2', '3', '4', 'Morning shift', 'Result'),
(25, 81, 22, '04/12/2017', 'English', '121', '1', '8', '9', 'Morning shift', 'Result'),
(26, 81, 22, '05/12/2017', 'Urdu', '1211', '1', '9', '10', 'Morning shift', 'Result'),
(27, 81, 22, '06/12/2017', 'Math', '232', '1', '10', '11', 'Morning shift', 'Result');

-- --------------------------------------------------------

--
-- Table structure for table `fee_item`
--

CREATE TABLE IF NOT EXISTS `fee_item` (
  `id` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `amount` int(11) NOT NULL,
  `parient_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fee_item`
--

INSERT INTO `fee_item` (`id`, `year`, `class_id`, `title`, `amount`, `parient_id`) VALUES
(5, 2017, 15, 'Admission Fee', 500, NULL),
(6, 2017, 1, 'Monthly Fee', 2000, NULL),
(7, 2017, 15, 'AC Charges', 700, NULL),
(8, 2017, 15, 'Monthly Fee', 2500, NULL),
(9, 2017, 15, 'event1', 100, NULL),
(10, 2017, 16, 'event1', 200, NULL),
(11, 2017, 11, 'event1', 54, NULL),
(12, 2017, 7, 'event1', 34534, NULL),
(15, 2017, 28, 'monthly fee', 2000, 81),
(16, 2017, 29, 'monthly fee', 2500, 81),
(17, 2017, 30, 'monthly fee', 3000, 81),
(18, 2017, 28, 'library fee', 500, 81),
(19, 2017, 28, 'sports fee', 500, 81);

-- --------------------------------------------------------

--
-- Table structure for table `final_result`
--

CREATE TABLE IF NOT EXISTS `final_result` (
  `id` int(11) NOT NULL,
  `parient_id` int(11) DEFAULT NULL,
  `class_id` int(11) NOT NULL,
  `section` varchar(100) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `exam_title` varchar(100) NOT NULL,
  `student_id` varchar(20) NOT NULL,
  `student_name` varchar(100) NOT NULL,
  `total_marks` varchar(100) NOT NULL,
  `obtained_marks` varchar(100) NOT NULL,
  `final_grade` varchar(10) NOT NULL,
  `maride_list` varchar(150) NOT NULL,
  `status` varchar(150) NOT NULL,
  `point` varchar(11) NOT NULL,
  `fail_amount` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `final_result`
--

INSERT INTO `final_result` (`id`, `parient_id`, `class_id`, `section`, `exam_id`, `exam_title`, `student_id`, `student_name`, `total_marks`, `obtained_marks`, `final_grade`, `maride_list`, `status`, `point`, `fail_amount`) VALUES
(1, NULL, 1, '', 4, 'Test Exam 1', '201601001', 'Benjamin D. Lampe', '249', '', 'A', '', 'Pass', '4.67', 0),
(2, NULL, 1, '', 4, 'Test Exam 1', '201601002', 'Rahim Hasan', '241', '', 'A', '', 'Pass', '4.33', 0),
(3, NULL, 1, '', 4, 'Test Exam 1', '201601003', 'Junayed Hak', '254', '', 'A', '', 'Pass', '4.67', 0),
(4, NULL, 1, '', 4, 'Test Exam 1', '201601004', 'Razia Akture', '239', '', 'A', '', 'Pass', '4.67', 0),
(5, NULL, 1, '', 4, 'Test Exam 1', '201601005', 'Polash Sarder', '230', '', 'A', '', 'Pass', '4.5', 0),
(6, NULL, 1, '', 4, 'Test Exam 1', '201601006', 'Sumon Akon', '234', '', 'A', '', 'Pass', '4.67', 0),
(7, 80, 26, '', 20, 'test exam', '2017cl1001', 'student3 student3', '241', '', 'A', '', 'Pass', '4.33', 0),
(8, 80, 26, '', 20, 'test exam', '2017cl1002', 'studen01 studen01', '259', '', 'A-', '', 'Fail', '3.67', 1),
(9, 80, 26, '', 20, 'test exam', '2017cl1003', 'studen02 studen02', '245', '', 'A-', '', 'Pass', '3.67', 0),
(10, 81, 28, '', 22, 'Mids', '20171001', 'syed junaid', '300', '251', 'A', '', 'Pass', '4', 0),
(11, 81, 28, '', 22, 'Mids', '20171002', 'ahmad asdf', '300', '205', 'B', '', 'Fail', '2.67', 1);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` mediumint(8) unsigned NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(3, 'student', 'This user is student''s groups member.'),
(4, 'teacher', 'This user is teacher''s groups member.'),
(5, 'parents', 'This user is parent''s groups member.'),
(6, 'accountant', 'This user is accountent''s groups member.'),
(7, 'library_man', 'The library man can manage library and library''s account information'),
(8, '4th_class_employ', ''),
(9, 'driver', '');

-- --------------------------------------------------------

--
-- Table structure for table `inven_category`
--

CREATE TABLE IF NOT EXISTS `inven_category` (
  `id` int(11) NOT NULL,
  `parient_id` int(11) DEFAULT NULL,
  `category_name` varchar(50) NOT NULL,
  `details` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inven_category`
--

INSERT INTO `inven_category` (`id`, `parient_id`, `category_name`, `details`) VALUES
(4, 81, 'Stationary', 'stationary items '),
(5, 81, 'canteen', '');

-- --------------------------------------------------------

--
-- Table structure for table `inve_item`
--

CREATE TABLE IF NOT EXISTS `inve_item` (
  `id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `parient_id` int(11) DEFAULT NULL,
  `category` varchar(50) NOT NULL,
  `item` varchar(200) NOT NULL,
  `quantity` int(11) NOT NULL,
  `rate` int(11) NOT NULL,
  `discount` int(11) NOT NULL,
  `total_rate` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inve_item`
--

INSERT INTO `inve_item` (`id`, `vendor_id`, `parient_id`, `category`, `item`, `quantity`, `rate`, `discount`, `total_rate`) VALUES
(6, 5, 81, '4', 'class 1 math books', 146, 60, 500, 8500),
(7, 5, 81, '4', 'math book class 2', 96, 50, 500, 4500);

-- --------------------------------------------------------

--
-- Table structure for table `issu_item`
--

CREATE TABLE IF NOT EXISTS `issu_item` (
  `id` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  `user_type` varchar(30) NOT NULL,
  `user_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `rate` int(11) NOT NULL,
  `total_price` int(11) NOT NULL,
  `status` varchar(20) NOT NULL,
  `parient_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `issu_item`
--

INSERT INTO `issu_item` (`id`, `date`, `user_type`, `user_id`, `item_id`, `quantity`, `rate`, `total_price`, `status`, `parient_id`) VALUES
(2, 1511305200, 'Employee', 107, 7, 3, 50, 150, 'Cash', 81),
(3, 1511305200, 'Employee', 107, 6, 2, 60, 120, 'Cash', 81),
(4, 1511305200, 'Student', 104, 6, 1, 60, 60, 'Cash', 81),
(6, 1511391600, 'Parents', 106, 7, 1, 70, 70, 'Cash', 81);

-- --------------------------------------------------------

--
-- Table structure for table `leave_application`
--

CREATE TABLE IF NOT EXISTS `leave_application` (
  `id` int(11) NOT NULL,
  `parient_id` int(11) DEFAULT NULL,
  `year` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `sender_title` varchar(150) NOT NULL,
  `subject` varchar(150) NOT NULL,
  `jobtype` text NOT NULL,
  `leave_start` int(11) NOT NULL,
  `leave_end` int(11) NOT NULL,
  `application_date` int(11) NOT NULL,
  `reason` varchar(500) NOT NULL,
  `cheack_by` varchar(150) NOT NULL,
  `status` text NOT NULL,
  `cheack_statuse` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `leave_application`
--

INSERT INTO `leave_application` (`id`, `parient_id`, `year`, `sender_id`, `sender_title`, `subject`, `jobtype`, `leave_start`, `leave_end`, `application_date`, `reason`, `cheack_by`, `status`, `cheack_statuse`) VALUES
(1, NULL, 2017, 1, 'Headmaster', 'Prayer for leave of absence.', 'part', 1486252800, 1486339200, 1496534400, 'uncle death', 'Headmaster', 'Approved', 'Checked'),
(2, 80, 2017, 80, 'sohail ahmad', 'Prayer for leave of absence.', 'part', 0, 0, 1511046000, 'avy', 'sohail ahmad', 'Compleated', 'Checked'),
(3, 81, 2017, 113, 'asif bilal', 'Prayer for leave of absence.', 'part', 1512082800, 1512169200, 1511910000, 'sick', 'syed junaid', 'Approved', 'Checked');

-- --------------------------------------------------------

--
-- Table structure for table `library_member`
--

CREATE TABLE IF NOT EXISTS `library_member` (
  `id` int(11) unsigned NOT NULL,
  `user_id` int(11) NOT NULL,
  `parient_id` int(11) DEFAULT NULL,
  `title` varchar(50) NOT NULL,
  `fine` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `library_member`
--

INSERT INTO `library_member` (`id`, `user_id`, `parient_id`, `title`, `fine`) VALUES
(1, 4, NULL, 'Benjamin D. Lampe', 0),
(2, 4, NULL, 'Benjamin D. Lampe', 0),
(3, 12, NULL, 'Rahim Hasan', 0),
(4, 13, NULL, 'Junayed Hak', 0),
(5, 1, NULL, 'Headmaster', 0),
(6, 2, NULL, 'Helen K Helton', 0),
(7, 6, NULL, 'Robert D. Franco', 0),
(8, 7, NULL, 'Michael R. Kemp', 0),
(9, 8, NULL, 'Willie B. Quint', 0),
(10, 9, NULL, 'Fredrick V. Keyes', 0),
(11, 10, NULL, 'mumar abboud', 0),
(12, 11, NULL, 'Inayah Asfour', 0),
(13, 13, NULL, 'Junayed Hak', 0),
(14, 104, 81, 'syed junaid', 0),
(15, 105, 81, 'ahmad asdf', 0);

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) unsigned NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `massage`
--

CREATE TABLE IF NOT EXISTS `massage` (
  `id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `receiver_id` varchar(50) NOT NULL,
  `message` varchar(1000) NOT NULL,
  `subject` varchar(200) NOT NULL,
  `read_unread` int(1) NOT NULL,
  `date` int(11) NOT NULL,
  `sender_delete` int(11) NOT NULL,
  `receiver_delete` int(11) NOT NULL,
  `class` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `massage`
--

INSERT INTO `massage` (`id`, `sender_id`, `receiver_id`, `message`, `subject`, `read_unread`, `date`, `sender_delete`, `receiver_delete`, `class`) VALUES
(1, 1, '4', '<p>This is test Message for all students in class 1. This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.</p>\\r\\n', 'This is test Message.', 0, 1465806801, 1, 1, ''),
(2, 1, '12', '<p>This is test Message for all students in class 1. This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.</p>\\r\\n', 'This is test Message.', 0, 1465806801, 1, 1, ''),
(3, 1, '13', '<p>This is test Message for all students in class 1. This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.</p>\\r\\n', 'This is test Message.', 0, 1465806801, 1, 1, ''),
(4, 1, '16', '<p>This is test Message for all students in class 1. This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.</p>\\r\\n', 'This is test Message.', 0, 1465806801, 1, 1, ''),
(5, 1, '23', '<p>This is test Message for all students in class 1. This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.</p>\\r\\n', 'This is test Message.', 0, 1465806801, 1, 1, ''),
(6, 1, '24', '<p>This is test Message for all students in class 1. This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.</p>\\r\\n', 'This is test Message.', 0, 1465806801, 1, 1, ''),
(7, 1, '4', '<p >This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.</p>\\r\\n', 'This is test Message.', 1, 1465807087, 1, 1, ''),
(8, 1, '12', '<p >This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.</p>\\r\\n', 'This is test Message.', 0, 1465807087, 1, 1, ''),
(9, 1, '13', '<p >This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.</p>\\r\\n', 'This is test Message.', 0, 1465807087, 1, 1, ''),
(10, 1, '16', '<p >This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.</p>\\r\\n', 'This is test Message.', 0, 1465807087, 1, 1, ''),
(11, 1, '23', '<p >This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.</p>\\r\\n', 'This is test Message.', 0, 1465807087, 1, 1, ''),
(12, 1, '24', '<p >This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.</p>\\r\\n', 'This is test Message.', 0, 1465807087, 1, 1, ''),
(13, 1, '58', '<p><font color=\\"#848484\\" face=\\"open sans, sans-serif\\"><span >greegqergerge</span></font></p>\\n', 'testing', 1, 1496232083, 1, 1, ''),
(14, 81, '104', '<p>this is for all students in this school</p>\\r\\n', 'all students in this school', 1, 1511430879, 1, 1, ''),
(15, 81, '105', '<p>this is for all students in this school</p>\\r\\n', 'all students in this school', 0, 1511430879, 1, 1, ''),
(16, 81, '108', '<p>this is for all students in this school</p>\\r\\n', 'all students in this school', 0, 1511430879, 1, 1, ''),
(17, 81, '104', '<p>this is for all students of class 1</p>\\r\\n', 'all student of class 1', 1, 1511431177, 1, 1, ''),
(18, 81, '105', '<p>this is for all students of class 1</p>\\r\\n', 'all student of class 1', 0, 1511431177, 1, 1, ''),
(19, 81, '104', '<p>for syed junaid only</p>\\r\\n', 'this is for syed junaid only', 1, 1511431284, 1, 0, ''),
(20, 81, '107', '<p>this is for all teachers here</p>\\r\\n', 'for all teachers', 0, 1511431327, 1, 1, ''),
(21, 81, '111', '<p>this is for all teachers here</p>\\r\\n', 'for all teachers', 0, 1511431327, 1, 1, ''),
(22, 81, '111', '<p>this is for tech3 only</p>\\r\\n', 'for tech3 only', 0, 1511431474, 1, 1, ''),
(23, 81, '106', '<p>this is for all parents in this school</p>\\r\\n', 'all parents in this school', 1, 1511431536, 1, 1, ''),
(24, 81, '109', '<p>this is for all parents in this school</p>\\r\\n', 'all parents in this school', 0, 1511431536, 1, 1, ''),
(25, 81, '110', '<p>this is for all parents in this school</p>\\r\\n', 'all parents in this school', 0, 1511431536, 1, 1, ''),
(26, 81, '106', '<p>this is for all parents of class 1</p>\\r\\n', 'all parents in this class', 1, 1511431673, 1, 1, ''),
(27, 81, '109', '<p>this is for all parents of class 1</p>\\r\\n', 'all parents in this class', 0, 1511431673, 1, 1, ''),
(28, 81, '106', '<p>this is for all parents of class 1</p>\\r\\n', 'parents of class 1', 1, 1511431868, 1, 1, ''),
(29, 81, '109', '<p>this is for all parents of class 1</p>\\r\\n', 'parents of class 1', 0, 1511431868, 1, 1, ''),
(30, 81, '109', '<p>this is only for abug</p>\\r\\n', 'only for abug', 0, 1511431954, 1, 1, ''),
(31, 81, '1', '<p>this is for our boss only</p>\\r\\n', 'for super', 1, 1511432862, 1, 1, ''),
(32, 81, '104', '<p>4u only</p>\\r\\n', '4u', 1, 1511437417, 1, 0, ''),
(33, 107, '107', '<p>for myself</p>\\r\\n', 'to me', 0, 1511526124, 1, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `notice_board`
--

CREATE TABLE IF NOT EXISTS `notice_board` (
  `id` int(11) NOT NULL,
  `parient_id` int(11) DEFAULT NULL,
  `date` varchar(11) NOT NULL,
  `sender` varchar(50) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `notice` varchar(1000) NOT NULL,
  `receiver` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notice_board`
--

INSERT INTO `notice_board` (`id`, `parient_id`, `date`, `sender`, `subject`, `notice`, `receiver`) VALUES
(1, NULL, '13/06/2016', 'Headmaster', 'Test notice for all users. Test notice for all users. Test notice for all users.', '<p><span style=\\"font-size:14px\\">This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. </span></p>\\r\\n', 'all'),
(2, NULL, '30/05/2017', 'Headmaster', 'subha jaldi aayn', '<p>ksfaskfjskdfjsdlkfjaslfjskadfjd</p>\\n\\n<p>asdfkjskdfjas;fjskdfj</p>\\n\\n<p>asdkfasjfklasjf</p>\\n\\n<p>asfkaskf</p>\\n\\n<p>asdkfasjdflkjsf;a</p>\\n', 'teacher'),
(3, 80, '18/11/2017', 'sohail ahmad', 'dsfsdf', '<p>sdfsdfsdf</p>\\r\\n', 'student'),
(4, 80, '19/11/2017', 'sohail ahmad', 'not good result', '<p>fdfdfd fhdfjkhdskjfh sdfjkhdsjfhsdf</p>\\r\\n\\r\\n<p>df</p>\\r\\n\\r\\n<p>&nbsp;sd</p>\\r\\n\\r\\n<p>fhdfkdsfkjdskhfkhjsdhfdks jfdsjf dkjfhsdjkfhksdjfhs</p>\\r\\n', 'all'),
(5, 81, '24/11/2017', 'syed junaid', 'teachers', '<p>for all teachers</p>\\r\\n', 'teacher'),
(6, 81, '24/11/2017', 'syed junaid', 'student', '<p>for students</p>\\r\\n', 'student'),
(7, 81, '24/11/2017', 'syed junaid', 'all', '<p>for all</p>\\r\\n', 'all');

-- --------------------------------------------------------

--
-- Table structure for table `parents_info`
--

CREATE TABLE IF NOT EXISTS `parents_info` (
  `id` int(11) NOT NULL,
  `parient_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `section` varchar(40) NOT NULL,
  `student_id` varchar(40) NOT NULL,
  `parents_name` varchar(100) NOT NULL,
  `relation` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `parents_info`
--

INSERT INTO `parents_info` (`id`, `parient_id`, `user_id`, `class_id`, `section`, `student_id`, `parents_name`, `relation`, `email`, `phone`) VALUES
(1, NULL, 14, 1, '', '201601001', 'John E. Williams  Deidra D. Shaw ', 'Parents', 'parents@parents.com', '+8801245852315'),
(2, NULL, 15, 1, '', '201601002', 'Jafor Uddin Julakha Begum', 'Parents', 'Jafor@Jafor.com', '+8801245852315'),
(3, NULL, 62, 15, '', '201700002', 'ahmad naveed', 'father', 'ahmad@gmail.com', '+923346053055'),
(4, NULL, 63, 15, '', '201700003', 'ali  rehman', 'father', 'rehmanali@gmail.com', '+923346053085'),
(9, 80, 96, 27, 'C', '2017cl2005', 'sadsa1 sadas1', 'asdsad1', 'dsada@gmail.com1', '333333333331'),
(10, 80, 97, 26, 'B', '2017cl1001', 'parient3 parient3', 'father', 'parient3@gmail.com', '33333333333'),
(11, 80, 98, 27, 'C', '2017cl2006', 'parent4 parent4', 'father', 'parent4@gmail.com', '33333333333'),
(12, 81, 106, 28, 'a', '20171001', 'Javaid Hussain', 'father', 'father@father.com', '21322434312'),
(13, 81, 109, 28, 'b', '20171002', 'abug abu1', 'father', 'f1@f1.com', '32133421321'),
(14, 81, 110, 29, '', '20172001', 'dddssdasd asdwqesdsa', 'father', 'f2@f2.com', '31235533123');

-- --------------------------------------------------------

--
-- Table structure for table `result_action`
--

CREATE TABLE IF NOT EXISTS `result_action` (
  `id` int(11) NOT NULL,
  `parient_id` int(11) DEFAULT NULL,
  `class_id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `exam_title` varchar(100) NOT NULL,
  `status` varchar(20) NOT NULL,
  `publish` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `result_action`
--

INSERT INTO `result_action` (`id`, `parient_id`, `class_id`, `exam_id`, `exam_title`, `status`, `publish`) VALUES
(1, NULL, 1, 4, 'Test Exam 1', 'Complete', 'Publish'),
(2, 80, 26, 20, 'test exam', 'Complete', 'Publish'),
(3, 81, 28, 22, 'Mids', 'Complete', 'Publish');

-- --------------------------------------------------------

--
-- Table structure for table `result_shit`
--

CREATE TABLE IF NOT EXISTS `result_shit` (
  `id` int(11) NOT NULL,
  `parient_id` int(11) DEFAULT NULL,
  `exam_title` varchar(100) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `section` varchar(100) NOT NULL,
  `student_name` varchar(100) NOT NULL,
  `student_id` varchar(100) NOT NULL,
  `exam_subject` varchar(100) NOT NULL,
  `total_marks` varchar(10) NOT NULL,
  `obtained_marks` varchar(10) NOT NULL,
  `point` varchar(5) NOT NULL,
  `grade` varchar(5) NOT NULL,
  `roll_number` int(11) NOT NULL,
  `result` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `result_shit`
--

INSERT INTO `result_shit` (`id`, `parient_id`, `exam_title`, `exam_id`, `class_id`, `section`, `student_name`, `student_id`, `exam_subject`, `total_marks`, `obtained_marks`, `point`, `grade`, `roll_number`, `result`) VALUES
(26, NULL, 'FInal Exam', 5, 3, '', 'Sabina Sumi', '201603004', 'AMAR BANGLA BOI', '68', '', '3', 'B', 4, 'Pass'),
(27, 80, 'test exam', 14, 26, '', 'student3 student3', '2017cl1001', 'abc', '81', '', '5', 'A', 1, 'Pass'),
(28, 80, 'test exam', 14, 26, '', 'studen01 studen01', '2017cl1002', 'abc', '99', '', '3', 'B', 2, 'Pass'),
(29, 80, 'test exam', 14, 26, '', 'studen02 studen02', '2017cl1003', 'abc', '77', '', '3', 'B', 3, 'Pass'),
(30, 80, 'test exam', 15, 26, '', 'student3 student3', '2017cl1001', 'math', '80', '', '4', 'A', 1, 'Pass'),
(31, 80, 'test exam', 15, 26, '', 'studen01 studen01', '2017cl1002', 'math', '70', '', '3', 'B', 2, 'Fail'),
(32, 80, 'test exam', 15, 26, '', 'studen02 studen02', '2017cl1003', 'math', '98', '', '5', 'A+', 3, 'Pass'),
(33, 80, 'math', 20, 26, '', 'student3 student3', '2017cl1001', 'english', '80', '', '4', 'A', 1, 'Pass'),
(34, 80, 'math', 20, 26, '', 'studen01 studen01', '2017cl1002', 'english', '90', '', '5', 'A+', 2, 'Pass'),
(35, 80, 'math', 20, 26, '', 'studen02 studen02', '2017cl1003', 'english', '70', '', '3', 'B', 3, 'Pass'),
(50, 81, 'Mids', 22, 28, '', 'syed junaid', '20171001', 'English', '100', '81', '4', 'A', 1, 'Pass'),
(51, 81, 'Mids', 22, 28, '', 'ahmad asdf', '20171002', 'English', '100', '80', '4', 'A', 2, 'Pass'),
(52, 81, 'Mids', 22, 28, '', 'syed junaid', '20171001', 'Urdu', '100', '78', '3', 'B', 1, 'Pass'),
(53, 81, 'Mids', 22, 28, '', 'ahmad asdf', '20171002', 'Urdu', '100', '80', '4', 'A', 2, 'Pass'),
(54, 81, 'Mids', 22, 28, '', 'syed junaid', '20171001', 'Math', '100', '92', '5', 'A+', 1, 'Pass'),
(55, 81, 'Mids', 22, 28, '', 'ahmad asdf', '20171002', 'Math', '100', '45', '0', 'F', 2, 'Fail');

-- --------------------------------------------------------

--
-- Table structure for table `result_submition_info`
--

CREATE TABLE IF NOT EXISTS `result_submition_info` (
  `id` int(11) NOT NULL,
  `parient_id` int(11) DEFAULT NULL,
  `class_id` int(11) NOT NULL,
  `section` varchar(100) NOT NULL,
  `exam_title` varchar(150) NOT NULL,
  `date` varchar(50) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `submited` varchar(50) NOT NULL,
  `teacher` varchar(100) NOT NULL,
  `exam_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `result_submition_info`
--

INSERT INTO `result_submition_info` (`id`, `parient_id`, `class_id`, `section`, `exam_title`, `date`, `subject`, `submited`, `teacher`, `exam_id`) VALUES
(1, NULL, 1, '', 'Test Exam 1', '10/06/2016', 'AMAR BANGLA BOI', '1', 'Willie B. Quint', 4),
(2, NULL, 1, '', 'Test Exam 1', '10/06/2016', 'ENGLISH FOR TODAY', '1', 'Fredrick V. Keyes', 4),
(3, NULL, 1, '', 'Test Exam 1', '11/06/2016', 'PRIMARY MATHEMATICS', '1', 'Willie B. Quint', 4),
(4, NULL, 3, '', 'FInal Exam', '08/10/2016', 'AMAR BANGLA BOI', '1', 'Headmaster', 5),
(5, NULL, 3, '', 'FInal Exam', '08/10/2016', 'AMAR BANGLA BOI', '1', 'Headmaster', 5),
(6, NULL, 26, '', 'kACHAY pAPER', '30/05/2017', '', '1', 'Headmaster', 6),
(7, 80, 26, '', 'test exam', '18/11/2017', 'abc', '1', 'sohail ahmad', 14),
(8, 80, 26, '', 'test exam', '18/11/2017', 'math', '1', 'sohail ahmad', 15),
(9, 80, 26, '', 'test exam', '18/11/2017', 'english', '1', 'sohail ahmad', 20),
(17, 81, 28, '', 'Mids', '06/12/2017', 'English', '1', 'syed junaid', 22),
(18, 81, 28, '', 'Mids', '06/12/2017', 'Urdu', '1', 'syed junaid', 22),
(19, 81, 28, '', 'Mids', '06/12/2017', 'Math', '1', 'syed junaid', 22);

-- --------------------------------------------------------

--
-- Table structure for table `role_based_access`
--

CREATE TABLE IF NOT EXISTS `role_based_access` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(1) NOT NULL,
  `das_top_info` int(1) NOT NULL,
  `das_grab_chart` int(1) NOT NULL,
  `das_class_info` int(1) NOT NULL,
  `das_message` int(1) NOT NULL,
  `das_employ_attend` int(1) NOT NULL,
  `das_notice` int(1) NOT NULL,
  `das_calender` int(1) NOT NULL,
  `admission` int(1) NOT NULL,
  `all_student_info` int(1) NOT NULL,
  `stud_edit_delete` int(1) NOT NULL,
  `stu_own_info` int(1) NOT NULL,
  `teacher_info` int(1) NOT NULL,
  `add_teacher` int(1) NOT NULL,
  `teacher_details` int(1) NOT NULL,
  `teacher_edit_delete` int(1) NOT NULL,
  `all_parents_info` int(1) NOT NULL,
  `own_parents_info` int(1) NOT NULL,
  `make_parents_id` int(1) NOT NULL,
  `parents_edit_dlete` int(1) NOT NULL,
  `add_employee` int(1) NOT NULL,
  `employee_list` int(1) NOT NULL,
  `employ_attendance` int(1) NOT NULL,
  `empl_atte_view` int(1) NOT NULL,
  `add_new_class` int(1) NOT NULL,
  `all_class_info` int(1) NOT NULL,
  `class_details` int(1) NOT NULL,
  `class_delete` int(1) NOT NULL,
  `class_promotion` int(1) NOT NULL,
  `add_class_routine` int(1) NOT NULL,
  `own_class_routine` int(1) NOT NULL,
  `all_class_routine` int(1) NOT NULL,
  `rutin_edit_delete` int(1) NOT NULL,
  `attendance_preview` int(1) NOT NULL,
  `take_studence_atten` int(1) NOT NULL,
  `edit_student_atten` int(1) NOT NULL,
  `add_subject` int(1) NOT NULL,
  `all_subject` int(1) NOT NULL,
  `assin_optio_sub` int(1) NOT NULL,
  `make_suggestion` int(1) NOT NULL,
  `all_suggestion` int(1) NOT NULL,
  `own_suggestion` int(1) NOT NULL,
  `add_exam_gread` int(1) NOT NULL,
  `exam_gread` int(1) NOT NULL,
  `gread_edit_dele` int(1) NOT NULL,
  `add_exam_routin` int(1) NOT NULL,
  `all_exam_routine` int(1) NOT NULL,
  `own_exam_routine` int(1) NOT NULL,
  `exam_attend_preview` int(1) NOT NULL,
  `approve_result` int(1) NOT NULL,
  `view_result` int(1) NOT NULL,
  `all_mark_sheet` int(1) NOT NULL,
  `own_mark_sheet` int(1) NOT NULL,
  `take_exam_attend` int(1) NOT NULL,
  `change_exam_attendance` int(1) NOT NULL,
  `make_result` int(1) NOT NULL,
  `add_category` int(1) NOT NULL,
  `all_category` int(1) NOT NULL,
  `edit_delete_category` int(1) NOT NULL,
  `add_books` int(1) NOT NULL,
  `all_books` int(1) NOT NULL,
  `edit_delete_books` int(1) NOT NULL,
  `add_library_mem` int(1) NOT NULL,
  `memb_list` int(1) NOT NULL,
  `issu_return` int(1) NOT NULL,
  `add_dormitories` int(1) NOT NULL,
  `add_set_dormi` int(1) NOT NULL,
  `set_member_bed` int(1) NOT NULL,
  `dormi_report` int(1) NOT NULL,
  `add_transport` int(1) NOT NULL,
  `all_transport` int(1) NOT NULL,
  `transport_edit_dele` int(1) NOT NULL,
  `add_account_title` int(1) NOT NULL,
  `edit_dele_acco` int(1) NOT NULL,
  `trensection` int(1) NOT NULL,
  `fee_collection` int(1) NOT NULL,
  `all_slips` int(1) NOT NULL,
  `own_slip` int(1) NOT NULL,
  `slip_edit_delete` int(1) NOT NULL,
  `pay_salary` int(1) NOT NULL,
  `creat_notice` int(1) NOT NULL,
  `send_message` int(1) NOT NULL,
  `vendor` int(1) NOT NULL,
  `delet_vendor` int(1) NOT NULL,
  `add_inv_cat` int(1) NOT NULL,
  `inve_item` int(1) NOT NULL,
  `delete_inve_ite` int(1) NOT NULL,
  `delete_inv_cat` int(1) NOT NULL,
  `inve_issu` int(1) NOT NULL,
  `delete_inven_issu` int(1) NOT NULL,
  `check_leav_appli` int(1) NOT NULL,
  `setting_accounts` int(1) NOT NULL,
  `other_setting` int(1) NOT NULL,
  `front_setings` int(1) NOT NULL,
  `set_optional` int(1) NOT NULL,
  `setting_manage_user` int(1) NOT NULL,
  `admin_info` int(1) NOT NULL DEFAULT '0',
  `add_admin` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_based_access`
--

INSERT INTO `role_based_access` (`id`, `user_id`, `group_id`, `das_top_info`, `das_grab_chart`, `das_class_info`, `das_message`, `das_employ_attend`, `das_notice`, `das_calender`, `admission`, `all_student_info`, `stud_edit_delete`, `stu_own_info`, `teacher_info`, `add_teacher`, `teacher_details`, `teacher_edit_delete`, `all_parents_info`, `own_parents_info`, `make_parents_id`, `parents_edit_dlete`, `add_employee`, `employee_list`, `employ_attendance`, `empl_atte_view`, `add_new_class`, `all_class_info`, `class_details`, `class_delete`, `class_promotion`, `add_class_routine`, `own_class_routine`, `all_class_routine`, `rutin_edit_delete`, `attendance_preview`, `take_studence_atten`, `edit_student_atten`, `add_subject`, `all_subject`, `assin_optio_sub`, `make_suggestion`, `all_suggestion`, `own_suggestion`, `add_exam_gread`, `exam_gread`, `gread_edit_dele`, `add_exam_routin`, `all_exam_routine`, `own_exam_routine`, `exam_attend_preview`, `approve_result`, `view_result`, `all_mark_sheet`, `own_mark_sheet`, `take_exam_attend`, `change_exam_attendance`, `make_result`, `add_category`, `all_category`, `edit_delete_category`, `add_books`, `all_books`, `edit_delete_books`, `add_library_mem`, `memb_list`, `issu_return`, `add_dormitories`, `add_set_dormi`, `set_member_bed`, `dormi_report`, `add_transport`, `all_transport`, `transport_edit_dele`, `add_account_title`, `edit_dele_acco`, `trensection`, `fee_collection`, `all_slips`, `own_slip`, `slip_edit_delete`, `pay_salary`, `creat_notice`, `send_message`, `vendor`, `delet_vendor`, `add_inv_cat`, `inve_item`, `delete_inve_ite`, `delete_inv_cat`, `inve_issu`, `delete_inven_issu`, `check_leav_appli`, `setting_accounts`, `other_setting`, `front_setings`, `set_optional`, `setting_manage_user`, `admin_info`, `add_admin`) VALUES
(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(75, 80, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0),
(76, 81, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0),
(77, 82, 4, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(78, 83, 4, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(80, 85, 4, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(81, 86, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0),
(82, 87, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(85, 90, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(86, 91, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(91, 96, 5, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(96, 101, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(97, 102, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(99, 104, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(100, 105, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(101, 106, 5, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(102, 107, 4, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(104, 108, 3, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(105, 109, 5, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(106, 110, 5, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(107, 111, 4, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(109, 113, 7, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(110, 114, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0),
(111, 115, 6, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `routes`
--

CREATE TABLE IF NOT EXISTS `routes` (
  `id` int(11) NOT NULL,
  `vehicle_no` varchar(10) NOT NULL,
  `route_code` varchar(10) NOT NULL,
  `route_start` varchar(30) NOT NULL,
  `route_end` varchar(30) NOT NULL,
  `parient_id` int(11) DEFAULT NULL,
  `fare` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `salary`
--

CREATE TABLE IF NOT EXISTS `salary` (
  `id` int(11) NOT NULL,
  `parient_id` int(11) DEFAULT NULL,
  `year` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  `month` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `employ_title` varchar(100) NOT NULL,
  `total_amount` int(11) NOT NULL,
  `method` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `salary`
--

INSERT INTO `salary` (`id`, `parient_id`, `year`, `date`, `month`, `user_id`, `employ_title`, `total_amount`, `method`) VALUES
(1, 0, 2017, 1496102400, 'February', 1, 'Headmaster', 17500, 'Cash'),
(2, 0, 2017, 1496102400, 'March', 1, 'Headmaster', 17500, 'Check'),
(3, 0, 2017, 1497218400, 'January', 66, 'sohail ahmad', 11320, 'Cash'),
(4, 0, 2017, 1511132400, 'November', 1, 'Headmaster', 0, 'Card');

-- --------------------------------------------------------

--
-- Table structure for table `set_salary`
--

CREATE TABLE IF NOT EXISTS `set_salary` (
  `id` int(11) NOT NULL,
  `year` int(5) NOT NULL,
  `employ_user_id` int(11) NOT NULL,
  `employe_title` varchar(100) NOT NULL,
  `job_post` varchar(50) NOT NULL,
  `basic` int(11) NOT NULL,
  `treatment` int(11) NOT NULL,
  `increased` int(11) NOT NULL,
  `others` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `month` int(3) NOT NULL,
  `parient_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `set_salary`
--

INSERT INTO `set_salary` (`id`, `year`, `employ_user_id`, `employe_title`, `job_post`, `basic`, `treatment`, `increased`, `others`, `total`, `month`, `parient_id`) VALUES
(1, 2016, 1, 'Headmaster', 'Headmaster', 15000, 2000, 0, 500, 17500, 11, NULL),
(2, 2017, 66, 'sohail ahmad', 'Accountant', 10000, 1200, 120, 0, 11320, 1, NULL),
(3, 2017, 107, 'asad amjad', 'Senior Teacher', 20000, 10000, 5000, 1000, 36000, 11, 81),
(4, 2017, 111, 'tech3 tech5', 'Teacher', 15000, 5000, 3000, 1000, 24000, 11, 81),
(6, 2017, 113, 'asif bilal', 'Librarian', 8000, 10000, 2000, 1000, 21000, 11, 81);

-- --------------------------------------------------------

--
-- Table structure for table `slip`
--

CREATE TABLE IF NOT EXISTS `slip` (
  `id` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `month` varchar(10) NOT NULL,
  `date` varchar(15) NOT NULL,
  `class_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `item_id` varchar(100) NOT NULL,
  `amount` int(11) NOT NULL,
  `dues` int(11) NOT NULL,
  `advance` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `paid` int(11) NOT NULL,
  `balance` int(11) NOT NULL,
  `edit_by` varchar(100) NOT NULL,
  `status` text NOT NULL,
  `mathod` text NOT NULL,
  `parient_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slip`
--

INSERT INTO `slip` (`id`, `year`, `month`, `date`, `class_id`, `student_id`, `item_id`, `amount`, `dues`, `advance`, `total`, `paid`, `balance`, `edit_by`, `status`, `mathod`, `parient_id`) VALUES
(1, 2017, 'June', '0', 1, 201601001, '6', 2000, 0, 0, 2000, 0, 0, '', 'Unpaid', '', 0),
(2, 2017, 'June', '0', 1, 201601002, '6', 2000, 0, 0, 2000, 0, 0, '', 'Unpaid', '', 0),
(3, 2017, 'June', '0', 1, 201601003, '6', 2000, 0, 0, 2000, 0, 0, '', 'Unpaid', '', 0),
(4, 2017, 'June', '0', 1, 201601004, '6', 2000, 0, 0, 2000, 0, 0, '', 'Unpaid', '', 0),
(5, 2017, 'June', '0', 1, 201601005, '6', 2000, 0, 0, 2000, 0, 0, '', 'Unpaid', '', 0),
(6, 2017, 'June', '0', 1, 201601006, '6', 2000, 0, 0, 2000, 0, 0, '', 'Unpaid', '', 0),
(7, 2017, 'June', '0', 15, 201700001, '5,7,8,9', 3800, 0, 0, 3800, 0, 0, '', 'Unpaid', '', 0),
(8, 2017, 'June', '0', 15, 201700002, '5,7,8,9', 3800, 0, 0, 3800, 0, 0, '', 'Unpaid', '', 0),
(9, 2017, 'June', '0', 15, 201700003, '5,7,8,9', 3800, 0, 0, 3800, 0, 0, '', 'Unpaid', '', 0),
(21, 2017, 'December', '08-12-2017', 28, 20171001, '15,18,19', 3000, 0, 0, 3000, 3000, 0, '', 'Paid', 'Cash', 81),
(22, 2017, 'December', '08-12-2017', 28, 20171002, '15,18,19', 3000, 0, 0, 3000, 3500, 500, '', 'Paid', 'Cash', 81),
(23, 2017, 'December', '08-12-2017', 29, 20172001, '16', 2500, 0, 0, 2500, 3000, 500, '', 'Paid', 'Cash', 81);

-- --------------------------------------------------------

--
-- Table structure for table `student_info`
--

CREATE TABLE IF NOT EXISTS `student_info` (
  `id` int(11) NOT NULL,
  `parient_id` int(11) DEFAULT NULL,
  `year` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `student_id` varchar(20) NOT NULL,
  `roll_number` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `student_nam` varchar(100) NOT NULL,
  `farther_name` varchar(150) NOT NULL,
  `mother_name` varchar(150) NOT NULL,
  `birth_date` varchar(100) NOT NULL,
  `sex` varchar(30) NOT NULL,
  `present_address` varchar(300) NOT NULL,
  `permanent_address` varchar(300) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `father_occupation` varchar(150) NOT NULL,
  `father_incom_range` varchar(100) NOT NULL,
  `mother_occupation` varchar(100) NOT NULL,
  `student_photo` varchar(200) NOT NULL,
  `last_class_certificate` text NOT NULL,
  `t_c` text NOT NULL,
  `national_birth_certificate` text NOT NULL,
  `academic_transcription` text NOT NULL,
  `testimonial` text NOT NULL,
  `documents_info` varchar(500) NOT NULL,
  `starting_year` int(11) NOT NULL,
  `transfer_year` int(11) NOT NULL,
  `transfer_to` text NOT NULL,
  `transfer_reason` text NOT NULL,
  `tc_appli_approved_by` text NOT NULL,
  `passing_year` int(11) NOT NULL,
  `compleat_level` text NOT NULL,
  `registration_number` text NOT NULL,
  `certificates_status` text NOT NULL,
  `admission_year` int(11) NOT NULL,
  `admission_class` varchar(100) NOT NULL,
  `admission_roll` int(5) NOT NULL,
  `admission_form_no` int(11) NOT NULL,
  `admission_test_result` int(11) NOT NULL,
  `tc_form` varchar(150) NOT NULL,
  `blood` varchar(3) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_info`
--

INSERT INTO `student_info` (`id`, `parient_id`, `year`, `user_id`, `student_id`, `roll_number`, `class_id`, `student_nam`, `farther_name`, `mother_name`, `birth_date`, `sex`, `present_address`, `permanent_address`, `phone`, `father_occupation`, `father_incom_range`, `mother_occupation`, `student_photo`, `last_class_certificate`, `t_c`, `national_birth_certificate`, `academic_transcription`, `testimonial`, `documents_info`, `starting_year`, `transfer_year`, `transfer_to`, `transfer_reason`, `tc_appli_approved_by`, `passing_year`, `compleat_level`, `registration_number`, `certificates_status`, `admission_year`, `admission_class`, `admission_roll`, `admission_form_no`, `admission_test_result`, `tc_form`, `blood`) VALUES
(1, 80, 2017, 87, '2017cl2005', 5, 27, 'dsda dsad', 'sadas', 'dsad', '12/12/1999', 'Male', ' dsada       ', 'asdas', '33333333333', 'Employer', '123', 'Teachers', 'bcbc53ca45f8d67806c9e8fc6c35094d.png', '', '', '', '', '', '123', 2017, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'O-'),
(4, 80, 2017, 90, '2017cl1001', 1, 26, 'student3 student3', 'sadsadsd', 'sdadsad', '12/12/1999', 'Male', ' sdasdas          ', 'asdasda', '99999999999', 'Business', '123', 'Employer', 'cb2954a3e9c36a586591b56bf6004f47.png', '', '', '', '', '', '321', 2017, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'A-'),
(5, 80, 2017, 91, '2017cl2006', 6, 27, 'student4 student4', 'asdf', 'asfd', '12/12/1999', 'Male', ' fgdfgd  ', 'fdgdfg', '99999999999', 'Business', '321', 'Business', 'ba5e72a4e57b17660d3bd89bad8f0586.png', '', '', '', '', '', '654', 2017, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'B-'),
(6, 80, 2017, 99, '2017cl1002', 2, 26, 'studen01 studen01', 'studen01', 'studen01', '12/12/1999', 'Male', ' dfsdf', 'dfsfdsf', '33333333333', 'Car Driver', '2343', 'Business', '9ffb50c9d068f887ef73f24a56dfc03d.png', '', '', '', '', '', '43423', 2017, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'O-'),
(7, 80, 2017, 100, '2017cl1003', 3, 26, 'studen02 studen02', 'studen02', 'studen02', '12/12/2000', 'Male', ' studen02', 'studen02', '33334444444', 'Banker', '343', 'Farmer', 'e7e494400c07caed6435448517396b93.png', '', '', '', '', '', 'ere', 2017, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'B+'),
(8, 81, 2017, 104, '20171001', 1, 28, 'syed junaid', 'javaid', 'mother', '11/11/1999', 'Male', ' lahore', 'Pakistan', '12311231232', 'Employer', '123', 'Business', '56e1e1d93b24f16ab3ea7ce1e3ae3db0.jpg', 'submited', 'submited', 'submited', 'submited', 'submited', 'std\\_1', 2017, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'O-'),
(9, 81, 2017, 105, '20171002', 2, 28, 'ahmad asdf', 'father', 'moth', '11/11/1998', 'Male', 'khi', 'khi', '12322332431', 'Business', '222', 'Business', '0cf459ef26cdadb4f2624787f211d717.PNG', 'submited', 'submited', 'submited', 'submited', 'submited', 'std\\_2', 2017, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'A-'),
(10, 81, 2017, 108, '20172001', 1, 29, 'dum1 dumy', 'fathe', 'mdfas', '11/11/1988', 'Male', ' fgads ', 'asdsadas', '23123213232', 'Banker', '212', 'Business', '6204c50a5d57341f498b7bc6658bd01e.jpg', 'submited', 'submited', 'submited', 'submited', 'submited', '4213', 2017, 0, '', '', '', 0, '', '', '', 0, '', 0, 0, 0, '', 'O-');

-- --------------------------------------------------------

--
-- Table structure for table `subjects_mark`
--

CREATE TABLE IF NOT EXISTS `subjects_mark` (
  `id` int(11) NOT NULL,
  `exam_title` varchar(100) NOT NULL,
  `class` varchar(100) NOT NULL,
  `section` varchar(100) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `student_id` varchar(20) NOT NULL,
  `student_name` varchar(100) NOT NULL,
  `roll_number` int(11) NOT NULL,
  `mark` int(11) NOT NULL,
  `grade` varchar(30) NOT NULL,
  `statud` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `suggestion`
--

CREATE TABLE IF NOT EXISTS `suggestion` (
  `id` int(11) NOT NULL,
  `parient_id` int(11) DEFAULT NULL,
  `author_id` int(11) NOT NULL,
  `author_name` varchar(150) NOT NULL,
  `class` varchar(20) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `suggestion_title` varchar(150) NOT NULL,
  `suggestion` varchar(2500) NOT NULL,
  `date` int(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suggestion`
--

INSERT INTO `suggestion` (`id`, `parient_id`, `author_id`, `author_name`, `class`, `subject`, `suggestion_title`, `suggestion`, `date`) VALUES
(2, NULL, 8, 'Willie B. Quint', 'Class 1', '', 'This is test suggestion.', '<p>This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.</p>\\r\\n', 1466152654),
(3, NULL, 1, 'Headmaster', 'KG1', '', 'Exam Result', '<p>bachay saii ni parh rhay,,sdafdfasfasdf</p>\\n\\n<p>sdfasfasdfasfa</p>\\n\\n<p>sdadfsdafasdfasfsdfasfasfasdfsdfafasdfsf</p>\\n', 1496143529),
(4, 80, 80, 'sohail ahmad', 'class 1', '', 'sdasd', '<p>sadsadasd</p>\\r\\n', 1511037704),
(5, 80, 80, 'sohail ahmad', 'class 1', '', 'sdfdfsdfdsf', '<p>fsdfdsfsdffdfsdfdsfddsff fdfd f</p>\\r\\n', 1511079128),
(6, 80, 80, 'sohail ahmad', 'class 1', '', 'not good result', '<p>hkasjd sdjhaskjdhsjdhasd asjdhjksdhs dhaskjhdjasdhjsd ashd shdjkadhk shda kdhaksjdhkashdksad ahsskdjhas dkhs djhaskhdkajshdk sdhjas khdjkashdkajshdjkahsdjk sdsh</p>\\r\\n\\r\\n<p>sd oasdishdajhdasd</p>\\r\\n\\r\\n<p>as</p>\\r\\n\\r\\n<p>das dh jdhaskjdhskdjahskd</p>\\r\\n', 1511079497);

-- --------------------------------------------------------

--
-- Table structure for table `teachers_info`
--

CREATE TABLE IF NOT EXISTS `teachers_info` (
  `id` int(11) NOT NULL,
  `user_id` varchar(30) NOT NULL,
  `parient_id` int(11) DEFAULT NULL,
  `fullname` varchar(100) NOT NULL,
  `farther_name` varchar(150) NOT NULL,
  `mother_name` varchar(150) NOT NULL,
  `birth_date` varchar(150) NOT NULL,
  `sex` varchar(30) NOT NULL,
  `present_address` varchar(300) NOT NULL,
  `permanent_address` varchar(300) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `subject` varchar(150) NOT NULL,
  `position` varchar(150) NOT NULL,
  `working_hour` varchar(50) NOT NULL,
  `educational_qualification_1` varchar(500) NOT NULL,
  `educational_qualification_2` varchar(500) NOT NULL,
  `educational_qualification_3` varchar(500) NOT NULL,
  `educational_qualification_4` varchar(500) NOT NULL,
  `educational_qualification_5` varchar(500) NOT NULL,
  `teachers_photo` varchar(200) NOT NULL,
  `cv` varchar(30) NOT NULL,
  `educational_certificat` varchar(30) NOT NULL,
  `exprieance_certificatte` varchar(30) NOT NULL,
  `files_info` varchar(500) NOT NULL,
  `index_no` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teachers_info`
--

INSERT INTO `teachers_info` (`id`, `user_id`, `parient_id`, `fullname`, `farther_name`, `mother_name`, `birth_date`, `sex`, `present_address`, `permanent_address`, `phone`, `subject`, `position`, `working_hour`, `educational_qualification_1`, `educational_qualification_2`, `educational_qualification_3`, `educational_qualification_4`, `educational_qualification_5`, `teachers_photo`, `cv`, `educational_certificat`, `exprieance_certificatte`, `files_info`, `index_no`) VALUES
(1, '8', 0, 'Willie B. Quint', 'Kevin A. Robledo', 'Mary T. McQuay', '12/05/1956', 'Male', '3133 Pointe Lane\\r\\nHollywood, FL 33020', '3133 Pointe Lane\\r\\nHollywood, FL 33020', '+8801245852315', 'English', 'Assistant Headmaster', 'Full time', 'SSC,Test School,A+,1978', 'HSC,Test College,A+,1980', 'Graduation ( Test ),Test University,A,1984', 'Masters Degree,Test University,A,1986', '', 'ac197cff181d9c58027800912c3e0855.png', 'submited', 'submited', 'submited', 'Teacher 2016', '12004'),
(2, '9', 0, 'Fredrick V. Keyes', 'Anthony T. Andrews', 'Mary J. Dahl', '20/12/1970', 'Male', '712 Beechwood Drive\\r\\nChurchville, MD 21028 ', '712 Beechwood Drive\\r\\nChurchville, MD 21028 ', '+8801245852315', 'Mathematics', 'Senior Teacher', 'Full time', 'SSC,Test School,A+,1988', 'HSC,Test College,A,1990', 'Graduation ( Test ),Test University,A,1994', 'Masters Degree,Test University,A,1996', '', '0f8649af98ca9c0e85957db7e9778191.png', 'submited', 'submited', 'submited', 'Teacher 2016', '12006'),
(3, '10', 0, 'mumar abboud', 'Hadi Shakir Essa', 'Yamha Dhakiyah Mikhail', '11/11/1980', 'Male', '71 City Walls Rd\\\\\\\\r\\\\\\\\nCLOCK FACE\\\\\\\\r\\\\\\\\nWA9 6BG', '71 City Walls Rd\\\\\\\\r\\\\\\\\nCLOCK FACE\\\\\\\\r\\\\\\\\nWA9 6BG', '+8801245852315', 'Bangla ', 'Teacher', 'Full time', 'SSC,Test School,A+,1998', 'HSC,Test College,A,2000', 'Graduation ( Test ),Test University,A,2004', 'Masters Degree,Test University,B,2006', '', '3566d34fc7ce565ba8841d0eaf537b28.png', 'submited', 'submited', 'submited', 'Teacher 2016', '12051'),
(4, '11', 0, 'Inayah Asfour', 'Fatin Husayn', 'Rukan Habeeba', '12/12/1980', 'Female', '31 Clasper Way\\r\\nHEST BANK\\r\\nLA2 2HF ', '31 Clasper Way\\r\\nHEST BANK\\r\\nLA2 2HF ', '+8801245852315', 'Science', 'Teacher', 'Full time', 'SSC,Test School,A+,1998', 'HSC,Test College,A+,2000', 'Graduation ( Test ),Test University,A,2004', 'Masters Degree,Test University,A,2006', '', 'f342f5b3412a5c4be681878ff0462e09.png', 'submited', 'submited', 'submited', 'Teacher 2016', '12056'),
(5, '58', 0, 'Mudasir Murtaza', 'Murtaza', 'abc', '10/10/1947', 'Male', 'Cybex Company', 'Cybex Company', '+923336308419', 'Math', 'Headmaster', 'Full time', 'MBA,BZU,3.7,1969', '', '', '', '', '00264ab08cddb9c1f522b7ee3c9040d4.jpg', 'submited', 'submited', 'submited', '12131231', '1'),
(6, '64', 0, 'Mr. Raheel naeem', 'naeem bakhtawar', 'neelam', '05/06/1988', 'Male', 'walton road lahore cantt', 'walton road lahore cantt', '+923346053095', 'Maths', 'Senior Teacher', 'Full time', 'Msc,Pu lahore,4.00/4.00,2013', 'Bsc,Pu lahore,3.9/4.00,2011', 'fsc,ahmad college lahore,903,2009', '', '', '8f77c8261d6698583823ceb647b50901.jpg', 'submited', 'submited', 'submited', 'file 215151', '56557'),
(7, '82', 80, 'sohail ahmad1', 'sfsafsdf', 'sdfafds', '12/12/1998', 'Male', 'safd', 'asdf', '03000333968', 'math', 'Teacher', 'Full time', 'abc,aba,abc,1990', '', '', '', '', '44d05827bba3b0664723e2248ed48dc3.jpg', '', 'submited', '', '123', '123'),
(8, '83', 80, 'sadas Ahmad1', 'afasf', 'sadfsdf', '12/12/1990', 'Male', 'asfdfs', 'asfsdf', '03001234567', 'saf', 'Headmaster', 'Part time', 'sdfsd,dsfd,sdfsd,1999', '', '', '', '', '87d4f36af5cba13dfe357bdfa97858b2.png', '', '', '', 'dsfdf', '12'),
(10, '85', 80, 'test user', 'asdf', 'asfd', '12/12/1999', 'Male', 'adfaf', 'asdf', '03001234562', 'asdf', 'Assistant Headmaster', 'Part time', 'asdf,ad,asdf,af', 'asdf,asfd,afsd,sdf', '', '', '', 'b09d97f6433482156c421937dd3af20d.png', '', '', '', 'afd', '123'),
(11, '107', 81, 'asad amjad', 'amjad', 'mothrre', '11/12/1980', 'Male', 'Isb', 'Isb', '31231232313', 'Science', 'Senior Teacher', 'Full time', 'SSC,test,pass,2000', 'HSSC,test1,pass,2002', 'BSC,test2,pass,2006', 'MSC,test3,pass,2010', '', 'f1e9e21f6acde2e7a5b461add62e13cb.jpg', 'submited', 'submited', 'submited', 'tchr\\_1', '1'),
(12, '111', 81, 'tech3 tech5', 'ndfasda', 'SADASDSA', '11/11/1996', 'Male', 'dfgasdasdas', 'dsadsasd', '31231243342', 'arts', 'Senior Teacher', 'Full time', 'asdd,sadsad,asdasd,1222', 'sadsad,sadsad,asdasd,2212', 'asdsd,asdsdsad,asdasdasd,2211', '', '', '05c421ec66017e03c4af774c3e982cfe.jpg', 'submited', 'submited', 'submited', '3321', '2');

-- --------------------------------------------------------

--
-- Table structure for table `teacher_attendance`
--

CREATE TABLE IF NOT EXISTS `teacher_attendance` (
  `id` int(11) NOT NULL,
  `parient_id` int(11) DEFAULT NULL,
  `year` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  `employ_id` int(11) NOT NULL,
  `employ_title` varchar(150) NOT NULL,
  `present_or_absent` text NOT NULL,
  `status` int(1) NOT NULL,
  `attend_time` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teacher_attendance`
--

INSERT INTO `teacher_attendance` (`id`, `parient_id`, `year`, `month`, `date`, `employ_id`, `employ_title`, `present_or_absent`, `status`, `attend_time`) VALUES
(1, NULL, 2016, 6, 1464991200, 1, 'Headmaster', '1', 0, '02:33 pm'),
(2, NULL, 2016, 6, 1464991200, 2, 'Helen K Helton', '1', 0, '02:34 pm'),
(3, NULL, 2016, 6, 1464991200, 6, 'Robert D. Franco', '1', 0, '02:34 pm'),
(4, NULL, 2016, 6, 1464991200, 7, 'Michael R. Kemp', '1', 0, '02:34 pm'),
(5, NULL, 2016, 6, 1464991200, 8, 'Willie B. Quint', '0', 0, ''),
(6, NULL, 2016, 6, 1464991200, 9, 'Fredrick V. Keyes', '1', 0, '02:34 pm'),
(7, NULL, 2016, 6, 1464991200, 10, 'mumar abboud', '0', 0, ''),
(8, NULL, 2016, 6, 1464991200, 11, 'Inayah Asfour', '1', 0, '02:34 pm'),
(9, NULL, 2016, 6, 1466287200, 1, 'Headmaster', '1', 0, '11:46 pm'),
(10, NULL, 2016, 6, 1466287200, 2, 'Helen K Helton', '0', 0, ''),
(11, NULL, 2016, 6, 1466287200, 6, 'Robert D. Franco', '1', 0, '11:47 pm'),
(12, NULL, 2016, 6, 1466287200, 7, 'Michael R. Kemp', '1', 0, '11:47 pm'),
(13, NULL, 2016, 6, 1466287200, 8, 'Willie B. Quint', '0', 0, ''),
(14, NULL, 2016, 6, 1466287200, 9, 'Fredrick V. Keyes', '1', 0, '11:47 pm'),
(15, NULL, 2016, 6, 1466287200, 10, 'mumar abboud', '0', 0, ''),
(16, NULL, 2016, 6, 1466287200, 11, 'Inayah Asfour', '1', 0, '11:47 pm'),
(17, NULL, 2016, 9, 1472767200, 1, 'Headmaster', '1', 0, ''),
(18, NULL, 2016, 9, 1472767200, 2, 'Helen K Helton', '0', 0, ''),
(19, NULL, 2016, 9, 1472767200, 6, 'Robert D. Franco', '0', 0, ''),
(20, NULL, 2016, 9, 1472767200, 7, 'Michael R. Kemp', '0', 0, ''),
(21, NULL, 2016, 9, 1472767200, 8, 'Willie B. Quint', '0', 0, ''),
(22, NULL, 2016, 9, 1472767200, 9, 'Fredrick V. Keyes', '0', 0, ''),
(23, NULL, 2016, 9, 1472767200, 10, 'mumar abboud', '0', 0, ''),
(24, NULL, 2016, 9, 1472767200, 11, 'Inayah Asfour', '0', 0, ''),
(35, 80, 2017, 0, 1511046000, 82, 'sohail ahmad1', 'Present', 0, '06:37 pm'),
(36, 80, 2017, 0, 1511046000, 83, 'sadas Ahmad1', 'Present', 0, '06:37 pm'),
(37, 80, 2017, 0, 1511046000, 85, 'test user', 'Present', 0, '06:37 pm'),
(38, 80, 2017, 0, 1511046000, 102, 'emplyee11 1', 'Present', 0, '06:38 pm'),
(39, 80, 2017, 0, 1511132400, 82, 'sohail ahmad1', 'Absent', 0, ''),
(40, 80, 2017, 0, 1511132400, 83, 'sadas Ahmad1', 'Absent', 0, ''),
(41, 80, 2017, 0, 1511132400, 85, 'test user', 'Absent', 0, ''),
(42, 80, 2017, 0, 1511132400, 102, 'emplyee11 1', 'Absent', 0, ''),
(51, 81, 2017, 11, 1511823600, 107, 'asad amjad', 'Present', 1, '08:49 am'),
(52, 81, 2017, 11, 1511823600, 111, 'tech3 tech5', 'Absent', 1, '08:49 am'),
(53, 81, 2017, 11, 1511823600, 112, 'usman ahmad', 'Present', 1, '08:49 am'),
(54, 81, 2017, 11, 1511910000, 107, 'asad amjad', 'Present', 1, '06:55 am'),
(55, 81, 2017, 11, 1511910000, 111, 'tech3 tech5', 'Present', 1, '06:55 am'),
(56, 81, 2017, 11, 1511910000, 113, 'asif bilal', 'Absent', 1, '06:55 am'),
(60, 81, 2017, 12, 1512514800, 107, 'asad amjad', 'Present', 1, '03:20 pm'),
(61, 81, 2017, 12, 1512514800, 111, 'tech3 tech5', 'Present', 1, '03:20 pm'),
(62, 81, 2017, 12, 1512514800, 113, 'asif bilal', 'Absent', 1, '03:20 pm'),
(63, 81, 2017, 12, 1512601200, 107, 'asad amjad', 'Present', 1, '07:16 am'),
(64, 81, 2017, 12, 1512601200, 111, 'tech3 tech5', 'Absent', 1, '07:16 am'),
(65, 81, 2017, 12, 1512601200, 113, 'asif bilal', 'Present', 1, '07:16 am');

-- --------------------------------------------------------

--
-- Table structure for table `transection`
--

CREATE TABLE IF NOT EXISTS `transection` (
  `id` int(11) NOT NULL,
  `parient_id` int(11) DEFAULT NULL,
  `date` int(11) NOT NULL,
  `acco_id` int(11) NOT NULL,
  `category` varchar(10) NOT NULL,
  `type` varchar(30) DEFAULT NULL,
  `amount` int(11) NOT NULL,
  `balance` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transection`
--

INSERT INTO `transection` (`id`, `parient_id`, `date`, `acco_id`, `category`, `type`, `amount`, `balance`) VALUES
(1, 80, 1511132400, 6, 'Income', NULL, 6000, 5600),
(2, 80, 1511132400, 4, 'Expense', NULL, 100, 900),
(3, 80, 1511132400, 7, 'Expense', NULL, 500, 400),
(4, 80, 1511132400, 8, 'Income', NULL, 200, 600),
(5, NULL, 1511132400, 2, 'Expense', NULL, 0, 6200),
(6, 80, 1511305200, 4, 'Expense', NULL, 800, 6100),
(7, 80, 1511305200, 8, 'Income', NULL, 1500, 7700),
(8, 80, 1511305200, 7, 'Expense', NULL, 500, 7200),
(21, 81, 1512687600, 10, 'Income', 'Fee', 3000, 3000),
(22, 81, 1512687600, 10, 'Income', 'Fee', 3500, 6500),
(23, 81, 1512687600, 10, 'Income', 'Fee', 3000, 9500),
(24, 81, 1512687600, 9, 'Expense', 'bill', 2000, 7500),
(25, 81, 1512687600, 11, 'Expense', 'Salary', 5000, 2500),
(26, 81, 1512687600, 10, 'Income', 'Fee', 10000, 12500),
(27, 81, 1512687600, 9, 'Expense', 'bill', 5000, 7500);

-- --------------------------------------------------------

--
-- Table structure for table `transport`
--

CREATE TABLE IF NOT EXISTS `transport` (
  `id` int(11) NOT NULL,
  `rout_title` varchar(200) NOT NULL,
  `start_end` varchar(300) NOT NULL,
  `vicles_amount` varchar(20) NOT NULL,
  `descriptions` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `userinfo`
--

CREATE TABLE IF NOT EXISTS `userinfo` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `parient_id` int(11) DEFAULT NULL,
  `full_name` varchar(50) NOT NULL,
  `farther_name` varchar(50) NOT NULL,
  `mother_name` varchar(50) NOT NULL,
  `birth_date` varchar(15) NOT NULL,
  `sex` varchar(10) NOT NULL,
  `present_address` varchar(200) NOT NULL,
  `permanent_address` varchar(200) NOT NULL,
  `working_hour` varchar(30) NOT NULL,
  `educational_qualification_1` varchar(200) NOT NULL,
  `educational_qualification_2` varchar(200) NOT NULL,
  `educational_qualification_3` varchar(200) NOT NULL,
  `educational_qualification_4` varchar(200) NOT NULL,
  `educational_qualification_5` varchar(200) NOT NULL,
  `users_photo` varchar(200) NOT NULL,
  `cv` varchar(30) NOT NULL,
  `educational_certificat` varchar(30) NOT NULL,
  `exprieance_certificatte` varchar(30) NOT NULL,
  `files_info` varchar(30) NOT NULL,
  `phone` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userinfo`
--

INSERT INTO `userinfo` (`id`, `user_id`, `group_id`, `parient_id`, `full_name`, `farther_name`, `mother_name`, `birth_date`, `sex`, `present_address`, `permanent_address`, `working_hour`, `educational_qualification_1`, `educational_qualification_2`, `educational_qualification_3`, `educational_qualification_4`, `educational_qualification_5`, `users_photo`, `cv`, `educational_certificat`, `exprieance_certificatte`, `files_info`, `phone`) VALUES
(11, 80, 1, 1, 'sohail ahmad', 'lahore', '', '', '', 'lahore', '', '', '', '', '', '', '', '9e0d5a9034297f77e0728f894c163a16.jpg', '', '', '', '', '+92333333'),
(12, 81, 1, 1, 'junaid ahmad', 'asd', '', '', '', 'dssa', '', '', '', '', '', '', '', 'a1863596650377a49ae0345c3f6bbc01.jpg', '', '', '', '', '+95232112'),
(13, 86, 1, 1, 'sdfsdf dsfsdf', 'dsfdf', '', '', '', 'sdfdsf', '', '', '', '', '', '', '', '810605782c9c91c53797813a4a2aae0e.png', '', '', '', '', 'fdsfdsf'),
(14, 101, 6, NULL, 'sohail ahmad', 'ahmad', 'ahmad', '12/12/1999', 'Male', 'ahmad', 'ahmad', 'Part time', 'ahmad,ahmad,ahmad,1999', '', '', '', '', 'b9a686fbf2675c75c5a2df85e961bd17.png', '', '', '', '23', '22222222222'),
(15, 102, 6, 80, 'emplyee11 1', 'emplyee', 'emplyee', '12/12/1999', 'Male', 'emplyee', 'emplyee', 'Part time', 'emplyee,emplyee,emplyee,1999', '', '', '', '', 'ff08ba5243e52361001e36b39638399d.jpg', '', '', '', '23', '22222222222'),
(17, 113, 7, 81, 'asif bilal', 'fath4', 'mother', '11/11/1994', 'Male', 'asddw', 'asdsd', 'Full time', 'asdsa,dasdasd,asdasd,2131', '', '', '', '', 'f357ece6467c1281f8196d1f2c18d8d2.png', 'submited', 'submited', 'submited', 'lib\\_1', '12321232132'),
(18, 114, 1, 1, 'Mister Admin', 'Sadar cantt', '', '', '', 'near sadar round about', '', '', '', '', '', '', '', '7e5ac1ffa524e91dfe1187b42f1d55f3.png', '', '', '', '', '03334623434'),
(19, 115, 6, 81, 'haji usman', 'fahterrrrr', 'motherrrrrrr', '11/11/1980', 'Male', 'lahore', 'lahore', 'Full time', 'asdasd,asdasdsd,sadsdasd,1990', '', '', '', '', 'd02f41d9734cf78785b16b011d5b356e.png', 'submited', 'submited', 'submited', 'accounts\\_1', '03001234567');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL,
  `is_super_user` tinyint(1) NOT NULL DEFAULT '0',
  `parient_id` int(11) DEFAULT NULL,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `profile_image` varchar(100) NOT NULL,
  `user_status` text NOT NULL,
  `leave_status` varchar(15) NOT NULL,
  `leave_start` int(11) NOT NULL,
  `leave_end` int(11) NOT NULL,
  `salary` int(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=116 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `is_super_user`, `parient_id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `phone`, `profile_image`, `user_status`, `leave_status`, `leave_start`, `leave_end`, `salary`) VALUES
(1, 1, 0, '127.0.0.1', 'Headmaster', '$2y$08$qBQ/MzJzXyil0yuVM.s1XewJerIMCntwxez/Jfs3x/xwxFnkKWo2y', NULL, 'admin@admin.com', NULL, 'HBj4C30st5pOHbjpHojzGu4667ad49e75655b131', 1420113369, 'IcD7gVAwU5DDX4jTuWOVXe', 1268889823, 1512652187, 1, 'Kermit J.', 'Jackson', '123456789', 'admin.png', 'Employee', 'Leave', 1486252800, 1486339200, 1),
(80, 0, 1, '::1', 'sohail ahmad', '$2y$08$yS3HQq76MOoBfXwNnvf.1.oDxaAulDlYGAfIqKpbGEkuVgmPqoWEi', NULL, 'sohail9689@gmail.com', NULL, NULL, NULL, NULL, 1510731505, 1512388975, 1, 'sohail', 'ahmad', '+92333333', '9e0d5a9034297f77e0728f894c163a16.jpg', 'Employee', 'Available', 0, 0, 0),
(81, 0, 1, '::1', 'syed junaid', '$2y$08$U5Zzhd6N9eiRiEJT04HfTui/.4k.u8fbGLCKwpPmQalbuRaAgJQZ.', NULL, 'junaid@gmail.com', NULL, NULL, NULL, NULL, 1510731621, 1512985747, 1, 'Sayyed', 'Junaid', '03244212412', '781dce182750adfda2f5810cc3d8c7f6.JPG', 'Employee', 'Available', 0, 0, 0),
(82, 0, 80, '::1', 'sohail ahmad1', '$2y$08$FO4ViS1pvbKf3BIjLHswTuq3O8JT7MuL1HAiKsAs.dr4/cYL1Vkom', NULL, 'sidhu@sidhu.com', NULL, NULL, NULL, NULL, 1510813001, 1510813001, 1, 'Sohail', 'Ahmad1', '03000333968', '44d05827bba3b0664723e2248ed48dc3.jpg', 'Employee', 'Available', 0, 0, 0),
(83, 0, 80, '::1', 'sadas Ahmad1', '$2y$08$br3smJCdwD6aVyWOHN3ZgOWY/lWD29YE35yVeRSb7JaRIqUSO6pzO', NULL, 'sameh21@gmail.com', NULL, NULL, NULL, NULL, 1510819513, 1510819513, 1, 'sadas', 'Ahmad1', '03001234567', '87d4f36af5cba13dfe357bdfa97858b2.png', 'Employee', 'Available', 0, 0, 0),
(85, 0, 80, '::1', 'test user', '$2y$08$DAy60nJusYOsOtKKVeSpIOi2cYQQnTaSwz9SFnhcfRXh20anXcN1C', NULL, 'aas@aass.com', NULL, NULL, NULL, NULL, 1510820342, 1510820342, 1, 'test', 'user', '03001234562', 'b09d97f6433482156c421937dd3af20d.png', 'Employee', 'Available', 0, 0, 0),
(86, 0, 1, '::1', 'sdfsdf dsfsdf', '$2y$08$DsPC9z3JsRyUE6S9fxusQei/3OkjJYUdiGvsO3gM0LXkWIbX/HWji', NULL, 'cybextech@cybextech.com', NULL, NULL, NULL, NULL, 1510820607, 1510820607, 1, 'sdfsdf', 'dsfsdf', 'fdsfdsf', '810605782c9c91c53797813a4a2aae0e.png', 'Employee', 'Available', 0, 0, 0),
(87, 0, 80, '::1', 'dsda dsad', '$2y$08$jvzsM6saf7GFBz/ivIUhG.p2N6iGjo5juBIHoRzimZRsPzj8otf06', NULL, 'dev@togetherpro.com', NULL, NULL, NULL, NULL, 1510842231, 1510842231, 1, 'dsda', 'dsad', '33333333333', 'bcbc53ca45f8d67806c9e8fc6c35094d.png', '', '', 0, 0, 0),
(90, 0, 80, '::1', 'student3 student3', '$2y$08$JvJDV9VkDkXSDI3zqwGZsuJ5qFiP3K1ZsKAg.uOENF6DXbdUNluQ.', NULL, 'dev2@togetherpro.com', NULL, NULL, NULL, NULL, 1510911043, 1510911043, 1, 'student3', 'student3', '99999999999', 'cb2954a3e9c36a586591b56bf6004f47.png', '', '', 0, 0, 0),
(91, 0, 80, '::1', 'student4 student4', '$2y$08$Kd43WpsdnAFiqPyxlxREcOXpF3MJ4EmWk.FORqdWEm8LiMBjDiwo6', NULL, 'student4@gmail.com', NULL, NULL, NULL, NULL, 1510914396, 1510914396, 1, 'student4', 'student4', '99999999999', 'ba5e72a4e57b17660d3bd89bad8f0586.png', '', '', 0, 0, 0),
(96, 0, 80, '::1', 'sadsa1 sadas1', '$2y$08$qp3U1lwmBGazyIHmt09BTusC56gY7Re7/laahzijfcdVmU7eXoGe.', NULL, 'dsada@gmail.com1', NULL, NULL, NULL, NULL, 1510927440, 1510927440, 1, 'sadsa1', 'sadas1', '333333333331', '1c549e24161bc1367cde7b98647468c5.png', '', '', 0, 0, 0),
(97, 0, 80, '::1', 'parient3 parient3', '$2y$08$X9OTBktQpvMokFNeR4MfTO0cz8cjEoyZv2kiAS5ab2XQiwAYqyc.u', NULL, 'parient3@gmail.com', NULL, NULL, NULL, NULL, 1511019127, 1511019127, 1, 'parient3', 'parient3', '33333333333', '9e0bf68b382946706f2abc5b25e23779.jpg', '', '', 0, 0, 0),
(98, 0, 80, '::1', 'parent4 parent4', '$2y$08$MZegg46n9i75f65mIee4Vesaaw8yMLPcQty/AuY0q2ttJc/o7Qi1K', NULL, 'parent4@gmail.com', NULL, NULL, NULL, NULL, 1511019267, 1511019267, 1, 'parent4', 'parent4', '33333333333', '6596679e47c5f30b80f10950d4072055.png', '', '', 0, 0, 0),
(99, 0, 80, '::1', 'studen01 studen01', '$2y$08$xu62z6FFdyawqsmKCLdpA.5T6L7/uu//0OHpoGtougQ8DdQZBmDxC', NULL, 'studen01@gmail.com', NULL, NULL, NULL, NULL, 1511020197, 1511020197, 1, 'studen01', 'studen01', '33333333333', '9ffb50c9d068f887ef73f24a56dfc03d.png', '', '', 0, 0, 0),
(100, 0, 80, '::1', 'studen02 studen02', '$2y$08$bs/cXAd3t3hK9xtWpxBeR.FKAkIqjpFUe2wxEqto316Sc5fNYdhdS', NULL, 'studen02@gmail.com', NULL, NULL, NULL, NULL, 1511020266, 1511020266, 1, 'studen02', 'studen02', '33334444444', 'e7e494400c07caed6435448517396b93.png', '', '', 0, 0, 0),
(101, 0, NULL, '::1', 'sohail ahmad2', '$2y$08$kihVKt/Ah/y419kadXwm0eovt9VweYUf8faE4a6P/07.zOKdEYAja', NULL, 'emplyee@gmail.com', NULL, NULL, NULL, NULL, 1511090224, 1511090224, 1, 'sohail', 'ahmad', '22222222222', 'b9a686fbf2675c75c5a2df85e961bd17.png', 'Employee', 'Available', 0, 0, 0),
(102, 0, 80, '::1', 'emplyee11 1', '$2y$08$26XXU9sU4TXEBC8Zv.2l..i/34samDmNrq9PbMFL0m9l.4PlJz18i', NULL, 'emplyee@emplyee.com', NULL, NULL, NULL, NULL, 1511090476, 1511090734, 1, 'emplyee11', '1', '22222222222', 'ff08ba5243e52361001e36b39638399d.jpg', 'Employee', 'Available', 0, 0, 0),
(104, 0, 81, '::1', 'syed junaid', '$2y$08$NKCumGZNC7VAE.j3Uhj/E.75ryGAPd/SYVirnZPryMQN.wcYhFK/O', NULL, 'syed@syed.com', NULL, NULL, NULL, 'reSh.YUjyoaeikIPkJRLNe', 1511335840, 1512985530, 1, 'syed', 'junaid', '12311231232', '56e1e1d93b24f16ab3ea7ce1e3ae3db0.jpg', '', '', 0, 0, 0),
(105, 0, 81, '::1', 'ahmad asdf', '$2y$08$vNeRHhyM0AjCq5qsfp3.kOdAfge3zjj3wO44G5WaTHxGj.93myyj.', NULL, 'asd@asd.com', NULL, NULL, NULL, NULL, 1511336063, 1511523513, 1, 'ahmad', 'asdf', '12322332431', '0cf459ef26cdadb4f2624787f211d717.PNG', '', '', 0, 0, 0),
(106, 0, 81, '::1', 'Javaid Hussain', '$2y$08$Bb4L6MqhoyKtad6XR8AyS.x0M2.tR7fGSpIzyl40kzmhXQUOjwpcC', NULL, 'father@father.com', NULL, NULL, NULL, NULL, 1511336745, 1512985615, 1, 'Javaid', 'Hussain', '21322434312', '7aa1ad0c4e22c30ed6b6ad25cb4cb6e5.jpg', '', '', 0, 0, 0),
(107, 0, 81, '::1', 'asad amjad', '$2y$08$u9PjVCG7Ts3j8Nx3xk74V.qak5OaJV8Q66jlrwf0fg3w2QGvLQh6e', NULL, 'teacher@teacher.com', NULL, NULL, NULL, NULL, 1511336950, 1512985578, 1, 'asad', 'amjad', '31231232313', 'f1e9e21f6acde2e7a5b461add62e13cb.jpg', 'Employee', 'Available', 0, 0, 1),
(108, 0, 81, '::1', 'dum1 dumy', '$2y$08$doMvnSnOoS2ugb6uSE9mN.VcdibuBaJPVC9oekGK0TLv1uNeWA1Wa', NULL, 'd1@d1.com', NULL, NULL, NULL, NULL, 1511430535, 1511432917, 1, 'dum1', 'dumy', '23123213232', '6204c50a5d57341f498b7bc6658bd01e.jpg', '', '', 0, 0, 0),
(109, 0, 81, '::1', 'abug abu1', '$2y$08$Fgal12Md15hgnqTGErQIcOz/bdwQfVc3HCy.6tutb5MioMaFbZFye', NULL, 'f1@f1.com', NULL, NULL, NULL, NULL, 1511430621, 1511430621, 1, 'abug', 'abu1', '32133421321', '647bd7365d3daa3658cbd5e1e5963bb6.jpg', '', '', 0, 0, 0),
(110, 0, 81, '::1', 'dddssdasd asdwqesdsa', '$2y$08$8bqDtL67fmh6gOb0KskAg.kmAB0JNxSC12wp5Zot3vxM6Sap/Z8si', NULL, 'f2@f2.com', NULL, NULL, NULL, NULL, 1511430735, 1511430735, 1, 'dddssdasd', 'asdwqesdsa', '31235533123', 'dbf2c56ec7d26aa11a3d8eceed8ae95d.jpg', '', '', 0, 0, 0),
(111, 0, 81, '::1', 'tech3 tech5', '$2y$08$tfxV1asI.mJxWKaXxvoAGOA3ETv19ckD3rcGDbc5rM308ClllOOf6', NULL, 't2@t2.com', NULL, NULL, NULL, NULL, 1511430836, 1511430836, 1, 'tech3', 'tech5', '31231243342', '05c421ec66017e03c4af774c3e982cfe.jpg', 'Employee', 'Available', 0, 0, 1),
(113, 0, 81, '::1', 'asif bilal', '$2y$08$i7LhRGlENrqjGJUqEFMhp.qGd71aoGE2DWOsCckK/in/1ABaVmJGq', NULL, 'library@library.com', NULL, NULL, NULL, NULL, 1511863066, 1512979324, 1, 'asif', 'bilal', '12321232132', 'f357ece6467c1281f8196d1f2c18d8d2.png', 'Employee', 'Leave', 1512082800, 1512169200, 1),
(114, 0, 1, '::1', 'Mister Admin', '$2y$08$50eUQCCRRUqncckfezekPO6bf.6PVSpVnbfA7wSzQM34PMcvdG8T6', NULL, 'admin1@admin1.com', NULL, NULL, NULL, NULL, 1512628508, 1512628526, 1, 'Mister', 'Admin', '03334623434', '7e5ac1ffa524e91dfe1187b42f1d55f3.png', 'Employee', 'Available', 0, 0, 0),
(115, 0, 81, '::1', 'haji usman', '$2y$08$pp2YA1rGFVt5sWYw88cXseObsfA.MyBEqITXas2k6NFGboIsVW0US', NULL, 'accounts@accounts.com', NULL, NULL, NULL, NULL, 1512978874, 1512979703, 1, 'haji', 'usman', '03001234567', 'd02f41d9734cf78785b16b011d5b356e.png', 'Employee', 'Available', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE IF NOT EXISTS `users_groups` (
  `id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=116 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(80, 80, 1),
(81, 81, 1),
(82, 82, 4),
(83, 83, 4),
(85, 85, 4),
(86, 86, 1),
(87, 87, 3),
(90, 90, 3),
(91, 91, 3),
(96, 96, 5),
(97, 97, 5),
(98, 98, 5),
(99, 99, 3),
(100, 100, 3),
(101, 101, 6),
(102, 102, 6),
(104, 104, 3),
(105, 105, 3),
(106, 106, 5),
(107, 107, 4),
(108, 108, 3),
(109, 109, 5),
(110, 110, 5),
(111, 111, 4),
(113, 113, 7),
(114, 114, 1),
(115, 115, 6);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle`
--

CREATE TABLE IF NOT EXISTS `vehicle` (
  `id` int(5) NOT NULL,
  `parient_id` int(11) DEFAULT NULL,
  `vehicle_no` varchar(10) NOT NULL,
  `vehicle_type` varchar(20) NOT NULL,
  `total_seats` int(3) NOT NULL,
  `fill_seats` int(3) NOT NULL,
  `vehicle_info` varchar(100) NOT NULL,
  `status` int(1) NOT NULL,
  `on_route` int(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle`
--

INSERT INTO `vehicle` (`id`, `parient_id`, `vehicle_no`, `vehicle_type`, `total_seats`, `fill_seats`, `vehicle_info`, `status`, `on_route`) VALUES
(1, 81, 'lec5423', 'bus', 50, 0, '  sadasdasd', 0, 0),
(2, 81, 'LEV3215', 'pickup', 20, 0, 'a pick up', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

CREATE TABLE IF NOT EXISTS `vendors` (
  `id` int(11) NOT NULL,
  `parient_id` int(11) DEFAULT NULL,
  `company_name` varchar(150) NOT NULL,
  `company_phone` varchar(15) NOT NULL,
  `company_email` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `cp_name` varchar(150) NOT NULL,
  `cp_address` varchar(200) NOT NULL,
  `cp_phone` varchar(15) NOT NULL,
  `bank_name` varchar(150) NOT NULL,
  `branch_name` varchar(15) NOT NULL,
  `account_no` varchar(30) NOT NULL,
  `ifsc_code` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendors`
--

INSERT INTO `vendors` (`id`, `parient_id`, `company_name`, `company_phone`, `company_email`, `country`, `state`, `city`, `cp_name`, `cp_address`, `cp_phone`, `bank_name`, `branch_name`, `account_no`, `ifsc_code`) VALUES
(5, 81, 'jbFurnitures1', '22222441223', 'v2@v1.com', 'Pak', 'punj', 'lhr', 'jun', 'lah', '11111111111', 'HBL', 'Walton', '24124123123', '222');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account_title`
--
ALTER TABLE `account_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `add_exam`
--
ALTER TABLE `add_exam`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `books_category`
--
ALTER TABLE `books_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `calender_events`
--
ALTER TABLE `calender_events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `class`
--
ALTER TABLE `class`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `class_routine`
--
ALTER TABLE `class_routine`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `class_students`
--
ALTER TABLE `class_students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `class_subject`
--
ALTER TABLE `class_subject`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `configuration`
--
ALTER TABLE `configuration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `config_week_day`
--
ALTER TABLE `config_week_day`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `daily_attendance`
--
ALTER TABLE `daily_attendance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dormitory`
--
ALTER TABLE `dormitory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dormitory_bed`
--
ALTER TABLE `dormitory_bed`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dormitory_room`
--
ALTER TABLE `dormitory_room`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dormitory_visitors`
--
ALTER TABLE `dormitory_visitors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `drivers`
--
ALTER TABLE `drivers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employe`
--
ALTER TABLE `employe`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_attendanc`
--
ALTER TABLE `exam_attendanc`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_grade`
--
ALTER TABLE `exam_grade`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_routine`
--
ALTER TABLE `exam_routine`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fee_item`
--
ALTER TABLE `fee_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `final_result`
--
ALTER TABLE `final_result`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inven_category`
--
ALTER TABLE `inven_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inve_item`
--
ALTER TABLE `inve_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `issu_item`
--
ALTER TABLE `issu_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leave_application`
--
ALTER TABLE `leave_application`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `library_member`
--
ALTER TABLE `library_member`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `massage`
--
ALTER TABLE `massage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notice_board`
--
ALTER TABLE `notice_board`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parents_info`
--
ALTER TABLE `parents_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `result_action`
--
ALTER TABLE `result_action`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `result_shit`
--
ALTER TABLE `result_shit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `result_submition_info`
--
ALTER TABLE `result_submition_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_based_access`
--
ALTER TABLE `role_based_access`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `routes`
--
ALTER TABLE `routes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `salary`
--
ALTER TABLE `salary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `set_salary`
--
ALTER TABLE `set_salary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slip`
--
ALTER TABLE `slip`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_info`
--
ALTER TABLE `student_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subjects_mark`
--
ALTER TABLE `subjects_mark`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suggestion`
--
ALTER TABLE `suggestion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teachers_info`
--
ALTER TABLE `teachers_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher_attendance`
--
ALTER TABLE `teacher_attendance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transection`
--
ALTER TABLE `transection`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transport`
--
ALTER TABLE `transport`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userinfo`
--
ALTER TABLE `userinfo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`), ADD KEY `fk_users_groups_users1_idx` (`user_id`), ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- Indexes for table `vehicle`
--
ALTER TABLE `vehicle`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendors`
--
ALTER TABLE `vendors`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account_title`
--
ALTER TABLE `account_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `add_exam`
--
ALTER TABLE `add_exam`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `books_category`
--
ALTER TABLE `books_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `calender_events`
--
ALTER TABLE `calender_events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `class`
--
ALTER TABLE `class`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `class_routine`
--
ALTER TABLE `class_routine`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `class_students`
--
ALTER TABLE `class_students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `class_subject`
--
ALTER TABLE `class_subject`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=123;
--
-- AUTO_INCREMENT for table `configuration`
--
ALTER TABLE `configuration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `config_week_day`
--
ALTER TABLE `config_week_day`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `daily_attendance`
--
ALTER TABLE `daily_attendance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `dormitory`
--
ALTER TABLE `dormitory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `dormitory_bed`
--
ALTER TABLE `dormitory_bed`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=106;
--
-- AUTO_INCREMENT for table `dormitory_room`
--
ALTER TABLE `dormitory_room`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=476;
--
-- AUTO_INCREMENT for table `dormitory_visitors`
--
ALTER TABLE `dormitory_visitors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `drivers`
--
ALTER TABLE `drivers`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `employe`
--
ALTER TABLE `employe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `exam_attendanc`
--
ALTER TABLE `exam_attendanc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `exam_grade`
--
ALTER TABLE `exam_grade`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `exam_routine`
--
ALTER TABLE `exam_routine`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `fee_item`
--
ALTER TABLE `fee_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `final_result`
--
ALTER TABLE `final_result`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `inven_category`
--
ALTER TABLE `inven_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `inve_item`
--
ALTER TABLE `inve_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `issu_item`
--
ALTER TABLE `issu_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `leave_application`
--
ALTER TABLE `leave_application`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `library_member`
--
ALTER TABLE `library_member`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `massage`
--
ALTER TABLE `massage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `notice_board`
--
ALTER TABLE `notice_board`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `parents_info`
--
ALTER TABLE `parents_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `result_action`
--
ALTER TABLE `result_action`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `result_shit`
--
ALTER TABLE `result_shit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `result_submition_info`
--
ALTER TABLE `result_submition_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `role_based_access`
--
ALTER TABLE `role_based_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=112;
--
-- AUTO_INCREMENT for table `routes`
--
ALTER TABLE `routes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `salary`
--
ALTER TABLE `salary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `set_salary`
--
ALTER TABLE `set_salary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `slip`
--
ALTER TABLE `slip`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `student_info`
--
ALTER TABLE `student_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `subjects_mark`
--
ALTER TABLE `subjects_mark`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `suggestion`
--
ALTER TABLE `suggestion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `teachers_info`
--
ALTER TABLE `teachers_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `teacher_attendance`
--
ALTER TABLE `teacher_attendance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `transection`
--
ALTER TABLE `transection`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `transport`
--
ALTER TABLE `transport`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `userinfo`
--
ALTER TABLE `userinfo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=116;
--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=116;
--
-- AUTO_INCREMENT for table `vehicle`
--
ALTER TABLE `vehicle`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `vendors`
--
ALTER TABLE `vendors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
