-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 14, 2017 at 03:13 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sms`
--

-- --------------------------------------------------------

--
-- Table structure for table `account_title`
--

CREATE TABLE `account_title` (
  `id` int(11) NOT NULL,
  `account_title` varchar(100) NOT NULL,
  `category` varchar(20) NOT NULL,
  `description` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account_title`
--

INSERT INTO `account_title` (`id`, `account_title`, `category`, `description`) VALUES
(1, 'Rent', 'Expense', ''),
(2, 'Student Fee', 'Income', ''),
(3, 'Teacher Salary', 'Expense', '');

-- --------------------------------------------------------

--
-- Table structure for table `add_exam`
--

CREATE TABLE `add_exam` (
  `id` int(11) NOT NULL,
  `year` int(5) NOT NULL,
  `exam_title` varchar(100) NOT NULL,
  `start_date` varchar(30) NOT NULL,
  `class_id` int(11) NOT NULL,
  `total_time` varchar(10) NOT NULL,
  `publish` varchar(50) NOT NULL,
  `final` text NOT NULL,
  `status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `add_exam`
--

INSERT INTO `add_exam` (`id`, `year`, `exam_title`, `start_date`, `class_id`, `total_time`, `publish`, `final`, `status`) VALUES
(4, 2016, 'Test Exam 1', '09/06/2016', 1, '1 Hour 30 ', 'Publish', 'NoFinal', 'NoResult'),
(5, 2016, 'FInal Exam', '08/10/2016', 3, '30 Minute', 'Not Publish', 'Final', 'NoResult'),
(6, 2017, 'kACHAY pAPER', '10/03/2017', 11, '1 Hour 30 ', 'Not Publish', 'NoFinal', 'NoResult');

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  `isbn_no` varchar(20) NOT NULL,
  `book_no` int(11) NOT NULL,
  `books_title` varchar(100) NOT NULL,
  `authore` varchar(150) NOT NULL,
  `category` varchar(100) NOT NULL,
  `edition` varchar(100) NOT NULL,
  `pages` int(11) NOT NULL,
  `language` varchar(30) NOT NULL,
  `uploderTitle` varchar(100) NOT NULL,
  `books_amount` int(11) NOT NULL,
  `status` varchar(50) NOT NULL,
  `issu_date` int(11) NOT NULL,
  `due_date` int(11) NOT NULL,
  `issu_member_no` int(11) NOT NULL,
  `cover_photo` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `date`, `isbn_no`, `book_no`, `books_title`, `authore`, `category`, `edition`, `pages`, `language`, `uploderTitle`, `books_amount`, `status`, `issu_date`, `due_date`, `issu_member_no`, `cover_photo`) VALUES
(1, 1464559200, '978-3-16-148410-0', 20161, 'Book Test 1', 'Author Test 1', 'English', '16 th', 1500, 'English', 'Headmaster', 1, 'Not Available', 1475245412, 1475186400, 1, 'f7cebda75f3c1c09ef31e2e525db8805.gif'),
(2, 1464559200, '978-3-16-148410-1', 20162, 'Book Test 2', 'Author Test 2', 'Bangla ', '16 th', 1500, 'Bangla', 'Headmaster', 2, 'Available', 0, 0, 0, 'eee468e20c7387d459487dafda58709d.gif'),
(3, 1464559200, '978-3-16-148410-2', 20163, 'Book Test 3', 'Author Test 3', 'English', '16 th', 522, 'English', 'Headmaster', 3, 'Available', 0, 0, 0, '6583655141ea7c33d3981bbd7332c586.gif'),
(4, 1464559200, '978-3-16-148410-3', 20164, 'Book Test 4', 'Author Test 4', 'Bangla ', '16 th', 520, 'Bangla', 'Headmaster', 4, 'Available', 0, 0, 0, 'ceabf75a2a7dead8862b7821d3fd3bd4.gif'),
(5, 1464559200, '978-3-16-148410-4', 20165, 'Book Test 5', 'Author Test 5', 'Bangla ', '14 th', 711, 'Bangla', 'Headmaster', 5, 'Available', 0, 0, 0, '9d4b20f013a24ebc882c84b0b344e526.gif'),
(6, 1464559200, '978-3-16-148410-5', 20166, 'Book Test 6', 'Author Test 6', 'English', '10 th', 485, 'English', 'Headmaster', 6, 'Available', 0, 0, 0, '1d28dd98b150b33ad4a708a95031408a.gif'),
(7, 1464559200, '978-3-16-148410-6', 20167, 'Book Test 7', 'Author Test 77', 'English', '9 th', 485, 'English', 'Headmaster', 7, 'Available', 0, 0, 0, '036337321c644b23bd7916f3e4b3eca4.gif'),
(8, 1464559200, '978-3-16-148410-7', 20168, 'Book Test 8', 'Author Test 8', 'English', '14 th', 950, 'English', 'Headmaster', 8, 'Available', 0, 0, 0, '3b174671512c28eaf95be33657e2af69.gif'),
(9, 1496102400, 'asfs-1231', 20179, 'C++', 'Suhail', 'Programming', '1234', 10, 'English', 'Headmaster', 9, 'Available', 0, 0, 0, '1f375fb56eada0f7786b55ae9681bb18.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `books_category`
--

CREATE TABLE `books_category` (
  `id` int(11) NOT NULL,
  `category_creator` varchar(100) NOT NULL,
  `category_title` varchar(100) NOT NULL,
  `parent_category` varchar(100) NOT NULL,
  `description` varchar(500) NOT NULL,
  `books_amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `books_category`
--

INSERT INTO `books_category` (`id`, `category_creator`, `category_title`, `parent_category`, `description`, `books_amount`) VALUES
(1, 'Headmaster', 'English', '', '', 0),
(2, 'Headmaster', 'Story ', 'English', '', 0),
(3, 'Headmaster', 'Bangla ', '', '', 0),
(4, 'Headmaster', 'Story', 'Bangla ', '', 0),
(5, 'Headmaster', 'poem', 'English', '', 0),
(6, 'Headmaster', 'Programming', 'Story ', 'qomiasanfkajsdfasflskjfakslfjksafjslfjsalfjasklf', 0),
(7, 'Headmaster', 'cute cat', 'poem', 'my cute little cat  ', 0);

-- --------------------------------------------------------

--
-- Table structure for table `calender_events`
--

CREATE TABLE `calender_events` (
  `id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `start_date` varchar(15) NOT NULL,
  `end_date` varchar(15) NOT NULL,
  `color` varchar(15) NOT NULL,
  `url` varchar(200) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `calender_events`
--

INSERT INTO `calender_events` (`id`, `title`, `start_date`, `end_date`, `color`, `url`, `user_id`) VALUES
(1, 'test', '05-05-2016', '06-05-2016', 'red', 'test.com', 1),
(2, 'exam', '08-07-2017', '31-07-2017', 'purple', '', 8);

-- --------------------------------------------------------

--
-- Table structure for table `class`
--

CREATE TABLE `class` (
  `id` int(11) NOT NULL,
  `class_title` varchar(50) NOT NULL,
  `class_group` varchar(150) NOT NULL,
  `section` varchar(100) NOT NULL,
  `section_student_capacity` varchar(5) NOT NULL,
  `classCode` int(11) NOT NULL,
  `student_amount` int(11) NOT NULL,
  `attendance_percentices_daily` int(11) NOT NULL,
  `attend_percentise_yearly` int(11) NOT NULL,
  `month_fee` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class`
--

INSERT INTO `class` (`id`, `class_title`, `class_group`, `section`, `section_student_capacity`, `classCode`, `student_amount`, `attendance_percentices_daily`, `attend_percentise_yearly`, `month_fee`) VALUES
(1, 'Class One', '', 'A', '40', 0, 0, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `class_routine`
--

CREATE TABLE `class_routine` (
  `id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `section` varchar(100) NOT NULL,
  `day_title` varchar(50) NOT NULL,
  `subject` varchar(300) NOT NULL,
  `subject_teacher` varchar(200) NOT NULL,
  `start_time` varchar(30) NOT NULL,
  `end_time` varchar(30) NOT NULL,
  `room_number` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class_routine`
--

INSERT INTO `class_routine` (`id`, `class_id`, `section`, `day_title`, `subject`, `subject_teacher`, `start_time`, `end_time`, `room_number`) VALUES
(1, 1, '', 'Sunday', 'AMAR BANGLA BOI', 'Willie B. Quint', '9.00am', '9.30', '101'),
(2, 1, '', 'Sunday', 'ENGLISH FOR TODAY', 'Fredrick V. Keyes', '9.31am', '10.00am', '101'),
(3, 1, '', 'Sunday', 'PRIMARY MATHEMATICS', 'mumar abboud', '10.01am', '10.30am', '101'),
(4, 1, '', 'Sunday', 'PRIMARY MATHEMATICS', 'mumar abboud', '10.01am', '10.30am', '101'),
(5, 2, '', 'Sunday', 'AMAR BANGLA BOI', 'Inayah Asfour', '9.00am', '9.30am', '102'),
(6, 2, '', 'Sunday', 'ENGLISH FOR TODAY', 'mumar abboud', '9.31am', '10.00am', '102'),
(7, 2, '', 'Sunday', 'PRIMARY MATHEMATICS', 'Fredrick V. Keyes', '10.01am', '10.30am', '102'),
(8, 3, '', 'Sunday', 'AMAR BANGLA BOI', 'Willie B. Quint', '9.00am', '9.30am', '103'),
(9, 3, '', 'Sunday', 'ENGLISH FOR TODAY', 'Fredrick V. Keyes', '9.31am', '10.00am', '103'),
(10, 3, '', 'Sunday', 'PRIMARY MATHEMATICS', 'mumar abboud', '10.01am', '10.30am', '103'),
(11, 3, '', 'Sunday', 'BANGLADESH AND GLOBAL STUDIES', 'Inayah Asfour', '10.30am', '11.00am', '103'),
(12, 3, '', 'Monday', 'ENGLISH FOR TODAY', 'Fredrick V. Keyes', '9.00am', '9.30', '103'),
(13, 3, '', 'Monday', 'PRIMARY MATHEMATICS', 'mumar abboud', '9.31am', '10.00am', '103'),
(14, 11, '', 'Monday', 'Math', 'mumar abboud', '8:00', '8:30', 'A1'),
(15, 11, '', 'Monday', 'General Science', 'Inayah Asfour', '8:30', '9:00', 'A1'),
(16, 11, '', 'Monday', 'Islamiat', 'Inayah Asfour', '9:00', '9:30', 'A1'),
(17, 11, '', 'Monday', 'Social Studies', 'mumar abboud', '9:30', '10:00', 'A1'),
(18, 11, '', 'Monday', 'English', 'Inayah Asfour', '10:00', '10:30', 'A1'),
(19, 8, '', 'Monday', 'AMAR BANGLA BOI', 'mumar abboud', '8:00', '8:30', 'A1'),
(20, 14, '', 'Wednesday', 'Math', 'Willie B. Quint', '7:00 AM', '8:00 AM', '13'),
(21, 14, '', 'Wednesday', 'English', 'Fredrick V. Keyes', '8:15 AM', '9:00 AM', '4'),
(22, 11, '', 'Monday', 'Islamiat', 'Mudasir Murtaza', '11:00', '12:00', 'A1'),
(23, 11, '', 'Tuesday', 'Social Studies', 'Mudasir Murtaza', '8:00', '8:30', 'A1'),
(24, 15, '', 'Monday', 'Maths', 'Mr. Raheel naeem', '7:00 AM', '7:45 AM', '3'),
(25, 15, '', 'Monday', 'English', 'Willie B. Quint', '8:00 AM', '8:45 AM', '3'),
(26, 15, '', 'Tuesday', 'Maths', 'Mr. Raheel naeem', '8:00AM', '8:45 AM', '3'),
(27, 15, '', 'Tuesday', 'English', 'Willie B. Quint', '7:00 AM', '7:45', '3');

-- --------------------------------------------------------

--
-- Table structure for table `class_students`
--

CREATE TABLE `class_students` (
  `id` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `roll_number` int(11) DEFAULT NULL,
  `student_id` varchar(100) NOT NULL,
  `class_id` int(11) NOT NULL,
  `class_title` varchar(50) NOT NULL,
  `section` varchar(150) NOT NULL,
  `student_title` varchar(100) NOT NULL,
  `attendance_percentices_daily` int(11) NOT NULL,
  `optional_sub` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `class_subject`
--

CREATE TABLE `class_subject` (
  `id` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `subject_title` varchar(100) NOT NULL,
  `group` varchar(100) NOT NULL,
  `subject_teacher` varchar(100) NOT NULL,
  `edition` varchar(100) NOT NULL,
  `writer_name` varchar(100) NOT NULL,
  `optional` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class_subject`
--

INSERT INTO `class_subject` (`id`, `year`, `class_id`, `subject_title`, `group`, `subject_teacher`, `edition`, `writer_name`, `optional`) VALUES
(1, 2016, 1, 'AMAR BANGLA BOI', '', 'Willie B. Quint', '2016', 'NCTB', 0),
(2, 2016, 1, 'ENGLISH FOR TODAY', '', 'Fredrick V. Keyes', '2016', 'NCTB', 0),
(3, 2016, 1, 'PRIMARY MATHEMATICS', '', 'mumar abboud', '2016', 'NCTB', 0),
(4, 2016, 2, 'AMAR BANGLA BOI', '', 'Inayah Asfour', '2016', 'NCTB', 0),
(5, 2016, 2, 'ENGLISH FOR TODAY', '', 'mumar abboud', '2016', 'NCTB', 0),
(6, 2016, 2, 'PRIMARY MATHEMATICS', '', 'Fredrick V. Keyes', '2016', 'NCTB', 0),
(7, 2016, 2, 'PRIMARY MATHEMATICS', '', 'Fredrick V. Keyes', '2016', 'NCTB', 0),
(8, 2016, 3, 'AMAR BANGLA BOI', '', 'Willie B. Quint', '2016', 'NCTB', 0),
(9, 2016, 3, 'ENGLISH FOR TODAY', '', 'Fredrick V. Keyes', '2016', 'NCTB', 0),
(10, 2016, 3, 'PRIMARY MATHEMATICS', '', 'mumar abboud', '2016', 'NCTB', 0),
(11, 2016, 3, 'BANGLADESH AND GLOBAL STUDIES', '', 'Inayah Asfour', '2016', 'NCTB', 0),
(12, 2016, 3, 'PRIMARY SCIENCE', '', '', '2016', 'NCTB', 0),
(13, 2016, 3, 'ISLAM AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(14, 2016, 3, 'HINDU RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(15, 2016, 3, 'BUDDIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(16, 2016, 3, 'CHRIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(17, 2016, 4, 'AMAR BANGLA BOI', '', '', '2016', 'NCTB', 0),
(18, 2016, 4, 'ENGLISH FOR TODAY', '', '', '2016', 'NCTB', 0),
(19, 2016, 4, 'PRIMARY MATHEMATICS', '', '', '2016', 'NCTB', 0),
(20, 2016, 4, 'BANGLADESH AND GLOBAL STUDIES', '', '', '2016', 'NCTB', 0),
(21, 2016, 4, 'PRIMARY SCIENCE', '', '', '2016', 'NCTB', 0),
(22, 2016, 4, 'ISLAM AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(23, 2016, 4, 'HINDU RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(24, 2016, 4, 'BUDDIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(25, 2016, 4, 'CHRIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(26, 2016, 5, 'AMAR BANGLA BOI', '', '', '2016', 'NCTB', 0),
(27, 2016, 5, 'ENGLISH FOR TODAY', '', '', '2016', 'NCTB', 0),
(28, 2016, 5, 'PRIMARY MATHEMATICS', '', '', '2016', 'NCTB', 0),
(29, 2016, 5, 'BANGLADESH AND GLOBAL STUDIES', '', '', '2016', 'NCTB', 0),
(30, 2016, 5, 'PRIMARY SCIENCE', '', '', '2016', 'NCTB', 0),
(31, 2016, 5, 'ISLAM AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(32, 2016, 5, 'HINDU RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(33, 2016, 5, 'BUDDIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(34, 2016, 5, 'CHRIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(35, 2016, 6, 'AMAR BANGLA BOI', '', '', '2016', 'NCTB', 0),
(36, 2016, 6, 'ENGLISH FOR TODAY', '', '', '2016', 'NCTB', 0),
(37, 2016, 6, 'PRIMARY MATHEMATICS', '', '', '2016', 'NCTB', 0),
(38, 2016, 6, 'BANGLADESH AND GLOBAL STUDIES', '', '', '2016', 'NCTB', 0),
(39, 2016, 6, 'PRIMARY SCIENCE', '', '', '2016', 'NCTB', 0),
(40, 2016, 6, 'ISLAM AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(41, 2016, 6, 'HINDU RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(42, 2016, 6, 'BUDDIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(43, 2016, 6, 'CHRIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(44, 2016, 7, 'AMAR BANGLA BOI', '', '', '2016', 'NCTB', 0),
(45, 2016, 7, 'ENGLISH FOR TODAY', '', '', '2016', 'NCTB', 0),
(46, 2016, 7, 'PRIMARY MATHEMATICS', '', '', '2016', 'NCTB', 0),
(47, 2016, 7, 'BANGLADESH AND GLOBAL STUDIES', '', '', '2016', 'NCTB', 0),
(48, 2016, 7, 'PRIMARY SCIENCE', '', '', '2016', 'NCTB', 0),
(49, 2016, 7, 'ISLAM AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(50, 2016, 7, 'HINDU RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(51, 2016, 7, 'BUDDIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(52, 2016, 7, 'CHRIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(53, 2016, 8, 'AMAR BANGLA BOI', '', 'mumar abboud', '2016', 'NCTB', 0),
(54, 2016, 8, 'ENGLISH FOR TODAY', '', '', '2016', 'NCTB', 0),
(55, 2016, 8, 'PRIMARY MATHEMATICS', '', '', '2016', 'NCTB', 0),
(56, 2016, 8, 'ISLAM AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(57, 2016, 8, 'HINDU RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(58, 2016, 8, 'BUDDIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(59, 2016, 8, 'CHRIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(60, 2016, 8, 'BANGLADESH AND GLOBAL STUDIES', '', '', '2016', 'NCTB', 0),
(61, 2016, 8, 'PRIMARY SCIENCE', '', '', '2016', 'NCTB', 0),
(62, 2016, 9, 'AMAR BANGLA BOI', '', '', '2016', 'NCTB', 0),
(63, 2016, 9, 'ENGLISH FOR TODAY', '', '', '2016', 'NCTB', 0),
(64, 2016, 9, 'PRIMARY MATHEMATICS', '', '', '2016', 'NCTB', 0),
(65, 2016, 9, 'BANGLADESH AND GLOBAL STUDIES', '', '', '2016', 'NCTB', 0),
(66, 2016, 9, 'PRIMARY SCIENCE', '', '', '2016', 'NCTB', 0),
(67, 2016, 9, 'ISLAM AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(68, 2016, 9, 'HINDU RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(69, 2016, 9, 'BUDDIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(70, 2016, 9, 'CHRIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(71, 2016, 10, 'AMAR BANGLA BOI', '', '', '2016', 'NCTB', 0),
(72, 2016, 10, 'ENGLISH FOR TODAY', '', '', '2016', 'NCTB', 0),
(73, 2016, 10, 'PRIMARY MATHEMATICS', '', '', '2016', 'NCTB', 0),
(74, 2016, 10, 'BANGLADESH AND GLOBAL STUDIES', '', '', '2016', 'NCTB', 0),
(75, 2016, 10, 'ISLAM AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(76, 2016, 10, 'HINDU RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(77, 2016, 10, 'BUDDIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(78, 2016, 10, 'CHRIST RELIGION AND MORAL EDUCATION', '', '', '2016', 'NCTB', 0),
(79, 2016, 10, 'PRIMARY SCIENCE', '', '', '2016', 'NCTB', 0),
(80, 2016, 9, 'AGRICULTURE STUDIES', '', '', '2016', 'NCTB', 1),
(81, 2016, 10, 'AGRICULTURE STUDIES', '', '', '2016', 'NCTB', 1),
(82, 2016, 10, 'ARTS & CRAFTS', '', '', '2016', 'NCTB', 1),
(83, 2016, 10, 'ARTS & CRAFTS', '', '', '2016', 'NCTB', 1),
(84, 2016, 9, 'MUSIC', '', '', '2016', 'NCTB', 1),
(85, 2016, 10, 'MUSIC', '', '', '2016', 'NCTB', 1),
(86, 2017, 11, 'Math', '', 'mumar abboud', 'Edition1', 'benjamin', 0),
(87, 2017, 11, 'General Science', '', 'Inayah Asfour', 'Edition 1st', 'Suhail', 0),
(88, 2017, 11, 'Islamiat', '', 'Mudasir Murtaza', '1st', 'Mohsin', 0),
(89, 2017, 11, 'Social Studies', '', 'Mudasir Murtaza', '1st', 'Umair', 0),
(90, 2017, 11, 'English', '', 'Inayah Asfour', '1st', 'Mudasir', 0),
(91, 2017, 11, 'Naazra', '', '', '1st', 'Kashif Junaid', 0),
(92, 2017, 14, 'Math', '', 'Willie B. Quint', '13', 'mr.sohail', 0),
(93, 2017, 14, 'English', '', 'Fredrick V. Keyes', 'new15', 'Mohsin', 0),
(94, 2017, 1, 'Maths', '', '', 'new15', 'kashif', 0),
(95, 2017, 15, 'Maths', '', 'Mr. Raheel naeem', 'edition 15', 'Mohsin', 0),
(96, 2017, 15, 'English', '', 'Willie B. Quint', 'edition 16', 'sohail', 0),
(97, 2017, 15, 'Urdu', '', '', 'edition 17', 'umair', 0),
(98, 2017, 15, 'English B', '', '', 'edition 18', 'ali', 0),
(99, 2017, 16, 'Maths', '', '', 'edition 15', 'Mohsin', 0),
(100, 2017, 16, 'english', '', '', 'edition 16', 'sohail', 0),
(101, 2017, 16, 'Urdu', '', '', 'edition 17', 'Syed Umer', 0),
(102, 2017, 16, 'English 2', '', '', 'edition 18', 'ali', 0);

-- --------------------------------------------------------

--
-- Table structure for table `configuration`
--

CREATE TABLE `configuration` (
  `id` int(11) NOT NULL,
  `logo` varchar(150) NOT NULL,
  `time_zone` varchar(150) NOT NULL,
  `school_name` varchar(150) NOT NULL,
  `starting_year` varchar(50) NOT NULL,
  `headmaster_name` varchar(150) NOT NULL,
  `address` varchar(150) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `currenct` varchar(50) NOT NULL,
  `country` varchar(150) NOT NULL,
  `language` text NOT NULL,
  `msg_apai_email` varchar(100) NOT NULL,
  `msg_hash_number` varchar(100) NOT NULL,
  `msg_sender_title` varchar(100) NOT NULL,
  `countryPhonCode` varchar(5) NOT NULL,
  `t_a_s_p` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `configuration`
--

INSERT INTO `configuration` (`id`, `logo`, `time_zone`, `school_name`, `starting_year`, `headmaster_name`, `address`, `phone`, `email`, `currenct`, `country`, `language`, `msg_apai_email`, `msg_hash_number`, `msg_sender_title`, `countryPhonCode`, `t_a_s_p`) VALUES
(0, '', 'UP5', 'Cybex Tech Model High School', '12/12/2000', 'Mr. Umair', 'Walton Lahore', '0123456789', 'info@info.com', 'fa fa-money', 'Pakistan', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `config_week_day`
--

CREATE TABLE `config_week_day` (
  `id` int(11) NOT NULL,
  `day_name` varchar(20) NOT NULL,
  `status` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `config_week_day`
--

INSERT INTO `config_week_day` (`id`, `day_name`, `status`) VALUES
(1, 'Sunday', 'Holyday'),
(2, 'Monday', 'Open'),
(3, 'Tuesday', 'Open'),
(4, 'Wednesday', 'Open'),
(5, 'Thursday', 'Open'),
(6, 'Friday', 'Open'),
(7, 'Saturday', 'Holyday');

-- --------------------------------------------------------

--
-- Table structure for table `daily_attendance`
--

CREATE TABLE `daily_attendance` (
  `id` int(11) NOT NULL,
  `date` varchar(50) NOT NULL,
  `user_id` varchar(30) NOT NULL,
  `student_id` varchar(150) NOT NULL,
  `class_title` varchar(30) NOT NULL,
  `section` varchar(100) NOT NULL,
  `days_amount` varchar(20) NOT NULL,
  `roll_no` int(11) NOT NULL,
  `present_or_absent` varchar(2) NOT NULL,
  `student_title` varchar(100) NOT NULL,
  `class_amount_monthly` int(11) NOT NULL,
  `class_amount_yearly` int(11) NOT NULL,
  `attend_amount_monthly` int(11) NOT NULL,
  `attend_amount_yearly` int(11) NOT NULL,
  `percentise_month` int(11) NOT NULL,
  `percentise_year` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `daily_attendance`
--

INSERT INTO `daily_attendance` (`id`, `date`, `user_id`, `student_id`, `class_title`, `section`, `days_amount`, `roll_no`, `present_or_absent`, `student_title`, `class_amount_monthly`, `class_amount_yearly`, `attend_amount_monthly`, `attend_amount_yearly`, `percentise_month`, `percentise_year`) VALUES
(1, '1464991200', '4', '201601001', 'Class 1', 'Section A,Section B,Section C,Section D', '', 1, 'P', 'Benjamin D. Lampe', 1, 1, 1, 1, 100, 100),
(2, '1464991200', '12', '201601002', 'Class 1', 'Section A,Section B,Section C,Section D', '', 2, 'P', 'Rahim Hasan', 1, 1, 1, 1, 100, 100),
(3, '1464991200', '13', '201601003', 'Class 1', 'Section A,Section B,Section C,Section D', '', 3, 'P', 'Junayed Hak', 1, 1, 1, 1, 100, 100),
(4, '1464991200', '16', '201601004', 'Class 1', '', '', 4, 'A', 'Razia Akture', 1, 1, 0, 0, 0, 0),
(5, '1464991200', '23', '201601005', 'Class 1', '', '', 5, 'P', 'Polash Sarder', 1, 1, 1, 1, 100, 100),
(6, '1464991200', '24', '201601006', 'Class 1', '', '', 6, 'A', 'Sumon Akon', 1, 1, 0, 0, 0, 0),
(7, '1464991200', '17', '201602001', 'Class 2', '', '', 1, 'P', 'Abdullah  hossain', 1, 1, 1, 1, 100, 100),
(8, '1464991200', '18', '201602002', 'Class 2', '', '', 2, 'P', 'Sujana Ahmed', 1, 1, 1, 1, 100, 100),
(9, '1464991200', '19', '201602003', 'Class 2', '', '', 3, 'P', 'Mahmud Hasan', 1, 1, 1, 1, 100, 100),
(10, '1464991200', '20', '201602004', 'Class 2', '', '', 4, 'P', 'Mahbuba Akter', 1, 1, 1, 1, 100, 100),
(11, '1464991200', '21', '201602005', 'Class 2', '', '', 5, 'P', 'Irfan Hossain', 1, 1, 1, 1, 100, 100),
(12, '1464991200', '22', '201602006', 'Class 2', '', '', 6, 'P', 'Imran Hasan', 1, 1, 1, 1, 100, 100),
(13, '1464991200', '25', '201603001', 'Class 3', '', '', 1, 'A', 'Farjana Akter', 1, 1, 0, 0, 0, 0),
(14, '1464991200', '26', '201603002', 'Class 3', '', '', 2, 'P', 'Akram Hossain', 1, 1, 1, 1, 100, 100),
(15, '1464991200', '27', '201603003', 'Class 3', '', '', 3, 'P', 'Alamin Saeder', 1, 1, 1, 1, 100, 100),
(16, '1464991200', '28', '201603004', 'Class 3', '', '', 4, 'P', 'Sabina Sumi', 1, 1, 1, 1, 100, 100),
(17, '1464991200', '29', '201604001', 'Class 4', '', '', 1, 'P', 'Sanjida Hossain', 1, 1, 1, 1, 100, 100),
(18, '1464991200', '30', '201604002', 'Class 4', '', '', 2, 'P', 'Kawser  Shikder', 1, 1, 1, 1, 100, 100),
(19, '1464991200', '31', '201604003', 'Class 4', '', '', 3, 'P', 'Shohana Akter', 1, 1, 1, 1, 100, 100),
(20, '1464991200', '32', '201604004', 'Class 4', '', '', 4, 'P', 'Juthi Khanam', 1, 1, 1, 1, 100, 100),
(21, '1464991200', '33', '201605001', 'Class 5', '', '', 1, 'P', 'Tanjila Akter', 1, 1, 1, 1, 100, 100),
(22, '1464991200', '34', '201605002', 'Class 5', '', '', 2, 'P', 'Nusrat Jahan', 1, 1, 1, 1, 100, 100),
(23, '1464991200', '35', '201605003', 'Class 5', '', '', 3, 'P', 'Amina Akter', 1, 1, 1, 1, 100, 100),
(24, '1464991200', '36', '201605004', 'Class 5', '', '', 4, 'A', 'Ebrahim Khondokar', 1, 1, 0, 0, 0, 0),
(25, '1464991200', '37', '201605005', 'Class 5', '', '', 5, 'A', 'Mintu  Fokir', 1, 1, 0, 0, 0, 0),
(26, '1464991200', '38', '201606001', 'Class 6', 'Section A', '', 1, 'P', 'Shohid Islam', 1, 1, 1, 1, 100, 100),
(27, '1464991200', '39', '201606002', 'Class 6', 'Section B', '', 2, 'P', 'Khadija Akter', 1, 1, 1, 1, 100, 100),
(28, '1464991200', '40', '201606003', 'Class 6', 'Section A', '', 3, 'P', 'Maruf Hossain', 1, 1, 1, 1, 100, 100),
(29, '1464991200', '41', '201606004', 'Class 6', 'Section B', '', 4, 'P', 'Mitu  Akter', 1, 1, 1, 1, 100, 100),
(30, '1464991200', '42', '201606005', 'Class 6', 'Section A', '', 5, 'A', 'Rayhan  Kebria', 1, 1, 0, 0, 0, 0),
(31, '1496188800', '17', '201602001', 'Class 2', '', '', 1, 'P', 'Abdullah  hossain', 2, 2, 2, 2, 100, 100),
(32, '1496188800', '18', '201602002', 'Class 2', '', '', 2, 'P', 'Sujana Ahmed', 2, 2, 2, 2, 100, 100),
(33, '1496188800', '19', '201602003', 'Class 2', '', '', 3, 'P', 'Mahmud Hasan', 2, 2, 2, 2, 100, 100),
(34, '1496188800', '20', '201602004', 'Class 2', '', '', 4, 'A', 'Mahbuba Akter', 2, 2, 1, 1, 50, 50),
(35, '1496188800', '21', '201602005', 'Class 2', '', '', 5, 'A', 'Irfan Hossain', 2, 2, 1, 1, 50, 50),
(36, '1496188800', '22', '201602006', 'Class 2', '', '', 6, 'A', 'Imran Hasan', 2, 2, 1, 1, 50, 50),
(37, '1496275200', '59', '201700001', 'Shajar Nursery 1 ', 'section2015 A', '', 1, 'P', 'Syed Umer', 4, 4, 3, 3, 75, 75),
(38, '1496275200', '60', '201700002', 'Shajar Nursery 1 ', 'section2015 A', '', 2, 'P', 'usman ahmad', 3, 3, 1, 1, 33, 33),
(39, '1496275200', '59', '201700001', 'Shajar Nursery 1 ', 'section2015 A', '', 1, '', 'Syed Umer', 4, 4, 3, 3, 75, 75),
(40, '1496275200', '60', '201700002', 'Shajar Nursery 1 ', 'section2015 A', '', 2, '', 'usman ahmad', 3, 3, 1, 1, 33, 33),
(41, '1496275200', '59', '201700001', 'Shajar Nursery 1 ', 'section2015 A', '', 1, 'P', 'Syed Umer', 4, 4, 3, 3, 75, 75),
(42, '1496275200', '60', '201700002', 'Shajar Nursery 1 ', 'section2015 A', '', 2, 'A', 'usman ahmad', 3, 3, 1, 1, 33, 33),
(43, '1496275200', '59', '201700001', 'Shajar Nursery 1 ', 'section2015 A', '', 1, 'P', 'Syed Umer', 4, 4, 3, 3, 75, 75);

-- --------------------------------------------------------

--
-- Table structure for table `dormitory`
--

CREATE TABLE `dormitory` (
  `id` int(11) NOT NULL,
  `dormitory_name` varchar(100) NOT NULL,
  `dormitory_for` varchar(100) NOT NULL,
  `room_amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dormitory`
--

INSERT INTO `dormitory` (`id`, `dormitory_name`, `dormitory_for`, `room_amount`) VALUES
(1, 'Boys Hostel', 'Only for male', 125),
(2, 'Girls Hostel', 'Only for female', 40),
(3, 'Teachers Dormitory', 'Only for teachers (Male Teacher)', 10);

-- --------------------------------------------------------

--
-- Table structure for table `dormitory_bed`
--

CREATE TABLE `dormitory_bed` (
  `id` int(11) NOT NULL,
  `dormitory_id` int(11) NOT NULL,
  `dormitory_name` varchar(100) NOT NULL,
  `room_number` varchar(50) NOT NULL,
  `bed_number` varchar(50) NOT NULL,
  `student_id` int(11) NOT NULL,
  `student_name` varchar(100) NOT NULL,
  `class` varchar(50) NOT NULL,
  `roll_number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dormitory_bed`
--

INSERT INTO `dormitory_bed` (`id`, `dormitory_id`, `dormitory_name`, `room_number`, `bed_number`, `student_id`, `student_name`, `class`, `roll_number`) VALUES
(1, 1, 'Boys Hostel', 'Room No: 1', 'Seat No: 1', 201601001, 'Benjamin D. Lampe', '1', 1),
(2, 1, 'Boys Hostel', 'Room No: 1', 'Seat No: 2', 0, '', '', 0),
(3, 1, 'Boys Hostel', 'Room No: 1', 'Seat No: 3', 0, '', '', 0),
(4, 1, 'Boys Hostel', 'Room No: 1', 'Seat No: 4', 0, '', '', 0),
(5, 1, 'Boys Hostel', 'Room No: 1', 'Seat No: 5', 0, '', '', 0),
(6, 1, 'Boys Hostel', 'Room No: 1', 'Seat No: 6', 0, '', '', 0),
(7, 1, 'Boys Hostel', 'Room No: 1', 'Seat No: 7', 0, '', '', 0),
(8, 1, 'Boys Hostel', 'Room No: 1', 'Seat No: 8', 0, '', '', 0),
(9, 1, 'Boys Hostel', 'Room No: 1', 'Seat No: 9', 0, '', '', 0),
(10, 1, 'Boys Hostel', 'Room No: 1', 'Seat No: 10', 0, '', '', 0),
(11, 1, 'Boys Hostel', 'Room No: 2', 'Seat No: 1', 0, '', '', 0),
(12, 1, 'Boys Hostel', 'Room No: 2', 'Seat No: 2', 0, '', '', 0),
(13, 1, 'Boys Hostel', 'Room No: 2', 'Seat No: 3', 0, '', '', 0),
(14, 1, 'Boys Hostel', 'Room No: 2', 'Seat No: 4', 0, '', '', 0),
(15, 1, 'Boys Hostel', 'Room No: 2', 'Seat No: 5', 0, '', '', 0),
(16, 1, 'Boys Hostel', 'Room No: 2', 'Seat No: 6', 0, '', '', 0),
(17, 1, 'Boys Hostel', 'Room No: 2', 'Seat No: 7', 0, '', '', 0),
(18, 1, 'Boys Hostel', 'Room No: 2', 'Seat No: 8', 0, '', '', 0),
(19, 1, 'Boys Hostel', 'Room No: 2', 'Seat No: 9', 0, '', '', 0),
(20, 1, 'Boys Hostel', 'Room No: 2', 'Seat No: 10', 0, '', '', 0),
(21, 2, 'Girls Hostel', 'Room No: 1', 'Seat No: 1', 0, '', '', 0),
(22, 2, 'Girls Hostel', 'Room No: 1', 'Seat No: 2', 0, '', '', 0),
(23, 2, 'Girls Hostel', 'Room No: 1', 'Seat No: 3', 0, '', '', 0),
(24, 2, 'Girls Hostel', 'Room No: 1', 'Seat No: 4', 0, '', '', 0),
(25, 2, 'Girls Hostel', 'Room No: 1', 'Seat No: 5', 0, '', '', 0),
(26, 2, 'Girls Hostel', 'Room No: 1', 'Seat No: 6', 0, '', '', 0),
(27, 2, 'Girls Hostel', 'Room No: 1', 'Seat No: 7', 0, '', '', 0),
(28, 2, 'Girls Hostel', 'Room No: 1', 'Seat No: 8', 0, '', '', 0),
(29, 2, 'Girls Hostel', 'Room No: 1', 'Seat No: 9', 0, '', '', 0),
(30, 2, 'Girls Hostel', 'Room No: 1', 'Seat No: 10', 0, '', '', 0),
(31, 2, 'Girls Hostel', 'Room No: 2', 'Seat No: 1', 0, '', '', 0),
(32, 2, 'Girls Hostel', 'Room No: 2', 'Seat No: 2', 0, '', '', 0),
(33, 2, 'Girls Hostel', 'Room No: 2', 'Seat No: 3', 0, '', '', 0),
(34, 2, 'Girls Hostel', 'Room No: 2', 'Seat No: 4', 0, '', '', 0),
(35, 2, 'Girls Hostel', 'Room No: 2', 'Seat No: 5', 0, '', '', 0),
(36, 2, 'Girls Hostel', 'Room No: 2', 'Seat No: 6', 0, '', '', 0),
(37, 2, 'Girls Hostel', 'Room No: 2', 'Seat No: 7', 0, '', '', 0),
(38, 2, 'Girls Hostel', 'Room No: 2', 'Seat No: 8', 0, '', '', 0),
(39, 2, 'Girls Hostel', 'Room No: 2', 'Seat No: 9', 0, '', '', 0),
(40, 2, 'Girls Hostel', 'Room No: 2', 'Seat No: 10', 0, '', '', 0),
(41, 2, 'Girls Hostel', 'Room No: 2', 'Seat No: 1', 0, '', '', 0),
(42, 2, 'Girls Hostel', 'Room No: 2', 'Seat No: 2', 0, '', '', 0),
(43, 2, 'Girls Hostel', 'Room No: 2', 'Seat No: 3', 0, '', '', 0),
(44, 2, 'Girls Hostel', 'Room No: 2', 'Seat No: 4', 0, '', '', 0),
(45, 2, 'Girls Hostel', 'Room No: 2', 'Seat No: 5', 0, '', '', 0),
(46, 2, 'Girls Hostel', 'Room No: 2', 'Seat No: 6', 0, '', '', 0),
(47, 2, 'Girls Hostel', 'Room No: 2', 'Seat No: 7', 0, '', '', 0),
(48, 2, 'Girls Hostel', 'Room No: 2', 'Seat No: 8', 0, '', '', 0),
(49, 2, 'Girls Hostel', 'Room No: 2', 'Seat No: 9', 0, '', '', 0),
(50, 2, 'Girls Hostel', 'Room No: 2', 'Seat No: 10', 0, '', '', 0),
(51, 2, 'Girls Hostel', 'Room No: 3', 'Seat No: 1', 0, '', '', 0),
(52, 2, 'Girls Hostel', 'Room No: 3', 'Seat No: 2', 0, '', '', 0),
(53, 2, 'Girls Hostel', 'Room No: 3', 'Seat No: 3', 0, '', '', 0),
(54, 2, 'Girls Hostel', 'Room No: 3', 'Seat No: 4', 0, '', '', 0),
(55, 2, 'Girls Hostel', 'Room No: 3', 'Seat No: 5', 0, '', '', 0),
(56, 2, 'Girls Hostel', 'Room No: 3', 'Seat No: 6', 0, '', '', 0),
(57, 2, 'Girls Hostel', 'Room No: 3', 'Seat No: 7', 0, '', '', 0),
(58, 2, 'Girls Hostel', 'Room No: 3', 'Seat No: 8', 0, '', '', 0),
(59, 2, 'Girls Hostel', 'Room No: 3', 'Seat No: 9', 0, '', '', 0),
(60, 2, 'Girls Hostel', 'Room No: 3', 'Seat No: 10', 0, '', '', 0),
(61, 3, 'Teachers Dormitory', 'Room No: 1', 'Seat No: 1', 0, '', '', 0),
(62, 3, 'Teachers Dormitory', 'Room No: 1', 'Seat No: 2', 0, '', '', 0),
(63, 3, 'Teachers Dormitory', 'Room No: 1', 'Seat No: 3', 0, '', '', 0),
(64, 3, 'Teachers Dormitory', 'Room No: 1', 'Seat No: 4', 0, '', '', 0),
(65, 3, 'Teachers Dormitory', 'Room No: 1', 'Seat No: 5', 0, '', '', 0),
(66, 3, 'Teachers Dormitory', 'Room No: 1', 'Seat No: 6', 0, '', '', 0),
(67, 3, 'Teachers Dormitory', 'Room No: 1', 'Seat No: 7', 0, '', '', 0),
(68, 3, 'Teachers Dormitory', 'Room No: 1', 'Seat No: 8', 0, '', '', 0),
(69, 3, 'Teachers Dormitory', 'Room No: 1', 'Seat No: 9', 0, '', '', 0),
(70, 3, 'Teachers Dormitory', 'Room No: 1', 'Seat No: 10', 0, '', '', 0),
(71, 3, 'Teachers Dormitory', 'Room No: 2', 'Seat No: 1', 0, '', '', 0),
(72, 3, 'Teachers Dormitory', 'Room No: 2', 'Seat No: 2', 0, '', '', 0),
(73, 3, 'Teachers Dormitory', 'Room No: 2', 'Seat No: 3', 0, '', '', 0),
(74, 3, 'Teachers Dormitory', 'Room No: 2', 'Seat No: 4', 0, '', '', 0),
(75, 3, 'Teachers Dormitory', 'Room No: 2', 'Seat No: 5', 0, '', '', 0),
(76, 3, 'Teachers Dormitory', 'Room No: 2', 'Seat No: 6', 0, '', '', 0),
(77, 3, 'Teachers Dormitory', 'Room No: 2', 'Seat No: 7', 0, '', '', 0),
(78, 3, 'Teachers Dormitory', 'Room No: 2', 'Seat No: 8', 0, '', '', 0),
(79, 3, 'Teachers Dormitory', 'Room No: 2', 'Seat No: 9', 0, '', '', 0),
(80, 3, 'Teachers Dormitory', 'Room No: 2', 'Seat No: 10', 0, '', '', 0),
(81, 1, 'Boys Hostel', 'Room No: 7', 'Seat No: 1', 201602001, 'Abdullah  hossain', '2', 1),
(82, 1, 'Boys Hostel', 'Room No: 7', 'Seat No: 2', 0, '', '', 0),
(83, 1, 'Boys Hostel', 'Room No: 7', 'Seat No: 3', 0, '', '', 0),
(84, 1, 'Boys Hostel', 'Room No: 7', 'Seat No: 4', 0, '', '', 0),
(85, 1, 'Boys Hostel', 'Room No: 7', 'Seat No: 5', 0, '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `dormitory_room`
--

CREATE TABLE `dormitory_room` (
  `id` int(11) NOT NULL,
  `dormitory_id` int(11) NOT NULL,
  `dormitory_name` varchar(100) NOT NULL,
  `room` varchar(50) NOT NULL,
  `bed_amount` int(11) NOT NULL,
  `free_seat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dormitory_room`
--

INSERT INTO `dormitory_room` (`id`, `dormitory_id`, `dormitory_name`, `room`, `bed_amount`, `free_seat`) VALUES
(1, 1, 'Boys Hostel', 'Room No: 1', 10, 10),
(2, 1, 'Boys Hostel', 'Room No: 2', 10, 10),
(3, 1, 'Boys Hostel', 'Room No: 3', 0, 0),
(4, 1, 'Boys Hostel', 'Room No: 4', 0, 0),
(5, 1, 'Boys Hostel', 'Room No: 5', 0, 0),
(6, 1, 'Boys Hostel', 'Room No: 6', 0, 0),
(7, 1, 'Boys Hostel', 'Room No: 7', 5, 5),
(8, 1, 'Boys Hostel', 'Room No: 8', 0, 0),
(9, 1, 'Boys Hostel', 'Room No: 9', 0, 0),
(10, 1, 'Boys Hostel', 'Room No: 10', 0, 0),
(11, 1, 'Boys Hostel', 'Room No: 11', 0, 0),
(12, 1, 'Boys Hostel', 'Room No: 12', 0, 0),
(13, 1, 'Boys Hostel', 'Room No: 13', 0, 0),
(14, 1, 'Boys Hostel', 'Room No: 14', 0, 0),
(15, 1, 'Boys Hostel', 'Room No: 15', 0, 0),
(16, 1, 'Boys Hostel', 'Room No: 16', 0, 0),
(17, 1, 'Boys Hostel', 'Room No: 17', 0, 0),
(18, 1, 'Boys Hostel', 'Room No: 18', 0, 0),
(19, 1, 'Boys Hostel', 'Room No: 19', 0, 0),
(20, 1, 'Boys Hostel', 'Room No: 20', 0, 0),
(21, 1, 'Boys Hostel', 'Room No: 21', 0, 0),
(22, 1, 'Boys Hostel', 'Room No: 22', 0, 0),
(23, 1, 'Boys Hostel', 'Room No: 23', 0, 0),
(24, 1, 'Boys Hostel', 'Room No: 24', 0, 0),
(25, 1, 'Boys Hostel', 'Room No: 25', 0, 0),
(26, 1, 'Boys Hostel', 'Room No: 26', 0, 0),
(27, 1, 'Boys Hostel', 'Room No: 27', 0, 0),
(28, 1, 'Boys Hostel', 'Room No: 28', 0, 0),
(29, 1, 'Boys Hostel', 'Room No: 29', 0, 0),
(30, 1, 'Boys Hostel', 'Room No: 30', 0, 0),
(31, 1, 'Boys Hostel', 'Room No: 31', 0, 0),
(32, 1, 'Boys Hostel', 'Room No: 32', 0, 0),
(33, 1, 'Boys Hostel', 'Room No: 33', 0, 0),
(34, 1, 'Boys Hostel', 'Room No: 34', 0, 0),
(35, 1, 'Boys Hostel', 'Room No: 35', 0, 0),
(36, 1, 'Boys Hostel', 'Room No: 36', 0, 0),
(37, 1, 'Boys Hostel', 'Room No: 37', 0, 0),
(38, 1, 'Boys Hostel', 'Room No: 38', 0, 0),
(39, 1, 'Boys Hostel', 'Room No: 39', 0, 0),
(40, 1, 'Boys Hostel', 'Room No: 40', 0, 0),
(41, 1, 'Boys Hostel', 'Room No: 41', 0, 0),
(42, 1, 'Boys Hostel', 'Room No: 42', 0, 0),
(43, 1, 'Boys Hostel', 'Room No: 43', 0, 0),
(44, 1, 'Boys Hostel', 'Room No: 44', 0, 0),
(45, 1, 'Boys Hostel', 'Room No: 45', 0, 0),
(46, 1, 'Boys Hostel', 'Room No: 46', 0, 0),
(47, 1, 'Boys Hostel', 'Room No: 47', 0, 0),
(48, 1, 'Boys Hostel', 'Room No: 48', 0, 0),
(49, 1, 'Boys Hostel', 'Room No: 49', 0, 0),
(50, 1, 'Boys Hostel', 'Room No: 50', 0, 0),
(51, 1, 'Boys Hostel', 'Room No: 51', 0, 0),
(52, 1, 'Boys Hostel', 'Room No: 52', 0, 0),
(53, 1, 'Boys Hostel', 'Room No: 53', 0, 0),
(54, 1, 'Boys Hostel', 'Room No: 54', 0, 0),
(55, 1, 'Boys Hostel', 'Room No: 55', 0, 0),
(56, 1, 'Boys Hostel', 'Room No: 56', 0, 0),
(57, 1, 'Boys Hostel', 'Room No: 57', 0, 0),
(58, 1, 'Boys Hostel', 'Room No: 58', 0, 0),
(59, 1, 'Boys Hostel', 'Room No: 59', 0, 0),
(60, 1, 'Boys Hostel', 'Room No: 60', 0, 0),
(61, 1, 'Boys Hostel', 'Room No: 61', 0, 0),
(62, 1, 'Boys Hostel', 'Room No: 62', 0, 0),
(63, 1, 'Boys Hostel', 'Room No: 63', 0, 0),
(64, 1, 'Boys Hostel', 'Room No: 64', 0, 0),
(65, 1, 'Boys Hostel', 'Room No: 65', 0, 0),
(66, 1, 'Boys Hostel', 'Room No: 66', 0, 0),
(67, 1, 'Boys Hostel', 'Room No: 67', 0, 0),
(68, 1, 'Boys Hostel', 'Room No: 68', 0, 0),
(69, 1, 'Boys Hostel', 'Room No: 69', 0, 0),
(70, 1, 'Boys Hostel', 'Room No: 70', 0, 0),
(71, 1, 'Boys Hostel', 'Room No: 71', 0, 0),
(72, 1, 'Boys Hostel', 'Room No: 72', 0, 0),
(73, 1, 'Boys Hostel', 'Room No: 73', 0, 0),
(74, 1, 'Boys Hostel', 'Room No: 74', 0, 0),
(75, 1, 'Boys Hostel', 'Room No: 75', 0, 0),
(76, 1, 'Boys Hostel', 'Room No: 76', 0, 0),
(77, 1, 'Boys Hostel', 'Room No: 77', 0, 0),
(78, 1, 'Boys Hostel', 'Room No: 78', 0, 0),
(79, 1, 'Boys Hostel', 'Room No: 79', 0, 0),
(80, 1, 'Boys Hostel', 'Room No: 80', 0, 0),
(81, 1, 'Boys Hostel', 'Room No: 81', 0, 0),
(82, 1, 'Boys Hostel', 'Room No: 82', 0, 0),
(83, 1, 'Boys Hostel', 'Room No: 83', 0, 0),
(84, 1, 'Boys Hostel', 'Room No: 84', 0, 0),
(85, 1, 'Boys Hostel', 'Room No: 85', 0, 0),
(86, 1, 'Boys Hostel', 'Room No: 86', 0, 0),
(87, 1, 'Boys Hostel', 'Room No: 87', 0, 0),
(88, 1, 'Boys Hostel', 'Room No: 88', 0, 0),
(89, 1, 'Boys Hostel', 'Room No: 89', 0, 0),
(90, 1, 'Boys Hostel', 'Room No: 90', 0, 0),
(91, 1, 'Boys Hostel', 'Room No: 91', 0, 0),
(92, 1, 'Boys Hostel', 'Room No: 92', 0, 0),
(93, 1, 'Boys Hostel', 'Room No: 93', 0, 0),
(94, 1, 'Boys Hostel', 'Room No: 94', 0, 0),
(95, 1, 'Boys Hostel', 'Room No: 95', 0, 0),
(96, 1, 'Boys Hostel', 'Room No: 96', 0, 0),
(97, 1, 'Boys Hostel', 'Room No: 97', 0, 0),
(98, 1, 'Boys Hostel', 'Room No: 98', 0, 0),
(99, 1, 'Boys Hostel', 'Room No: 99', 0, 0),
(100, 1, 'Boys Hostel', 'Room No: 100', 0, 0),
(101, 1, 'Boys Hostel', 'Room No: 101', 0, 0),
(102, 1, 'Boys Hostel', 'Room No: 102', 0, 0),
(103, 1, 'Boys Hostel', 'Room No: 103', 0, 0),
(104, 1, 'Boys Hostel', 'Room No: 104', 0, 0),
(105, 1, 'Boys Hostel', 'Room No: 105', 0, 0),
(106, 1, 'Boys Hostel', 'Room No: 106', 0, 0),
(107, 1, 'Boys Hostel', 'Room No: 107', 0, 0),
(108, 1, 'Boys Hostel', 'Room No: 108', 0, 0),
(109, 1, 'Boys Hostel', 'Room No: 109', 0, 0),
(110, 1, 'Boys Hostel', 'Room No: 110', 0, 0),
(111, 1, 'Boys Hostel', 'Room No: 111', 0, 0),
(112, 1, 'Boys Hostel', 'Room No: 112', 0, 0),
(113, 1, 'Boys Hostel', 'Room No: 113', 0, 0),
(114, 1, 'Boys Hostel', 'Room No: 114', 0, 0),
(115, 1, 'Boys Hostel', 'Room No: 115', 0, 0),
(116, 1, 'Boys Hostel', 'Room No: 116', 0, 0),
(117, 1, 'Boys Hostel', 'Room No: 117', 0, 0),
(118, 1, 'Boys Hostel', 'Room No: 118', 0, 0),
(119, 1, 'Boys Hostel', 'Room No: 119', 0, 0),
(120, 1, 'Boys Hostel', 'Room No: 120', 0, 0),
(121, 1, 'Boys Hostel', 'Room No: 121', 0, 0),
(122, 1, 'Boys Hostel', 'Room No: 122', 0, 0),
(123, 1, 'Boys Hostel', 'Room No: 123', 0, 0),
(124, 1, 'Boys Hostel', 'Room No: 124', 0, 0),
(125, 1, 'Boys Hostel', 'Room No: 125', 0, 0),
(126, 2, 'Girls Hostel', 'Room No: 1', 10, 10),
(127, 2, 'Girls Hostel', 'Room No: 2', 10, 10),
(128, 2, 'Girls Hostel', 'Room No: 3', 10, 10),
(129, 2, 'Girls Hostel', 'Room No: 4', 0, 0),
(130, 2, 'Girls Hostel', 'Room No: 5', 0, 0),
(131, 2, 'Girls Hostel', 'Room No: 6', 0, 0),
(132, 2, 'Girls Hostel', 'Room No: 7', 0, 0),
(133, 2, 'Girls Hostel', 'Room No: 8', 0, 0),
(134, 2, 'Girls Hostel', 'Room No: 9', 0, 0),
(135, 2, 'Girls Hostel', 'Room No: 10', 0, 0),
(136, 2, 'Girls Hostel', 'Room No: 11', 0, 0),
(137, 2, 'Girls Hostel', 'Room No: 12', 0, 0),
(138, 2, 'Girls Hostel', 'Room No: 13', 0, 0),
(139, 2, 'Girls Hostel', 'Room No: 14', 0, 0),
(140, 2, 'Girls Hostel', 'Room No: 15', 0, 0),
(141, 2, 'Girls Hostel', 'Room No: 16', 0, 0),
(142, 2, 'Girls Hostel', 'Room No: 17', 0, 0),
(143, 2, 'Girls Hostel', 'Room No: 18', 0, 0),
(144, 2, 'Girls Hostel', 'Room No: 19', 0, 0),
(145, 2, 'Girls Hostel', 'Room No: 20', 0, 0),
(146, 2, 'Girls Hostel', 'Room No: 21', 0, 0),
(147, 2, 'Girls Hostel', 'Room No: 22', 0, 0),
(148, 2, 'Girls Hostel', 'Room No: 23', 0, 0),
(149, 2, 'Girls Hostel', 'Room No: 24', 0, 0),
(150, 2, 'Girls Hostel', 'Room No: 25', 0, 0),
(151, 2, 'Girls Hostel', 'Room No: 26', 0, 0),
(152, 2, 'Girls Hostel', 'Room No: 27', 0, 0),
(153, 2, 'Girls Hostel', 'Room No: 28', 0, 0),
(154, 2, 'Girls Hostel', 'Room No: 29', 0, 0),
(155, 2, 'Girls Hostel', 'Room No: 30', 0, 0),
(156, 2, 'Girls Hostel', 'Room No: 31', 0, 0),
(157, 2, 'Girls Hostel', 'Room No: 32', 0, 0),
(158, 2, 'Girls Hostel', 'Room No: 33', 0, 0),
(159, 2, 'Girls Hostel', 'Room No: 34', 0, 0),
(160, 2, 'Girls Hostel', 'Room No: 35', 0, 0),
(161, 2, 'Girls Hostel', 'Room No: 36', 0, 0),
(162, 2, 'Girls Hostel', 'Room No: 37', 0, 0),
(163, 2, 'Girls Hostel', 'Room No: 38', 0, 0),
(164, 2, 'Girls Hostel', 'Room No: 39', 0, 0),
(165, 2, 'Girls Hostel', 'Room No: 40', 0, 0),
(166, 3, 'Teachers Dormitory', 'Room No: 1', 10, 10),
(167, 3, 'Teachers Dormitory', 'Room No: 2', 10, 10),
(168, 3, 'Teachers Dormitory', 'Room No: 3', 0, 0),
(169, 3, 'Teachers Dormitory', 'Room No: 4', 0, 0),
(170, 3, 'Teachers Dormitory', 'Room No: 5', 0, 0),
(171, 3, 'Teachers Dormitory', 'Room No: 6', 0, 0),
(172, 3, 'Teachers Dormitory', 'Room No: 7', 0, 0),
(173, 3, 'Teachers Dormitory', 'Room No: 8', 0, 0),
(174, 3, 'Teachers Dormitory', 'Room No: 9', 0, 0),
(175, 3, 'Teachers Dormitory', 'Room No: 10', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `employe`
--

CREATE TABLE `employe` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `father_name` varchar(150) NOT NULL,
  `mother_name` varchar(150) NOT NULL,
  `birth_date` varchar(100) NOT NULL,
  `sex` varchar(50) NOT NULL,
  `present_address` varchar(150) NOT NULL,
  `permanent_address` varchar(150) NOT NULL,
  `job_title_post` varchar(100) NOT NULL,
  `working_hour` varchar(20) NOT NULL,
  `salary_amount` varchar(100) NOT NULL,
  `educational_qualifation_1` varchar(300) NOT NULL,
  `educational_qualifation_2` varchar(300) NOT NULL,
  `educational_qualifation_3` varchar(300) NOT NULL,
  `educational_qualifation_4` varchar(300) NOT NULL,
  `educational_qualifation_5` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `exam_attendanc`
--

CREATE TABLE `exam_attendanc` (
  `id` int(11) NOT NULL,
  `date` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `student_id` varchar(20) NOT NULL,
  `student_title` varchar(100) NOT NULL,
  `class_id` int(11) NOT NULL,
  `roll_no` varchar(11) NOT NULL,
  `section` varchar(100) NOT NULL,
  `exam_title` varchar(150) NOT NULL,
  `exam_subject` varchar(100) NOT NULL,
  `attendance` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_attendanc`
--

INSERT INTO `exam_attendanc` (`id`, `date`, `user_id`, `student_id`, `student_title`, `class_id`, `roll_no`, `section`, `exam_title`, `exam_subject`, `attendance`) VALUES
(1, '10/06/2016', 4, '201601001', 'Benjamin D. Lampe', 1, '1', 'Section A,Section B,Section C,Section D', 'Test Exam 1', 'AMAR BANGLA BOI', 'P'),
(2, '10/06/2016', 12, '201601002', 'Rahim Hasan', 1, '2', 'Section A,Section B,Section C,Section D', 'Test Exam 1', 'AMAR BANGLA BOI', 'P'),
(3, '10/06/2016', 13, '201601003', 'Junayed Hak', 1, '3', 'Section A,Section B,Section C,Section D', 'Test Exam 1', 'AMAR BANGLA BOI', 'P'),
(4, '10/06/2016', 16, '201601004', 'Razia Akture', 1, '4', '', 'Test Exam 1', 'AMAR BANGLA BOI', 'P'),
(5, '10/06/2016', 23, '201601005', 'Polash Sarder', 1, '5', '', 'Test Exam 1', 'AMAR BANGLA BOI', 'P'),
(6, '10/06/2016', 24, '201601006', 'Sumon Akon', 1, '6', '', 'Test Exam 1', 'AMAR BANGLA BOI', 'P'),
(7, '10/06/2016', 4, '201601001', 'Benjamin D. Lampe', 1, '1', 'Section A,Section B,Section C,Section D', 'Test Exam 1', 'ENGLISH FOR TODAY', 'P'),
(8, '10/06/2016', 12, '201601002', 'Rahim Hasan', 1, '2', 'Section A,Section B,Section C,Section D', 'Test Exam 1', 'ENGLISH FOR TODAY', 'P'),
(9, '10/06/2016', 13, '201601003', 'Junayed Hak', 1, '3', 'Section A,Section B,Section C,Section D', 'Test Exam 1', 'ENGLISH FOR TODAY', 'P'),
(10, '10/06/2016', 16, '201601004', 'Razia Akture', 1, '4', '', 'Test Exam 1', 'ENGLISH FOR TODAY', 'P'),
(11, '10/06/2016', 23, '201601005', 'Polash Sarder', 1, '5', '', 'Test Exam 1', 'ENGLISH FOR TODAY', 'P'),
(12, '10/06/2016', 24, '201601006', 'Sumon Akon', 1, '6', '', 'Test Exam 1', 'ENGLISH FOR TODAY', 'P'),
(13, '10/06/2016', 4, '201601001', 'Benjamin D. Lampe', 1, '1', 'Section A,Section B,Section C,Section D', 'Test Exam 1', 'PRIMARY MATHEMATICS', 'P'),
(14, '10/06/2016', 12, '201601002', 'Rahim Hasan', 1, '2', 'Section A,Section B,Section C,Section D', 'Test Exam 1', 'PRIMARY MATHEMATICS', 'P'),
(15, '10/06/2016', 13, '201601003', 'Junayed Hak', 1, '3', 'Section A,Section B,Section C,Section D', 'Test Exam 1', 'PRIMARY MATHEMATICS', 'P'),
(16, '10/06/2016', 16, '201601004', 'Razia Akture', 1, '4', '', 'Test Exam 1', 'PRIMARY MATHEMATICS', 'P'),
(17, '10/06/2016', 23, '201601005', 'Polash Sarder', 1, '5', '', 'Test Exam 1', 'PRIMARY MATHEMATICS', 'P'),
(18, '10/06/2016', 24, '201601006', 'Sumon Akon', 1, '6', '', 'Test Exam 1', 'PRIMARY MATHEMATICS', 'P'),
(19, '08/10/2016', 25, '201603001', 'Farjana Akter', 3, '1', '', 'FInal Exam', 'AMAR BANGLA BOI', 'P'),
(20, '08/10/2016', 26, '201603002', 'Akram Hossain', 3, '2', '', 'FInal Exam', 'AMAR BANGLA BOI', 'P'),
(21, '08/10/2016', 27, '201603003', 'Alamin Saeder', 3, '3', '', 'FInal Exam', 'AMAR BANGLA BOI', 'P'),
(22, '08/10/2016', 28, '201603004', 'Sabina Sumi', 3, '4', '', 'FInal Exam', 'AMAR BANGLA BOI', 'P');

-- --------------------------------------------------------

--
-- Table structure for table `exam_grade`
--

CREATE TABLE `exam_grade` (
  `id` int(11) NOT NULL,
  `grade_name` varchar(30) NOT NULL,
  `point` varchar(4) NOT NULL,
  `number_form` varchar(5) NOT NULL,
  `number_to` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_grade`
--

INSERT INTO `exam_grade` (`id`, `grade_name`, `point`, `number_form`, `number_to`) VALUES
(1, 'F', '0', '0', '32'),
(2, 'D', '1', '33', '39'),
(3, 'C', '2', '40', '49'),
(4, 'B', '3', '50', '59'),
(5, 'A-', '3.5', '60', '69'),
(6, 'A', '4', '70', '79'),
(7, 'A+', '5', '80', '100'),
(8, 'A+', '5', '90', '100');

-- --------------------------------------------------------

--
-- Table structure for table `exam_routine`
--

CREATE TABLE `exam_routine` (
  `id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `exam_date` varchar(30) NOT NULL,
  `exam_subject` varchar(100) NOT NULL,
  `subject_code` varchar(15) NOT NULL,
  `rome_number` varchar(10) NOT NULL,
  `start_time` varchar(10) NOT NULL,
  `end_time` varchar(30) NOT NULL,
  `exam_shift` varchar(50) NOT NULL,
  `status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_routine`
--

INSERT INTO `exam_routine` (`id`, `exam_id`, `exam_date`, `exam_subject`, `subject_code`, `rome_number`, `start_time`, `end_time`, `exam_shift`, `status`) VALUES
(1, 4, '9/06/2016', 'AMAR BANGLA BOI', '101', '101', '09.00am', '10.30am', 'Morning shift', 'Result'),
(2, 4, '10/06/2016', 'ENGLISH FOR TODAY', '102', '101', '09.00am', '10.30am', 'Morning shift', 'Result'),
(3, 4, '11/06/2016', 'PRIMARY MATHEMATICS', '103', '101', '09.00am', '10.30am', 'Morning shift', 'Result'),
(4, 5, '08/10/2016', 'AMAR BANGLA BOI', '101', '101', '10.30am', '11.00am', 'Morning shift', 'Result'),
(5, 6, '10/03/2017', 'Math', 'math-01', 'A1', '9:00', '10:30', 'Morning shift', 'NoResult'),
(6, 6, '11/03/2017', 'General Science', 'english-01', 'A1', '9:00', '10:30', 'Morning shift', 'NoResult'),
(7, 6, '12/03/2017', 'Islamiat', 'science-01', 'A1', '9:00', '10:30', 'Morning shift', 'NoResult');

-- --------------------------------------------------------

--
-- Table structure for table `fee_item`
--

CREATE TABLE `fee_item` (
  `id` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fee_item`
--

INSERT INTO `fee_item` (`id`, `year`, `class_id`, `title`, `amount`) VALUES
(5, 2017, 15, 'Admission Fee', 500),
(6, 2017, 1, 'Monthly Fee', 2000),
(7, 2017, 15, 'AC Charges', 700),
(8, 2017, 15, 'Monthly Fee', 2500),
(9, 2017, 15, 'event1', 100),
(10, 2017, 16, 'event1', 200),
(11, 2017, 11, 'event1', 54),
(12, 2017, 7, 'event1', 34534);

-- --------------------------------------------------------

--
-- Table structure for table `final_result`
--

CREATE TABLE `final_result` (
  `id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `section` varchar(100) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `exam_title` varchar(100) NOT NULL,
  `student_id` varchar(20) NOT NULL,
  `student_name` varchar(100) NOT NULL,
  `total_mark` varchar(100) NOT NULL,
  `final_grade` varchar(10) NOT NULL,
  `maride_list` varchar(150) NOT NULL,
  `status` varchar(150) NOT NULL,
  `point` varchar(11) NOT NULL,
  `fail_amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `final_result`
--

INSERT INTO `final_result` (`id`, `class_id`, `section`, `exam_id`, `exam_title`, `student_id`, `student_name`, `total_mark`, `final_grade`, `maride_list`, `status`, `point`, `fail_amount`) VALUES
(1, 1, '', 4, 'Test Exam 1', '201601001', 'Benjamin D. Lampe', '249', 'A', '', 'Pass', '4.67', 0),
(2, 1, '', 4, 'Test Exam 1', '201601002', 'Rahim Hasan', '241', 'A', '', 'Pass', '4.33', 0),
(3, 1, '', 4, 'Test Exam 1', '201601003', 'Junayed Hak', '254', 'A', '', 'Pass', '4.67', 0),
(4, 1, '', 4, 'Test Exam 1', '201601004', 'Razia Akture', '239', 'A', '', 'Pass', '4.67', 0),
(5, 1, '', 4, 'Test Exam 1', '201601005', 'Polash Sarder', '230', 'A', '', 'Pass', '4.5', 0),
(6, 1, '', 4, 'Test Exam 1', '201601006', 'Sumon Akon', '234', 'A', '', 'Pass', '4.67', 0);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(3, 'student', 'This user is student''s groups member.'),
(4, 'teacher', 'This user is teacher''s groups member.'),
(5, 'parents', 'This user is parent''s groups member.'),
(6, 'accountant', 'This user is accountent''s groups member.'),
(7, 'library_man', 'The library man can manage library and library''s account information'),
(8, '4th_class_employ', ''),
(9, 'driver', '');

-- --------------------------------------------------------

--
-- Table structure for table `inven_category`
--

CREATE TABLE `inven_category` (
  `id` int(11) NOT NULL,
  `category_name` varchar(50) NOT NULL,
  `details` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inven_category`
--

INSERT INTO `inven_category` (`id`, `category_name`, `details`) VALUES
(1, 'Furniture', ''),
(2, 'Khata', ''),
(3, 'Book', '');

-- --------------------------------------------------------

--
-- Table structure for table `inve_item`
--

CREATE TABLE `inve_item` (
  `id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `category` varchar(50) NOT NULL,
  `item` varchar(200) NOT NULL,
  `quantity` int(11) NOT NULL,
  `rate` int(11) NOT NULL,
  `discount` int(11) NOT NULL,
  `total_rate` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inve_item`
--

INSERT INTO `inve_item` (`id`, `vendor_id`, `category`, `item`, `quantity`, `rate`, `discount`, `total_rate`) VALUES
(1, 1, '1', 'Chair', 40, 110, 0, 5500),
(2, 2, '1', 'Table', 20, 750, 0, 15000),
(3, 1, '2', 'Whit Khata', 1000, 15, 0, 15000),
(4, 2, '2', 'Pen', 2000, 5, 0, 9000),
(5, 1, '3', 'Story Book', 50, 220, 0, 11000);

-- --------------------------------------------------------

--
-- Table structure for table `issu_item`
--

CREATE TABLE `issu_item` (
  `id` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  `user_type` varchar(30) NOT NULL,
  `user_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `rate` int(11) NOT NULL,
  `total_price` int(11) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `issu_item`
--

INSERT INTO `issu_item` (`id`, `date`, `user_type`, `user_id`, `item_id`, `quantity`, `rate`, `total_price`, `status`) VALUES
(1, 1464818400, 'Employee', 2, 1, 10, 110, 1100, 'Due');

-- --------------------------------------------------------

--
-- Table structure for table `leave_application`
--

CREATE TABLE `leave_application` (
  `id` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `sender_title` varchar(150) NOT NULL,
  `subject` varchar(150) NOT NULL,
  `jobtype` text NOT NULL,
  `leave_start` int(11) NOT NULL,
  `leave_end` int(11) NOT NULL,
  `application_date` int(11) NOT NULL,
  `reason` varchar(500) NOT NULL,
  `cheack_by` varchar(150) NOT NULL,
  `status` text NOT NULL,
  `cheack_statuse` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `leave_application`
--

INSERT INTO `leave_application` (`id`, `year`, `sender_id`, `sender_title`, `subject`, `jobtype`, `leave_start`, `leave_end`, `application_date`, `reason`, `cheack_by`, `status`, `cheack_statuse`) VALUES
(1, 2017, 1, 'Headmaster', 'Prayer for leave of absence.', 'part', 1486252800, 1486339200, 1496534400, 'uncle death', 'Headmaster', 'Approved', 'Checked');

-- --------------------------------------------------------

--
-- Table structure for table `library_member`
--

CREATE TABLE `library_member` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `fine` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `library_member`
--

INSERT INTO `library_member` (`id`, `user_id`, `title`, `fine`) VALUES
(1, 4, 'Benjamin D. Lampe', 0),
(2, 4, 'Benjamin D. Lampe', 0),
(3, 12, 'Rahim Hasan', 0),
(4, 13, 'Junayed Hak', 0),
(5, 1, 'Headmaster', 0),
(6, 2, 'Helen K Helton', 0),
(7, 6, 'Robert D. Franco', 0),
(8, 7, 'Michael R. Kemp', 0),
(9, 8, 'Willie B. Quint', 0),
(10, 9, 'Fredrick V. Keyes', 0),
(11, 10, 'mumar abboud', 0),
(12, 11, 'Inayah Asfour', 0),
(13, 13, 'Junayed Hak', 0);

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `massage`
--

CREATE TABLE `massage` (
  `id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `receiver_id` varchar(50) NOT NULL,
  `message` varchar(1000) NOT NULL,
  `subject` varchar(200) NOT NULL,
  `read_unread` int(1) NOT NULL,
  `date` int(11) NOT NULL,
  `sender_delete` int(11) NOT NULL,
  `receiver_delete` int(11) NOT NULL,
  `class` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `massage`
--

INSERT INTO `massage` (`id`, `sender_id`, `receiver_id`, `message`, `subject`, `read_unread`, `date`, `sender_delete`, `receiver_delete`, `class`) VALUES
(1, 1, '4', '<p>This is test Message for all students in class 1. This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.</p>\\r\\n', 'This is test Message.', 0, 1465806801, 1, 1, ''),
(2, 1, '12', '<p>This is test Message for all students in class 1. This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.</p>\\r\\n', 'This is test Message.', 0, 1465806801, 1, 1, ''),
(3, 1, '13', '<p>This is test Message for all students in class 1. This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.</p>\\r\\n', 'This is test Message.', 0, 1465806801, 1, 1, ''),
(4, 1, '16', '<p>This is test Message for all students in class 1. This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.</p>\\r\\n', 'This is test Message.', 0, 1465806801, 1, 1, ''),
(5, 1, '23', '<p>This is test Message for all students in class 1. This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.</p>\\r\\n', 'This is test Message.', 0, 1465806801, 1, 1, ''),
(6, 1, '24', '<p>This is test Message for all students in class 1. This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.This is test Message for all students in class 1.</p>\\r\\n', 'This is test Message.', 0, 1465806801, 1, 1, ''),
(7, 1, '4', '<p >This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.</p>\\r\\n', 'This is test Message.', 1, 1465807087, 1, 1, ''),
(8, 1, '12', '<p >This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.</p>\\r\\n', 'This is test Message.', 0, 1465807087, 1, 1, ''),
(9, 1, '13', '<p >This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.</p>\\r\\n', 'This is test Message.', 0, 1465807087, 1, 1, ''),
(10, 1, '16', '<p >This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.</p>\\r\\n', 'This is test Message.', 0, 1465807087, 1, 1, ''),
(11, 1, '23', '<p >This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.</p>\\r\\n', 'This is test Message.', 0, 1465807087, 1, 1, ''),
(12, 1, '24', '<p >This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.This is test Message.</p>\\r\\n', 'This is test Message.', 0, 1465807087, 1, 1, ''),
(13, 1, '58', '<p><font color=\\"#848484\\" face=\\"open sans, sans-serif\\"><span >greegqergerge</span></font></p>\\n', 'testing', 1, 1496232083, 1, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `notice_board`
--

CREATE TABLE `notice_board` (
  `id` int(11) NOT NULL,
  `date` varchar(11) NOT NULL,
  `sender` varchar(50) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `notice` varchar(1000) NOT NULL,
  `receiver` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notice_board`
--

INSERT INTO `notice_board` (`id`, `date`, `sender`, `subject`, `notice`, `receiver`) VALUES
(1, '13/06/2016', 'Headmaster', 'Test notice for all users. Test notice for all users. Test notice for all users.', '<p><span style=\\"font-size:14px\\">This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. This is test notice. </span></p>\\r\\n', 'all'),
(2, '30/05/2017', 'Headmaster', 'subha jaldi aayn', '<p>ksfaskfjskdfjsdlkfjaslfjskadfjd</p>\\n\\n<p>asdfkjskdfjas;fjskdfj</p>\\n\\n<p>asdkfasjfklasjf</p>\\n\\n<p>asfkaskf</p>\\n\\n<p>asdkfasjdflkjsf;a</p>\\n', 'teacher');

-- --------------------------------------------------------

--
-- Table structure for table `parents_info`
--

CREATE TABLE `parents_info` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `parents_name` varchar(100) NOT NULL,
  `relation` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `parents_info`
--

INSERT INTO `parents_info` (`id`, `user_id`, `class_id`, `student_id`, `parents_name`, `relation`, `email`, `phone`) VALUES
(1, 14, 1, 201601001, 'John E. Williams  Deidra D. Shaw ', 'Parents', 'parents@parents.com', '+8801245852315'),
(2, 15, 1, 201601002, 'Jafor Uddin Julakha Begum', 'Parents', 'Jafor@Jafor.com', '+8801245852315'),
(3, 62, 15, 201700002, 'ahmad naveed', 'father', 'ahmad@gmail.com', '+923346053055'),
(4, 63, 15, 201700003, 'ali  rehman', 'father', 'rehmanali@gmail.com', '+923346053085');

-- --------------------------------------------------------

--
-- Table structure for table `result_action`
--

CREATE TABLE `result_action` (
  `id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `exam_title` varchar(100) NOT NULL,
  `status` varchar(20) NOT NULL,
  `publish` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `result_action`
--

INSERT INTO `result_action` (`id`, `class_id`, `exam_id`, `exam_title`, `status`, `publish`) VALUES
(1, 1, 4, 'Test Exam 1', 'Complete', 'Publish');

-- --------------------------------------------------------

--
-- Table structure for table `result_shit`
--

CREATE TABLE `result_shit` (
  `id` int(11) NOT NULL,
  `exam_title` varchar(100) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `section` varchar(100) NOT NULL,
  `student_name` varchar(100) NOT NULL,
  `student_id` varchar(100) NOT NULL,
  `exam_subject` varchar(100) NOT NULL,
  `mark` varchar(10) NOT NULL,
  `point` varchar(5) NOT NULL,
  `grade` varchar(5) NOT NULL,
  `roll_number` int(11) NOT NULL,
  `result` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `result_shit`
--

INSERT INTO `result_shit` (`id`, `exam_title`, `exam_id`, `class_id`, `section`, `student_name`, `student_id`, `exam_subject`, `mark`, `point`, `grade`, `roll_number`, `result`) VALUES
(1, 'Test Exam 1', 4, 1, '', 'Benjamin D. Lampe', '201601001', 'AMAR BANGLA BOI', '87', '5', 'A+', 1, 'Pass'),
(2, 'Test Exam 1', 4, 1, '', 'Rahim Hasan', '201601002', 'AMAR BANGLA BOI', '78', '4', 'A', 2, 'Pass'),
(3, 'Test Exam 1', 4, 1, '', 'Junayed Hak', '201601003', 'AMAR BANGLA BOI', '89', '5', 'A+', 3, 'Pass'),
(4, 'Test Exam 1', 4, 1, '', 'Razia Akture', '201601004', 'AMAR BANGLA BOI', '82', '5', 'A+', 4, 'Pass'),
(5, 'Test Exam 1', 4, 1, '', 'Polash Sarder', '201601005', 'AMAR BANGLA BOI', '81', '5', 'A+', 5, 'Pass'),
(6, 'Test Exam 1', 4, 1, '', 'Sumon Akon', '201601006', 'AMAR BANGLA BOI', '80', '5', 'A+', 6, 'Pass'),
(7, 'Test Exam 1', 4, 1, '', 'Benjamin D. Lampe', '201601001', 'ENGLISH FOR TODAY', '75', '4', 'A', 1, 'Pass'),
(8, 'Test Exam 1', 4, 1, '', 'Rahim Hasan', '201601002', 'ENGLISH FOR TODAY', '85', '5', 'A+', 2, 'Pass'),
(9, 'Test Exam 1', 4, 1, '', 'Junayed Hak', '201601003', 'ENGLISH FOR TODAY', '76', '4', 'A', 3, 'Pass'),
(10, 'Test Exam 1', 4, 1, '', 'Razia Akture', '201601004', 'ENGLISH FOR TODAY', '82', '5', 'A+', 4, 'Pass'),
(11, 'Test Exam 1', 4, 1, '', 'Polash Sarder', '201601005', 'ENGLISH FOR TODAY', '81', '5', 'A+', 5, 'Pass'),
(12, 'Test Exam 1', 4, 1, '', 'Sumon Akon', '201601006', 'ENGLISH FOR TODAY', '74', '4', 'A', 6, 'Pass'),
(13, 'Test Exam 1', 4, 1, '', 'Benjamin D. Lampe', '201601001', 'PRIMARY MATHEMATICS', '87', '5', 'A+', 1, 'Pass'),
(14, 'Test Exam 1', 4, 1, '', 'Rahim Hasan', '201601002', 'PRIMARY MATHEMATICS', '78', '4', 'A', 2, 'Pass'),
(15, 'Test Exam 1', 4, 1, '', 'Junayed Hak', '201601003', 'PRIMARY MATHEMATICS', '89', '5', 'A+', 3, 'Pass'),
(16, 'Test Exam 1', 4, 1, '', 'Razia Akture', '201601004', 'PRIMARY MATHEMATICS', '75', '4', 'A', 4, 'Pass'),
(17, 'Test Exam 1', 4, 1, '', 'Polash Sarder', '201601005', 'PRIMARY MATHEMATICS', '68', '3.5', 'A-', 5, 'Pass'),
(18, 'Test Exam 1', 4, 1, '', 'Sumon Akon', '201601006', 'PRIMARY MATHEMATICS', '80', '5', 'A+', 6, 'Pass'),
(19, 'FInal Exam', 5, 3, '', 'Farjana Akter', '201603001', 'AMAR BANGLA BOI', '84', '5', 'A+', 1, 'Pass'),
(20, 'FInal Exam', 5, 3, '', 'Akram Hossain', '201603002', 'AMAR BANGLA BOI', '75', '4', 'A', 2, 'Pass'),
(21, 'FInal Exam', 5, 3, '', 'Alamin Saeder', '201603003', 'AMAR BANGLA BOI', '66', '3', 'B', 3, 'Pass'),
(22, 'FInal Exam', 5, 3, '', 'Sabina Sumi', '201603004', 'AMAR BANGLA BOI', '68', '3', 'B', 4, 'Pass'),
(23, 'FInal Exam', 5, 3, '', 'Farjana Akter', '201603001', 'AMAR BANGLA BOI', '84', '5', 'A+', 1, 'Pass'),
(24, 'FInal Exam', 5, 3, '', 'Akram Hossain', '201603002', 'AMAR BANGLA BOI', '75', '4', 'A', 2, 'Pass'),
(25, 'FInal Exam', 5, 3, '', 'Alamin Saeder', '201603003', 'AMAR BANGLA BOI', '66', '3', 'B', 3, 'Pass'),
(26, 'FInal Exam', 5, 3, '', 'Sabina Sumi', '201603004', 'AMAR BANGLA BOI', '68', '3', 'B', 4, 'Pass');

-- --------------------------------------------------------

--
-- Table structure for table `result_submition_info`
--

CREATE TABLE `result_submition_info` (
  `id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `section` varchar(100) NOT NULL,
  `exam_title` varchar(150) NOT NULL,
  `date` varchar(50) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `submited` varchar(50) NOT NULL,
  `teacher` varchar(100) NOT NULL,
  `exam_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `result_submition_info`
--

INSERT INTO `result_submition_info` (`id`, `class_id`, `section`, `exam_title`, `date`, `subject`, `submited`, `teacher`, `exam_id`) VALUES
(1, 1, '', 'Test Exam 1', '10/06/2016', 'AMAR BANGLA BOI', '1', 'Willie B. Quint', 4),
(2, 1, '', 'Test Exam 1', '10/06/2016', 'ENGLISH FOR TODAY', '1', 'Fredrick V. Keyes', 4),
(3, 1, '', 'Test Exam 1', '11/06/2016', 'PRIMARY MATHEMATICS', '1', 'Willie B. Quint', 4),
(4, 3, '', 'FInal Exam', '08/10/2016', 'AMAR BANGLA BOI', '1', 'Headmaster', 5),
(5, 3, '', 'FInal Exam', '08/10/2016', 'AMAR BANGLA BOI', '1', 'Headmaster', 5),
(6, 11, '', 'kACHAY pAPER', '30/05/2017', '', '0', 'Headmaster', 6);

-- --------------------------------------------------------

--
-- Table structure for table `role_based_access`
--

CREATE TABLE `role_based_access` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(1) NOT NULL,
  `das_top_info` int(1) NOT NULL,
  `das_grab_chart` int(1) NOT NULL,
  `das_class_info` int(1) NOT NULL,
  `das_message` int(1) NOT NULL,
  `das_employ_attend` int(1) NOT NULL,
  `das_notice` int(1) NOT NULL,
  `das_calender` int(1) NOT NULL,
  `admission` int(1) NOT NULL,
  `all_student_info` int(1) NOT NULL,
  `stud_edit_delete` int(1) NOT NULL,
  `stu_own_info` int(1) NOT NULL,
  `teacher_info` int(1) NOT NULL,
  `add_teacher` int(1) NOT NULL,
  `teacher_details` int(1) NOT NULL,
  `teacher_edit_delete` int(1) NOT NULL,
  `all_parents_info` int(1) NOT NULL,
  `own_parents_info` int(1) NOT NULL,
  `make_parents_id` int(1) NOT NULL,
  `parents_edit_dlete` int(1) NOT NULL,
  `add_employee` int(1) NOT NULL,
  `employee_list` int(1) NOT NULL,
  `employ_attendance` int(1) NOT NULL,
  `empl_atte_view` int(1) NOT NULL,
  `add_new_class` int(1) NOT NULL,
  `all_class_info` int(1) NOT NULL,
  `class_details` int(1) NOT NULL,
  `class_delete` int(1) NOT NULL,
  `class_promotion` int(1) NOT NULL,
  `add_class_routine` int(1) NOT NULL,
  `own_class_routine` int(1) NOT NULL,
  `all_class_routine` int(1) NOT NULL,
  `rutin_edit_delete` int(1) NOT NULL,
  `attendance_preview` int(1) NOT NULL,
  `take_studence_atten` int(1) NOT NULL,
  `edit_student_atten` int(1) NOT NULL,
  `add_subject` int(1) NOT NULL,
  `all_subject` int(1) NOT NULL,
  `assin_optio_sub` int(1) NOT NULL,
  `make_suggestion` int(1) NOT NULL,
  `all_suggestion` int(1) NOT NULL,
  `own_suggestion` int(1) NOT NULL,
  `add_exam_gread` int(1) NOT NULL,
  `exam_gread` int(1) NOT NULL,
  `gread_edit_dele` int(1) NOT NULL,
  `add_exam_routin` int(1) NOT NULL,
  `all_exam_routine` int(1) NOT NULL,
  `own_exam_routine` int(1) NOT NULL,
  `exam_attend_preview` int(1) NOT NULL,
  `approve_result` int(1) NOT NULL,
  `view_result` int(1) NOT NULL,
  `all_mark_sheet` int(1) NOT NULL,
  `own_mark_sheet` int(1) NOT NULL,
  `take_exam_attend` int(1) NOT NULL,
  `change_exam_attendance` int(1) NOT NULL,
  `make_result` int(1) NOT NULL,
  `add_category` int(1) NOT NULL,
  `all_category` int(1) NOT NULL,
  `edit_delete_category` int(1) NOT NULL,
  `add_books` int(1) NOT NULL,
  `all_books` int(1) NOT NULL,
  `edit_delete_books` int(1) NOT NULL,
  `add_library_mem` int(1) NOT NULL,
  `memb_list` int(1) NOT NULL,
  `issu_return` int(1) NOT NULL,
  `add_dormitories` int(1) NOT NULL,
  `add_set_dormi` int(1) NOT NULL,
  `set_member_bed` int(1) NOT NULL,
  `dormi_report` int(1) NOT NULL,
  `add_transport` int(1) NOT NULL,
  `all_transport` int(1) NOT NULL,
  `transport_edit_dele` int(1) NOT NULL,
  `add_account_title` int(1) NOT NULL,
  `edit_dele_acco` int(1) NOT NULL,
  `trensection` int(1) NOT NULL,
  `fee_collection` int(1) NOT NULL,
  `all_slips` int(1) NOT NULL,
  `own_slip` int(1) NOT NULL,
  `slip_edit_delete` int(1) NOT NULL,
  `pay_salary` int(1) NOT NULL,
  `creat_notice` int(1) NOT NULL,
  `send_message` int(1) NOT NULL,
  `vendor` int(1) NOT NULL,
  `delet_vendor` int(1) NOT NULL,
  `add_inv_cat` int(1) NOT NULL,
  `inve_item` int(1) NOT NULL,
  `delete_inve_ite` int(1) NOT NULL,
  `delete_inv_cat` int(1) NOT NULL,
  `inve_issu` int(1) NOT NULL,
  `delete_inven_issu` int(1) NOT NULL,
  `check_leav_appli` int(1) NOT NULL,
  `setting_accounts` int(1) NOT NULL,
  `other_setting` int(1) NOT NULL,
  `front_setings` int(1) NOT NULL,
  `set_optional` int(1) NOT NULL,
  `setting_manage_user` int(1) NOT NULL,
  `admin_info` int(1) NOT NULL DEFAULT '0',
  `add_admin` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_based_access`
--

INSERT INTO `role_based_access` (`id`, `user_id`, `group_id`, `das_top_info`, `das_grab_chart`, `das_class_info`, `das_message`, `das_employ_attend`, `das_notice`, `das_calender`, `admission`, `all_student_info`, `stud_edit_delete`, `stu_own_info`, `teacher_info`, `add_teacher`, `teacher_details`, `teacher_edit_delete`, `all_parents_info`, `own_parents_info`, `make_parents_id`, `parents_edit_dlete`, `add_employee`, `employee_list`, `employ_attendance`, `empl_atte_view`, `add_new_class`, `all_class_info`, `class_details`, `class_delete`, `class_promotion`, `add_class_routine`, `own_class_routine`, `all_class_routine`, `rutin_edit_delete`, `attendance_preview`, `take_studence_atten`, `edit_student_atten`, `add_subject`, `all_subject`, `assin_optio_sub`, `make_suggestion`, `all_suggestion`, `own_suggestion`, `add_exam_gread`, `exam_gread`, `gread_edit_dele`, `add_exam_routin`, `all_exam_routine`, `own_exam_routine`, `exam_attend_preview`, `approve_result`, `view_result`, `all_mark_sheet`, `own_mark_sheet`, `take_exam_attend`, `change_exam_attendance`, `make_result`, `add_category`, `all_category`, `edit_delete_category`, `add_books`, `all_books`, `edit_delete_books`, `add_library_mem`, `memb_list`, `issu_return`, `add_dormitories`, `add_set_dormi`, `set_member_bed`, `dormi_report`, `add_transport`, `all_transport`, `transport_edit_dele`, `add_account_title`, `edit_dele_acco`, `trensection`, `fee_collection`, `all_slips`, `own_slip`, `slip_edit_delete`, `pay_salary`, `creat_notice`, `send_message`, `vendor`, `delet_vendor`, `add_inv_cat`, `inve_item`, `delete_inve_ite`, `delete_inv_cat`, `inve_issu`, `delete_inven_issu`, `check_leav_appli`, `setting_accounts`, `other_setting`, `front_setings`, `set_optional`, `setting_manage_user`, `admin_info`, `add_admin`) VALUES
(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(65, 70, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0),
(66, 71, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0),
(67, 72, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0),
(68, 73, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0),
(69, 74, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0),
(70, 75, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0),
(71, 76, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0),
(72, 77, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0),
(73, 78, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `salary`
--

CREATE TABLE `salary` (
  `id` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  `month` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `employ_title` varchar(100) NOT NULL,
  `total_amount` int(11) NOT NULL,
  `method` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `salary`
--

INSERT INTO `salary` (`id`, `year`, `date`, `month`, `user_id`, `employ_title`, `total_amount`, `method`) VALUES
(1, 2017, 1496102400, 'February', 1, 'Headmaster', 17500, 'Cash'),
(2, 2017, 1496102400, 'March', 1, 'Headmaster', 17500, 'Check'),
(3, 2017, 1497218400, 'January', 66, 'sohail ahmad', 11320, 'Cash');

-- --------------------------------------------------------

--
-- Table structure for table `set_salary`
--

CREATE TABLE `set_salary` (
  `id` int(11) NOT NULL,
  `year` int(5) NOT NULL,
  `employ_user_id` int(11) NOT NULL,
  `employe_title` varchar(100) NOT NULL,
  `job_post` varchar(50) NOT NULL,
  `basic` int(11) NOT NULL,
  `treatment` int(11) NOT NULL,
  `increased` int(11) NOT NULL,
  `others` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `month` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `set_salary`
--

INSERT INTO `set_salary` (`id`, `year`, `employ_user_id`, `employe_title`, `job_post`, `basic`, `treatment`, `increased`, `others`, `total`, `month`) VALUES
(1, 2016, 1, 'Headmaster', 'Headmaster', 15000, 2000, 0, 500, 17500, 3),
(2, 2017, 66, 'sohail ahmad', 'Accountant', 10000, 1200, 120, 0, 11320, 1);

-- --------------------------------------------------------

--
-- Table structure for table `slip`
--

CREATE TABLE `slip` (
  `id` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `month` varchar(10) NOT NULL,
  `date` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `item_id` varchar(100) NOT NULL,
  `amount` int(11) NOT NULL,
  `dues` int(11) NOT NULL,
  `advance` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `paid` int(11) NOT NULL,
  `balance` int(11) NOT NULL,
  `edit_by` varchar(100) NOT NULL,
  `status` text NOT NULL,
  `mathod` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slip`
--

INSERT INTO `slip` (`id`, `year`, `month`, `date`, `class_id`, `student_id`, `item_id`, `amount`, `dues`, `advance`, `total`, `paid`, `balance`, `edit_by`, `status`, `mathod`) VALUES
(1, 2017, 'June', 0, 1, 201601001, '6', 2000, 0, 0, 2000, 0, 0, '', 'Unpaid', ''),
(2, 2017, 'June', 0, 1, 201601002, '6', 2000, 0, 0, 2000, 0, 0, '', 'Unpaid', ''),
(3, 2017, 'June', 0, 1, 201601003, '6', 2000, 0, 0, 2000, 0, 0, '', 'Unpaid', ''),
(4, 2017, 'June', 0, 1, 201601004, '6', 2000, 0, 0, 2000, 0, 0, '', 'Unpaid', ''),
(5, 2017, 'June', 0, 1, 201601005, '6', 2000, 0, 0, 2000, 0, 0, '', 'Unpaid', ''),
(6, 2017, 'June', 0, 1, 201601006, '6', 2000, 0, 0, 2000, 0, 0, '', 'Unpaid', ''),
(7, 2017, 'June', 0, 15, 201700001, '5,7,8,9', 3800, 0, 0, 3800, 0, 0, '', 'Unpaid', ''),
(8, 2017, 'June', 0, 15, 201700002, '5,7,8,9', 3800, 0, 0, 3800, 0, 0, '', 'Unpaid', ''),
(9, 2017, 'June', 0, 15, 201700003, '5,7,8,9', 3800, 0, 0, 3800, 0, 0, '', 'Unpaid', '');

-- --------------------------------------------------------

--
-- Table structure for table `student_info`
--

CREATE TABLE `student_info` (
  `id` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `student_id` varchar(20) NOT NULL,
  `roll_number` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `student_nam` varchar(100) NOT NULL,
  `farther_name` varchar(150) NOT NULL,
  `mother_name` varchar(150) NOT NULL,
  `birth_date` varchar(100) NOT NULL,
  `sex` varchar(30) NOT NULL,
  `present_address` varchar(300) NOT NULL,
  `permanent_address` varchar(300) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `father_occupation` varchar(150) NOT NULL,
  `father_incom_range` varchar(100) NOT NULL,
  `mother_occupation` varchar(100) NOT NULL,
  `student_photo` varchar(200) NOT NULL,
  `last_class_certificate` text NOT NULL,
  `t_c` text NOT NULL,
  `national_birth_certificate` text NOT NULL,
  `academic_transcription` text NOT NULL,
  `testimonial` text NOT NULL,
  `documents_info` varchar(500) NOT NULL,
  `starting_year` int(11) NOT NULL,
  `transfer_year` int(11) NOT NULL,
  `transfer_to` text NOT NULL,
  `transfer_reason` text NOT NULL,
  `tc_appli_approved_by` text NOT NULL,
  `passing_year` int(11) NOT NULL,
  `compleat_level` text NOT NULL,
  `registration_number` text NOT NULL,
  `certificates_status` text NOT NULL,
  `admission_year` int(11) NOT NULL,
  `admission_class` varchar(100) NOT NULL,
  `admission_roll` int(5) NOT NULL,
  `admission_form_no` int(11) NOT NULL,
  `admission_test_result` int(11) NOT NULL,
  `tc_form` varchar(150) NOT NULL,
  `blood` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subjects_mark`
--

CREATE TABLE `subjects_mark` (
  `id` int(11) NOT NULL,
  `exam_title` varchar(100) NOT NULL,
  `class` varchar(100) NOT NULL,
  `section` varchar(100) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `student_id` varchar(20) NOT NULL,
  `student_name` varchar(100) NOT NULL,
  `roll_number` int(11) NOT NULL,
  `mark` int(11) NOT NULL,
  `grade` varchar(30) NOT NULL,
  `statud` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `suggestion`
--

CREATE TABLE `suggestion` (
  `id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `author_name` varchar(150) NOT NULL,
  `class` varchar(20) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `suggestion_title` varchar(150) NOT NULL,
  `suggestion` varchar(2500) NOT NULL,
  `date` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suggestion`
--

INSERT INTO `suggestion` (`id`, `author_id`, `author_name`, `class`, `subject`, `suggestion_title`, `suggestion`, `date`) VALUES
(2, 8, 'Willie B. Quint', 'Class 1', '', 'This is test suggestion.', '<p>This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.This is test suggestion.</p>\\r\\n', 1466152654),
(3, 1, 'Headmaster', 'KG1', '', 'Exam Result', '<p>bachay saii ni parh rhay,,sdafdfasfasdf</p>\\n\\n<p>sdfasfasdfasfa</p>\\n\\n<p>sdadfsdafasdfasfsdfasfasfasdfsdfafasdfsf</p>\\n', 1496143529);

-- --------------------------------------------------------

--
-- Table structure for table `teachers_info`
--

CREATE TABLE `teachers_info` (
  `id` int(11) NOT NULL,
  `user_id` varchar(30) NOT NULL,
  `fullname` varchar(100) NOT NULL,
  `farther_name` varchar(150) NOT NULL,
  `mother_name` varchar(150) NOT NULL,
  `birth_date` varchar(150) NOT NULL,
  `sex` varchar(30) NOT NULL,
  `present_address` varchar(300) NOT NULL,
  `permanent_address` varchar(300) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `subject` varchar(150) NOT NULL,
  `position` varchar(150) NOT NULL,
  `working_hour` varchar(50) NOT NULL,
  `educational_qualification_1` varchar(500) NOT NULL,
  `educational_qualification_2` varchar(500) NOT NULL,
  `educational_qualification_3` varchar(500) NOT NULL,
  `educational_qualification_4` varchar(500) NOT NULL,
  `educational_qualification_5` varchar(500) NOT NULL,
  `teachers_photo` varchar(200) NOT NULL,
  `cv` varchar(30) NOT NULL,
  `educational_certificat` varchar(30) NOT NULL,
  `exprieance_certificatte` varchar(30) NOT NULL,
  `files_info` varchar(500) NOT NULL,
  `index_no` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teachers_info`
--

INSERT INTO `teachers_info` (`id`, `user_id`, `fullname`, `farther_name`, `mother_name`, `birth_date`, `sex`, `present_address`, `permanent_address`, `phone`, `subject`, `position`, `working_hour`, `educational_qualification_1`, `educational_qualification_2`, `educational_qualification_3`, `educational_qualification_4`, `educational_qualification_5`, `teachers_photo`, `cv`, `educational_certificat`, `exprieance_certificatte`, `files_info`, `index_no`) VALUES
(1, '8', 'Willie B. Quint', 'Kevin A. Robledo', 'Mary T. McQuay', '12/05/1956', 'Male', '3133 Pointe Lane\\r\\nHollywood, FL 33020', '3133 Pointe Lane\\r\\nHollywood, FL 33020', '+8801245852315', 'English', 'Assistant Headmaster', 'Full time', 'SSC,Test School,A+,1978', 'HSC,Test College,A+,1980', 'Graduation ( Test ),Test University,A,1984', 'Masters Degree,Test University,A,1986', '', 'ac197cff181d9c58027800912c3e0855.png', 'submited', 'submited', 'submited', 'Teacher 2016', '12004'),
(2, '9', 'Fredrick V. Keyes', 'Anthony T. Andrews', 'Mary J. Dahl', '20/12/1970', 'Male', '712 Beechwood Drive\\r\\nChurchville, MD 21028 ', '712 Beechwood Drive\\r\\nChurchville, MD 21028 ', '+8801245852315', 'Mathematics', 'Senior Teacher', 'Full time', 'SSC,Test School,A+,1988', 'HSC,Test College,A,1990', 'Graduation ( Test ),Test University,A,1994', 'Masters Degree,Test University,A,1996', '', '0f8649af98ca9c0e85957db7e9778191.png', 'submited', 'submited', 'submited', 'Teacher 2016', '12006'),
(3, '10', 'mumar abboud', 'Hadi Shakir Essa', 'Yamha Dhakiyah Mikhail', '11/11/1980', 'Male', '71 City Walls Rd\\\\\\\\r\\\\\\\\nCLOCK FACE\\\\\\\\r\\\\\\\\nWA9 6BG', '71 City Walls Rd\\\\\\\\r\\\\\\\\nCLOCK FACE\\\\\\\\r\\\\\\\\nWA9 6BG', '+8801245852315', 'Bangla ', 'Teacher', 'Full time', 'SSC,Test School,A+,1998', 'HSC,Test College,A,2000', 'Graduation ( Test ),Test University,A,2004', 'Masters Degree,Test University,B,2006', '', '3566d34fc7ce565ba8841d0eaf537b28.png', 'submited', 'submited', 'submited', 'Teacher 2016', '12051'),
(4, '11', 'Inayah Asfour', 'Fatin Husayn', 'Rukan Habeeba', '12/12/1980', 'Female', '31 Clasper Way\\r\\nHEST BANK\\r\\nLA2 2HF ', '31 Clasper Way\\r\\nHEST BANK\\r\\nLA2 2HF ', '+8801245852315', 'Science', 'Teacher', 'Full time', 'SSC,Test School,A+,1998', 'HSC,Test College,A+,2000', 'Graduation ( Test ),Test University,A,2004', 'Masters Degree,Test University,A,2006', '', 'f342f5b3412a5c4be681878ff0462e09.png', 'submited', 'submited', 'submited', 'Teacher 2016', '12056'),
(5, '58', 'Mudasir Murtaza', 'Murtaza', 'abc', '10/10/1947', 'Male', 'Cybex Company', 'Cybex Company', '+923336308419', 'Math', 'Headmaster', 'Full time', 'MBA,BZU,3.7,1969', '', '', '', '', '00264ab08cddb9c1f522b7ee3c9040d4.jpg', 'submited', 'submited', 'submited', '12131231', '1'),
(6, '64', 'Mr. Raheel naeem', 'naeem bakhtawar', 'neelam', '05/06/1988', 'Male', 'walton road lahore cantt', 'walton road lahore cantt', '+923346053095', 'Maths', 'Senior Teacher', 'Full time', 'Msc,Pu lahore,4.00/4.00,2013', 'Bsc,Pu lahore,3.9/4.00,2011', 'fsc,ahmad college lahore,903,2009', '', '', '8f77c8261d6698583823ceb647b50901.jpg', 'submited', 'submited', 'submited', 'file 215151', '56557');

-- --------------------------------------------------------

--
-- Table structure for table `teacher_attendance`
--

CREATE TABLE `teacher_attendance` (
  `id` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  `employ_id` int(11) NOT NULL,
  `employ_title` varchar(150) NOT NULL,
  `present_or_absent` text NOT NULL,
  `attend_time` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teacher_attendance`
--

INSERT INTO `teacher_attendance` (`id`, `year`, `date`, `employ_id`, `employ_title`, `present_or_absent`, `attend_time`) VALUES
(1, 2016, 1464991200, 1, 'Headmaster', 'Present', '02:33 pm'),
(2, 2016, 1464991200, 2, 'Helen K Helton', 'Present', '02:34 pm'),
(3, 2016, 1464991200, 6, 'Robert D. Franco', 'Present', '02:34 pm'),
(4, 2016, 1464991200, 7, 'Michael R. Kemp', 'Present', '02:34 pm'),
(5, 2016, 1464991200, 8, 'Willie B. Quint', 'Absent', ''),
(6, 2016, 1464991200, 9, 'Fredrick V. Keyes', 'Present', '02:34 pm'),
(7, 2016, 1464991200, 10, 'mumar abboud', 'Absent', ''),
(8, 2016, 1464991200, 11, 'Inayah Asfour', 'Present', '02:34 pm'),
(9, 2016, 1466287200, 1, 'Headmaster', 'Present', '11:46 pm'),
(10, 2016, 1466287200, 2, 'Helen K Helton', 'Absent', ''),
(11, 2016, 1466287200, 6, 'Robert D. Franco', 'Present', '11:47 pm'),
(12, 2016, 1466287200, 7, 'Michael R. Kemp', 'Present', '11:47 pm'),
(13, 2016, 1466287200, 8, 'Willie B. Quint', 'Absent', ''),
(14, 2016, 1466287200, 9, 'Fredrick V. Keyes', 'Present', '11:47 pm'),
(15, 2016, 1466287200, 10, 'mumar abboud', 'Absent', ''),
(16, 2016, 1466287200, 11, 'Inayah Asfour', 'Present', '11:47 pm'),
(17, 2016, 1472767200, 1, 'Headmaster', 'Absent', ''),
(18, 2016, 1472767200, 2, 'Helen K Helton', 'Absent', ''),
(19, 2016, 1472767200, 6, 'Robert D. Franco', 'Absent', ''),
(20, 2016, 1472767200, 7, 'Michael R. Kemp', 'Absent', ''),
(21, 2016, 1472767200, 8, 'Willie B. Quint', 'Absent', ''),
(22, 2016, 1472767200, 9, 'Fredrick V. Keyes', 'Absent', ''),
(23, 2016, 1472767200, 10, 'mumar abboud', 'Absent', ''),
(24, 2016, 1472767200, 11, 'Inayah Asfour', 'Absent', '');

-- --------------------------------------------------------

--
-- Table structure for table `transection`
--

CREATE TABLE `transection` (
  `id` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  `acco_id` int(11) NOT NULL,
  `category` varchar(10) NOT NULL,
  `amount` int(11) NOT NULL,
  `balance` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transport`
--

CREATE TABLE `transport` (
  `id` int(11) NOT NULL,
  `rout_title` varchar(200) NOT NULL,
  `start_end` varchar(300) NOT NULL,
  `vicles_amount` varchar(20) NOT NULL,
  `descriptions` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `userinfo`
--

CREATE TABLE `userinfo` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `parient_id` int(11) DEFAULT NULL,
  `full_name` varchar(50) NOT NULL,
  `farther_name` varchar(50) NOT NULL,
  `mother_name` varchar(50) NOT NULL,
  `birth_date` varchar(15) NOT NULL,
  `sex` varchar(10) NOT NULL,
  `present_address` varchar(200) NOT NULL,
  `permanent_address` varchar(200) NOT NULL,
  `working_hour` varchar(30) NOT NULL,
  `educational_qualification_1` varchar(200) NOT NULL,
  `educational_qualification_2` varchar(200) NOT NULL,
  `educational_qualification_3` varchar(200) NOT NULL,
  `educational_qualification_4` varchar(200) NOT NULL,
  `educational_qualification_5` varchar(200) NOT NULL,
  `users_photo` varchar(200) NOT NULL,
  `cv` varchar(30) NOT NULL,
  `educational_certificat` varchar(30) NOT NULL,
  `exprieance_certificatte` varchar(30) NOT NULL,
  `files_info` varchar(30) NOT NULL,
  `phone` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userinfo`
--

INSERT INTO `userinfo` (`id`, `user_id`, `group_id`, `parient_id`, `full_name`, `farther_name`, `mother_name`, `birth_date`, `sex`, `present_address`, `permanent_address`, `working_hour`, `educational_qualification_1`, `educational_qualification_2`, `educational_qualification_3`, `educational_qualification_4`, `educational_qualification_5`, `users_photo`, `cv`, `educational_certificat`, `exprieance_certificatte`, `files_info`, `phone`) VALUES
(7, 76, 1, 1, 'Muhammad Junaid ', '', '', '', '', 'Lahore Branch', '', '', '', '', '', '', '', '82960d7da953ed3906185599fd6f2200.jpg', '', '', '', '', '+92333333'),
(8, 77, 1, 1, 'sohail ahmad', 'sfdsf', '', '', '', 'dfsdfs', '', '', '', '', '', '', '', '843c459f5731228424c2e91d0113ac26.png', '', '', '', '', '+9233333'),
(9, 78, 1, 1, 'abc abc', 'CT CABBy', '', '', '', 'Lahore , Walton', '', '', '', '', '', '', '', 'ec16bdcfbced47005edddd3efa3fe1c3.jpg', '', '', '', '', '+922323333');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `is_super_user` tinyint(1) NOT NULL DEFAULT '0',
  `parient_id` int(11) DEFAULT NULL,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `profile_image` varchar(100) NOT NULL,
  `user_status` text NOT NULL,
  `leave_status` varchar(15) NOT NULL,
  `leave_start` int(11) NOT NULL,
  `leave_end` int(11) NOT NULL,
  `salary` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `is_super_user`, `parient_id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `phone`, `profile_image`, `user_status`, `leave_status`, `leave_start`, `leave_end`, `salary`) VALUES
(1, 1, 0, '127.0.0.1', 'Headmaster', '$2y$08$qBQ/MzJzXyil0yuVM.s1XewJerIMCntwxez/Jfs3x/xwxFnkKWo2y', NULL, 'admin@admin.com', NULL, 'HBj4C30st5pOHbjpHojzGu4667ad49e75655b131', 1420113369, 'IcD7gVAwU5DDX4jTuWOVXe', 1268889823, 1510667245, 1, 'Kermit J.', 'Jackson', '123456789', 'admin.png', 'Employee', 'Leave', 1486252800, 1486339200, 1),
(76, 0, 1, '::1', 'Muhammad Junaid ', '$2y$08$ag.1ewvaj.2EDinzhBVH5.t8RGplciLx3ygJ7kew3wDRTkwQ0JJGe', NULL, 'junaid@gmail.com', NULL, NULL, NULL, NULL, 1510663061, 1510663323, 1, 'Muhammad', 'Junaid ', '+92333333', '82960d7da953ed3906185599fd6f2200.jpg', 'Employee', 'Available', 0, 0, 0),
(77, 0, 1, '::1', 'sohail ahmad', '$2y$08$t9XzQeNXdfoVYR0hgiHe5ObGfk8K83jhFmtlPVFlC0Rwifen0Sr46', NULL, 'sohail9689@gmail.com', NULL, NULL, NULL, NULL, 1510663306, 1510668389, 1, 'sohail', 'ahmad', '+9233333', '843c459f5731228424c2e91d0113ac26.png', 'Employee', 'Available', 0, 0, 0),
(78, 0, 1, '::1', 'abc abc', '$2y$08$gu9/zn0CpqFdJI1hc3JOC.aoM6tyMw1Yi5LoTJyvvpxcA89k/UpYO', NULL, 'abc@3gmail.com', NULL, NULL, NULL, NULL, 1510664636, 1510664636, 1, 'abc', 'abc', '+922323333', 'ec16bdcfbced47005edddd3efa3fe1c3.jpg', 'Employee', 'Available', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(76, 76, 1),
(77, 77, 1),
(78, 78, 1);

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

CREATE TABLE `vendors` (
  `id` int(11) NOT NULL,
  `company_name` varchar(150) NOT NULL,
  `company_phone` varchar(15) NOT NULL,
  `company_email` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `cp_name` varchar(150) NOT NULL,
  `cp_address` varchar(200) NOT NULL,
  `cp_phone` varchar(15) NOT NULL,
  `bank_name` varchar(150) NOT NULL,
  `branch_name` varchar(15) NOT NULL,
  `account_no` varchar(30) NOT NULL,
  `ifsc_code` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendors`
--

INSERT INTO `vendors` (`id`, `company_name`, `company_phone`, `company_email`, `country`, `state`, `city`, `cp_name`, `cp_address`, `cp_phone`, `bank_name`, `branch_name`, `account_no`, `ifsc_code`) VALUES
(1, 'Roize Ltd.', '01245367', 'test@gmail.com', 'Bangladesh', '', 'Dhaka', 'Roize Uddin', 'Contact,person,test address', '11223344556', 'DBBL LTD.', 'Gulshan1', '123456789425214', ''),
(2, 'Moin Group Ltd.', '01245367', 'moin@gmail.com', 'Bangladesh', '', 'Dhaka', 'Moin Mia', 'Contact,person,test address', '11223344556', 'DBBL LTD.', 'Gulshan1', '123456789425214', ''),
(3, 'Abu Daout', '01245367', 'daout@gmail.com', 'Bangladesh', '', 'Dhaka', 'Moin Mia', 'Contact,person,test address', '11223344556', 'DBBL LTD.', 'Gulshan1', '123456789425214', ''),
(4, 'Bablu Furniture Ltd.', '012453671425', 'bablu@gmail.com', 'Bangladesh', '', 'Dhaka', 'Babul', 'Contact,person,test address', '11223344556', 'DBBL LTD.', 'Gulshan1', '123456789425214', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account_title`
--
ALTER TABLE `account_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `add_exam`
--
ALTER TABLE `add_exam`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `books_category`
--
ALTER TABLE `books_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `calender_events`
--
ALTER TABLE `calender_events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `class`
--
ALTER TABLE `class`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `class_routine`
--
ALTER TABLE `class_routine`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `class_students`
--
ALTER TABLE `class_students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `class_subject`
--
ALTER TABLE `class_subject`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `configuration`
--
ALTER TABLE `configuration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `config_week_day`
--
ALTER TABLE `config_week_day`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `daily_attendance`
--
ALTER TABLE `daily_attendance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dormitory`
--
ALTER TABLE `dormitory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dormitory_bed`
--
ALTER TABLE `dormitory_bed`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dormitory_room`
--
ALTER TABLE `dormitory_room`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employe`
--
ALTER TABLE `employe`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_attendanc`
--
ALTER TABLE `exam_attendanc`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_grade`
--
ALTER TABLE `exam_grade`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_routine`
--
ALTER TABLE `exam_routine`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fee_item`
--
ALTER TABLE `fee_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `final_result`
--
ALTER TABLE `final_result`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inven_category`
--
ALTER TABLE `inven_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inve_item`
--
ALTER TABLE `inve_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `issu_item`
--
ALTER TABLE `issu_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leave_application`
--
ALTER TABLE `leave_application`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `library_member`
--
ALTER TABLE `library_member`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `massage`
--
ALTER TABLE `massage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notice_board`
--
ALTER TABLE `notice_board`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parents_info`
--
ALTER TABLE `parents_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `result_action`
--
ALTER TABLE `result_action`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `result_shit`
--
ALTER TABLE `result_shit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `result_submition_info`
--
ALTER TABLE `result_submition_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_based_access`
--
ALTER TABLE `role_based_access`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `salary`
--
ALTER TABLE `salary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `set_salary`
--
ALTER TABLE `set_salary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slip`
--
ALTER TABLE `slip`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_info`
--
ALTER TABLE `student_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subjects_mark`
--
ALTER TABLE `subjects_mark`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suggestion`
--
ALTER TABLE `suggestion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teachers_info`
--
ALTER TABLE `teachers_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher_attendance`
--
ALTER TABLE `teacher_attendance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transection`
--
ALTER TABLE `transection`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transport`
--
ALTER TABLE `transport`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userinfo`
--
ALTER TABLE `userinfo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- Indexes for table `vendors`
--
ALTER TABLE `vendors`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account_title`
--
ALTER TABLE `account_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `add_exam`
--
ALTER TABLE `add_exam`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `books_category`
--
ALTER TABLE `books_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `calender_events`
--
ALTER TABLE `calender_events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `class`
--
ALTER TABLE `class`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `class_routine`
--
ALTER TABLE `class_routine`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `class_students`
--
ALTER TABLE `class_students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `class_subject`
--
ALTER TABLE `class_subject`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;
--
-- AUTO_INCREMENT for table `config_week_day`
--
ALTER TABLE `config_week_day`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `daily_attendance`
--
ALTER TABLE `daily_attendance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `dormitory`
--
ALTER TABLE `dormitory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `dormitory_bed`
--
ALTER TABLE `dormitory_bed`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;
--
-- AUTO_INCREMENT for table `dormitory_room`
--
ALTER TABLE `dormitory_room`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=176;
--
-- AUTO_INCREMENT for table `employe`
--
ALTER TABLE `employe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `exam_attendanc`
--
ALTER TABLE `exam_attendanc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `exam_grade`
--
ALTER TABLE `exam_grade`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `exam_routine`
--
ALTER TABLE `exam_routine`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `fee_item`
--
ALTER TABLE `fee_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `final_result`
--
ALTER TABLE `final_result`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `inven_category`
--
ALTER TABLE `inven_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `inve_item`
--
ALTER TABLE `inve_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `issu_item`
--
ALTER TABLE `issu_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `leave_application`
--
ALTER TABLE `leave_application`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `library_member`
--
ALTER TABLE `library_member`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `massage`
--
ALTER TABLE `massage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `notice_board`
--
ALTER TABLE `notice_board`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `parents_info`
--
ALTER TABLE `parents_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `result_action`
--
ALTER TABLE `result_action`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `result_shit`
--
ALTER TABLE `result_shit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `result_submition_info`
--
ALTER TABLE `result_submition_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `role_based_access`
--
ALTER TABLE `role_based_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;
--
-- AUTO_INCREMENT for table `salary`
--
ALTER TABLE `salary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `set_salary`
--
ALTER TABLE `set_salary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `slip`
--
ALTER TABLE `slip`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `student_info`
--
ALTER TABLE `student_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `subjects_mark`
--
ALTER TABLE `subjects_mark`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `suggestion`
--
ALTER TABLE `suggestion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `teachers_info`
--
ALTER TABLE `teachers_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `teacher_attendance`
--
ALTER TABLE `teacher_attendance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `transection`
--
ALTER TABLE `transection`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `transport`
--
ALTER TABLE `transport`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `userinfo`
--
ALTER TABLE `userinfo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;
--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;
--
-- AUTO_INCREMENT for table `vendors`
--
ALTER TABLE `vendors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
